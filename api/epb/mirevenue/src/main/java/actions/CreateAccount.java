package actions;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import pojos.requests.createaccount.Envelope;
import pojos.requests.createaccount.body.Body;
import pojos.requests.createaccount.body.CreateAccountRequest;
import pojos.requests.createaccount.body.createaccountrequest.AccountDetailsPojo;
import pojos.requests.createaccount.body.createaccountrequest.CreateAccountDetailsPojo;
import pojos.requests.createaccount.body.header.WSHeader;
import pojos.requests.createparty.header.Header;
import pojos.response.createaccount.CreateAccountResponse;
import util.RestHelper;
import util.SoapHelper;
import za.co.nedbank.execution.config.api.ApiConfig;

public class CreateAccount {
    private SoapHelper soapHelper;
    private RestHelper restHelper;
    private ApiConfig apiConfig;
    private static final String END_POINT = "create_account_endpoint";
    private static final Logger logger = LogManager.getLogger(CreateParty.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE =  "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE =  "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE =  "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE =  "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE =  "Sending request finished for class {}.....";

    public CreateAccount(){
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject createAccountOperation(String strAccountNumber, String strPartyId, String PricingPlanId, String OpenedDate, String strProductCode, String Brand, String Currency, String GLProductCode, String AccountStatus, String NonResidentStatus, String strReserveBankCode, String strRelationshipType, String strFinancialBranch, String strTypeOfBusiness, String strSuppressFeeIndicator, String strSourceSystemId)  {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();
        String path = restHelper.getApiConfig().getOtherElements().get("create_account_endpoint");

        String strPayload = getXML(strAccountNumber, strPartyId, PricingPlanId, OpenedDate, strProductCode, Brand, Currency, GLProductCode, AccountStatus, NonResidentStatus, strReserveBankCode, strRelationshipType, strFinancialBranch, strTypeOfBusiness, strSuppressFeeIndicator, strSourceSystemId);

        Response response = soapHelper.postRequest(strPayload, END_POINT);
        response.prettyPrint();

        if (logger.isInfoEnabled()) {
//            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if(response.getBody().asString().contains("faultstring")){
            System.out.println("Fault");
        }else{
            CreateAccountResponse createAccountResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), CreateAccountResponse.class);
            createAccountResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, createAccountResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());


        return jsonResponse;
    }

    private String getXML(String strAccountNumber, String strPartyId, String PricingPlanId, String OpenedDate, String strProductCode, String Brand, String Currency, String GLProductCode, String AccountStatus, String NonResidentStatus, String strReserveBankCode, String strRelationshipType, String strFinancialBranch, String strTypeOfBusiness, String strSuppressFeeIndicator, String strSourceSystemId) {
        Header header = soapHelper.getHeader();

        //body
        AccountDetailsPojo accountDetailsPojo = new AccountDetailsPojo(strAccountNumber, strPartyId, PricingPlanId, OpenedDate, strProductCode, Brand, Currency, GLProductCode, AccountStatus, NonResidentStatus, strReserveBankCode, strRelationshipType, strFinancialBranch, strTypeOfBusiness, strSuppressFeeIndicator, strSourceSystemId);
        CreateAccountDetailsPojo createAccountDetailsPojo =  new CreateAccountDetailsPojo(accountDetailsPojo);
        WSHeader wsHeader =  new WSHeader("");

        CreateAccountRequest createAccountRequest= new CreateAccountRequest(wsHeader, createAccountDetailsPojo);

        Body body = new Body(createAccountRequest);

        Envelope envelope = new Envelope(header,body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }
}

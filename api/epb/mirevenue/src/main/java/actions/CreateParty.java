package actions;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import pojos.requests.createparty.Envelope;
import pojos.requests.createparty.body.Body;
import pojos.requests.createparty.body.CreatePartyRequest;
import pojos.requests.createparty.body.createpartyrequest.CreatePartyDetailsPojo;
import pojos.requests.createparty.body.createpartyrequest.PartyDetailsPojo;
import pojos.requests.createparty.body.header.WSHeader;
import pojos.requests.createparty.header.Header;
import pojos.response.createparty.CreatePartyResponse;
import pojos.response.createpartyfault.CreatePartyFaultResponse;
import util.RestHelper;
import util.SoapHelper;

public class CreateParty {
    private SoapHelper soapHelper;
    private RestHelper restHelper;
    private static final String END_POINT = "create_party_endpoint";
    private static final Logger logger = LogManager.getLogger(CreateParty.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE =  "Sending request started for class {}.....";
    private static final String PAYLOAD_MESSAGE =  "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE =  "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE =  "Sending request finished for class {}.....";

    public CreateParty(){
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject createPartyOperation(String strPartyId, String strCustStatus, String strClientType, String strCluster, String strSegment, String strSourceIdentifier, String strRecordAction)  {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String strPayload = getXML(strPartyId, strCustStatus , strClientType , strCluster , strSegment , strSourceIdentifier , strRecordAction);

        Response response = soapHelper.postRequest(strPayload, END_POINT);
        response.prettyPrint();

        if (logger.isInfoEnabled()) {
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if(response.getBody().asString().contains("faultstring")){
            CreatePartyFaultResponse createPartyFaultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), CreatePartyFaultResponse.class);
            jsonResponse.put(RESPONSE, createPartyFaultResponse);
        }else{
            CreatePartyResponse createPartyResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), CreatePartyResponse.class);
            createPartyResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, createPartyResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());


        return jsonResponse;
    }

    private String getXML(String strPartyId, String strCustStatus, String strClientType, String strCluster, String strSegment, String strSourceIdentifier, String strRecordAction) {
        Header header = soapHelper.getHeader();

        //body
        PartyDetailsPojo partyDetailsPojo = new PartyDetailsPojo(strPartyId, strCustStatus, strClientType, strCluster, strSegment, strSourceIdentifier, strRecordAction);
        CreatePartyDetailsPojo createPartyDetailsPojo =  new CreatePartyDetailsPojo(partyDetailsPojo);
        WSHeader wsHeader =  new WSHeader("");

        CreatePartyRequest createPartyRequest= new CreatePartyRequest(wsHeader, createPartyDetailsPojo);

        Body body = new Body(createPartyRequest);

        Envelope envelope = new Envelope(header,body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }
}

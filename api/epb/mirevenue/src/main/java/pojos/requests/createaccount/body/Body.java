package pojos.requests.createaccount.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="CreateAccountRequest", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
    CreateAccountRequest createAccountRequest;

    public Body() {
    }

    public Body(CreateAccountRequest createAccountRequest){
        this.setCreateAccountRequest(createAccountRequest);
    }

    public CreateAccountRequest getCreateAccountRequest() {
        return createAccountRequest;
    }

    public void setCreateAccountRequest(CreateAccountRequest createAccountRequest) {
        this.createAccountRequest = createAccountRequest;
    }
}

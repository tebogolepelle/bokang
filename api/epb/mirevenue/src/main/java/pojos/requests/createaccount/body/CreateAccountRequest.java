package pojos.requests.createaccount.body;

import pojos.requests.createaccount.body.createaccountrequest.CreateAccountDetailsPojo;
import pojos.requests.createaccount.body.header.WSHeader;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class CreateAccountRequest {
    @XmlElement(name="WSHeader", namespace = "http://mirevenue.zafin.com/commonheader/v1")
    WSHeader wsHeader;
    @XmlElement(name="CreateAccount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
    CreateAccountDetailsPojo createAccountDetailsPojo;

    public CreateAccountRequest(){}

    public CreateAccountRequest(WSHeader wsHeader, CreateAccountDetailsPojo createAccountDetailsPojo) {
        this.createAccountDetailsPojo = createAccountDetailsPojo;
    }

    public CreateAccountDetailsPojo getCreateAccountDetailsPojo() {
        return createAccountDetailsPojo;
    }

    public void setCreateAccountDetailsPojo(CreateAccountDetailsPojo createAccountDetailsPojo) {
        this.createAccountDetailsPojo = createAccountDetailsPojo;
    }

    public WSHeader getWsHeader() {
        return wsHeader;
    }

    public void setWsHeader(WSHeader wsHeader) {
        this.wsHeader = wsHeader;
    }
}

package pojos.requests.createaccount.body.createaccountrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class AccountDetailsPojo {
    @XmlElement(name="AccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String AccountNumber;

    @XmlElement(name="PartyId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PartyId;

    @XmlElement(name="PricingPlanId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PricingPlanId;

    @XmlElement(name="OpenedDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String OpenedDate;

    @XmlElement(name="ClosedDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ClosedDate;

    @XmlElement(name="ProductCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ProductCode;

    @XmlElement(name="Brand", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String Brand;

    @XmlElement(name="ProductCategory", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ProductCategory;

    @XmlElement(name="ProductSubCategory", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ProductSubCategory;

    @XmlElement(name="Currency", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String Currency;

    @XmlElement(name="AccountMode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String AccountMode;

    @XmlElement(name="IbanNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String IbanNumber;

    @XmlElement(name="StatementDeliveryIndicator", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String StatementDeliveryIndicator;

    @XmlElement(name="GLProductCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String GLProductCode;

    @XmlElement(name="AccountStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String AccountStatus;

    @XmlElement(name="IntroductionMethod", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String IntroductionMethod;

    @XmlElement(name="StatementtNoOfCopies", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String StatementtNoOfCopies;

    @XmlElement(name="Language", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String Language;

    @XmlElement(name="Segment", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String Segment;

    @XmlElement(name="BranchUtilRate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String BranchUtilRate;

    @XmlElement(name="MultiDropRate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MultiDropRate;

    @XmlElement(name="IncidentRate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String IncidentRate;

    @XmlElement(name="YouthIndicator", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String YouthIndicator;

    @XmlElement(name="CollectionStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CollectionStatus;

    @XmlElement(name="NonResidentStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NonResidentStatus;

    @XmlElement(name="ReserveBankCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ReserveBankCode;

    @XmlElement(name="RelationshipType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String RelationshipType;

    @XmlElement(name="DomicileBranch", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String DomicileBranch;

    @XmlElement(name="FinancialBranch", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String FinancialBranch;

    @XmlElement(name="BillingFrequency", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String BillingFrequency;

    @XmlElement(name="BillingDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String BillingDate;

    @XmlElement(name="BankId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String BankId;

    @XmlElement(name="ClosingBalance", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ClosingBalance;

    @XmlElement(name="ClosingBalanceSign", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ClosingBalanceSign;

    @XmlElement(name="OpeningBalance", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String OpeningBalance;

    @XmlElement(name="OpeningBalanceSign", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String OpeningBalanceSign;

    @XmlElement(name="PreviousMinBal", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PreviousMinBal;

    @XmlElement(name="PreviousMinBalSign", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PreviousMinBalSign;

    @XmlElement(name="AverageBal", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String AverageBal;

    @XmlElement(name="AverageBalSign", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String AverageBalSign;

    @XmlElement(name="PreviousAvgBal", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PreviousAvgBal;

    @XmlElement(name="PreviousAvgBalSign", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PreviousAvgBalSign;

    @XmlElement(name="InvestmentBalance", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String InvestmentBalance;

    @XmlElement(name="InvestmentBalanceSign", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String InvestmentBalanceSign;

    @XmlElement(name="CountryCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CountryCode;

    @XmlElement(name="PriceId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PriceId;

    @XmlElement(name="PriceType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PriceType;

    @XmlElement(name="PricePkgId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PricePkgId;

    @XmlElement(name="BrandType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String BrandType;

    @XmlElement(name="ChargeAccountInd", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ChargeAccountInd;

    @XmlElement(name="ChargeAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ChargeAccountNumber;

    @XmlElement(name="ChargeAccountType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String ChargeAccountType;

    @XmlElement(name="TermInMonths", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String TermInMonths;

    @XmlElement(name="MaturityDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MaturityDate;

    @XmlElement(name="OrginalMaturityDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String OrginalMaturityDate;

    @XmlElement(name="AssetType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String AssetType;

    @XmlElement(name="OriginalAmount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String OriginalAmount;

    @XmlElement(name="OutstandingBalance", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String OutstandingBalance;

    @XmlElement(name="OutstandingBalanceSign", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String OutstandingBalanceSign;

    @XmlElement(name="LateChargeId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String LateChargeId;

    @XmlElement(name="LateChargeGracePeriod", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String LateChargeGracePeriod;

    @XmlElement(name="FirstReleaseDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String FirstReleaseDate;

    @XmlElement(name="LastReleaseDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String LastReleaseDate;

    @XmlElement(name="FullReleaseDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String FullReleaseDate;

    @XmlElement(name="RestBalance", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String RestBalance;

    @XmlElement(name="EarmarkAmount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String EarmarkAmount;

    @XmlElement(name="CreditLimit", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CreditLimit;

    @XmlElement(name="DrawingLimit", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String DrawingLimit;

    @XmlElement(name="TrancheAmount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String TrancheAmount;

    @XmlElement(name="SyndicationLoanInd", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String SyndicationLoanInd;

    @XmlElement(name="LockInFlag", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String LockInFlag;

    @XmlElement(name="CollectedBalance", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CollectedBalance;

    @XmlElement(name="NumOfInstallmentsPaid", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NumOfInstallmentsPaid;

    @XmlElement(name="TopUpAmount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String TopUpAmount;

    @XmlElement(name="NplStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NplStatus;

    @XmlElement(name="NplDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NplDate;

    @XmlElement(name="PreviousNplDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PreviousNplDate;

    @XmlElement(name="PaymentAmount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String PaymentAmount;

    @XmlElement(name="MerchantCateCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MerchantCateCode;

    @XmlElement(name="MerchantCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MerchantCode;

    @XmlElement(name="MerchantName", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MerchantName;

    @XmlElement(name="MerchantIndustryCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MerchantIndustryCode;

    @XmlElement(name="CashCntrNo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CashCntrNo;

    @XmlElement(name="CashCenterName", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CashCenterName;

    @XmlElement(name="CanisterValueInd", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CanisterValueInd;

    @XmlElement(name="MerchantNarrative", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MerchantNarrative;

    @XmlElement(name="NarrativeSwap", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NarrativeSwap;

    @XmlElement(name="NoOfDevices", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NoOfDevices;

    @XmlElement(name="NoOfUsers", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NoOfUsers;

    @XmlElement(name="VendorCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String VendorCode;

    @XmlElement(name="CorrectNarrative", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CorrectNarrative;

    @XmlElement(name="CardNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CardNumber;

    @XmlElement(name="CardTypeFlag", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CardTypeFlag;

    @XmlElement(name="CardActivationFlag", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String CardActivationFlag;

    @XmlElement(name="DirectDebitAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String DirectDebitAccountNumber;

    @XmlElement(name="NoOfYearsToWaiveAnnual", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String NoOfYearsToWaiveAnnual;

    @XmlElement(name="TypeOfBusiness", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String TypeOfBusiness;

    @XmlElement(name="MinBalance", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String MinBalance;

    @XmlElement(name="EAlertsFlag", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String EAlertsFlag;

    @XmlElement(name="SuppressFeeIndicator", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String SuppressFeeIndicator;

    @XmlElement(name="SourceSystemId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String SourceSystemId;

    public AccountDetailsPojo() {
    }

    public AccountDetailsPojo(String strAccountNumber, String strPartyId, String strPricingPlanId, String strOpenedDate, String strProductCode, String strBrand, String strCurrency, String strGLProductCode, String strAccountStatus, String strNonResidentStatus, String strReserveBankCode, String strRelationshipType, String strFinancialBranch, String strTypeOfBusiness, String strSuppressFeeIndicator, String strSourceSystemId) {
        setAccountNumber(strAccountNumber);
        setPartyId(strPartyId);
        setPricingPlanId(strPricingPlanId);
        setOpenedDate(strOpenedDate);
        setProductCode(strProductCode);
        setBrand(strBrand);
        setCurrency(strCurrency);
        setGLProductCode(strGLProductCode);
        setAccountStatus(strAccountStatus);
        setNonResidentStatus(strNonResidentStatus);
        setReserveBankCode(strReserveBankCode);
        setRelationshipType(strRelationshipType);
        setFinancialBranch(strFinancialBranch);
        setTypeOfBusiness(strTypeOfBusiness);
        setSuppressFeeIndicator(strSuppressFeeIndicator);
        setSourceSystemId(strSourceSystemId);
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getPartyId() {
        return PartyId;
    }

    public void setPartyId(String partyId) {
        PartyId = partyId;
    }

    public String getPricingPlanId() {
        return PricingPlanId;
    }

    public void setPricingPlanId(String pricingPlanId) {
        PricingPlanId = pricingPlanId;
    }

    public String getOpenedDate() {
        return OpenedDate;
    }

    public void setOpenedDate(String openedDate) {
        OpenedDate = openedDate;
    }

    public String getClosedDate() {
        return ClosedDate;
    }

    public void setClosedDate(String closedDate) {
        ClosedDate = closedDate;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getProductCategory() {
        return ProductCategory;
    }

    public void setProductCategory(String productCategory) {
        ProductCategory = productCategory;
    }

    public String getProductSubCategory() {
        return ProductSubCategory;
    }

    public void setProductSubCategory(String productSubCategory) {
        ProductSubCategory = productSubCategory;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getAccountMode() {
        return AccountMode;
    }

    public void setAccountMode(String accountMode) {
        AccountMode = accountMode;
    }

    public String getIbanNumber() {
        return IbanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        IbanNumber = ibanNumber;
    }

    public String getStatementDeliveryIndicator() {
        return StatementDeliveryIndicator;
    }

    public void setStatementDeliveryIndicator(String statementDeliveryIndicator) {
        StatementDeliveryIndicator = statementDeliveryIndicator;
    }

    public String getGLProductCode() {
        return GLProductCode;
    }

    public void setGLProductCode(String GLProductCode) {
        this.GLProductCode = GLProductCode;
    }

    public String getAccountStatus() {
        return AccountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        AccountStatus = accountStatus;
    }

    public String getIntroductionMethod() {
        return IntroductionMethod;
    }

    public void setIntroductionMethod(String introductionMethod) {
        IntroductionMethod = introductionMethod;
    }

    public String getStatementtNoOfCopies() {
        return StatementtNoOfCopies;
    }

    public void setStatementtNoOfCopies(String statementtNoOfCopies) {
        StatementtNoOfCopies = statementtNoOfCopies;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getSegment() {
        return Segment;
    }

    public void setSegment(String segment) {
        Segment = segment;
    }

    public String getBranchUtilRate() {
        return BranchUtilRate;
    }

    public void setBranchUtilRate(String branchUtilRate) {
        BranchUtilRate = branchUtilRate;
    }

    public String getMultiDropRate() {
        return MultiDropRate;
    }

    public void setMultiDropRate(String multiDropRate) {
        MultiDropRate = multiDropRate;
    }

    public String getIncidentRate() {
        return IncidentRate;
    }

    public void setIncidentRate(String incidentRate) {
        IncidentRate = incidentRate;
    }

    public String getYouthIndicator() {
        return YouthIndicator;
    }

    public void setYouthIndicator(String youthIndicator) {
        YouthIndicator = youthIndicator;
    }

    public String getCollectionStatus() {
        return CollectionStatus;
    }

    public void setCollectionStatus(String collectionStatus) {
        CollectionStatus = collectionStatus;
    }

    public String getNonResidentStatus() {
        return NonResidentStatus;
    }

    public void setNonResidentStatus(String nonResidentStatus) {
        NonResidentStatus = nonResidentStatus;
    }

    public String getReserveBankCode() {
        return ReserveBankCode;
    }

    public void setReserveBankCode(String reserveBankCode) {
        ReserveBankCode = reserveBankCode;
    }

    public String getRelationshipType() {
        return RelationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        RelationshipType = relationshipType;
    }

    public String getDomicileBranch() {
        return DomicileBranch;
    }

    public void setDomicileBranch(String domicileBranch) {
        DomicileBranch = domicileBranch;
    }

    public String getFinancialBranch() {
        return FinancialBranch;
    }

    public void setFinancialBranch(String financialBranch) {
        FinancialBranch = financialBranch;
    }

    public String getBillingFrequency() {
        return BillingFrequency;
    }

    public void setBillingFrequency(String billingFrequency) {
        BillingFrequency = billingFrequency;
    }

    public String getBillingDate() {
        return BillingDate;
    }

    public void setBillingDate(String billingDate) {
        BillingDate = billingDate;
    }

    public String getBankId() {
        return BankId;
    }

    public void setBankId(String bankId) {
        BankId = bankId;
    }

    public String getClosingBalance() {
        return ClosingBalance;
    }

    public void setClosingBalance(String closingBalance) {
        ClosingBalance = closingBalance;
    }

    public String getClosingBalanceSign() {
        return ClosingBalanceSign;
    }

    public void setClosingBalanceSign(String closingBalanceSign) {
        ClosingBalanceSign = closingBalanceSign;
    }

    public String getOpeningBalance() {
        return OpeningBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        OpeningBalance = openingBalance;
    }

    public String getOpeningBalanceSign() {
        return OpeningBalanceSign;
    }

    public void setOpeningBalanceSign(String openingBalanceSign) {
        OpeningBalanceSign = openingBalanceSign;
    }

    public String getPreviousMinBal() {
        return PreviousMinBal;
    }

    public void setPreviousMinBal(String previousMinBal) {
        PreviousMinBal = previousMinBal;
    }

    public String getPreviousMinBalSign() {
        return PreviousMinBalSign;
    }

    public void setPreviousMinBalSign(String previousMinBalSign) {
        PreviousMinBalSign = previousMinBalSign;
    }

    public String getAverageBal() {
        return AverageBal;
    }

    public void setAverageBal(String averageBal) {
        AverageBal = averageBal;
    }

    public String getAverageBalSign() {
        return AverageBalSign;
    }

    public void setAverageBalSign(String averageBalSign) {
        AverageBalSign = averageBalSign;
    }

    public String getPreviousAvgBal() {
        return PreviousAvgBal;
    }

    public void setPreviousAvgBal(String previousAvgBal) {
        PreviousAvgBal = previousAvgBal;
    }

    public String getPreviousAvgBalSign() {
        return PreviousAvgBalSign;
    }

    public void setPreviousAvgBalSign(String previousAvgBalSign) {
        PreviousAvgBalSign = previousAvgBalSign;
    }

    public String getInvestmentBalance() {
        return InvestmentBalance;
    }

    public void setInvestmentBalance(String investmentBalance) {
        InvestmentBalance = investmentBalance;
    }

    public String getInvestmentBalanceSign() {
        return InvestmentBalanceSign;
    }

    public void setInvestmentBalanceSign(String investmentBalanceSign) {
        InvestmentBalanceSign = investmentBalanceSign;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getPriceId() {
        return PriceId;
    }

    public void setPriceId(String priceId) {
        PriceId = priceId;
    }

    public String getPriceType() {
        return PriceType;
    }

    public void setPriceType(String priceType) {
        PriceType = priceType;
    }

    public String getPricePkgId() {
        return PricePkgId;
    }

    public void setPricePkgId(String pricePkgId) {
        PricePkgId = pricePkgId;
    }

    public String getBrandType() {
        return BrandType;
    }

    public void setBrandType(String brandType) {
        BrandType = brandType;
    }

    public String getChargeAccountInd() {
        return ChargeAccountInd;
    }

    public void setChargeAccountInd(String chargeAccountInd) {
        ChargeAccountInd = chargeAccountInd;
    }

    public String getChargeAccountNumber() {
        return ChargeAccountNumber;
    }

    public void setChargeAccountNumber(String chargeAccountNumber) {
        ChargeAccountNumber = chargeAccountNumber;
    }

    public String getChargeAccountType() {
        return ChargeAccountType;
    }

    public void setChargeAccountType(String chargeAccountType) {
        ChargeAccountType = chargeAccountType;
    }

    public String getTermInMonths() {
        return TermInMonths;
    }

    public void setTermInMonths(String termInMonths) {
        TermInMonths = termInMonths;
    }

    public String getMaturityDate() {
        return MaturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        MaturityDate = maturityDate;
    }

    public String getOrginalMaturityDate() {
        return OrginalMaturityDate;
    }

    public void setOrginalMaturityDate(String orginalMaturityDate) {
        OrginalMaturityDate = orginalMaturityDate;
    }

    public String getAssetType() {
        return AssetType;
    }

    public void setAssetType(String assetType) {
        AssetType = assetType;
    }

    public String getOriginalAmount() {
        return OriginalAmount;
    }

    public void setOriginalAmount(String originalAmount) {
        OriginalAmount = originalAmount;
    }

    public String getOutstandingBalance() {
        return OutstandingBalance;
    }

    public void setOutstandingBalance(String outstandingBalance) {
        OutstandingBalance = outstandingBalance;
    }

    public String getOutstandingBalanceSign() {
        return OutstandingBalanceSign;
    }

    public void setOutstandingBalanceSign(String outstandingBalanceSign) {
        OutstandingBalanceSign = outstandingBalanceSign;
    }

    public String getLateChargeId() {
        return LateChargeId;
    }

    public void setLateChargeId(String lateChargeId) {
        LateChargeId = lateChargeId;
    }

    public String getLateChargeGracePeriod() {
        return LateChargeGracePeriod;
    }

    public void setLateChargeGracePeriod(String lateChargeGracePeriod) {
        LateChargeGracePeriod = lateChargeGracePeriod;
    }

    public String getFirstReleaseDate() {
        return FirstReleaseDate;
    }

    public void setFirstReleaseDate(String firstReleaseDate) {
        FirstReleaseDate = firstReleaseDate;
    }

    public String getLastReleaseDate() {
        return LastReleaseDate;
    }

    public void setLastReleaseDate(String lastReleaseDate) {
        LastReleaseDate = lastReleaseDate;
    }

    public String getFullReleaseDate() {
        return FullReleaseDate;
    }

    public void setFullReleaseDate(String fullReleaseDate) {
        FullReleaseDate = fullReleaseDate;
    }

    public String getRestBalance() {
        return RestBalance;
    }

    public void setRestBalance(String restBalance) {
        RestBalance = restBalance;
    }

    public String getEarmarkAmount() {
        return EarmarkAmount;
    }

    public void setEarmarkAmount(String earmarkAmount) {
        EarmarkAmount = earmarkAmount;
    }

    public String getCreditLimit() {
        return CreditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        CreditLimit = creditLimit;
    }

    public String getDrawingLimit() {
        return DrawingLimit;
    }

    public void setDrawingLimit(String drawingLimit) {
        DrawingLimit = drawingLimit;
    }

    public String getTrancheAmount() {
        return TrancheAmount;
    }

    public void setTrancheAmount(String trancheAmount) {
        TrancheAmount = trancheAmount;
    }

    public String getSyndicationLoanInd() {
        return SyndicationLoanInd;
    }

    public void setSyndicationLoanInd(String syndicationLoanInd) {
        SyndicationLoanInd = syndicationLoanInd;
    }

    public String getLockInFlag() {
        return LockInFlag;
    }

    public void setLockInFlag(String lockInFlag) {
        LockInFlag = lockInFlag;
    }

    public String getCollectedBalance() {
        return CollectedBalance;
    }

    public void setCollectedBalance(String collectedBalance) {
        CollectedBalance = collectedBalance;
    }

    public String getNumOfInstallmentsPaid() {
        return NumOfInstallmentsPaid;
    }

    public void setNumOfInstallmentsPaid(String numOfInstallmentsPaid) {
        NumOfInstallmentsPaid = numOfInstallmentsPaid;
    }

    public String getTopUpAmount() {
        return TopUpAmount;
    }

    public void setTopUpAmount(String topUpAmount) {
        TopUpAmount = topUpAmount;
    }

    public String getNplStatus() {
        return NplStatus;
    }

    public void setNplStatus(String nplStatus) {
        NplStatus = nplStatus;
    }

    public String getNplDate() {
        return NplDate;
    }

    public void setNplDate(String nplDate) {
        NplDate = nplDate;
    }

    public String getPreviousNplDate() {
        return PreviousNplDate;
    }

    public void setPreviousNplDate(String previousNplDate) {
        PreviousNplDate = previousNplDate;
    }

    public String getPaymentAmount() {
        return PaymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        PaymentAmount = paymentAmount;
    }

    public String getMerchantCateCode() {
        return MerchantCateCode;
    }

    public void setMerchantCateCode(String merchantCateCode) {
        MerchantCateCode = merchantCateCode;
    }

    public String getMerchantCode() {
        return MerchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        MerchantCode = merchantCode;
    }

    public String getMerchantName() {
        return MerchantName;
    }

    public void setMerchantName(String merchantName) {
        MerchantName = merchantName;
    }

    public String getMerchantIndustryCode() {
        return MerchantIndustryCode;
    }

    public void setMerchantIndustryCode(String merchantIndustryCode) {
        MerchantIndustryCode = merchantIndustryCode;
    }

    public String getCashCntrNo() {
        return CashCntrNo;
    }

    public void setCashCntrNo(String cashCntrNo) {
        CashCntrNo = cashCntrNo;
    }

    public String getCashCenterName() {
        return CashCenterName;
    }

    public void setCashCenterName(String cashCenterName) {
        CashCenterName = cashCenterName;
    }

    public String getCanisterValueInd() {
        return CanisterValueInd;
    }

    public void setCanisterValueInd(String canisterValueInd) {
        CanisterValueInd = canisterValueInd;
    }

    public String getMerchantNarrative() {
        return MerchantNarrative;
    }

    public void setMerchantNarrative(String merchantNarrative) {
        MerchantNarrative = merchantNarrative;
    }

    public String getNarrativeSwap() {
        return NarrativeSwap;
    }

    public void setNarrativeSwap(String narrativeSwap) {
        NarrativeSwap = narrativeSwap;
    }

    public String getNoOfDevices() {
        return NoOfDevices;
    }

    public void setNoOfDevices(String noOfDevices) {
        NoOfDevices = noOfDevices;
    }

    public String getNoOfUsers() {
        return NoOfUsers;
    }

    public void setNoOfUsers(String noOfUsers) {
        NoOfUsers = noOfUsers;
    }

    public String getVendorCode() {
        return VendorCode;
    }

    public void setVendorCode(String vendorCode) {
        VendorCode = vendorCode;
    }

    public String getCorrectNarrative() {
        return CorrectNarrative;
    }

    public void setCorrectNarrative(String correctNarrative) {
        CorrectNarrative = correctNarrative;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public String getCardTypeFlag() {
        return CardTypeFlag;
    }

    public void setCardTypeFlag(String cardTypeFlag) {
        CardTypeFlag = cardTypeFlag;
    }

    public String getCardActivationFlag() {
        return CardActivationFlag;
    }

    public void setCardActivationFlag(String cardActivationFlag) {
        CardActivationFlag = cardActivationFlag;
    }

    public String getDirectDebitAccountNumber() {
        return DirectDebitAccountNumber;
    }

    public void setDirectDebitAccountNumber(String directDebitAccountNumber) {
        DirectDebitAccountNumber = directDebitAccountNumber;
    }

    public String getNoOfYearsToWaiveAnnual() {
        return NoOfYearsToWaiveAnnual;
    }

    public void setNoOfYearsToWaiveAnnual(String noOfYearsToWaiveAnnual) {
        NoOfYearsToWaiveAnnual = noOfYearsToWaiveAnnual;
    }

    public String getTypeOfBusiness() {
        return TypeOfBusiness;
    }

    public void setTypeOfBusiness(String typeOfBusiness) {
        TypeOfBusiness = typeOfBusiness;
    }

    public String getMinBalance() {
        return MinBalance;
    }

    public void setMinBalance(String minBalance) {
        MinBalance = minBalance;
    }

    public String getEAlertsFlag() {
        return EAlertsFlag;
    }

    public void setEAlertsFlag(String EAlertsFlag) {
        this.EAlertsFlag = EAlertsFlag;
    }

    public String getSuppressFeeIndicator() {
        return SuppressFeeIndicator;
    }

    public void setSuppressFeeIndicator(String suppressFeeIndicator) {
        SuppressFeeIndicator = suppressFeeIndicator;
    }

    public String getSourceSystemId() {
        return SourceSystemId;
    }

    public void setSourceSystemId(String sourceSystemId) {
        SourceSystemId = sourceSystemId;
    }
}

package pojos.requests.createaccount.body.createaccountrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class CreateAccountDetailsPojo {

    @XmlElement(name="AccountDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
    AccountDetailsPojo accountDetailsPojo;

    public CreateAccountDetailsPojo(){

    }

    public CreateAccountDetailsPojo(AccountDetailsPojo accountDetailsPojo){
        this.accountDetailsPojo = accountDetailsPojo;
    }
    public AccountDetailsPojo getAccountDetailsPojo() {
        return accountDetailsPojo;
    }

    public void setAccountDetailsPojo(AccountDetailsPojo accountDetailsPojo) {
        this.accountDetailsPojo = accountDetailsPojo;
    }
}

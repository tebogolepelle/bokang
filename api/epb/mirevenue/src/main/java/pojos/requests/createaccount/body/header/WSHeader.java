package pojos.requests.createaccount.body.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class WSHeader {
    @XmlElement(name="WSContext", namespace = "http://mirevenue.zafin.com/commonheader/v1")
    String WSContextObject;

    public WSHeader() {
    }

    public WSHeader(String wsContextObject) {
        WSContextObject = wsContextObject;
    }

    public String getWSContext() {
        return WSContextObject;
    }

    public void setWSContext(String WSContextObject) {
        this.WSContextObject = WSContextObject;
    }

}


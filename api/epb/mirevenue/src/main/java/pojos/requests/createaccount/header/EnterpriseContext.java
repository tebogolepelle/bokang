package pojos.requests.createaccount.header;

import javax.xml.bind.annotation.*;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"ContextInfo", "RequestOriginator","InstrumentationInfo"})

public class EnterpriseContext {
    @XmlElement(name="ContextInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    pojos.requests.createaccount.header.ContextInfo ContextInfo;
    @XmlElement(name="RequestOriginator", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    pojos.requests.createaccount.header.RequestOriginator RequestOriginator;
    @XmlElement(name="InstrumentationInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    pojos.requests.createaccount.header.InstrumentationInfo InstrumentationInfo;

    public EnterpriseContext() {
    }

    public EnterpriseContext(pojos.requests.createaccount.header.ContextInfo contextInfo, pojos.requests.createaccount.header.RequestOriginator RequestOriginator, pojos.requests.createaccount.header.InstrumentationInfo InstrumentationInfo) {
        this.ContextInfo = contextInfo;
        this.RequestOriginator = RequestOriginator;
        this.InstrumentationInfo = InstrumentationInfo;
    }

    public pojos.requests.createaccount.header.ContextInfo getContextInfo() {
        return ContextInfo;
    }
    public void setContextInfo(pojos.requests.createaccount.header.ContextInfo contextInfo) {
        this.ContextInfo = contextInfo;
    }

    public pojos.requests.createaccount.header.RequestOriginator getRequestOriginator() {
        return RequestOriginator;
    }
    public void setRequestOriginator(pojos.requests.createaccount.header.RequestOriginator requestOriginator) {
        this.RequestOriginator = requestOriginator;
    }

    public pojos.requests.createaccount.header.InstrumentationInfo getInstrumentationInfo() {
        return InstrumentationInfo;
    }
    public void setInstrumentationInfo(pojos.requests.createaccount.header.InstrumentationInfo instrumentationInfo) {
        this.InstrumentationInfo = instrumentationInfo;
    }
}

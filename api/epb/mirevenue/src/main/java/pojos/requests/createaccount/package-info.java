@XmlSchema(
        namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1",
        xmlns = {@XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="ent", namespaceURI="http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext"),
                @XmlNs(prefix="v1", namespaceURI="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1"),
                @XmlNs(prefix="v11", namespaceURI="http://mirevenue.zafin.com/commonheader/v1")
                },

        elementFormDefault = XmlNsForm.QUALIFIED)

package pojos.requests.createaccount;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
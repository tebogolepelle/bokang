package pojos.requests.createparty.body;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="CreatePartyRequest", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    CreatePartyRequest createPartyRequest;

    public Body() {
    }

    public Body(CreatePartyRequest createPartyRequest){
        this.setCreatePartyRequest(createPartyRequest);
    }

    public CreatePartyRequest getCreatePartyRequest() {
        return createPartyRequest;
    }

    public void setCreatePartyRequest(CreatePartyRequest createPartyRequest) {
        this.createPartyRequest = createPartyRequest;
    }
}

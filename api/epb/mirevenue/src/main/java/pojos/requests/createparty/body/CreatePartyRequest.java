package pojos.requests.createparty.body;

import pojos.requests.createparty.body.createpartyrequest.CreatePartyDetailsPojo;
import pojos.requests.createparty.body.header.WSHeader;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class CreatePartyRequest {
    @XmlElement(name="WSHeader", namespace = "http://mirevenue.zafin.com/commonheader/v1")
    WSHeader wsHeader;
    @XmlElement(name="CreateParty", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    CreatePartyDetailsPojo createPartyDetailsPojo;

    public CreatePartyRequest(){}

    public CreatePartyRequest(WSHeader wsHeader, CreatePartyDetailsPojo createPartyDetailsPojo) {
        this.createPartyDetailsPojo = createPartyDetailsPojo;
    }

    public CreatePartyDetailsPojo getCreatePartyDetailsPojo() {
        return createPartyDetailsPojo;
    }

    public void setCreatePartyDetailsPojo(CreatePartyDetailsPojo createPartyDetailsPojo) {
        this.createPartyDetailsPojo = createPartyDetailsPojo;
    }

    public WSHeader getWsHeader() {
        return wsHeader;
    }

    public void setWsHeader(WSHeader wsHeader) {
        this.wsHeader = wsHeader;
    }
}

package pojos.requests.createparty.body.createpartyrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class CreatePartyDetailsPojo {

    @XmlElement(name="PartyDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    PartyDetailsPojo partyDetailsPojo;

    public CreatePartyDetailsPojo(){

    }

    public CreatePartyDetailsPojo(PartyDetailsPojo partyDetailsPojo){
        this.partyDetailsPojo = partyDetailsPojo;
    }
    public PartyDetailsPojo getPartyDetailsPojo() {
        return partyDetailsPojo;
    }

    public void setPartyDetailsPojo(PartyDetailsPojo partyDetailsPojo) {
        this.partyDetailsPojo = partyDetailsPojo;
    }
}

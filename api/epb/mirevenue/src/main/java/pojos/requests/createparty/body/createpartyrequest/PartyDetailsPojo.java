package pojos.requests.createparty.body.createpartyrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)

public class PartyDetailsPojo {
    @XmlElement(name="PartyId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strPartyId;
    @XmlElement(name="CustomerStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strCustStatus;
    @XmlElement(name="ClientType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strClientType;
    @XmlElement(name="Cluster", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strCluster;
    @XmlElement(name="Segment", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strSegment;
    @XmlElement(name="SourceIdentifier", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strSourceIdentifier;
    @XmlElement(name="RecordAction", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strRecordAction;

    public PartyDetailsPojo() {
    }

    public PartyDetailsPojo(String strPartyId, String strCustStatus, String strClientType, String strCluster, String strSegment, String strSourceIdentifier, String strRecordAction) {
        this.strPartyId = strPartyId;
        this.strCustStatus = strCustStatus;
        this.strClientType = strClientType;
        this.strCluster = strCluster;
        this.strSegment = strSegment;
        this.strSourceIdentifier = strSourceIdentifier;
        this.strRecordAction = strRecordAction;
    }

    public String getStrPartyId() {
        return strPartyId;
    }

    public void setStrPartyId(String strPartyId) {
        this.strPartyId = strPartyId;
    }

    public String getStrCustStatus() {
        return strCustStatus;
    }

    public void setStrCustStatus(String strCustStatus) {
        this.strCustStatus = strCustStatus;
    }

    public String getStrClientType() {
        return strClientType;
    }

    public void setStrClientType(String strClientType) {
        this.strClientType = strClientType;
    }

    public String getStrCluster() {
        return strCluster;
    }

    public void setStrCluster(String strCluster) {
        this.strCluster = strCluster;
    }

    public String getStrSegment() {
        return strSegment;
    }

    public void setStrSegment(String strSegment) {
        this.strSegment = strSegment;
    }

    public String getStrSourceIdentifier() {
        return strSourceIdentifier;
    }

    public void setStrSourceIdentifier(String strSourceIdentifier) {
        this.strSourceIdentifier = strSourceIdentifier;
    }

    public String getStrRecordAction() {
        return strRecordAction;
    }

    public void setStrRecordAction(String strRecordAction) {
        this.strRecordAction = strRecordAction;
    }
}

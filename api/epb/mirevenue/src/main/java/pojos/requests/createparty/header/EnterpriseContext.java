package pojos.requests.createparty.header;

import javax.xml.bind.annotation.*;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"ContextInfo", "RequestOriginator","InstrumentationInfo"})

public class EnterpriseContext {
    @XmlElement(name="ContextInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    ContextInfo ContextInfo;
    @XmlElement(name="RequestOriginator", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    RequestOriginator RequestOriginator;
    @XmlElement(name="InstrumentationInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    InstrumentationInfo InstrumentationInfo;

    public EnterpriseContext() {
    }

    public EnterpriseContext(ContextInfo contextInfo, RequestOriginator RequestOriginator, InstrumentationInfo InstrumentationInfo) {
        this.ContextInfo = contextInfo;
        this.RequestOriginator = RequestOriginator;
        this.InstrumentationInfo = InstrumentationInfo;
    }

    public ContextInfo getContextInfo() {
        return ContextInfo;
    }
    public void setContextInfo(ContextInfo contextInfo) {
        this.ContextInfo = contextInfo;
    }

    public RequestOriginator getRequestOriginator() {
        return RequestOriginator;
    }
    public void setRequestOriginator(RequestOriginator requestOriginator) {
        this.RequestOriginator = requestOriginator;
    }

    public InstrumentationInfo getInstrumentationInfo() {
        return InstrumentationInfo;
    }
    public void setInstrumentationInfo(InstrumentationInfo instrumentationInfo) {
        this.InstrumentationInfo = instrumentationInfo;
    }
}

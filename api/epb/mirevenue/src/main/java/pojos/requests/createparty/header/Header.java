package pojos.requests.createparty.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class Header {
    @XmlElement(name="EnterpriseContext", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    EnterpriseContext EnterpriseContextObject;

    public Header() {
    }

    public Header(EnterpriseContext enterpriseContextObject) {
        EnterpriseContextObject = enterpriseContextObject;
    }

    public EnterpriseContext getEnterpriseContext() {
        return EnterpriseContextObject;
    }

    public void setEnterpriseContext(EnterpriseContext EnterpriseContextObject) {
        this.EnterpriseContextObject = EnterpriseContextObject;
    }

}


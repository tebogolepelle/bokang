package pojos.requests.createparty.header;

import util.RestHelper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InstrumentationInfo {
    @XmlElement(name="ParentInstrumentationId", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ParentInstrumentationId;
    @XmlElement(name="ChildInstrumentationId", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ChildInstrumentationId;

    public InstrumentationInfo() {
        RestHelper restHelper = new RestHelper();
        String parentInstrumentationId = restHelper.getApiConfig().getOtherElements().get("ParentInstrumentationId");
        String childInstrumentationId = restHelper.getApiConfig().getOtherElements().get("ChildInstrumentationId");

       this.ParentInstrumentationId = parentInstrumentationId;
       this.ChildInstrumentationId = childInstrumentationId;
    }

    public InstrumentationInfo(String parentInstrumentationId, String childInstrumentationId) {
        ParentInstrumentationId = parentInstrumentationId;
        ChildInstrumentationId = childInstrumentationId;
    }

    // Getter Methods

    public String getParentInstrumentationId() {
        return ParentInstrumentationId;
    }

    public String getChildInstrumentationId() {
        return ChildInstrumentationId;
    }

    // Setter Methods
    public void setParentInstrumentationId(String ParentInstrumentationIdObject) {
        this.ParentInstrumentationId = ParentInstrumentationIdObject;
    }
    public void setChildInstrumentationId(String ChildInstrumentationIdObject) {
        this.ChildInstrumentationId = ChildInstrumentationIdObject;
    }
}


package pojos.requests.createparty.header;

import util.RestHelper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class RequestOriginator {
    @XmlElement(name="MachineIPAddress", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String MachineIPAddress;

    @XmlElement(name="UserPrincipleName", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String UserPrincipleName;

    @XmlElement(name="MachineDNSName", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String MachineDNSName;

    @XmlElement(name="ChannelId", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ChannelId;

    public RequestOriginator() {
        RestHelper restHelper = new RestHelper();
        String machineIPAddress = restHelper.getApiConfig().getOtherElements().get("MachineIPAddress");
        String userPrincipleName = restHelper.getApiConfig().getOtherElements().get("UserPrincipleName");
        String machineDNSName = restHelper.getApiConfig().getOtherElements().get("MachineDNSName");
        String channelId = restHelper.getApiConfig().getOtherElements().get("ChannelId");

        this.MachineIPAddress = machineIPAddress;
        this.UserPrincipleName = userPrincipleName;
        this.MachineDNSName = machineDNSName;
        this.ChannelId = channelId;
    }

    public RequestOriginator(String machineIPAddress, String userPrincipleName, String machineDNSName, String channelId) {
        MachineIPAddress = machineIPAddress;
        UserPrincipleName = userPrincipleName;
        MachineDNSName = machineDNSName;
        ChannelId = channelId;
    }

    // Getter Methods

    public String getMachineIPAddress() {
        return MachineIPAddress;
    }

    public String getUserPrincipleName() {
        return UserPrincipleName;
    }

    public String getMachineDNSName() {
        return MachineDNSName;
    }

    public String getChannelId() {
        return ChannelId;
    }


    // Setter Methods
    public void setMachineIPAddress(String MachineIPAddressObject) {
        this.MachineIPAddress = MachineIPAddressObject;
    }
    public void setUserPrincipleName(String UserPrincipleNameObject) {
        this.UserPrincipleName = UserPrincipleNameObject;
    }
    public void setMachineDNSName(String MachineDNSNameObject) {
        this.MachineDNSName = MachineDNSNameObject;
    }
    public void setChannelId(String ChannelIdObject) {
        this.ChannelId = ChannelIdObject;
    }

}
package pojos.response.createaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class AccountResponse {
    @XmlElement(name="AccountNumber",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
    String AccountNumber;
    @XmlElement(name="ResponseMessage",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
    String ResponseMessage;
    @XmlElement(name="MessageDescription",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
    String MessageDescription;

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    public String getMessageDescription() {
        return MessageDescription;
    }

    public void setMessageDescription(String messageDescription) {
        MessageDescription = messageDescription;
    }
}

package pojos.response.createaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreateAccountResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)

public class CreateAccountResponse {
    @XmlElement(name="AccountResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBAccountManagement/v1")
    AccountResponse accountResponse;

//    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /*public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }*/


    public AccountResponse getAccountResponse() {
        return accountResponse;
    }

    public void setAccountResponse(AccountResponse accountResponse) {
        this.accountResponse = accountResponse;
    }
}

package pojos.response.createparty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreatePartyResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)

public class CreatePartyResponse {
    @XmlElement(name="PartyResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    PartyResponse partyResponse;

//    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /*public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }*/


    public PartyResponse getPartyResponse() {
        return partyResponse;
    }

    public void setPartyResponse(PartyResponse partyResponse) {
        this.partyResponse = partyResponse;
    }
}

package pojos.response.createpartyfault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreatePartyResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)

public class CreatePartyFaultResponse {
    @XmlElement(name="PartyResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    PartyFaultResponse partyFaultResponse;

    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }


    public PartyFaultResponse getPartyFaultResponse() {
        return partyFaultResponse;
    }

    public void setPartyFaultResponse(PartyFaultResponse partyFaultResponse) {
        this.partyFaultResponse = partyFaultResponse;
    }
}

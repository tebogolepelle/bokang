package pojos.response.createpartyfault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class PartyFaultResponse {
    @XmlElement(name="PartyId",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strPartyId;
    @XmlElement(name="ResponseMessage",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strResponseMessage;
    @XmlElement(name="ErrorCode",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strErrorCode;
    @XmlElement(name="MessageDescription",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/EPBPartyManagement/v1")
    String strMessageDescription;

    public String getStrErrorCode() {
        return strErrorCode;
    }

    public void setStrErrorCode(String strErrorCode) {
        this.strErrorCode = strErrorCode;
    }

    public String getStrPartyId() {
        return strPartyId;
    }

    public void setStrPartyId(String strPartyId) {
        this.strPartyId = strPartyId;
    }

    public String getStrResponseMessage() {
        return strResponseMessage;
    }

    public void setStrResponseMessage(String strResponseMessage) {
        this.strResponseMessage = strResponseMessage;
    }

    public String getStrMessageDescription() {
        return strMessageDescription;
    }

    public void setStrMessageDescription(String strMessageDescription) {
        this.strMessageDescription = strMessageDescription;
    }
}

package util;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.SSLConfig;
import io.restassured.http.Header;
import io.restassured.specification.ProxySpecification;
import io.restassured.specification.RequestSpecification;
import jcifs.util.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RestHelper {

    private static final String AUTHORIZATION = "Authorization";
    private static final String PROXYHOST = "172.17.2.9";
    private static final String PROXY_USERNAME = "proxy_username";
    private static final String PROXY_PWORD = "proxy_password";

    private ApiConfig apiConfig;
    private RestUtil restUtil;
    private static final Logger logger = LogManager.getLogger(RestHelper.class);

    public RestHelper()  {
        logger.info("Initialling class {}.....", getClass().getName());

        restUtil = new RestUtil();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
    }

    public RestUtil getRestUtil() {
        logger.info("getting RestUtil....");
        return restUtil;
    }

    public ApiConfig getApiConfig() {
        logger.info("getting apiConfig.....");
        return apiConfig;
    }



    public void configureRequestWithNoProxy(String baseUrl) {
        logger.info("configuring request by base URL {}.....", getClass().getName());
        logger.info("Base URL:  {}.....", baseUrl);

        configureRequest( baseUrl);
        logger.info("configuring request by base URL complete  {}.....", getClass().getName());
    }

    public void configureRequestWithProxyNoFramework(String baseUrl,  String bearerToken) {
        logger.info("configuring request by base URL {}.....", getClass().getName());
        logger.info("Base URL:  {}.....", baseUrl);

        String proxyUsername = apiConfig.getOtherElements().get(PROXY_USERNAME);
        String proxyPassword = apiConfig.getOtherElements().get(PROXY_PWORD);
        configureRequestWithTokenAndProxyNoFramework(bearerToken, proxyUsername, proxyPassword, baseUrl);
        logger.info("configuring request by base URL complete  {}.....", getClass().getName());
    }

    public void configureRequestAndCheckValidations(String baseUrl,  String bearerToken) {
        logger.info("configuring request by base URL {}.....", getClass().getName());
        logger.info("Base URL:  {}.....", baseUrl);

        String proxyUsername = apiConfig.getOtherElements().get(PROXY_USERNAME);
        String proxyPassword = apiConfig.getOtherElements().get(PROXY_PWORD);
        configureRequestWithTokenAndProxy(bearerToken, proxyUsername, proxyPassword, baseUrl);
        logger.info("configuring request by base URL complete  {}.....", getClass().getName());
    }

    public void configureRequestAndCheckValidations(String baseUrl) {
        logger.info("configuring request by base URL {}.....", getClass().getName());
        logger.info("Base URL:  {}.....", baseUrl);

        String bearerToken = readBearerFromFile();
        String proxyUsername = apiConfig.getOtherElements().get(PROXY_USERNAME);
        String proxyPassword = apiConfig.getOtherElements().get(PROXY_PWORD);
        configureRequestWithTokenAndProxy(bearerToken, proxyUsername, proxyPassword, baseUrl);
        logger.info("configuring request by base URL complete  {}.....", getClass().getName());
    }

    public RequestSpecification configureRequestForPostWithSpec(String baseUrl, String bearerToken) {
        logger.info("configuring request for post by base URL and bearer token  {}.....", getClass().getName());
        logger.info("Base URL:  {}", baseUrl);
        logger.info("bearerToken:  {}", bearerToken);

        String certPath = this.apiConfig.getOtherElements().get("cert_name");
        String certPassword = this.apiConfig.getOtherElements().get("cert_password");
        String proxyUsername = this.apiConfig.getOtherElements().get(PROXY_USERNAME);
        String proxyPassword = this.apiConfig.getOtherElements().get(PROXY_PWORD);
        String proxyHost = this.apiConfig.getOtherElements().get("proxyHost");
        String proxyPort = this.apiConfig.getOtherElements().get("proxyPort");

        RequestSpecification soapSpec = new RequestSpecBuilder().build();

        if(baseUrl.contains("localhost")){
            soapSpec.baseUri(baseUrl);
            return soapSpec;
        }

        soapSpec.config(RestAssured.config().sslConfig(new SSLConfig().trustStore(certPath, certPassword)));

        System.setProperty("http.proxyHost", proxyHost);
        System.setProperty("http.proxyPort", proxyPort);
        System.setProperty("http.proxyUser", proxyUsername);
        System.setProperty("http.proxyPassword", proxyPassword);
        System.setProperty("https.proxyHost", proxyHost);
        System.setProperty("https.proxyPort", proxyPort);
        System.setProperty("https.proxyUser", proxyUsername);
        System.setProperty("https.proxyPassword", proxyPassword);
        ProxySpecification specification = new ProxySpecification(proxyHost, Integer.parseInt(proxyPort), "http");
        specification.withAuth(proxyUsername, proxyPassword);

        soapSpec.baseUri(baseUrl).proxy(specification).header(AUTHORIZATION,"Bearer " + bearerToken);

        logger.info("Proxy details - proxyHost :  {}", proxyHost);
        logger.info("Proxy details - proxyPort :  {}", proxyPort);
        logger.info("Proxy details - proxyUsername :  {}", proxyUsername);
        logger.info("Proxy details - proxyPassword :  {}", proxyPassword);

        logger.info("configuring request for post by base URL and bearer token complete {}.....", getClass().getName());

        return soapSpec;
    }


    /**
     * Description: A method that configure the request to be called before every request.
     */
    private void configureRequestWithTokenAndProxy(String strBearerToken, String username, String password, String baseUrl) {
        logger.info("configuring request for post by base URL, token, username, password, token  {}.....", getClass().getName());

        restUtil.initSpec(baseUrl);
        if(!baseUrl.contains("localhost")){
            addProxyAuthentication(username, password);
        }

        restUtil.appendHeader(new Header("Accept", "application/json"));
        restUtil.appendHeader(new Header(AUTHORIZATION, "Bearer " + strBearerToken));

        logger.info("username: {}", username);
        logger.info("password: {}", password);
        logger.info("Bearer Token: {}", strBearerToken);
        logger.info("baseUrl: {}", baseUrl);

        logger.info("configuring request for post by base URL, token, username, password, token complete  {}.....", getClass().getName());
    }


    private void configureRequestWithTokenAndProxyNoFramework(String strBearerToken, String username, String password, String baseUrl) {
        logger.info("configuring request for post by base URL, token, username, password, token  {}.....", getClass().getName());

        if(!baseUrl.contains("localhost")){
            addProxyAuthentication(username, password);
        }

        RestAssured.given().header(new Header(AUTHORIZATION, "Bearer " + strBearerToken));
        //restUtil.appendHeader(new Header(AUTHORIZATION, "Bearer " + strBearerToken));

        logger.info("username: {}", username);
        logger.info("password: {}", password);
        logger.info("Bearer Token: {}", strBearerToken);
        logger.info("baseUrl: {}", baseUrl);

        logger.info("configuring request for post by base URL, token, username, password, token complete  {}.....", getClass().getName());
    }


    /**
     * Description: A method that configure the request to be called before every request.
     */
    private void configureRequest(String baseUrl) {
        logger.info("configuring request for post by base URL, token, username, password, token  {}.....", getClass().getName());

        restUtil.initSpec(baseUrl);
        //restUtil.addProxyAuthentication("cc319881", "Automation4life@02");

        logger.info("baseUrl: {}", baseUrl);

        logger.info("configuring request for post by base URL, token, username, password, token complete  {}.....", getClass().getName());
    }

    private String readBearerFromFile() {
        logger.info("reading Bearer token From File  {}.....", getClass().getName());

        String bearerToken = readFileFromResources( apiConfig.getOtherElements().get("bearer_token_name"));

        logger.info("Bearer token {}.....", bearerToken);

        logger.info("reading Bearer token From File complete {}.....", getClass().getName());

        return bearerToken;
    }

    public String readBearerFromFile(String consumer) {
        logger.info("reading  Bearer token From File  {}.....", getClass().getName());

        String bearerToken = readFileFromResources(apiConfig.getOtherElements().get(consumer + "_bearer_token_name"));

        logger.info("Bearer token {}.....", bearerToken);

        logger.info("reading CR3 Bearer token From File complete {}.....", getClass().getName());

        return bearerToken;
    }

    private void addProxyAuthentication(final String username, final String password) {
        logger.info("adding Proxy Authentication for post by base URL and bearer token started {}.....", getClass().getName());

        System.setProperty("http.proxyHost", PROXYHOST);
        System.setProperty("http.proxyPort", String.valueOf(80));
        System.setProperty("http.proxyUser", username);
        System.setProperty("http.proxyPassword", password);
        System.setProperty("https.proxyHost", PROXYHOST);
        System.setProperty("https.proxyPort", String.valueOf(80));
        System.setProperty("https.proxyUser", username);
        System.setProperty("https.proxyPassword", password);
        ProxySpecification specification = new ProxySpecification(PROXYHOST, 80, "http");
        specification.withAuth(username, password);
        System.setProperty("java.net.useSystemProxies", "true");
        String proxyAuth = Base64.encode((username + ":" + password).getBytes());
        PreemptiveBasicAuthScheme authSpec = new PreemptiveBasicAuthScheme();
        authSpec.setUserName(username);
        authSpec.setPassword(password);
        RestAssured.proxy(specification);

        RestAssured.given().header("Proxy-Connection", "Keep-Alive").header("Proxy-Authorization", proxyAuth).relaxedHTTPSValidation().with().auth().oauth2(authSpec.generateAuthToken()).proxy(specification);

        if (logger.isInfoEnabled()) {
            logger.info("Proxy details - proxyHost :  {}", PROXYHOST);
            logger.info("Proxy details - proxyPort :  {}", String.valueOf(80));
            logger.info("Proxy details - proxyUsername :  {}", username);
            logger.info("Proxy details - proxyPassword :  {}", password);
            logger.info("adding Proxy Authentication for post by base URL and bearer token complete {}.....", getClass().getName());
        }
    }


    public static String replaceParameter(String strXML, String strParameterName, String strParameterValue) {
        logger.info("replacing parameter started {}.....", RestHelper.class.getName());
        logger.info("XML: {}.....", strXML);
        logger.info("Parameter name to replace: {}.....", strParameterName);
        logger.info("New Value: {}.....", strParameterValue);

        String newXML = strXML.replace("{" + strParameterName + "}", strParameterValue);
        logger.info("Updated XML: {}.....", newXML);
        logger.info("replacing parameter complete {}.....", RestHelper.class.getName());
        return newXML;

    }

    public String readFileFromResources(String strFileName){
        StringBuilder fileContent = new StringBuilder();

        try (InputStream is = RestHelper.class.getResourceAsStream("/" + strFileName)) {
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            String l;
            while((l = r.readLine()) != null) {
                fileContent.append(l);
            }
        } catch (IOException e) {
            logger.info("Error reading file {}", e.getMessage());
        }

        return fileContent.toString();

    }

}

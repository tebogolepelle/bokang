package util;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.EncoderConfig;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pojos.requests.createparty.header.*;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.*;
import java.security.*;
//import plm.pojo.request.header.*;

public class SoapHelper {
    private static final Logger logger = LogManager.getLogger(SoapHelper.class);
    private static final String READING_FILE_ERROR = "Error reading file {}";
    private ApiConfig apiConfig;
    private static final String PROXYHOST = "172.17.2.9";
    protected RestUtil util;
    KeyStore keyStore;
    String keystorepwd = "Testing!@#$%1L";


    public SoapHelper() {
        logger.info("Initialling class {}.....", getClass().getName());
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }    }

    private String getBody(String strOperationName) {
        logger.info("getting xml body started {}.....", SoapHelper.class.getName());

        String strFileName = strOperationName + ".xml";
        StringBuilder body = new StringBuilder();

        try (InputStream is = SoapHelper.class.getResourceAsStream("/soap_body/" + strFileName)) {
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            String l;
            while ((l = r.readLine()) != null) {
                body.append(l);
            }
        } catch (IOException e) {
            logger.info(READING_FILE_ERROR, e.getMessage());
        }

        logger.info("Body: {}", body);

        logger.info("getting xml body complete {}.....", SoapHelper.class.getName());

        return body.toString();
    }

    private String getHeader(String strServicePath) {
        logger.info("getting xml header started {}.....", SoapHelper.class.getName());
        String xmlnsVersion = strServicePath.substring(strServicePath.lastIndexOf('/') + 1);
        StringBuilder strHeader = new StringBuilder();
        try (InputStream is = SoapHelper.class.getResourceAsStream("/soap_body/header.xml")) {
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            String l;
            while ((l = r.readLine()) != null) {
                strHeader.append(l);
            }
        } catch (IOException e) {
            logger.info(READING_FILE_ERROR, e.getMessage());
        }

        strHeader = new StringBuilder(strHeader.toString().replace("{xmlns_version}", xmlnsVersion));
        logger.info("header: {}", strHeader);
        logger.info("getting xml header complete {}.....", SoapHelper.class.getName());

        return strHeader.toString();
    }

    public Header getHeader(){
        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        RequestOriginator requestOriginator = new RequestOriginator();
        ContextInfo contextInfo = new ContextInfo();
        EnterpriseContext enterpriseContext = new EnterpriseContext(contextInfo, requestOriginator, instrumentationInfo);

        return new Header(enterpriseContext);
    }

    private FileInputStream getCertificate() {
        logger.info("getting Certificate  started {}.....", SoapHelper.class.getName());
        FileInputStream fileInputStream = null;

        File tempFile;
        try {
            tempFile = File.createTempFile("prefix", "suffix");
        } catch (IOException e) {
            logger.error(e.getMessage());
            return null;
        }
        tempFile.deleteOnExit();

        try (InputStream is = SoapHelper.class.getResourceAsStream("/" + apiConfig.getOtherElements().get("cert_name"))) {
            FileUtils.copyInputStreamToFile(is, tempFile);
            fileInputStream = new FileInputStream(tempFile);
        } catch (IOException e) {
            logger.info(READING_FILE_ERROR, e.getMessage());
        }

        logger.info("getting Certificate  complete {}.....", SoapHelper.class.getName());
        return fileInputStream;
    }

    private RequestSpecification configureRestAssuredAuthentication() throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        logger.info("configure Rest Assured Authentication started {}.....", SoapHelper.class.getName());
        //System.setProperty("javax.net.ssl.trustStore", "C:/Program Files/Java/jdk1.8.0_161/jre/lib/security");

        /*KeyStore keyStore = null;
        String keyStorePassword = apiConfig.getOtherElements().get("cert_password");

        try {
            keyStore = KeyStore.getInstance("PKCS12");
        } catch (KeyStoreException e) {
            logger.error(e.getMessage());
        }

        try {
            assert keyStore != null;
            keyStore.load(this.getCertificate(), keyStorePassword.toCharArray());
        } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
            logger.error(e.getMessage());
            return null;
        }*/


        /*X509HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SSLSocketFactory lSchemeSocketFactory;
        try {
            lSchemeSocketFactory = new SSLSocketFactory(keyStore, keystorepwd);
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException | UnrecoverableKeyException e) {
            logger.error(e.getMessage());
            return null;
        }*/

//        lSchemeSocketFactory.setHostnameVerifier(hostnameVerifier);
        X509HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SSLSocketFactory lSchemeSocketFactory = null;
        lSchemeSocketFactory = new SSLSocketFactory(keyStore, keystorepwd);
        lSchemeSocketFactory.setHostnameVerifier(hostnameVerifier);
        RequestSpecification soapSpec = new RequestSpecBuilder().setConfig(RestAssured.config().sslConfig(new SSLConfig().with().sslSocketFactory(lSchemeSocketFactory).and().allowAllHostnames()).encoderConfig(EncoderConfig.encoderConfig().encodeContentTypeAs(" multipart/related; type=\"application/xop+xml\"; " +
                "start=\"<rootpart@soapui.org>\"; start-info=\"text/xml\"; " +
                "boundary=\"----=_Part_8_1547798259.1570440458254\"", ContentType.XML))).build();
        logger.info("configure Rest Assured Authentication complete {}.....", SoapHelper.class.getName());
        return soapSpec;
    }


    private void clearProxyProperties() {
        logger.info("clearing Proxy properties started {}.....", getClass().getName());

        System.clearProperty("http.proxyHost");
        System.clearProperty("http.proxyPort");
        System.clearProperty("http.proxyUser");
        System.clearProperty("http.proxyPassword");
        System.clearProperty("https.proxyHost");
        System.clearProperty("https.proxyPort");
        System.clearProperty("https.proxyUser");
        System.clearProperty("https.proxyPassword");
        System.clearProperty("java.net.useSystemProxies");

    }

    public Response postRequest(String strPayload, String endPoint) {
        logger.info("posting Request started {}.....", SoapHelper.class.getName());

        Response response = null;
        try {
            RestAssured.reset();
            clearProxyProperties();
            RequestSpecification spec;
            spec = configureRestAssuredAuthentication();
            util = new RestUtil();


            response = util.initSpec(apiConfig.getOtherElements().get("baseUrl")).postFromString(apiConfig.getOtherElements().get(endPoint), strPayload).thenReturn().prettyPeek();

            if (logger.isInfoEnabled()) {
                logger.info("Soap end point used {}", endPoint);
                logger.info("payload: {}", strPayload);
                logger.info("Response body for response {}", response.getBody().prettyPrint());
                logger.info("posting request finished for class {}.....", SoapHelper.class.getName());
            }
            
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (KeyStoreException e) {
            e.printStackTrace();
            return null;
        } catch (KeyManagementException e) {
            e.printStackTrace();
            return null;
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return response;
    }

    public String replaceParameter(String strXML, String strParameterName, String strParameterValue) {
        logger.info("replacing parameter started {}.....", SoapHelper.class.getName());
        logger.info("XML: {}.....", strXML);
        logger.info("Parameter name to replace: {}.....", strParameterName);
        logger.info("New Value: {}.....", strParameterValue);

        String newXML = strXML.replace("{" + strParameterName + "}", strParameterValue);

        logger.debug("Updated XML: {}.....", newXML);
        logger.info("replacing parameter complete {}.....", SoapHelper.class.getName());

        return newXML;
    }

    public static <T> T unmarshall(InputStream xml, Class<T> clazz) {
        T obj = null;
        SOAPMessage message = null;
        try {
            message = MessageFactory.newInstance().createMessage(null, xml);
            JAXBContext jaxbContext = null;
            jaxbContext = JAXBContext.newInstance(clazz);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            obj = clazz.cast(jaxbUnmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument()));

        } catch (SOAPException | JAXBException | IOException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public String marshallEnvelopeObject(Object ob){
        StringWriter sw = new StringWriter();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ob.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(ob, sw);
            jaxbMarshaller.marshal(ob, System.out);
            return sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        }finally {
            return sw.toString();
        }
    }



}

import actions.CreateAccount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pojos.requests.createparty.Envelope;
import pojos.requests.createparty.body.Body;
import pojos.requests.createparty.body.CreatePartyRequest;
import pojos.requests.createparty.body.createpartyrequest.CreatePartyDetailsPojo;
import pojos.requests.createparty.body.createpartyrequest.PartyDetailsPojo;
import pojos.requests.createparty.body.header.WSHeader;
import pojos.requests.createparty.header.Header;
import pojos.response.createaccount.CreateAccountResponse;
import util.RestHelper;
import util.SoapHelper;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class CreateAccountTest {
    private SoapHelper soapHelper;
    private RestHelper restHelper;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(CreatePartyTest.class);
    private static final String RESPONSE = "response";
    CreateAccount createAccount;


    @Before
    public void setUp() {
        try {
            createAccount = new CreateAccount();
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("AccountManagementService", "mock_response_createAccount.xml");
    }

    @Test
    public void createAccountOperation()  {
        String strAccNum = "2234567890120045";
        String strPartyId = "10101010101011";
        String strPlanId = "123";
        String strOpenedDate = "20110986";
        String strProductCode = "000017001";
        String strBrand = "06";
        String strCurrency = "ZAR";
        String strGlProductCode = "123456";
        String strAccStatus = "00";
        String strNonResStatus = "1234";
        String strReserveBankCode = "1234" ;
        String strRelationshipType = "123";
        String strFinancialBranch = "123";
        String strTypeOfBusiness = "1";
        String strSuppressFeeInd = "0";
        String strSourceSysId = "GDP";

        logger.info("Starting create operation test", getClass().getName());
        JSONObject responseJson = createAccount.createAccountOperation(strAccNum, strPartyId, strPlanId, strOpenedDate, strProductCode, strBrand,strCurrency, strGlProductCode, strAccStatus, strNonResStatus, strReserveBankCode, strRelationshipType, strFinancialBranch, strTypeOfBusiness, strSuppressFeeInd, strSourceSysId);

        CreateAccountResponse createAccountResponse = (CreateAccountResponse) responseJson.get(RESPONSE);
        Assert.assertEquals("2234567890120045", createAccountResponse.getAccountResponse().getAccountNumber());
    }

    //    @Test
    public void getXML(String strPartyId, String strCustStatus, String strClientType, String strCluster, String strSegment, String strSourceIdentifier, String strRecordAction) {
        Header header = soapHelper.getHeader();

        //body
        PartyDetailsPojo partyDetailsPojo = new PartyDetailsPojo(strPartyId, strCustStatus, strClientType, strCluster, strSegment, strSourceIdentifier, strRecordAction);
        CreatePartyDetailsPojo createPartyDetailsPojo =  new CreatePartyDetailsPojo(partyDetailsPojo);
        WSHeader wsHeader =  new WSHeader("");

        CreatePartyRequest createPartyRequest= new CreatePartyRequest(wsHeader, createPartyDetailsPojo);

        Body body = new Body(createPartyRequest);

        Envelope envelope = new Envelope(header,body);

        //marshall the POJO to xml and return string
        soapHelper.marshallEnvelopeObject(envelope);
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

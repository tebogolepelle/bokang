
import actions.CreateParty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pojos.requests.createparty.Envelope;
import pojos.requests.createparty.body.Body;
import pojos.requests.createparty.body.CreatePartyRequest;
import pojos.requests.createparty.body.createpartyrequest.CreatePartyDetailsPojo;
import pojos.requests.createparty.body.createpartyrequest.PartyDetailsPojo;
import pojos.requests.createparty.body.header.WSHeader;
import pojos.requests.createparty.header.Header;
import pojos.response.createparty.CreatePartyResponse;
import pojos.response.createpartyfault.CreatePartyFaultResponse;
import util.RestHelper;
import util.SoapHelper;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class CreatePartyTest {
    private SoapHelper soapHelper;
    private RestHelper restHelper;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(CreatePartyTest.class);
    private static final String RESPONSE = "response";
    CreateParty createParty;

    @Before
    public void setUp() {
        try {
            createParty = new CreateParty();
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("PartyManagementService", "mock_response_createParty.xml");
    }

    @Test
    public void createPartyOperation()  {
        String strPartyID = "20101010109014";

        logger.info("Starting create operation test", getClass().getName());
        JSONObject responseJson = createParty.createPartyOperation(strPartyID,"01","01","01","01","1","1");

        if (responseJson.get(RESPONSE) instanceof CreatePartyFaultResponse) {
            CreatePartyFaultResponse createPartyFaultResponse = (CreatePartyFaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("ns2:ErrorCode", createPartyFaultResponse.getPartyFaultResponse().getStrErrorCode());
        } else {
            CreatePartyResponse createPartyResponse = (CreatePartyResponse) responseJson.get(RESPONSE);
            Assert.assertEquals(strPartyID, createPartyResponse.getPartyResponse().getStrPartyId());
        }

    }

//    @Test
    public void getXML(String strPartyId, String strCustStatus, String strClientType, String strCluster, String strSegment, String strSourceIdentifier, String strRecordAction) {
        Header header = soapHelper.getHeader();

        //body
        PartyDetailsPojo partyDetailsPojo = new PartyDetailsPojo(strPartyId, strCustStatus, strClientType, strCluster, strSegment, strSourceIdentifier, strRecordAction);
        CreatePartyDetailsPojo createPartyDetailsPojo =  new CreatePartyDetailsPojo(partyDetailsPojo);
        WSHeader wsHeader =  new WSHeader("");

        CreatePartyRequest createPartyRequest= new CreatePartyRequest(wsHeader, createPartyDetailsPojo);

        Body body = new Body(createPartyRequest);

        Envelope envelope = new Envelope(header,body);

        //marshall the POJO to xml and return string
        soapHelper.marshallEnvelopeObject(envelope);
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }

}

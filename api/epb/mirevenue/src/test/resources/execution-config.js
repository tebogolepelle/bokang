var configFunction = function (envName) {
  if (!envName) {
    envName = 'DEV';
  }

//Use this config object to declare properties such as paths to various APIs that you can use in your asset.
//You can declare other appropriate properties. See below example.
  var config = {
      getPartyUrl : "party/v4/parties",
      branchesUrl : "channeloperations/branchdcar/v1/dcar/01174/branches",
      createOrgUrl : "party/v4/organizations",
      getTypeMaxRecords : "x-ned-maxrecordsreturn",
      getTypePageIndex : "x-ned-pageendindex",
      getPartyResultCount : "x-ned-returnavailableresultcount",
      getPartyStartIndex : "x-ned-pagestartindex",
      getOrgByEPNUrl : "party/v4/parties/"
  };

//config.baseUrl is the property that holds the base URL for all the requests.
  if (envName == 'DEV') {
    config.baseUrl = 'http://localhost';
    config.create_party_endpoint = "PartyManagementService";
    config.create_account_endpoint = "AccountManagementService";
  }
  else if(envName == 'ETE'){
      config.baseUrl = 'http://zeepbwn1:9081/pbmWebService/remoting/services/';
      config.create_party_endpoint = "PartyManagementService";
      config.create_account_endpoint = "AccountManagementService";
    }
    else if(envName == 'QA'){
      config.baseUrl = 'http://zqepbwn1:9081/pbmWebService/remoting/services/';
      config.create_party_endpoint = "PartyManagementService";
      config.create_account_endpoint = "AccountManagementService";
    }

  config.environment = envName;
  return config;
}

var reportConfig = function(envName,scenario) {
  if (!envName) {
    envName = 'QA';
  }

  if(!scenario) {
    scenario = 'Smoke Test';
  }
   var config = {
            platform:'API',
            product: 'EPB',
            project: 'miRevenue',
            tool: 'Framework 2.0',
            version: '1.0.1',
            environment:envName,
            scenario:scenario
   };

   return config;
}
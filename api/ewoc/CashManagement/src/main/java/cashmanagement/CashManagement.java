package cashmanagement;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import java.io.*;

public class CashManagement {

    protected RestUtil restUtil;
    protected ApiConfig apiConfig;
    protected JsonUtil jsonUtil;
    private String pfxFile;
    private String pfxPsword = "Y6iHdAD4802Vk568";
    private Logger logger = LogManager.getLogger(CashManagement.class);

    public CashManagement() throws ApiException {
        restUtil = new RestUtil();
        apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        jsonUtil = new JsonUtil();
        pfxFile = getClass().getClassLoader().getResource("AP234182.pfx").getPath();
        logger.info("Certificate Path:  {}.....", pfxFile);
    }

    /**
     * This method configures the request
     *
     * @param pfxPath path to the pfx certificate
     * @param password password of the pfx certificate
     */
    public void configureRequest(String pfxPath,String password) {
        restUtil.initSpec(apiConfig.getEnvironmentBaseUrl()).addCertAuthentication(pfxPath, password);
    }

    /**
     * This method appends the token to the configured request
     *
     * @param pfxpath path to the pfx certificate
     * @param pfxpswd password of the pfx certificate
     * @param auth token value parsed as a string in the request
     */
    public void readAndAppendToken(String pfxpath, String  pfxpswd, String auth){
        this.configureRequest(pfxpath, pfxpswd);
        this.jsonUtil = new JsonUtil();
        restUtil.relaxHttpsValidation("SSL").appendHeader("Authorization", auth);

    }

    /**
     * This method returns the value of string element within a json object
     *
     * @param fileLocation location of the json file
     * @param valueLocation location of the string element
     * @return the value of the string element
     * @throws IOException
     */
    public String getJsonValue(String fileLocation, String valueLocation) throws IOException {
        this.jsonUtil = new JsonUtil();
        JsonObject gison = (JsonObject) this.jsonUtil.parseJsonFile(fileLocation);
        String getText = gison.toString();
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(getText);
        return (JsonPath.read(document, valueLocation));
    }

    /**
     * The response to the get call is a json object which represents
     * the configuration of a prepack before it gets loaded into an atm.
     * This method writes the response to a file and returns the order
     * remaining value of the prepack using the getFirstOrderRemainingValue method.
     *
     * @param token
     * @param firstJsonLocation location where the response to the get call gets written to
     * @return the order remainig value as an integer
     * @throws IOException
     * @throws InterruptedException
     * @throws ApiException
     */
    public int getOrderRemainingValueBeforeLoadingPrepack(String token, String firstJsonLocation) throws IOException, InterruptedException, ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        Response response = this.restUtil.get(apiConfig.getOtherElements().get("retrievePrepackDetailsUrl"));
        String outPut = response.body().prettyPrint();
        response.then().log().all();
        String strRelClassPath = (new File("classpath:")).getAbsolutePath();
        strRelClassPath = strRelClassPath.substring(0, strRelClassPath.indexOf("classpath"));

        try(FileWriter fileR = new FileWriter(strRelClassPath + firstJsonLocation))
        {
            fileR.write(outPut);
            fileR.flush();
        }
        Thread.sleep(2000L);

        File file = new File(firstJsonLocation);
        String absolutePath = file.getAbsolutePath();
        String orderRemainingValue = "$.orderRemainingValue";
        return Integer.parseInt(this.getJsonValue(absolutePath, orderRemainingValue));

    }

    /**
     * This method loads the prepack into the atm by making a put call
     * to the api and prints out the response.
     *
     * @param token
     * @param file the json object that represents the body sent in our put request
     * @throws IOException
     */
    public void loadPrepack(String token, File file) throws IOException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        Response response = this.restUtil.addContentType(ContentType.JSON).putFromFile(apiConfig.getOtherElements().get("loadPrepackUrl"), file);
        response.then().log().all();
    }

    /**
     * This method reads a file and returns the total amount loaded into the atm
     *
     * @param aFile json object which contains all the details of the loaded atm
     * @return the value of the amount loaded as an integer
     * @throws FileNotFoundException
     */
    public int getTotalValue(File aFile) throws FileNotFoundException {
        JsonParser parser = new JsonParser();
        FileReader fileReader = new FileReader(aFile);
        JsonObject json = (JsonObject) parser.parse(fileReader);
        JsonArray atmCashList = (JsonArray) json.get("atmCashList");
        int totalValue = 0;

        for (int i=0;i<atmCashList.size();i++)
        {
            totalValue = atmCashList.get(i).getAsJsonObject().get("totalValue").getAsInt();
        }
        return totalValue;
    }

    /**
     * The response to the get call is a json object which represents
     * the configuration of a prepack after it has been loaded into an atm.
     * This method writes the response to a file and  asserts that the order
     * remaining value of the prepack after the atm has been loaded is equal
     * to the initial order remaining value minus the amount loaded into the atm.
     *
     * @param token
     * @param secondJsonLocation location where the response to the get call gets written to
     * @throws IOException
     * @throws InterruptedException
     * @throws ApiException
     */
    public int getOrderRemainingValueAfterLoadingPrepack(String token, String secondJsonLocation) throws IOException, InterruptedException, ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        Response response = this.restUtil.get(apiConfig.getOtherElements().get("retrievePrepackDetailsUrl"));
        String outPut = response.body().prettyPrint();
        response.then().log().all();
        String strRelClassPath = (new File("classpath:")).getAbsolutePath();
        strRelClassPath = strRelClassPath.substring(0, strRelClassPath.indexOf("classpath"));
        try (FileWriter fileR = new FileWriter(strRelClassPath + secondJsonLocation)) {
            fileR.write(outPut);
            fileR.flush();
        }
        Thread.sleep(2000L);

        File file = new File(secondJsonLocation);
        String absolutePath = file.getAbsolutePath();
        String orderRemainingValue = "$.orderRemainingValue";
        return Integer.parseInt(this.getJsonValue(absolutePath, orderRemainingValue));

    }

    public Response viewPrepack(String token) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("retrievePrepackDetailsUrl"));
        Response response = this.restUtil.get(String.valueOf(endpoint), true);
        return response;
    }

    public Response returnPrepacks(String token, File payload) throws ApiException, IOException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromFile(apiConfig.getOtherElements().get("returnPrepackUrl"), payload);
        response.then().log().all();
        return response;
    }

    public Response enquirePrepackReturns(String token, String prepackStatus) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("returnPrepackUrl"));
        Response response = this.restUtil.addParam("prepackStatus", prepackStatus).get(String.valueOf(endpoint), true);
        return response;
    }

    public Response orderCashFromAlternateVendor(String token, File payload) throws ApiException, IOException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromFile(apiConfig.getOtherElements().get("orderCashFromAlternateVendorUrl"), payload);
        response.then().log().all();
        return response;
    }

    public Response enquireCashOrder(String token, String atmNumber, String fromDate, String toDate) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("returnPrepackUrl"));
       Response response = this.restUtil.addParam("atmNumber", atmNumber).addParam("fromDate", fromDate).addParam("toDate", toDate).get(String.valueOf(endpoint), true);
        return response;
    }

}

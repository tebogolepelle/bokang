package cashmanagement;

import io.restassured.response.Response;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnquireCashOrderTest {

    private CashManagement cashManObject;
    private Logger logger = LogManager.getLogger(EnquirePrepackReturnsTest.class);
    File file = new File("src\\main\\resources\\Data\\token.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    String location = "src\\test\\resources\\payloads\\OrderCash.json";
    File payload = new File(location);
    String atmNumber = "BB33";
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date yesterday = DateUtils.addDays(new Date(), -1);
    Date today = new Date();
    String yesterdayDate = dateFormat.format(yesterday);
    String todayDate = dateFormat.format(today);

    public EnquireCashOrderTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            cashManObject = new CashManagement();

        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void assertTotalValueOfReturnedPrepacks() throws ApiException, IOException {
        cashManObject.orderCashFromAlternateVendor(token, payload);
        Response response = cashManObject.enquireCashOrder(token, atmNumber, yesterdayDate, todayDate);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));

    }


    @After
    public void tearDown() {

        cashManObject = null;

    }
}

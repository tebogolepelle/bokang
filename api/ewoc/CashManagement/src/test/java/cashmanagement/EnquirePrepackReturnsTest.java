package cashmanagement;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class EnquirePrepackReturnsTest {

    private CashManagement cashManObject;
    private Logger logger = LogManager.getLogger(EnquirePrepackReturnsTest.class);
    File file = new File("src\\main\\resources\\Data\\token.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    String prepackStatus = "RET";
    String location = "src\\test\\resources\\payloads\\ReturnPrepacks.json";

    public EnquirePrepackReturnsTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            cashManObject = new CashManagement();

        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void assertTotalValueOfReturnedPrepacks() throws ApiException, IOException {
        Response response = cashManObject.viewPrepack(token);
        String prepackValue = response.getBody().jsonPath().getString("prepackValue");
        File payload = new File(location);
        String absolutePath = payload.getAbsolutePath();
        String prepackNumber = "$.numberOfPrepacks";
        int numberOfPrepacks = Integer.parseInt(cashManObject.getJsonValue(absolutePath, prepackNumber));
        int totVal = (Integer.parseInt(prepackValue)) * (numberOfPrepacks);
        cashManObject.returnPrepacks(token, payload);
        Response reply = cashManObject.enquirePrepackReturns(token, prepackStatus);
        assertEquals(200,reply.getStatusCode());
        assertEquals("R00",reply.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
        String totalValue = reply.getBody().jsonPath().getString("prepackOrderList[0].totalValue");
       assertEquals(totalValue,Integer.toString(totVal));
    }

    @After
    public void tearDown() {

        cashManObject = null;

    }
}

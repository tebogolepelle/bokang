package cashmanagement;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

public class LoadPrepackTest {

    private CashManagement loadPrepackObject;
    private Logger logger = LogManager.getLogger(LoadPrepackTest.class);
    File file = new File("src\\main\\resources\\Data\\token.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    private File aFile = new File("src\\main\\java\\cashmanagement\\responses\\Put_LoadAtm.json");
    private String firstLocation = "src\\main\\java\\cashmanagement\\responses\\PrepackDetailsBeforeLoadingAtm_Response.json";
    private String secondLocation = "src\\main\\java\\cashmanagement\\responses\\PrepackDetailsAfterLoadingAtm_Response.json";
    private static WireMockServer wireMockServer;

    public LoadPrepackTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            loadPrepackObject = new CashManagement();
            wireMockServer = new WireMockServer(8090);
            setupStub();
            wireMockServer.start();
        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void assertOrderRemainingValue() throws IOException, InterruptedException, ApiException {
        int initialOrderRemainingValue = loadPrepackObject.getOrderRemainingValueBeforeLoadingPrepack(token, firstLocation);
        System.out.println("The initial order remaining value is: " + initialOrderRemainingValue);
        loadPrepackObject.loadPrepack(token, aFile);
        int finalOrderRemainingValue = loadPrepackObject.getOrderRemainingValueAfterLoadingPrepack(token, secondLocation);
        System.out.println("The final order remaining value is: " + finalOrderRemainingValue);
        int orderTotalValue = loadPrepackObject.getTotalValue(aFile);
        assertEquals(finalOrderRemainingValue, (initialOrderRemainingValue - orderTotalValue));
    }

    private static void setupStub() {
        wireMockServer.stubFor(get(urlPathEqualTo("stockmanagement/cashoperations/v2/prepacks/T958"))
                .willReturn(aResponse().withHeader("Content-Type", "text/plain")
                        .withStatus(200)));
        wireMockServer.stubFor(post(urlPathEqualTo("stockmanagement/cashoperations/v2/atms/cash"))
                .withRequestBody(equalToJson("{\n" +
                        "  \"regionId\":8,\n" +
                        "  \"atmCashList\":[{\"atmNumber\":\"B689\",\"totalValue\":1056000,\n" +
                        "    \"denominationConfigList\":\n" +
                        "    [{\"denominationCode\":\"1\",\"denominationAmount\":440000},\n" +
                        "      {\"denominationCode\":\"2\",\"denominationAmount\":300000},\n" +
                        "      {\"denominationCode\":\"3\",\"denominationAmount\":170000},\n" +
                        "      {\"denominationCode\":\"4\",\"denominationAmount\":96000},\n" +
                        "      {\"denominationCode\":\"5\",\"denominationAmount\":50000}\n" +
                        "    ],\n" +
                        "    \"selectedatm\":\n" +
                        "    {\"atmNumber\":\"B689\",\n" +
                        "      \"atmName\":\"OLYMPUS PLAZA\",\n" +
                        "      \"regionId\":\"8\"\n" +
                        "    }\n" +
                        "  }\n" +
                        "  ]\n" +
                        "}"))
                .willReturn(aResponse().withHeader("Content-Type", "text/plain")
                        .withStatus(200)));

    }

    @After
    public void tearDown() {

        loadPrepackObject = null;
        wireMockServer.stop();
    }

}

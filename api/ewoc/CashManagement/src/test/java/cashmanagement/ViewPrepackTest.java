package cashmanagement;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ViewPrepackTest {

    private CashManagement cashManObject;
    private Logger logger = LogManager.getLogger(ViewPrepackTest.class);
    File file = new File("src\\main\\resources\\Data\\token.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();

    public ViewPrepackTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            cashManObject = new CashManagement();

        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void viewPrepackDetails() throws ApiException {
        Response response = cashManObject.viewPrepack(token);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @After
    public void tearDown() {

        cashManObject = null;

    }

}

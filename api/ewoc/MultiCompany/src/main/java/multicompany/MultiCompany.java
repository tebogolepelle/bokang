package multicompany;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class MultiCompany {

    protected RestUtil restUtil;
    protected ApiConfig apiConfig;
    protected JsonUtil jsonUtil;
    private String pfxFile;
    private String pfxPsword = "Y6iHdAD4802Vk568";
    private Logger logger = LogManager.getLogger(MultiCompany.class);

    public MultiCompany() throws ApiException {
        restUtil = new RestUtil();
        apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        jsonUtil = new JsonUtil();
        pfxFile = getClass().getClassLoader().getResource("AP234182.pfx").getPath();
        logger.info("Certificate Path:  {}.....", pfxFile);
    }

    /**
     * This method configures the request
     *
     * @param pfxPath path to the pfx certificate
     * @param password password of the pfx certificate
     */
    public void configureRequest(String pfxPath,String password) {
        restUtil.initSpec(apiConfig.getEnvironmentBaseUrl()).addCertAuthentication(pfxPath, password);
    }

    /**
     * This method appends the token to the configured request
     *
     * @param pfxpath path to the pfx certificate
     * @param pfxpswd password of the pfx certificate
     * @param auth token value parsed as a string in the request
     */
    public void readAndAppendToken(String pfxpath, String  pfxpswd, String auth){
        this.configureRequest(pfxpath, pfxpswd);
        this.jsonUtil = new JsonUtil();
        restUtil.relaxHttpsValidation("SSL").appendHeader("Authorization", auth);

    }

    public Response seeExistingUnits(String token, String unitType, int unitNumber) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("seeExistingUnitsUrl"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{unit_type}", unitType));
        endpoint =  new StringBuilder(endpoint.toString().replace("{unit_number}", Integer.toString(unitNumber)));
        Response response = this.restUtil.get(String.valueOf(endpoint), true);
        return response;
    }

    public Response seeExistingBrand(String token, int unitNumber) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("seeExistingBrandUrl"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{unit_number}", Integer.toString(unitNumber)));
        Response response = this.restUtil.get(String.valueOf(endpoint), true);
        return response;
    }

    public Response createNewUnit(String token, String payload, String unitType){
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("createNewUnitUrl"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{unit_type}", unitType));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;
    }

    public Response createNewBrand(String token, String payload){
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("createBrandUrl"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;
    }

    public Response seeBranchStructure(String token, int unitNumber) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("seeOrgStructureUrl"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{branch_number}", Integer.toString(unitNumber)));
        Response response = this.restUtil.get(String.valueOf(endpoint), true);
        return response;
    }


}

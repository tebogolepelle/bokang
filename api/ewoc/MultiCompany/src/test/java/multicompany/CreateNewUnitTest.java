package multicompany;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateNewUnitTest {

    private MultiCompany mcObject;
    private Logger logger = LogManager.getLogger(SeeExistingUnitsTest.class);
    File file = new File("src\\main\\resources\\Data\\token.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    String [] unitTypes = {"BRN", "ARA", "RGN", "DIV", "CLU", "CPY", "GRP", "BPC"};

    public CreateNewUnitTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            mcObject = new MultiCompany();
        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void createNewBranchUnit() throws FileNotFoundException {

        String unitNumber = "12" + createRandomIntegerString(2);
        String fullNameAfr = "Toets Tak" + createRandomIntegerString(2);
        String fullNameEng = "Test Branch" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateBranch.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[0]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewAreaUnit() throws FileNotFoundException {

        String unitNumber = "11" + createRandomIntegerString(2);
        String fullNameAfr = "Toets Area" + createRandomIntegerString(2);
        String fullNameEng = "Test Area" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateArea.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[1]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewRegionUnit() throws FileNotFoundException {

        String unitNumber = "21" + createRandomIntegerString(2);
        String fullNameAfr = "Toets Streek" + createRandomIntegerString(2);
        String fullNameEng = "Test Region" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateRegion.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[2]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewDivisionUnit() throws FileNotFoundException {

        String unitNumber = "13" + createRandomIntegerString(2);
        String fullNameAfr = "Toetsafdeling" + createRandomIntegerString(2);
        String fullNameEng = "Test Division" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateDivision.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[3]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewClusterUnit() throws FileNotFoundException {

        String unitNumber = "14" + createRandomIntegerString(2);
        String fullNameAfr = "Toetsgroep" + createRandomIntegerString(2);
        String fullNameEng = "Test Cluster" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateCluster.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[4]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewCompanyUnit() throws FileNotFoundException {

        String unitNumber = "1" + createRandomIntegerString(2);
        String fullNameAfr = "Toetsonderneming" + createRandomIntegerString(2);
        String fullNameEng = "Test Company" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateCompany.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[5]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewGroupUnit() throws FileNotFoundException {

        String unitNumber = "1" + createRandomIntegerString(1);
        String fullNameAfr = "Toetsgroep" + createRandomIntegerString(2);
        String fullNameEng = "Test Group" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateGroup.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[6]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewBPCUnit() throws FileNotFoundException {

        String unitNumber = "15" + createRandomIntegerString(2);
        String fullNameEng = "Test BPC" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateBPC.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{unitNumber}", unitNumber);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewUnit(token, payload, unitTypes[7]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @Test
    public void createNewBrand() throws FileNotFoundException {

        String brandNumber = "" + createRandomIntegerString(2);
        String fullNameEng = "Test Brand" + createRandomIntegerString(2);
        String fullNameAfr = "Test Handelsmerk" + createRandomIntegerString(2);

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader("src\\test\\resources\\payloads\\CreateBrand.json"));
        JsonObject requestBody = (JsonObject)obj;
        String payload = requestBody.toString();
        payload = payload.replace("{brandNumber}", brandNumber);
        payload = payload.replace("{fullNameAfr}", fullNameAfr);
        payload = payload.replace("{fullNameEng}", fullNameEng);

        System.out.println(payload);

        Response response = mcObject.createNewBrand(token, payload);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("resultsSet.resultCode"));

    }

    @After
    public void tearDown() {

        mcObject = null;
    }

    public String createRandomIntegerString(int numberOfRandomIntegers){
        String randomIntegerString="";

        for (int i=0;i<numberOfRandomIntegers;i++){
            int addingInt=(int)(Math.random()*10);
            randomIntegerString += Integer.toString(addingInt);
        }
        return randomIntegerString;
    }


}

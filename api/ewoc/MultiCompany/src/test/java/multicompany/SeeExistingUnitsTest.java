package multicompany;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SeeExistingUnitsTest {

    private MultiCompany mcObject;
    private Logger logger = LogManager.getLogger(SeeExistingUnitsTest.class);
    File file = new File("src\\main\\resources\\Data\\token.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    String [] unitTypes = {"BRN", "ARA", "RGN", "DIV", "CLU", "CPY", "GRP", "BPC"};
    int [] unitNumbers = {5, 9, 15, 10, 12, 16, 2, 1, 120};

    public SeeExistingUnitsTest() throws IOException {

    }

    @Before
    public void setUp() throws IOException {
        try {
            mcObject = new MultiCompany();
            /*wireMockServer = new WireMockServer(8090);
            setupStub();
            wireMockServer.start();*/
        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void enquireAboutUnitTypeWithNonExistingUnitNumber() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[1], unitNumbers[0]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R01",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutBranchUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[0], unitNumbers[1]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutAreaUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[1], unitNumbers[2]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutRegionUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[2], unitNumbers[3]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutDivisionUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[3], unitNumbers[4]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutClusterUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[4], unitNumbers[5]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutCompanyUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[5], unitNumbers[6]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutGroupUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[6], unitNumbers[7]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutBPCUnitType() throws ApiException {
        Response response = mcObject.seeExistingUnits(token, unitTypes[7], unitNumbers[8]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void enquireAboutBrand() throws ApiException {
        Response response = mcObject.seeExistingBrand(token, unitNumbers[0]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void viewBranchOrgStructure() throws ApiException {
        Response response = mcObject.seeBranchStructure(token, unitNumbers[2]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @After
    public void tearDown() {

        mcObject = null;

    }

}

var configFunction = function (envName) {
  if (!envName) {
    envName = 'QA';
  }
  print('conf env'+envName);
  var config = {
      Authorization : 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJuYjMxMTQ1NCIsInRva2VuX3R5cGUiOiJCZWFyZXIiLCJsYXN0TmFtZSI6InVuZGVmaW5lZCIsImlzcyI6ImlkcC5pdC5uZWRuZXQuY28uemEiLCJ1aWQiOiJuYjMxMTQ1NCIsImF1ZCI6IjlmZWNjMzE1LWZlNjQtNDcwMC1hYjMyLTJhZTgzN2Y0NDUwMCIsInNlc3Npb25pZCI6IjY4NWQxYmJhLWU4ZWEtMTFlOS1hNWFiLTAwNTA1NmFmNzE4MSIsIm5iZiI6MTU3MDQ0Mjg0OCwiZmlyc3ROYW1lIjoidW5kZWZpbmVkIiwiaWF0IjoxNTcwNDQyOTA4LCJlbWFpbCI6IkNhdGhlcmluZVBATmVkYmFuay5jby56YSIsImV4cCI6MTU3MDQ4NjEwOCwiZ3JhbnRfdHlwZSI6ImNsaWVudF9jcmVkZW50aWFscyIsImNpZCI6IjM3OCIsInNjb3BlcyI6WyJTdGFmZiJdLCJncm91cCI6IlN0YWZmIiwianRpIjoiZGNlZGMxZWQxYzBlNDI0ZGI1NDVhN2EwZWE2NmRjNjUifQ.nUBgbV1MZPoGQWzjJOIbhMjUbe7XRxiSxpfhPZ36O9OQeq2F1RuW__zR08dT6NQfzw2zmNCDTImEmmHsWUfe8xzPdVqpxyZJ0Va-SlhQC7imdBbPhMF2VQgbVUOzL_aEtbTYjWrsbqMXhkyCKzAtK3G5YhCQPtLDyKC6qg3HAo6_3LAQnkb_9IYLtla6_UDBgPJw5ZC3z6JOTMv6TEWDoWQ2870Pf5QrwaTziB1BuwNuC6wF3eMXxLQ9RcVg3V2T_raw3L6uHvMYhBbizSkMXdDLzljlQXCKJGiqcNdBeLC6Cx_w5JoriWi2SQOIvmiu6DlBIae9K06Lw2FtJ5D07g'
  };
   config.environment = envName;
  return config;
}
var reportConfig = function(envName) {
  if (!envName) {
    envName = 'QA';
  }
   var config = {
            platform:'API',
            product : 'EWOC',
            program : 'Core Banking',
            project : 'LMD',
            chapter : 'Product & Servicing',
            release:'August 2020',
            sprint: 'Undefined',
            squad: 'LMD',
            tool : 'api-framework',
            version : '1.3.0',
            environment:envName,
            scenario: 'Multi-company_scenario'
   };
   return config;
}
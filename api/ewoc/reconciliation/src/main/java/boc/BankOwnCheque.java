package boc;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import java.io.File;
import java.io.IOException;

/**
 * The BankOwnCheque program implements the process
 * of seeing all cheques issued by retail branches to clients,
 * and managing discrepancies
 *
 * @author Christian Nseka
 * @since 2020-05-12
 */
public class BankOwnCheque {

     protected RestUtil restUtil;
     protected ApiConfig apiConfig;
     protected JsonUtil jsonUtil;
     private String pfxFile;
     private String pfxPsword = "Y6iHdAD4802Vk568";
     private Logger logger = LogManager.getLogger(BankOwnCheque.class);

     public BankOwnCheque() throws ApiException, IOException {
     restUtil = new RestUtil();
     apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
     jsonUtil = new JsonUtil();
     pfxFile = getClass().getClassLoader().getResource("AP234182.pfx").getPath();
     logger.info("Certificate Path:  {}.....", pfxFile);
     }

     /**
     * This method configures the request
     *
     * @param pfxPath path to the pfx certificate
     * @param password password of the pfx certificate
     */
    public void configureRequest(String pfxPath,String password) {
        restUtil.initSpec(apiConfig.getEnvironmentBaseUrl()).addCertAuthentication(pfxPath, password);
    }

    /**
     * This method appends the token to the configured request
     *
     * @param pfxpath path to the pfx certificate
     * @param pfxpswd password of the pfx certificate
     * @param auth token value parsed as a string in the request
     */
    public void readAndAppendToken(String pfxpath, String  pfxpswd, String auth){
        this.configureRequest(pfxpath, pfxpswd);
        this.jsonUtil = new JsonUtil();
        restUtil.relaxHttpsValidation("SSL").appendHeader("Authorization", auth);

    }

    public Response retrieveListOfBOC(String token, String bocAccount, String chequeId) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("getListOfBOC"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{boc_account}", bocAccount));
        Response response = this.restUtil.addParam("banksownedchequeid", chequeId).get(String.valueOf(endpoint), true);
        return response;
    }

    public Response createBankOwnCheque(String token, String payload, String bocAccount) throws IOException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("createBankOwnCheque"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{boc_account}", bocAccount));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;
    }

    public Response updateBankOwnCheque (String token, File file, String bocAccount, String chequeId) throws IOException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("updateBankOwnCheque"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{boc_account}", bocAccount));
        endpoint =  new StringBuilder(endpoint.toString().replace("{cheque_id}", chequeId));
        Response response = this.restUtil.addContentType(ContentType.JSON).putFromFile(String.valueOf(endpoint), file);
        response.then().log().all();
        return response;
    }

    public Response deleteBankOwnCheque (String token, String bocAccount, String chequeId, String transactionDate, String eventTime, String creditAmount, String debitAmount) throws IOException, ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("deleteBankOwnCheque"));
        endpoint =  new StringBuilder(endpoint.toString().replace("{boc_account}", bocAccount));
        endpoint =  new StringBuilder(endpoint.toString().replace("{cheque_id}", chequeId));
        endpoint =  new StringBuilder(endpoint.toString().replace("{transactiondateid}", transactionDate));
        endpoint =  new StringBuilder(endpoint.toString().replace("{eventtimestampid}", eventTime));
        endpoint =  new StringBuilder(endpoint.toString().replace("{creditamountid}", creditAmount));
        endpoint =  new StringBuilder(endpoint.toString().replace("{debitamountid}", debitAmount));
        Response response = this.restUtil.addParam("banksownedchequeid", chequeId).get(String.valueOf(endpoint), true);
        response.then().log().all();
        return response;
    }

}

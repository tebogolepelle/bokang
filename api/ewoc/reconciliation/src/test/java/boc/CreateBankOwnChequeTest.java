package boc;

import io.restassured.response.Response;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateBankOwnChequeTest {

    private BankOwnCheque bocObject;
    private Logger logger = LogManager.getLogger(EnquireBankOwnChequeTest.class);
    File file = new File("src\\main\\resources\\Data\\auth.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    private String bocAccount = "1548991104";

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date yesterday = DateUtils.addDays(new Date(), -1);
    Date today = new Date();
    Date tomorrow = DateUtils.addDays(new Date(), +1);
    String yesterdayDate = dateFormat.format(yesterday);
    String todayDate = dateFormat.format(today);
    String tomorrowDate = dateFormat.format(tomorrow);

    public CreateBankOwnChequeTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            bocObject = new BankOwnCheque();
            /*wireMockServer = new WireMockServer(8090);
            setupStub();
            wireMockServer.start();*/
        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void addBankOwnChequeWithCredit() throws IOException{

        Map<String, Object> part = new HashMap<String, Object>();
        part.put("creditAmountId", 550);
        part.put("debitAmountId", 0);
        JSONObject json2 = new JSONObject();
        json2.putAll(part);
        String jsonPart = json2.toString();
        JSONObject json = new JSONObject();
        json.put("banksOwnedChequeId","10056235");
        json.put("transactionDateId",yesterdayDate);
        json.put("transactionAmount",jsonPart);
        String jsonFinal = json.toJSONString().replaceAll("\\\\", "");
        StringBuffer a = new StringBuffer(jsonFinal);
        StringBuffer b = a.deleteCharAt(21);
        StringBuffer c = b.deleteCharAt(61);
        String payload = c.toString();
        Response response = bocObject.createBankOwnCheque(token, payload, bocAccount);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void addBankOwnChequeWithDebit() throws IOException{

        Map<String, Object> part = new HashMap<String, Object>();
        part.put("creditAmountId", 0);
        part.put("debitAmountId", 550);
        JSONObject json2 = new JSONObject();
        json2.putAll(part);
        String jsonPart = json2.toString();
        JSONObject json = new JSONObject();
        json.put("banksOwnedChequeId","10056235");
        json.put("transactionDateId",todayDate);
        json.put("transactionAmount",jsonPart);
        String jsonFinal = json.toJSONString().replaceAll("\\\\", "");
        System.out.println(jsonFinal);
        StringBuffer a = new StringBuffer(jsonFinal);
        StringBuffer b = a.deleteCharAt(21);
        StringBuffer c = b.deleteCharAt(61);
        String payload = c.toString();
        Response response = bocObject.createBankOwnCheque(token, payload, bocAccount);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void addBankOwnChequeWithBothCreditAndDebit() throws IOException{

        Map<String, Object> part = new HashMap<String, Object>();
        part.put("creditAmountId", 600);
        part.put("debitAmountId", 400);
        JSONObject json2 = new JSONObject();
        json2.putAll(part);
        String jsonPart = json2.toString();
        JSONObject json = new JSONObject();
        json.put("banksOwnedChequeId","10056235");
        json.put("transactionDateId",todayDate);
        json.put("transactionAmount",jsonPart);
        String jsonFinal = json.toJSONString().replaceAll("\\\\", "");
        System.out.println(jsonFinal);
        StringBuffer a = new StringBuffer(jsonFinal);
        StringBuffer b = a.deleteCharAt(21);
        StringBuffer c = b.deleteCharAt(63);
        String payload = c.toString();
        System.out.println(payload);
        Response response = bocObject.createBankOwnCheque(token, payload, bocAccount);
        assertEquals(200,response.getStatusCode());
        assertEquals("R13",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void addBankOwnChequeWithInvalidCredit() throws IOException{

        Map<String, Object> part = new HashMap<String, Object>();
        part.put("creditAmountId", -550);
        part.put("debitAmountId", 0);
        JSONObject json2 = new JSONObject();
        json2.putAll(part);
        String jsonPart = json2.toString();
        JSONObject json = new JSONObject();
        json.put("banksOwnedChequeId","10056235");
        json.put("transactionDateId",yesterdayDate);
        json.put("transactionAmount",jsonPart);
        String jsonFinal = json.toJSONString().replaceAll("\\\\", "");
        System.out.println(jsonFinal);
        StringBuffer a = new StringBuffer(jsonFinal);
        StringBuffer b = a.deleteCharAt(21);
        StringBuffer c = b.deleteCharAt(62);
        String payload = c.toString();
        System.out.println(payload);
        Response response = bocObject.createBankOwnCheque(token, payload, bocAccount);
        assertEquals(200,response.getStatusCode());
        assertEquals("R07",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void addBankOwnChequeWithInvalidDebit() throws IOException{

        Map<String, Object> part = new HashMap<String, Object>();
        part.put("creditAmountId", 0);
        part.put("debitAmountId", -550);
        JSONObject json2 = new JSONObject();
        json2.putAll(part);
        String jsonPart = json2.toString();
        JSONObject json = new JSONObject();
        json.put("banksOwnedChequeId","10056235");
        json.put("transactionDateId",todayDate);
        json.put("transactionAmount",jsonPart);
        String jsonFinal = json.toJSONString().replaceAll("\\\\", "");
        System.out.println(jsonFinal);
        StringBuffer a = new StringBuffer(jsonFinal);
        StringBuffer b = a.deleteCharAt(21);
        StringBuffer c = b.deleteCharAt(62);
        String payload = c.toString();
        System.out.println(payload);
        Response response = bocObject.createBankOwnCheque(token, payload, bocAccount);
        assertEquals(200,response.getStatusCode());
        assertEquals("R08",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void addBankOwnChequeWithFutureDate() throws IOException{

        Map<String, Object> part = new HashMap<String, Object>();
        part.put("creditAmountId", 550);
        part.put("debitAmountId", 0);
        JSONObject json2 = new JSONObject();
        json2.putAll(part);
        String jsonPart = json2.toString();
        JSONObject json = new JSONObject();
        json.put("banksOwnedChequeId","10056235");
        json.put("transactionDateId",tomorrowDate);
        json.put("transactionAmount",jsonPart);
        String jsonFinal = json.toJSONString().replaceAll("\\\\", "");
        StringBuffer a = new StringBuffer(jsonFinal);
        StringBuffer b = a.deleteCharAt(21);
        StringBuffer c = b.deleteCharAt(61);
        String payload = c.toString();
        Response response = bocObject.createBankOwnCheque(token, payload, bocAccount);
        assertEquals(200,response.getStatusCode());
        assertEquals("R12",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @After
    public void tearDown() {

        bocObject = null;
        //wireMockServer.stop();
    }

}

package boc;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnquireBankOwnChequeTest {

    private BankOwnCheque bocObject;
    private Logger logger = LogManager.getLogger(EnquireBankOwnChequeTest.class);
    File file = new File("src\\main\\resources\\Data\\auth.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    String [] bocAccounts = {"1548991104","15489911","0154899114"};
    String [] chequeIds = {" ", "10056230", "14", "abc"};

    public EnquireBankOwnChequeTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            bocObject = new BankOwnCheque();
            /*wireMockServer = new WireMockServer(8090);
            setupStub();
            wireMockServer.start();*/
        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void searchForChequesLinkedToBranchAccountNumber() throws ApiException {
        Response response = bocObject.retrieveListOfBOC(token, bocAccounts[0], chequeIds[0]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void searchForSpecificChequeLinkedToBranchAccountNumber() throws ApiException {
        Response response = bocObject.retrieveListOfBOC(token, bocAccounts[0], chequeIds[1]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R00",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void searchForChequeNotLinkedToBranchAccountNumber() throws ApiException {
        Response response = bocObject.retrieveListOfBOC(token, bocAccounts[0], chequeIds[2]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R04",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void searchForChequeWithInvalidBranchAccountNumber() throws ApiException {
        Response response = bocObject.retrieveListOfBOC(token, bocAccounts[1], chequeIds[1]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R01",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void searchForChequeWithInvalidChequeNumber() throws ApiException {
        Response response = bocObject.retrieveListOfBOC(token, bocAccounts[0], chequeIds[3]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R02",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }


    @After
    public void tearDown() {

        bocObject = null;
        //wireMockServer.stop();
    }
}

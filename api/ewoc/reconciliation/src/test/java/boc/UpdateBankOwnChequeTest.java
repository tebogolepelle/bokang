package boc;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UpdateBankOwnChequeTest {

    private BankOwnCheque bocObject;
    private Logger logger = LogManager.getLogger(EnquireBankOwnChequeTest.class);
    File file = new File("src\\main\\resources\\Data\\auth.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    private File jsonFile1 = new File("src\\main\\resources\\requests_body\\UpdateRequest1.json");
    private File jsonFile2 = new File("src\\main\\resources\\requests_body\\UpdateRequest2.json");
    private File jsonFile3 = new File("src\\main\\resources\\requests_body\\UpdateRequest3.json");
    private File jsonFile4 = new File("src\\main\\resources\\requests_body\\UpdateRequest4.json");
    private String bocAccount = "1548991104";
    String [] chequeIds = {" ", "10056230", "14", "abc"};

    public UpdateBankOwnChequeTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            bocObject = new BankOwnCheque();
            /*wireMockServer = new WireMockServer(8090);
            setupStub();
            wireMockServer.start();*/
        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void updateChequeWithInvalidOldChequeNumber() throws IOException {
        Response response = bocObject.updateBankOwnCheque(token, jsonFile1, bocAccount, chequeIds[3]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R05",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void updateChequeWithInvalidNewChequeNumber() throws IOException {
        Response response = bocObject.updateBankOwnCheque(token, jsonFile2, bocAccount, chequeIds[1]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R06",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    /*@Test
    public void updateChequeWithInvalidCreditAmount() throws IOException {
        Response response = bocObject.updateBankOwnCheque(token, jsonFile3, bocAccount, chequeIds[1]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R07",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }

    @Test
    public void updateChequeWithInvalidDebitAmount() throws IOException {
        Response response = bocObject.updateBankOwnCheque(token, jsonFile4, bocAccount, chequeIds[1]);
        assertEquals(200,response.getStatusCode());
        assertEquals("R08",response.getBody().jsonPath().getString("codesDefined.resultsSet.resultCode"));
    }*/


    @After
    public void tearDown() {

        bocObject = null;
        //wireMockServer.stop();
    }

}

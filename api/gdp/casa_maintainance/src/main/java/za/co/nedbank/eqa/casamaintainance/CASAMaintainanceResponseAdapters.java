package za.co.nedbank.eqa.casamaintainance;

import io.restassured.response.ResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.reporter.server.kibana.model.AuditLog;

public class CASAMaintainanceResponseAdapters {


    private static CASAMaintainanceResponseAdapters ourInstance = new CASAMaintainanceResponseAdapters();
    private ResponseBody body = null;
    private static final Logger logger = LogManager.getLogger(CASAMaintainanceResponseAdapters.class);

    public static CASAMaintainanceResponseAdapters getInstance() {
        return ourInstance;
    }


    public CASAMaintainanceResponseAdapters() {
        RestUtil restUtil = new RestUtil();
        JsonUtil jsonUtil = new JsonUtil();
    }

    /**
     * Description: This method is to verify only 200 response
     * @param statuscode - status code to verify whether API is up and Running or not
     */

}
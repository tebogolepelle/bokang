package za.co.nedbank.eqa.casamaintainance.pojosoap.FaultResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Fault", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class FaultResponse {
    @XmlElement(name = "faultcode")
    String faultCode;

    @XmlElement(name = "faultstring")
    String faultString;

    public String getFaultCode() {
        return faultCode;
    }
    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultString() {
        return faultString;
    }
    public void setFaultString(String faultString) {
        this.faultString = faultString;
    }

    @Override
    public String toString() {
        return "FaultResponse{" +
                "faultcode='" + faultCode + '\'' +
                ", faultstring='" + faultString + '\'' +
                '}';
    }
}

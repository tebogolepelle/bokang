package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.body;

import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.MaintainAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "MaintainAccountDetails",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public MaintainAccountDetails maintainAccountDetails;

    public MaintainAccountDetails getMaintainAccountDetails() {
        return maintainAccountDetails;
    }

    public void setMaintainAccountDetails(MaintainAccountDetails maintainAccountDetails) {
        this.maintainAccountDetails = maintainAccountDetails;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "relationshipType")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccToBranchRelationshipType implements Serializable {
    @XmlElement(name = "description", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccToBranchRelationshipTypeDescription;

    public String getAccToBranchRelationshipTypeDescription() {
        return AccToBranchRelationshipTypeDescription;
    }

    public void setAccToBranchRelationshipTypeDescription(String accToBranchRelationshipTypeDescription) {
        AccToBranchRelationshipTypeDescription = accToBranchRelationshipTypeDescription;
    }
}

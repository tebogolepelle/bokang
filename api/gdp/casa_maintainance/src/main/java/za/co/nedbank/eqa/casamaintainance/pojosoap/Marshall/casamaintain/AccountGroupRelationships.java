package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "accountGroupRelationships", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountGroupRelationships implements Serializable {
    @XmlElement(name = "relationshipType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public RelationshipType relationshipType;

    public RelationshipType getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(RelationshipType relationshipType) {
        this.relationshipType = relationshipType;
    }
}

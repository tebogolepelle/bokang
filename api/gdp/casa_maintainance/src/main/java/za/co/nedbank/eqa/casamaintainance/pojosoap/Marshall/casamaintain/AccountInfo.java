package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "accountInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountInfo implements Serializable {
    @XmlElement(name = "transactionalAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TransactionalAccount transactionalAccount;

    public TransactionalAccount getTransactionalAccount() {
        return transactionalAccount;
    }

    public void setTransactionalAccount(TransactionalAccount transactionalAccount) {
        this.transactionalAccount = transactionalAccount;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "accountRelationships", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountRelationships implements Serializable {
    @XmlElement(name = "accountToBranchRelationships",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public List<AccountToBranchRelationships> accountToBranchRelationships;

    @XmlElement(name = "accountGroupRelationships",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountGroupRelationships accountGroupRelationships;

    public List<AccountToBranchRelationships> getAccountToBranchRelationships() {
        return accountToBranchRelationships;
    }
    public void setAccountToBranchRelationships(List<AccountToBranchRelationships> accountToBranchRelationships) {
        this.accountToBranchRelationships = accountToBranchRelationships;
    }

    public AccountGroupRelationships getAccountGroupRelationships() {
        return accountGroupRelationships;
    }
    public void setAccountGroupRelationships(AccountGroupRelationships accountGroupRelationships) {
        this.accountGroupRelationships = accountGroupRelationships;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "accountToBranchRelationships", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountToBranchRelationships implements Serializable {
    @XmlElement(name = "relationshipType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccToBranchRelationshipType accToBranchRelationshipType;

    @XmlElement(name = "branchCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String BranchCode;

    @XmlElement(name = "relationshipStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String RelationshipStatus;

    public AccToBranchRelationshipType getAccToBranchRelationshipType() {
        return accToBranchRelationshipType;
    }

    public void setAccToBranchRelationshipType(AccToBranchRelationshipType accToBranchRelationshipType) {
        this.accToBranchRelationshipType = accToBranchRelationshipType;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public String getRelationshipStatus() {
        return RelationshipStatus;
    }

    public void setRelationshipStatus(String relationshipStatus) {
        RelationshipStatus = relationshipStatus;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "chunkRequestHdr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChunkRequestHdr implements Serializable {

    @XmlElement(name = "continuationValue",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ContinuationValue;

    @XmlElement(name = "maxRecords",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String MaxRecords;

    public String getContinuationValue() {
        return ContinuationValue;
    }

    public void setContinuationValue(String continuationValue) {
        ContinuationValue = continuationValue;
    }

    public String getMaxRecords() {
        return MaxRecords;
    }

    public void setMaxRecords(String maxRecords) {
        MaxRecords = maxRecords;
    }
}

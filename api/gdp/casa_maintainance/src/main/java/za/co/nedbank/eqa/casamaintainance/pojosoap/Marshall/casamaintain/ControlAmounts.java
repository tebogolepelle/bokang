package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "controlAmounts")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"controlConditionType", "controlAmount", "controlCodes"})

public class ControlAmounts implements Serializable {
    @XmlElement(name = "controlCodes",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ControlCodes controlCodes;

    @XmlElement(name = "controlConditionType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ControlConditionType controlConditionType;

    @XmlElement(name = "controlAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ControlAmount controlAmount;



    public ControlCodes getControlCodes() {
        return controlCodes;
    }
    public void setControlCodes(ControlCodes controlCodes) {
        this.controlCodes = controlCodes;
    }

    public ControlAmount getControlAmount() {
        return controlAmount;
    }
    public void setControlAmount(ControlAmount controlAmount) {
        this.controlAmount = controlAmount;
    }

    public ControlConditionType getControlConditionType() {
        return controlConditionType;
    }
    public void setControlConditionType(ControlConditionType controlConditionType) {
        this.controlConditionType = controlConditionType;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "controlCodes")
@XmlAccessorType(XmlAccessType.FIELD)
public class ControlCodes implements Serializable {
    @XmlElement(name = "controlConditionType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ControlConditionType controlConditionType;

    @XmlElement(name = "controlCode",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ControlCode controlCode;

    public ControlConditionType getControlConditionType() {
        return controlConditionType;
    }

    public void setControlConditionType(ControlConditionType controlConditionType) {
        this.controlConditionType = controlConditionType;
    }

    public ControlCode getControlCode() {
        return controlCode;
    }

    public void setControlCode(ControlCode controlCode) {
        this.controlCode = controlCode;
    }
}

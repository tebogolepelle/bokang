package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "controlConditionType")
@XmlAccessorType(XmlAccessType.FIELD)
public class ControlConditionType implements Serializable {
    @XmlElement(name = "description",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String Description;

    public ControlConditionType() {
    }

    public ControlConditionType(String description) {
        Description = description;
    }

    public String getDescription() {
        return Description;
    }
    public void setDescription(String description) {
        Description = description;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "controlConditions")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"controlAmounts", "controlCodes", "controlIndicators"})

public class ControlConditions implements Serializable {
    @XmlElement(name = "controlCodes",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public List<ControlCodes> controlCodes;

    @XmlElement(name = "controlAmounts",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public List<ControlAmounts> controlAmounts;

    @XmlElement(name = "controlIndicators",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public List<ControlIndicators> controlIndicators;

    public List<ControlCodes> getControlCodes() {
        return controlCodes;
    }
    public void setControlCodes(List<ControlCodes> controlCodes) {
        this.controlCodes = controlCodes;
    }

    public List<ControlAmounts> getControlAmounts() {
        return controlAmounts;
    }
    public void setControlAmounts(List<ControlAmounts> controlAmounts) {
        this.controlAmounts = controlAmounts;
    }

    public List<ControlIndicators> getControlIndicators() {
        return controlIndicators;
    }

    public void setControlIndicators(List<ControlIndicators> controlIndicators) {
        this.controlIndicators = controlIndicators;
    }
}

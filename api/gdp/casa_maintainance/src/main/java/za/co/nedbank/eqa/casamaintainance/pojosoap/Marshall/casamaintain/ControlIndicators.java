package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.ControlConditionType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "controlIndicators")
@XmlAccessorType(XmlAccessType.FIELD)
public class ControlIndicators implements Serializable {
    @XmlElement(name = "controlConditionType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ControlConditionType controlConditionType;

    @XmlElement(name = "controlInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ControlInd;

    public ControlConditionType getControlConditionType() {
        return controlConditionType;
    }
    public void setControlConditionType(ControlConditionType controlConditionType) {
        this.controlConditionType = controlConditionType;
    }

    public String getControlInd() {
        return ControlInd;
    }
    public void setControlInd(String controlInd) {
        ControlInd = controlInd;
    }
}

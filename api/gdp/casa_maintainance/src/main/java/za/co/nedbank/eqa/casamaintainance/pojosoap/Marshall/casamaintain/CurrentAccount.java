package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "currentAccount")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrentAccount implements Serializable {
    @XmlElement(name = "attorneyTrustInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AttorneyTrustInd;

    @XmlElement(name = "encInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String EncInd;

    @XmlElement(name = "liquidityInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String LiquidityInd;

    @XmlElement(name = "reserveBankCode",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ReserveBankCode reserveBankCode;

    @XmlElement(name = "sbsPortfolioInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String SbsPortfolioInd;

    @XmlElement(name = "supressCreditInterestInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String SupressCreditInterestInd;

    public String getAttorneyTrustInd() {
        return AttorneyTrustInd;
    }
    public void setAttorneyTrustInd(String attorneyTrustInd) {
        AttorneyTrustInd = attorneyTrustInd;
    }

    public String getEncInd() {
        return EncInd;
    }
    public void setEncInd(String encInd) {
        EncInd = encInd;
    }

    public String getLiquidityInd() {
        return LiquidityInd;
    }
    public void setLiquidityInd(String liquidityInd) {
        LiquidityInd = liquidityInd;
    }

    public ReserveBankCode getReserveBankCode() {
        return reserveBankCode;
    }
    public void setReserveBankCode(ReserveBankCode reserveBankCode) {
        this.reserveBankCode = reserveBankCode;
    }

    public String getSbsPortfolioInd() {
        return SbsPortfolioInd;
    }
    public void setSbsPortfolioInd(String sbsPortfolioInd) {
        SbsPortfolioInd = sbsPortfolioInd;
    }

    public String getSupressCreditInterestInd() {
        return SupressCreditInterestInd;
    }
    public void setSupressCreditInterestInd(String supressCreditInterestInd) {
        SupressCreditInterestInd = supressCreditInterestInd;
    }
}

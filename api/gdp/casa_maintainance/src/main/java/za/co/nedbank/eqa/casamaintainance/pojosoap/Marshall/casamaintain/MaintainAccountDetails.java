package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "MaintainAccountDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaintainAccountDetails implements Serializable {
    @XmlElement(name = "productInfo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ProductInfo productInfo;

    @XmlElement(name = "personIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PersonIdentification personIdentification;

    @XmlElement(name = "accountInfo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountInfo accountInfo;

    @XmlElement(name = "controlConditions",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ControlConditions controlConditions;

    @XmlElement(name = "preferences",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Preferences preferences;

    @XmlElement(name = "accountRelationships",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountRelationships accountRelationships;

    @XmlElement(name = "accountDetails",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountDetails accountDetails;


    public ProductInfo getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public PersonIdentification getPersonIdentification() {
        return personIdentification;
    }

    public void setPersonIdentification(PersonIdentification personIdentification) {
        this.personIdentification = personIdentification;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo){
        this.accountInfo = accountInfo;
    }

    public ControlConditions getControlConditions() {
        return controlConditions;
    }

    public void setControlConditions(ControlConditions controlConditions){
        this.controlConditions = controlConditions;
    }

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(AccountDetails accountDetails){
        this.accountDetails = accountDetails;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public void setPreferences(Preferences preferences){
        this.preferences = preferences;
    }

    public AccountRelationships getAccountRelationships() {
        return accountRelationships;
    }

    public void setAccountRelationships(AccountRelationships accountRelationships){
        this.accountRelationships = accountRelationships;
    }
}

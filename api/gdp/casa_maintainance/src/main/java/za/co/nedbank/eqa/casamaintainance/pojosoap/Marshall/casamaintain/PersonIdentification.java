package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "personIdentification", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonIdentification implements Serializable {
    @XmlElement(name = "personIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String PersonIdentifier;

    @XmlElement(name = "personIDType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PersonIDType personIDType;

    public String getPersonIdentifier() {
        return PersonIdentifier;
    }
    public void setPersonIdentifier(String personIdentifier) {
        PersonIdentifier = personIdentifier;
    }

    public PersonIDType getPersonIDType() {
        return personIDType;
    }

    public void setPersonIDType(PersonIDType personIDType) {
        this.personIDType = personIDType;
    }
}

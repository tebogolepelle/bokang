package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "preferences")
@XmlAccessorType(XmlAccessType.FIELD)
public class Preferences implements Serializable {
    @XmlElement(name = "statementPreferences",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public StatementPreferences statementPreferences;

    public StatementPreferences getStatementPreferences() {
        return statementPreferences;
    }

    public void setStatementPreferences(StatementPreferences statementPreferences) {
        this.statementPreferences = statementPreferences;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ProductCategory")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductCategory implements Serializable {
    @XmlElement(name = "productCategoryID",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ProductCategoryID;

    @XmlElement(name = "productCategoryName",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ProductCategoryName;

    @XmlElement(name = "productCategory",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ProdCategory prodCategory;

    public String getProductCategoryID() {
        return ProductCategoryID;
    }
    public void setProductCategoryID(String productCategoryID) {
        ProductCategoryID = productCategoryID;
    }

    public String getProductCategoryName() {
        return ProductCategoryName;
    }
    public void setProductCategoryName(String productCategoryName) {
        ProductCategoryName = productCategoryName;
    }

    public ProdCategory getProdCategory() {
        return prodCategory;
    }
    public void setProdCategory(ProdCategory prodCategory) {
        this.prodCategory = prodCategory;
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "productInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductInfo implements Serializable {
    @XmlElement(name = "productIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ProductIdentification productIdentification;

    @XmlElement(name = "productCategory",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ProductCategory productCategory;

    public ProductIdentification getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(ProductIdentification productIdentification) {
        this.productIdentification = productIdentification;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

}

package za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "transactionalAccount")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionalAccount implements Serializable {
    @XmlElement(name = "accountIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountIdentification accountIdentification;

    @XmlElement(name = "currentAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CurrentAccount currentAccount;

    public AccountIdentification getAccountIdentification() {
        return accountIdentification;
    }
    public void setAccountIdentification(AccountIdentification accountIdentification) {
        this.accountIdentification = accountIdentification;
    }

    public CurrentAccount getCurrentAccount() {
        return currentAccount;
    }
    public void setCurrentAccount(CurrentAccount currentAccount) {
        this.currentAccount = currentAccount;
    }
}

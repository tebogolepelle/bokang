package za.co.nedbank.eqa.casamaintainance.pojosoap.soappayloadgeneration;

import org.json.JSONObject;
import za.co.nedbank.eqa.casamaintainance.RestAssistance;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.CurrentAccount;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.ReserveBankCode;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.Envelope;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.DeliveryMode;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.Frequency;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.Preferences;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.StatementPreferences;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.ControlAmount;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.ControlAmounts;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.ControlIndicators;
import za.co.nedbank.eqa.casamaintainance.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.body.Body;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.casamaintain.*;
import za.co.nedbank.eqa.casamaintainance.pojosoap.Marshall.header.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CasaMaintainPayload {
    RestAssistance restAssistance = new RestAssistance();


    public String casaSavingsManaitain(JSONObject parameters) {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        ProductIdentification productIdentification = new ProductIdentification();
        productIdentification.setProductIdentifier(parameters.get("productIdentifier").toString());
        productIdentification.setproductIDType(parameters.get("productIDType").toString());

        //accountDetails
        AccountType accountType = new AccountType();
        JSONObject json;
        json = parameters.getJSONObject("accountDetails");
        accountType.setCode(json.get("accountTypeCode").toString());
        accountType.setDescription(json.get("accountTypeDescription").toString());

        AccountDetails accountDetails = new AccountDetails();
        accountDetails.setAccountType(accountType);

        ControlConditions controlConditions = new ControlConditions();

        //controlCodes

        List<ControlAmounts> controlAmountsList = new ArrayList<ControlAmounts>();

        json = parameters.getJSONObject("controlAmounts");

        Iterator<String> keys = json.keys();

        while (keys.hasNext()) {
            String key = keys.next();
            ControlAmounts controlAmounts = new ControlAmounts();

            ControlConditionType controlConditionType = new ControlConditionType();
            ControlAmount controlAmount = new ControlAmount();

            controlConditionType.setDescription(key);
            controlAmount.setAmount(json.get(key).toString());

            controlAmounts.setControlConditionType(controlConditionType);
            controlAmounts.setControlAmount(controlAmount);

            controlAmountsList.add(controlAmounts);
        }
        controlConditions.setControlAmounts(controlAmountsList);

        //controlCodes
        List<ControlCodes> controlCodesList = new ArrayList<ControlCodes>();

        json = parameters.getJSONObject("controlCodes");
        keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            ControlCodes controlCodes = new ControlCodes();

            ControlConditionType controlConditionType = new ControlConditionType();
            ControlCode controlCode = new ControlCode();

            controlConditionType.setDescription(key);
            controlCode.setCode(json.get(key).toString());

            controlCodes.setControlCode(controlCode);
            controlCodes.setControlConditionType(controlConditionType);
            controlCodesList.add(controlCodes);
        }

        controlConditions.setControlCodes(controlCodesList);


        //controlIndicators
        List<ControlIndicators> controlIndicatorsList = new ArrayList<ControlIndicators>();

        json = parameters.getJSONObject("controlIndicators");
        keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            ControlIndicators controlIndicators = new ControlIndicators();

            ControlConditionType controlConditionType = new ControlConditionType();

            controlConditionType.setDescription(key);
            controlIndicators.setControlInd((json.get(key).toString()));
            controlIndicators.setControlConditionType(controlConditionType);

            controlIndicatorsList.add(controlIndicators);
        }
        controlConditions.setControlIndicators(controlIndicatorsList);

        //accountInfo
        AccountIdentification accountIdentification = new AccountIdentification();
        accountIdentification.setAccountIdentifier(parameters.get("accountIdentifier").toString());

        CurrentAccount currentAccount = new CurrentAccount();
        currentAccount.setEncInd(parameters.get("currentAccountEncInd").toString());
        currentAccount.setLiquidityInd(parameters.get("currentAccountLiquidityInd").toString());
        ReserveBankCode reserveBankCode = new ReserveBankCode();
        reserveBankCode.setCode(parameters.get("currentAccountReserveBankCode").toString());
        currentAccount.setReserveBankCode(reserveBankCode);
        currentAccount.setSupressCreditInterestInd(parameters.get("currentAccountSupressCreditInterestInd").toString());
        currentAccount.setAttorneyTrustInd(parameters.get("currentAccountAttorneyTrustInd").toString());

        TransactionalAccount transactionalAccount = new TransactionalAccount();
        transactionalAccount.setAccountIdentification(accountIdentification);
        transactionalAccount.setCurrentAccount(currentAccount);

        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setTransactionalAccount(transactionalAccount);

        //personIdentification
        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode(parameters.get("personIDTypeCode").toString());
        personIDType.setDescription(parameters.get("personIDTypeDescription").toString());

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIDType(personIDType);
        personIdentification.setPersonIdentifier(parameters.get("personIdentifier").toString());

        //productCategory
        ProdCategory prodCategory = new ProdCategory();
        prodCategory.setPrdCategoryID(parameters.get("subProductCategoryID").toString());

        ProductCategory productCategory = new ProductCategory();
        productCategory.setProdCategory(prodCategory);
        productCategory.setProductCategoryName(parameters.get("productCategoryName").toString());
        productCategory.setProductCategoryID(parameters.get("productCategoryID").toString());

        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductCategory(productCategory);
        productInfo.setProductIdentification(productIdentification);


        //preferences
        json = parameters.getJSONObject("preferences");

        Preferences preferences = new Preferences();
        StatementPreferences statementPreferences = new StatementPreferences();

        DeliveryMode deliveryMode = new DeliveryMode();
        deliveryMode.setCode(json.get("deliveryMode").toString());
        Frequency frequency = new Frequency();
        frequency.setCode(json.get("frequency").toString());

        statementPreferences.setDeliveryMode(deliveryMode);
        statementPreferences.setFrequency(frequency);
        preferences.setStatementPreferences(statementPreferences);

        //accountRelationships
        json = parameters.getJSONObject("accountRelationships");

        AccountRelationships accountRelationships = new AccountRelationships();
        List<AccountToBranchRelationships> accountToBranchRelationshipsList = new ArrayList<AccountToBranchRelationships>();

        AccountToBranchRelationships accountToBranchRelationships = new AccountToBranchRelationships();

        AccToBranchRelationshipType relationshipType = new AccToBranchRelationshipType();
        relationshipType.setAccToBranchRelationshipTypeDescription("DomicileBranch");
        accountToBranchRelationships.setAccToBranchRelationshipType(relationshipType);
        accountToBranchRelationships.setBranchCode(json.get("DomicileBranch").toString());
        accountToBranchRelationships.setRelationshipStatus("");
        accountToBranchRelationshipsList.add(accountToBranchRelationships);

        relationshipType = new AccToBranchRelationshipType();
        accountToBranchRelationships = new AccountToBranchRelationships();
        relationshipType.setAccToBranchRelationshipTypeDescription("FinancialBranch");
        accountToBranchRelationships.setAccToBranchRelationshipType(relationshipType);
        accountToBranchRelationships.setBranchCode(json.get("FinancialBranch").toString());
        accountToBranchRelationships.setRelationshipStatus("");

        accountToBranchRelationshipsList.add(accountToBranchRelationships);

        accountRelationships.setAccountToBranchRelationships(accountToBranchRelationshipsList);

        AccountGroupRelationships accountGroupRelationships = new AccountGroupRelationships();

        RelationshipType accountGroupRelationshipsRelationshipType = new RelationshipType();
        accountGroupRelationshipsRelationshipType.setDescription("BranchGroup");
        accountGroupRelationshipsRelationshipType.setCode(json.get("BranchGroup").toString());
        accountGroupRelationships.setRelationshipType(accountGroupRelationshipsRelationshipType);

        accountRelationships.setAccountGroupRelationships(accountGroupRelationships);

        MaintainAccountDetails maintainAccountDetails = new MaintainAccountDetails();
        maintainAccountDetails.setProductInfo(productInfo);
        maintainAccountDetails.setPersonIdentification(personIdentification);
        maintainAccountDetails.setAccountInfo(accountInfo);
        maintainAccountDetails.setControlConditions(controlConditions);
        maintainAccountDetails.setAccountDetails(accountDetails);
        maintainAccountDetails.setPreferences(preferences);
        maintainAccountDetails.setAccountRelationships(accountRelationships);

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setMaintainAccountDetails(maintainAccountDetails);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class, envelope);
    }
}

package za.co.nedbank.eqa.casamaintainance.pojosoap.unmarshall.casamaintainresponce;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private MaintainAccountDetailsResponse MaintainAccountDetailsResponse;

    public MaintainAccountDetailsResponse getMaintainAccountDetailsResponse() {
        return MaintainAccountDetailsResponse;
    }
    public void setMaintainAccountDetailsResponse(MaintainAccountDetailsResponse maintainAccountDetailsResponse) {
        this.MaintainAccountDetailsResponse = maintainAccountDetailsResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "MaintainAccountDetailsResponse='" + MaintainAccountDetailsResponse + '\'' +
                '}';
    }
}

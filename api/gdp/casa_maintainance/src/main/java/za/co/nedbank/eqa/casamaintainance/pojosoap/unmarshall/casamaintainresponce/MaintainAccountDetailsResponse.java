package za.co.nedbank.eqa.casamaintainance.pojosoap.unmarshall.casamaintainresponce;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="MaintainAccountDetailsResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaintainAccountDetailsResponse {
    @XmlElement(name = "resultSet", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private ResultSet ResultSet;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    private ResultSet getResultSet() {
        return ResultSet;
    }
    private void setResultSet(ResultSet resultSet) {
        ResultSet = resultSet;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "MaintainAccountDetailsResponse{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", resultSet=" + ResultSet +
                '}';
    }
}

package za.co.nedbank.eqa.casamaintainance.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.Assert;
import za.co.nedbank.eqa.casamaintainance.CASAMaintainanceResponseAdapters;
import za.co.nedbank.eqa.casamaintainance.RestAssistance;
import za.co.nedbank.eqa.casamaintainance.pojosoap.FaultResponse.FaultResponse;
import za.co.nedbank.eqa.casamaintainance.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.casamaintainance.pojosoap.soappayloadgeneration.*;
import za.co.nedbank.eqa.casamaintainance.pojosoap.unmarshall.casamaintainresponce.MaintainAccountDetailsResponse;

public class SoapMethods extends CASAMaintainanceResponseAdapters {
    private static final Logger logger = LogManager.getLogger(SoapMethods.class);
    private RestAssistance restAssistance;
    private String payload = null;
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for request {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    private CasaMaintainPayload casaMaintainPayload;


    public SoapMethods() {
        restAssistance = new RestAssistance();
        casaMaintainPayload = new CasaMaintainPayload();

    }

    public Response CasaSavingsMaintain(JSONObject json) {


        payload = casaMaintainPayload.casaSavingsManaitain(json);

        return sendSoapRequest(payload);
    }


    public Response sendSoapRequest(String payLoad) {

        System.out.println(payLoad);
        String endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, restAssistance.getApiConfig().getOtherElements().get("Casa_Maintain_endpoint"));

        MaintainAccountDetailsResponse maintainAccountDetailsResponse;

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            maintainAccountDetailsResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), MaintainAccountDetailsResponse.class);

            assert maintainAccountDetailsResponse != null;
            maintainAccountDetailsResponse.setHeaders(response.getHeaders());
            maintainAccountDetailsResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public void verify200Response(int statuscode) {
        Assert.assertEquals(200, statuscode);
    }
}
package za.co.nedbank.eqa.casamaintainance.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;

public class SoapResponseAdapters {

private String responsechequeenqvalue = "R00";

    private static final Logger logger = LogManager.getLogger(SoapResponseAdapters.class);

    /**
     * Description: The method is used to retrieve response details of retrieve response mdm request and return response values
     * @param response
     * @return
     * @throws JAXBException
     * @throws SOAPException
     * @throws IOException
     */

//    public String [] validateChequeEnqResponse(Response response) throws JAXBException, SOAPException, IOException {
//
//        String chequeenqresponsevalues[] = new String[0];
//        ChequeEnqRs chequeEnqRs = MarshallHelperClass.Unmarshall(response.getBody().asInputStream(),ChequeEnqRs.class);
//
//        chequeenqresponsevalues[0]= chequeEnqRs.getChequeEnqItem().getResultCode();
//        if (chequeenqresponsevalues[0].equalsIgnoreCase(responsechequeenqvalue)) {
//            chequeenqresponsevalues[1] = chequeEnqRs.getChequeEnqItem().getChequeAccountNumber();
//        }
//        return chequeenqresponsevalues;
//    }
}

package za.co.nedbank.eqa.casa_maintain;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import za.co.nedbank.eqa.casamaintainance.soap.SoapMethods;

public class CasaSavingsMaintainTest {
    Logger logger = LogManager.getLogger(CasaSavingsMaintainTest.class);
    private static final String STATUS_MESSAGE = "API response status {}";


    private Response response = null;

    @Before
    public void setUp() {
        TestUtils.setupStub("depositaccountsmanagement", "CASA_SA_Maintain.xml");
    }

    @Test
    public void validateSoapTests() {
        SoapMethods soapMethods = new SoapMethods();

        /**
         * Description : The below methods are used to validate Casa Maintain details based on the on text provided and validates the response returned
         */

        JSONObject json = new JSONObject();
        json.put("productIdentifier", "1127");
        json.put("accountDetailsCode", "SA");
        json.put("accountDetailsDescription", "SavingsAccount");

        json.put("productIDType", "Channel Product Catelogue");
        json.put("productCategoryID", "06");
        json.put("subProductCategoryID", "0");
        json.put("productCategoryName", "TestName as Mandatory");
        json.put("personIdentifier", "12345668");
        json.put("personIDTypeCode", "1041");
        json.put("personIDTypeDescription", "ENTERPRISE PARTY NUMBER");
        json.put("accountIdentifier", "2010119037");
        json.put("currentAccountAttorneyTrustInd", "2");
        json.put("currentAccountEncInd", "1");
        json.put("currentAccountLiquidityInd", "2");
        json.put("currentAccountReserveBankCode", "15");
        json.put("currentAccountSupressCreditInterestInd", "Y");

        JSONObject controlAmountsJson = new JSONObject();
        controlAmountsJson.put("ATMOnlinelimit", "2000");
        controlAmountsJson.put("ATMOfflinelimit", "200");
        json.put("controlAmounts", controlAmountsJson);


        JSONObject controlCodesJson = new JSONObject();
        controlCodesJson.put("SplitMixedDeposit", "1");
        controlCodesJson.put("NoFundsAlert", "1");
        controlCodesJson.put("VIPRateInd", "1");
        controlCodesJson.put("TellerAlert", "0");
        controlCodesJson.put("RejectDepositInd", "2");
        json.put("controlCodes", controlCodesJson);


        JSONObject controlIndicatorsJson = new JSONObject();
        controlIndicatorsJson.put("BlockDebitOrderInd", "Y");
        controlIndicatorsJson.put("RejectWithdrawalInd", "N");
        json.put("controlIndicators", controlIndicatorsJson);


        JSONObject preferencesJson = new JSONObject();
        preferencesJson.put("deliveryMode", "9");
        preferencesJson.put("frequency", "1100");
        json.put("preferences", preferencesJson);


        JSONObject accountRelationshipsJson = new JSONObject();
        accountRelationshipsJson.put("DomicileBranch", "123");
        accountRelationshipsJson.put("FinancialBranch", "666");
        accountRelationshipsJson.put("BranchGroup", "786");
        json.put("accountRelationships", accountRelationshipsJson);


        JSONObject accountDetailsJson = new JSONObject();
        accountDetailsJson.put("accountTypeCode", "SA");
        accountDetailsJson.put("accountTypeDescription", "SavingsAccount");
        json.put("accountDetails", accountDetailsJson);


        response = soapMethods.CasaSavingsMaintain(json);
        assert response != null;
        if (response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("CASA Saving Maintain successfully executed");
        } else {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("CASA Saving Maintain not successfully executed");
        }

    }

}

package za.co.nedbank.eqa.cheque;

import org.junit.Assert;
import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.reporter.server.kibana.model.AuditLog;

public class ChequeResponseAdapters {
    private static ChequeResponseAdapters ourInstance = new ChequeResponseAdapters();

    public static ChequeResponseAdapters getInstance() {
        return ourInstance;
    }

    protected RestUtil restUtil;
    protected JsonUtil jsonUtil;
    protected AuditLog auditLog;


    public ChequeResponseAdapters() {
        restUtil = new RestUtil();
        jsonUtil = new JsonUtil();
        auditLog = new AuditLog();
    }

    /**
     * Description: This method is to verify only 200 response
     *
     * @param statuscode - status code to verify whether API is up and Running or not
     */

    public void verify200Response(int statuscode) {
        Assert.assertEquals(200, statuscode);
    }
}
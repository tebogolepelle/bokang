package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.chequeenq;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
public class Body {
    @XmlElement(name = "ChequeEnqRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    ArrayList<ChequeEnqRs> ChequeEnqRs;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public ArrayList<ChequeEnqRs> getChequeEnqRs() {
        return ChequeEnqRs;
    }

    private void setChequeEnqRs(ArrayList<ChequeEnqRs> chequeEnqRs) {
        ChequeEnqRs = chequeEnqRs;
    }

    @Override
    public String toString() {
        return "Body{" +
                "ChequeEnqRs=" + ChequeEnqRs +
                ", statusCode=" + statusCode +
                ", ChequeEnqRs=" + ChequeEnqRs +
                '}';
    }

}

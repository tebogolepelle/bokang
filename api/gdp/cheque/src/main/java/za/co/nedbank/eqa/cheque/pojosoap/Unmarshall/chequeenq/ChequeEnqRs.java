package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.chequeenq;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ChequeEnqRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequeEnqRs implements Serializable{
    @XmlElement(name = "ChequeEnqRc", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeEnqRc;

    @XmlElement(name = "ChequeEnqItem", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private ChequeEnqItem chequeEnqItem;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getChequeEnqRc() {
        return ChequeEnqRc;
    }

    public void setChequeEnqRc(String chequeEnqRc) {
        ChequeEnqRc = chequeEnqRc;
    }

    public ChequeEnqItem getChequeEnqItem() {
        return chequeEnqItem;
    }

    public void setChequeEnqItem(ChequeEnqItem chequeEnqItem) {
        this.chequeEnqItem = chequeEnqItem;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ChequeEnqRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", ChequeEnqRc=" + ChequeEnqRc +
                ", ChequeEnqItem=" + chequeEnqItem +
                '}';
    }
}

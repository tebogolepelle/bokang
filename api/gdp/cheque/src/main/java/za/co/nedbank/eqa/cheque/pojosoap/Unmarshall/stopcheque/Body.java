package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopcheque;

import javax.xml.bind.annotation.XmlElement;

public class Body {
    @XmlElement(name = "StopChequeRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    StopChequeRs stopChequeRs;

    public StopChequeRs getStopChequeRs() {
        return stopChequeRs;
    }
    public void setStopChequeRs(StopChequeRs stopChequeRs) {
        this.stopChequeRs = stopChequeRs;
    }
}

package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopcheque;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ChequeItemRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequeItemRs {

    @XmlElement(name = "ResultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ResultCode;

    @XmlElement(name = "ChequeAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeAccountNumber;

    @XmlElement(name = "ChequeReason", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeReason;

    @XmlElement(name = "ChequeDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeDate;

    @XmlElement(name = "ChequeCrossed", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeCrossed;

    public String getResultCode() {
        return ResultCode;
    }

    public void setResultCode(String resultCode) {
        ResultCode = resultCode;
    }

    public String getChequeAccountNumber() {
        return ChequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAccountNumber) {
        ChequeAccountNumber = chequeAccountNumber;
    }

    public String getChequeReason() {
        return ChequeReason;
    }

    public void setChequeReason(String chequeReason) {
        ChequeReason = chequeReason;
    }

    public String getChequeDate() {
        return ChequeDate;
    }

    public void setChequeDate(String chequeDate) {
        ChequeDate = chequeDate;
    }

    public String getChequeCrossed() {
        return ChequeCrossed;
    }

    public void setChequeCrossed(String chequeCrossed) {
        ChequeCrossed = chequeCrossed;
    }

}

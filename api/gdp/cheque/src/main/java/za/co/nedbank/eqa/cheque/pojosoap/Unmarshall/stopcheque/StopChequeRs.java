package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopcheque;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name="StopChequeRs",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)

public class StopChequeRs{
    @XmlElement(name = "StopChequeRc", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
     String StopChequeRc;

    @XmlElement(name = "UserId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
     String UserId;

    @XmlElement(name = "ChequeItemRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
     ChequeItemRs chequeItemRs;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }


    public String getStopChequeRc() {
        return StopChequeRc;
    }
    public void setStopChequeRc(String stopChequeRc) {
        StopChequeRc = stopChequeRc;
    }

    public String getUserId() {
        return UserId;
    }
    public void setUserId(String userId) {
        UserId = userId;
    }

    public ChequeItemRs getChequeItemRs() {
        return chequeItemRs;
    }
    public void setChequeItemRs(ChequeItemRs chequeItemRs) {
        this.chequeItemRs = chequeItemRs;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ChequeEnqRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", StopChequeRc=" + StopChequeRc +
                ", UserId=" + UserId +
                ", ChequeItemRs=" + chequeItemRs +
                '}';
    }
}

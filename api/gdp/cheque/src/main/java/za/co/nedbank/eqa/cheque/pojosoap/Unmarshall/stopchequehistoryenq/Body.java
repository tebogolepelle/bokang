package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequehistoryenq;

import javax.xml.bind.annotation.XmlElement;

public class Body {
    @XmlElement(name = "StopChequeHistoryEnqRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private StopChequeHistoryEnqRs StopChequeHistoryEnqRs;

    private StopChequeHistoryEnqRs getStopChequeHistoryEnqRs() {
        return StopChequeHistoryEnqRs;
    }

    private void setStopChequeHistoryEnqRs(StopChequeHistoryEnqRs stopChequeHistoryEnqRs) {
        StopChequeHistoryEnqRs = stopChequeHistoryEnqRs;
    }
}

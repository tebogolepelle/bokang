package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequehistoryenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RequestorIdentifier")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestorIdentifier {
    @XmlElement(name = "UserId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String UserId;

    public String getUserId() {
        return UserId;
    }
    public void setUserId(String userId) { UserId = userId;
    }
}

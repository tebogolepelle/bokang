package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequehistoryenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "StopChequeHistoryEnqItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeHistoryEnqItem {
    @XmlElement(name = "ChequeAmount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeAmount;

    @XmlElement(name = "ChequeReason", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeReason;

    @XmlElement(name = "ChequeBeneficiary", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeBeneficiary;

    public String getChequeAmount() {
        return ChequeAmount;
    }

    public void setChequeAmount(String chequeAmount) {
        ChequeAmount = chequeAmount;
    }

    public String getChequeReason() {
        return ChequeReason;
    }

    public void setChequeReason(String chequeReason) {
        ChequeReason = chequeReason;
    }

    public String getChequeBeneficiary() {
        return ChequeBeneficiary;
    }

    public void setChequeBeneficiary(String chequeBeneficiary) {
        ChequeBeneficiary = chequeBeneficiary;
    }
}

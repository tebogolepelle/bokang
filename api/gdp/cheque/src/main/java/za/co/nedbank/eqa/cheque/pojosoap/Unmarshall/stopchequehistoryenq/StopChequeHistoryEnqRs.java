package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequehistoryenq;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "StopChequeHistoryEnqRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeHistoryEnqRs implements Serializable{
    @XmlElement(name = "ResultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ResultCode;

    @XmlElement(name = "ChequeAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeAccountNumber;

    @XmlElement(name = "RequestorIdentifier", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private RequestorIdentifier RequestorIdentifier;

    @XmlElement(name = "StopChequeHistoryEnqItem", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private StopChequeHistoryEnqItem StopChequeHistoryEnqItem;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getResultCode() {
        return ResultCode;
    }

    public void setResultCode(String resultCode) {
        ResultCode = resultCode;
    }

    public String getChequeAccountNumber() {
        return ChequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAccountNumber) {
        ChequeAccountNumber = chequeAccountNumber;
    }

    public RequestorIdentifier getRequestorIdentifier() {
        return RequestorIdentifier;
    }

    public void setRequestorIdentifier(RequestorIdentifier requestorIdentifier) {
        RequestorIdentifier = requestorIdentifier;
    }
    public StopChequeHistoryEnqItem getStopChequeHistoryEnqItem() {
        return StopChequeHistoryEnqItem;
    }

    public void seStopChequeHistoryEnqItem(StopChequeHistoryEnqItem stopChequeHistoryEnqItem) {
        StopChequeHistoryEnqItem = stopChequeHistoryEnqItem;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ChequeEnqRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", ResultCode=" + ResultCode +
                ", ChequeAccountNumber=" + ChequeAccountNumber +
                ", RequestorIdentifier=" + RequestorIdentifier +
                ", StopChequeHistoryEnqItem=" + StopChequeHistoryEnqItem +
                '}';
    }
}

package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval;

import javax.xml.bind.annotation.XmlElement;

public class Body {
    @XmlElement(name = "StopChequeValRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval.StopChequeValRs StopChequeValRs;

    private za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval.StopChequeValRs getStopChequeValRs() {
        return StopChequeValRs;
    }

    private void seStopChequeValRs(za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval.StopChequeValRs stopChequeValRs) {
        StopChequeValRs = stopChequeValRs;
    }
}

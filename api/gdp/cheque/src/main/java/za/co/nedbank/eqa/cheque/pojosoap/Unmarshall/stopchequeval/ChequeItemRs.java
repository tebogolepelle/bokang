package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ChequeItemRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequeItemRs {
    @XmlElement(name = "ChequeAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeAccountNumber;

    @XmlElement(name = "ChequeReason", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeReason;

    @XmlElement(name = "ChequeCrossed", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeCrossed;

    public String getChequeAccountNumber() {
        return ChequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAccountNumber) {
        ChequeAccountNumber = chequeAccountNumber;
    }

    public String getChequeReason() {
        return ChequeReason;
    }

    public void setChequeReason(String chequeReason) {
        ChequeReason = chequeReason;
    }

    public String getChequeCrossed() {
        return ChequeCrossed;
    }

    public void setChequeCrossed(String chequeCrossed) {
        ChequeCrossed = chequeCrossed;
    }

}

package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "StopChequeValRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeValRs implements Serializable{
    @XmlElement(name = "StopChequeRc", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String StopChequeRc;

    @XmlElement(name = "ChequeItemRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private ChequeItemRs ChequeItemRs;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStopChequeRc() {
        return StopChequeRc;
    }

    public void setStopChequeRc(String stopChequeRc) {
        StopChequeRc = stopChequeRc;
    }

    public ChequeItemRs getChequeItemRs() {
        return ChequeItemRs;
    }

    public void setChequeItemRs(ChequeItemRs chequeItemRs) {
        ChequeItemRs = chequeItemRs;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ChequeEnqRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", StopChequeRc=" + StopChequeRc +
                ", ChequeItemRs=" + ChequeItemRs +
                '}';
    }
}

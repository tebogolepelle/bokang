package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stophistorychequesrch;

import javax.xml.bind.annotation.XmlElement;

public class Body {
    @XmlElement(name = "StopChequeHistorySrchRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private StopChequeHistorySrchRs StopChequeHistorySrchRs;

    private StopChequeHistorySrchRs getStopChequeHistorySrchRs() {
        return StopChequeHistorySrchRs;
    }

    private void setStopChequeHistorySrchRs(StopChequeHistorySrchRs stopChequeHistorySrchRs) {
        StopChequeHistorySrchRs = stopChequeHistorySrchRs;
    }
}

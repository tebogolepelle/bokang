package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stophistorychequesrch;

public class Envelope {
    private String Header;
    private za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.chequeenq.Body Body;

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.chequeenq.Body getBody() {
        return Body;
    }

    public void setBody(za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.chequeenq.Body body) {
        Body = body;
    }
}

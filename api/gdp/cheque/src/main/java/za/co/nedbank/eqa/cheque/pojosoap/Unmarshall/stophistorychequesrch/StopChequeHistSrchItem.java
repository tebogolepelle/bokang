package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stophistorychequesrch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "StopChequeHistSrchItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeHistSrchItem {

    @XmlElement(name = "ChequeAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ChequeAccountNumber;

    @XmlElement(name = "LogUserId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String LogUserId;

    public String getChequeAccountNumber() {
        return ChequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAccountNumber) {
        ChequeAccountNumber = chequeAccountNumber;
    }

    public String getCLogUserId() {
        return LogUserId;
    }

    public void setLogUserId(String logUserId) {
        LogUserId = logUserId;
    }
}

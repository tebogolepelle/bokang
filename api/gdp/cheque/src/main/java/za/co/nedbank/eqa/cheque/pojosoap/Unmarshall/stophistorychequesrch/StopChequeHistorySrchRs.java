package za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stophistorychequesrch;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "StopChequeHistorySrchRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeHistorySrchRs implements Serializable{
    @XmlElement(name = "ResultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private String ResultCode;

    @XmlElement(name = "StopChequeHistSrchItem", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    private StopChequeHistSrchItem StopChequeHistSrchItem;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }


    public String getResultCode() {
        return ResultCode;
    }

    public void setResultCode(String resultCode) {
        ResultCode = resultCode;
    }

    public StopChequeHistSrchItem getStopChequeHistSrchItem() {
        return StopChequeHistSrchItem;
    }

    public void setStopChequeHistSrchItem(StopChequeHistSrchItem stopChequeHistSrchItem) {
        StopChequeHistSrchItem = stopChequeHistSrchItem;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ChequeEnqRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", ResultCode=" + ResultCode +
                ", StopChequeHistSrchItem=" + StopChequeHistSrchItem +
                '}';
    }
}

package za.co.nedbank.eqa.cheque.pojosoap.marshall.chequeenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "ChequeEnqRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public ChequeEnqRq chequeEnqRq;

    public ChequeEnqRq getChequeEnqRq() {
        return chequeEnqRq;
    }

    public void setChequeEnqRq(ChequeEnqRq chequeEnqRq) {
        this.chequeEnqRq = chequeEnqRq;
    }

}

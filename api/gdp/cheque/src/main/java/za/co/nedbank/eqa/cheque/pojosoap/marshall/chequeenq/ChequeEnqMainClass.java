package za.co.nedbank.eqa.cheque.pojosoap.marshall.chequeenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement(name = "ChequeEnqMainClass")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequeEnqMainClass implements Serializable {
    private Envelope envelope;

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }
}

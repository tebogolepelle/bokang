package za.co.nedbank.eqa.cheque.pojosoap.marshall.chequeenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ChequeAccountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)

public class ChequeEnqRq implements Serializable {

    @XmlElement(name = "ChequeAccountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeAccountNumber;

    @XmlElement(name = "ChequeNumberFrom",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeNumberFrom;

    @XmlElement(name = "ChequeNumberTo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeNumberTo;

    @XmlElement(name = "RequestChannelId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String RequestChannelId;

    public String getChequeAccountNumber() {
        return ChequeAccountNumber;
    }

    public void setChequeAccountNumber(String ChequeAccountNumber) {
        this.ChequeAccountNumber = ChequeAccountNumber;
    }

    public String getChequeNumberFrom() {
        return ChequeNumberFrom;
    }

    public void setChequeNumberFrom(String ChequeNumberFrom) {
        this.ChequeNumberFrom = ChequeNumberFrom;
    }

    public String getChequeNumberTo() {
        return ChequeNumberTo;
    }

    public void setChequeNumberTo(String ChequeNumberTo) {
        this.ChequeNumberTo = ChequeNumberTo;
    }

    public String getRequestChannelId() {
        return RequestChannelId;
    }

    public void setRequestChannelId(String RequestChannelId) {
        this.RequestChannelId = RequestChannelId;
    }
}

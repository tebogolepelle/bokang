package za.co.nedbank.eqa.cheque.pojosoap.marshall.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "InstrumentationInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstrumentationInfo implements Serializable {
    @XmlElement(name = "ParentInstrumentationId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String ParentInstrumentationId;

    @XmlElement(name = "ChildInstrumentationId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String ChildInstrumentationId;

    public String getParentInstrumentationId() {
        return ParentInstrumentationId;
    }

    public void setParentInstrumentationId(String parentInstrumentationId) {
        ParentInstrumentationId = parentInstrumentationId;
    }

    public String getChildInstrumentationId() {
        return ChildInstrumentationId;
    }

    public void setChildInstrumentationId(String childInstrumentationId) {
        ChildInstrumentationId = childInstrumentationId;
    }
}

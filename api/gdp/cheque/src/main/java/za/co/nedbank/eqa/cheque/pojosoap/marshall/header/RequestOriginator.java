package za.co.nedbank.eqa.cheque.pojosoap.marshall.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "RequestOriginator", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestOriginator  implements Serializable {
    @XmlElement(name = "MachineIPAddress",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String MachineIPAddress;
    @XmlElement(name = "UserPrincipleName",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String UserPrincipleName;
    @XmlElement(name = "MachineDNSName",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String MachineDNSName;
    @XmlElement(name = "ChannelId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String ChannelId;

    public String getMachineIPAddress() {
        return MachineIPAddress;
    }

    public void setMachineIPAddress(String machineIPAddress) {
        MachineIPAddress = machineIPAddress;
    }

    public String getUserPrincipleName() {
        return UserPrincipleName;
    }

    public void setUserPrincipleName(String userPrincipleName) {
        UserPrincipleName = userPrincipleName;
    }

    public String getMachineDNSName() {
        return MachineDNSName;
    }

    public void setMachineDNSName(String machineDNSName) {
        MachineDNSName = machineDNSName;
    }

    public String getChannelId() {
        return ChannelId;
    }

    public void setChannelId(String channelId) {
        ChannelId = channelId;
    }
}

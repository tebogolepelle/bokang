package za.co.nedbank.eqa.cheque.pojosoap.marshall.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "UsernameToken")
@XmlAccessorType(XmlAccessType.FIELD)
public class UsernameToken implements Serializable {
    @XmlElement(name = "Username",namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    public String Username;

//    @XmlElement(name = "Password", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
//    public String Password;

//    @XmlElement(name = "Nonce",namespace = "http://www.ibm.com/KD4Soap")
    public String Nonce	;


    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

//    public String getPassword() {
//        return Password;
//    }
//
//    public void setPassword(String password) {
//        Password = password;
//    }

//    public String getNonce() {
//        return Nonce;
//    }
//
//    public void setNonce(String nonce) {
//        Nonce = nonce;
//    }

}

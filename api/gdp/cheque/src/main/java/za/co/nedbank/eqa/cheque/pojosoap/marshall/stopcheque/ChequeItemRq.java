package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopcheque;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ChequeAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequeItemRq implements Serializable {

    @XmlElement(name = "ChequeAccountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeAccountNumber;
    @XmlElement(name = "ChequeNumberFrom",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeNumberFrom;
    @XmlElement(name = "ChequeNumberTo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeNumberTo;
    @XmlElement(name = "ChequeAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeAmount;
    @XmlElement(name = "ChequeBeneficiary",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeBeneficiary;
    @XmlElement(name = "ChequeReason",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeReason;
    @XmlElement(name = "ChequeDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeDate;
    @XmlElement(name = "ChequeCrossed",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ChequeCrossed;

    public String getChequeAccountNumber() {
        return ChequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAccountNumber) {
        ChequeAccountNumber = chequeAccountNumber;
    }
    public String getChequeNumberFrom() {
        return ChequeNumberFrom;
    }

    public void setChequeNumberFrom(String chequeNumberFrom) {
        ChequeNumberFrom = chequeNumberFrom;
    }
    public String getChequeNumberTo() {
        return ChequeNumberTo;
    }

    public void setChequeNumberTo(String chequeNumberTo) {
        ChequeNumberTo = chequeNumberTo;
    }
    public String getChequeAmount() {
        return ChequeAmount;
    }

    public void setChequeAmount(String chequeAmount) {
        ChequeAmount = chequeAmount;
    }
    public String getChequeBeneficiary() {
        return ChequeBeneficiary;
    }

    public void setChequeBeneficiary(String chequeBeneficiary) {
        ChequeBeneficiary = chequeBeneficiary;
    }
    public String getChequeReason() {
        return ChequeReason;
    }

    public void setChequeReason(String chequeReason) {
        ChequeReason = chequeReason;
    }
    public String getChequeDate() {
        return ChequeDate;
    }

    public void setChequeDate(String chequeDate) {
        ChequeDate = chequeDate;
    }
    public String getChequeCrossed() {
        return ChequeCrossed;
    }

    public void setChequeCrossed(String chequeCrossed) {
        ChequeCrossed = chequeCrossed;
    }
}

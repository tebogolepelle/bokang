package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopcheque;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement(name = "ProfileNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestorIdentifier implements Serializable {

    @XmlElement(name = "ProfileNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String ProfileNumber;
    @XmlElement(name = "UserId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String UserId;

    public String getProfileNumber() {
        return ProfileNumber;
    }

    public void setProfileNumber(String profileNumber) {
        ProfileNumber = profileNumber;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }
}

package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopcheque;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "StopChequeRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeRq implements Serializable{

    @XmlElement(name = "RequestorIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public RequestorIdentifier requestorIdentifier;

    @XmlElement(name = "RequestChannelId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String RequestChannelId;

    @XmlElement(name = "ChequeItemRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public ChequeItemRq chequeItemRq;


    public String getRequestChannelId() {
        return RequestChannelId;
    }

    public void setRequestChannelId(String requestChannelId) {
        RequestChannelId = requestChannelId;
    }

    public RequestorIdentifier getRequestorIdentifier() {
        return requestorIdentifier;
    }

    public void setRequestorIdentifier(RequestorIdentifier requestorIdentifier) {
        this.requestorIdentifier = requestorIdentifier;
    }
}

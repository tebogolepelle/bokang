package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequehistoryenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "StopChequeHistoryEnqRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeHistoryEnqRq implements Serializable {

    @XmlElement(name = "ChequeAccountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeAccountNumber;

    @XmlElement(name = "RequestorIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public RequestorIdentifier requestorIdentifier;

    @XmlElement(name = "RequestChannelId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String requestChannelId;

    public String getChequeAccountNumber() {
        return chequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAccountNumber) {
        this.chequeAccountNumber = chequeAccountNumber;
    }

    public RequestorIdentifier getRequestorIdentifier() {
        return requestorIdentifier;
    }

    public void setRequestorIdentifier(RequestorIdentifier requestorIdentifier) {
        this.requestorIdentifier = requestorIdentifier;
    }

    public String getRequestChannelId() {
        return requestChannelId;
    }

    public void setRequestChannelId(String requestChannelId) {
        this.requestChannelId = requestChannelId;
    }

}

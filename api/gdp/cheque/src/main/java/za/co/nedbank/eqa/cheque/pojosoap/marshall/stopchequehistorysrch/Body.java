package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequehistorysrch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {

    @XmlElement(name = "StopChequeHistorySrchRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public StopChequeHistorySrchRq stopChequeHistorySrchRq;

    public StopChequeHistorySrchRq getStopChequeHistorySrchRq() {
        return stopChequeHistorySrchRq;
    }

    public void setStopChequeHistorySrchRq(StopChequeHistorySrchRq stopChequeHistorySrchRq) {
        this.stopChequeHistorySrchRq = stopChequeHistorySrchRq;
    }
}

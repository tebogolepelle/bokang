package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequehistorysrch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ChequeAmountFrom",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeHistorySrchPar implements Serializable {

    @XmlElement(name = "ChequeAmountFrom",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeAmountFrom;

    @XmlElement(name = "ChequeAmountTo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeAmountTo;

    @XmlElement(name = "ChequeNumberFrom",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeNumberFrom;

    @XmlElement(name = "ChequeNumberTo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeNumberTo;

    @XmlElement(name = "DateStoppedFrom",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String dateStoppedFrom;

    @XmlElement(name = "DateStoppedTo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String dateStoppedTo;

    public String getChequeAmountFrom() {
        return chequeAmountFrom;
    }

    public void setChequeAmountFrom(String chequeAmountFrom) {
        this.chequeAmountFrom = chequeAmountFrom;
    }

    public String getChequeAmountTo() {
        return chequeAmountTo;
    }

    public void setChequeAmountTo(String chequeAmountTo) {
        this.chequeAmountTo = chequeAmountTo;
    }

    public String getChequeNumberFrom() {
        return chequeNumberFrom;
    }

    public void setChequeNumberFrom(String chequeNumberFrom) {
        this.chequeNumberFrom = chequeNumberFrom;
    }

    public String getChequeNumberTo() {
        return chequeNumberTo;
    }

    public void setChequeNumberTo(String chequeNumberTo) {
        this.chequeNumberTo = chequeNumberTo;
    }

    public String getDateStoppedFrom() {
        return dateStoppedFrom;
    }

    public void setDateStoppedFrom(String dateStoppedFrom) {
        this.dateStoppedFrom = dateStoppedFrom;
    }

    public String getDateStoppedTo() {
        return dateStoppedTo;
    }

    public void setDateStoppedTo(String dateStoppedTo) {
        this.dateStoppedTo = dateStoppedTo;
    }

}

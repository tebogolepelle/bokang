package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequehistorysrch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "StopChequeHistorySrchRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeHistorySrchRq implements Serializable {

    @XmlElement(name = "ChequeAccountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeAccountNumber;

    @XmlElement(name = "RequestorIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public RequestorIdentifier requestorIdentifier;

    @XmlElement(name = "StopChequeHistorySrchPar",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public StopChequeHistorySrchPar stopChequeHistorySrchPar;

    @XmlElement(name = "RequestChannelId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String requestChannelId;

    public String getChequeAccountNumber() {
        return chequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAccountNumber) {
        this.chequeAccountNumber = chequeAccountNumber;
    }

    public RequestorIdentifier getRequestorIdentifier() {
        return requestorIdentifier;
    }

    public void setRequestorIdentifier(RequestorIdentifier requestorIdentifier) {
        this.requestorIdentifier = requestorIdentifier;
    }

    public StopChequeHistorySrchPar getStopChequeHistorySrchPar() {
        return stopChequeHistorySrchPar;
    }

    public void setStopChequeHistorySrchPar(StopChequeHistorySrchPar stopChequeHistorySrchPar) {
        this.stopChequeHistorySrchPar = stopChequeHistorySrchPar;
    }

    public String getRequestChannelId() {
        return requestChannelId;
    }

    public void setRequestChannelId(String requestChannelId) {
        this.requestChannelId = requestChannelId;
    }


}

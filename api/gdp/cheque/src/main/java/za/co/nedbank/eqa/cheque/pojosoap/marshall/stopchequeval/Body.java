package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequeval;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable{
    @XmlElement(name = "StopChequeValRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public StopChequeValRq stopChequeValRq;

    public StopChequeValRq getStopChequeValRq() {
        return stopChequeValRq;
    }

    public void setStopChequeValRq(StopChequeValRq stopChequeValRq) {
        this.stopChequeValRq = stopChequeValRq;
    }
}

package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequeval;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ChequeAccountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequeItemRq implements Serializable{

    @XmlElement(name = "ChequeAccountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeAccountNumber;

    @XmlElement(name = "ChequeNumberFrom",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeNumberFrom;

    @XmlElement(name = "ChequeNumberTo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeNumberTo;

    @XmlElement(name = "ChequeAmountTo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeAmountTo;

    @XmlElement(name = "ChequeAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeAmount;

    @XmlElement(name = "ChequeBeneficiary",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeBeneficiary;

    @XmlElement(name = "ChequeReason",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeReason;

    @XmlElement(name = "ChequeDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeDate;

    @XmlElement(name = "ChequeCrossed",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String chequeCrossed;

    public String getChequeAccountNumber() {
        return chequeAccountNumber;
    }

    public void setChequeAccountNumber(String chequeAmountFrom) {
        this.chequeAccountNumber = chequeAccountNumber;
    }

    public String getChequeNumberFrom() {
        return chequeNumberFrom;
    }

    public void setChequeNumberFrom(String chequeNumberFrom) {
        this.chequeNumberFrom = chequeNumberFrom;
    }

    public String getChequeNumberTo() {
        return chequeNumberTo;
    }

    public void setChequeNumberTo(String chequeNumberTo) {
        this.chequeNumberTo = chequeNumberTo;
    }

    public String getChequeAmountTo() {
        return chequeAmountTo;
    }

    public void setChequeAmountTo(String chequeAmountTo) {
        this.chequeAmountTo = chequeAmountTo;
    }

    public String getChequeAmount() {
        return chequeAmount;
    }

    public void setChequeAmount(String chequeAmount) {
        this.chequeAmount = chequeAmount;
    }

    public String getChequeBeneficiary() {
        return chequeBeneficiary;
    }

    public void setChequeBeneficiary(String chequeBeneficiary) {
        this.chequeBeneficiary = chequeBeneficiary;
    }

    public String getChequeReason() {
        return chequeReason;
    }

    public void setChequeReason(String chequeReason) {
        this.chequeReason = chequeReason;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getChequeCrossed() {
        return chequeCrossed;
    }

    public void setChequeCrossed(String chequeCrossed) {
        this.chequeCrossed = chequeCrossed;
    }
}

package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequeval;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "StopChequeValRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class StopChequeValRq implements Serializable{

    @XmlElement(name = "RequestorIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public RequestorIdentifier requestorIdentifier;

    @XmlElement(name = "RequestChannelId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public String requestChannelId;

    @XmlElement(name = "ChequeItemRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")
    public ChequeItemRq chequeItemRq;


    public RequestorIdentifier getRequestorIdentifier() {
        return requestorIdentifier;
    }

    public void setRequestorIdentifier(RequestorIdentifier requestorIdentifier) {
        this.requestorIdentifier = requestorIdentifier;
    }

    public ChequeItemRq getChequeItemRq() {
        return chequeItemRq;
    }

    public void setChequeItemRq(ChequeItemRq chequeItemRq) {
        this.chequeItemRq = chequeItemRq;
    }

    public String getRequestChannelId() {
        return requestChannelId;
    }

    public void setRequestChannelId(String requestChannelId) {
        this.requestChannelId = requestChannelId;
    }

}

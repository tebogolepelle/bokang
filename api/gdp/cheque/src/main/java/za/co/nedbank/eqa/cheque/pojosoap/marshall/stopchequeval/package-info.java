@XmlSchema(
        namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3",
        xmlns = {
                @XmlNs(prefix="ent", namespaceURI="http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext"),
                @XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="v3", namespaceURI="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v3")

        },
        elementFormDefault = XmlNsForm.QUALIFIED)
package za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequeval;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

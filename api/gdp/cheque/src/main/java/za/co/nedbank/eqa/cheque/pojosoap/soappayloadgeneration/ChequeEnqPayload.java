package za.co.nedbank.eqa.cheque.pojosoap.soappayloadgeneration;

import za.co.nedbank.eqa.cheque.RestAssistance;
import za.co.nedbank.eqa.cheque.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.chequeenq.Body;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.chequeenq.ChequeEnqRq;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.chequeenq.Envelope;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.header.*;

import javax.xml.bind.JAXBException;

public class ChequeEnqPayload {
    RestAssistance restAssistance = new RestAssistance();

    public String ChequeEnq(String accNumber) {

        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        ChequeEnqRq chequeEnqRq = new ChequeEnqRq();
        chequeEnqRq.setChequeAccountNumber(accNumber);
        chequeEnqRq.setChequeNumberFrom("1");
        chequeEnqRq.setChequeNumberTo("9999");
        chequeEnqRq.setRequestChannelId("B");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("558");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setContextInfo(contextInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();

        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setChequeEnqRq(chequeEnqRq);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class, envelope);
    }
}

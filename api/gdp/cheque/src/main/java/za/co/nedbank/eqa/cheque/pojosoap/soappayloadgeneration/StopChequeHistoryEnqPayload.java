package za.co.nedbank.eqa.cheque.pojosoap.soappayloadgeneration;

import za.co.nedbank.eqa.cheque.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.header.*;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequehistoryenq.*;

import javax.xml.bind.JAXBException;

public class StopChequeHistoryEnqPayload {
    public String stopChequeHistoryEnq(String profileNum, String accNum){

        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        RequestorIdentifier requestorIdentifier = new RequestorIdentifier();
        requestorIdentifier.setProfileNumber(profileNum);
        requestorIdentifier.setUserId("433058");

        StopChequeHistoryEnqRq stopChequeHistoryEnqRq = new StopChequeHistoryEnqRq();
        stopChequeHistoryEnqRq.setChequeAccountNumber(accNum);
        stopChequeHistoryEnqRq.setRequestorIdentifier(requestorIdentifier);
        stopChequeHistoryEnqRq.setRequestChannelId("B");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("558");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();

        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setStopChequeHistoryEnqRq(stopChequeHistoryEnqRq);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

package za.co.nedbank.eqa.cheque.pojosoap.soappayloadgeneration;

import za.co.nedbank.eqa.cheque.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.stopchequehistorysrch.*;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.header.*;

import javax.xml.bind.JAXBException;

public class StopChequeHistorySrchPayload {

    public String stopChequeHistorySearch(String profileNum, String accNum){

        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        RequestorIdentifier requestorIdentifier = new RequestorIdentifier();
        requestorIdentifier.setProfileNumber(profileNum);
        requestorIdentifier.setUserId("433058");

        StopChequeHistorySrchPar stopChequeHistorySrchPar = new StopChequeHistorySrchPar();
        stopChequeHistorySrchPar.setChequeAmountFrom("0000");
        stopChequeHistorySrchPar.setChequeAmountTo("999999");
        stopChequeHistorySrchPar.setChequeNumberFrom("0001");
        stopChequeHistorySrchPar.setChequeNumberTo("9999");
        stopChequeHistorySrchPar.setDateStoppedFrom("2019-01-01");
        stopChequeHistorySrchPar.setDateStoppedTo("2020-07-08");

        StopChequeHistorySrchRq stopChequeHistorySrchRq = new StopChequeHistorySrchRq();
        stopChequeHistorySrchRq.setChequeAccountNumber(accNum);
        stopChequeHistorySrchRq.setRequestorIdentifier(requestorIdentifier);
        stopChequeHistorySrchRq.setStopChequeHistorySrchPar(stopChequeHistorySrchPar);

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("558");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();

        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setStopChequeHistorySrchRq(stopChequeHistorySrchRq);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

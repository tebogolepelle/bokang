package za.co.nedbank.eqa.cheque.pojosoap.soappayloadgeneration;

import za.co.nedbank.eqa.cheque.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.stopcheque.*;
import za.co.nedbank.eqa.cheque.pojosoap.marshall.header.*;

import javax.xml.bind.JAXBException;

public class StopChequePayload {
    public String stopCheque(String profileNum, String accNum){
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        RequestorIdentifier requestorIdentifier = new RequestorIdentifier();
        requestorIdentifier.setProfileNumber(profileNum);
        requestorIdentifier.setUserId("433058");

        StopChequeRq stopChequeRq = new StopChequeRq();
        stopChequeRq.setRequestChannelId("B");
        stopChequeRq.setRequestorIdentifier(requestorIdentifier);

        ChequeItemRq chequeItemRq = new ChequeItemRq();
        chequeItemRq.setChequeAccountNumber(accNum);
        chequeItemRq.setChequeNumberFrom("6");
        chequeItemRq.setChequeNumberTo("6");
        chequeItemRq.setChequeAmount("500");
        chequeItemRq.setChequeBeneficiary("Stop cheque number");
        chequeItemRq.setChequeReason("01");
        chequeItemRq.setChequeDate("2020-07-28");
        chequeItemRq.setChequeCrossed("Y");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("558");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();

        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setStopChequeRq(stopChequeRq);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }

}

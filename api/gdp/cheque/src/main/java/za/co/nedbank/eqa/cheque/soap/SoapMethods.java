package za.co.nedbank.eqa.cheque.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.cheque.ChequeResponseAdapters;
import za.co.nedbank.eqa.cheque.RestAssistance;
import za.co.nedbank.eqa.cheque.pojosoap.FaultResponse.FaultResponse;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.chequeenq.ChequeEnqRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopcheque.StopChequeRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequehistoryenq.StopChequeHistoryEnqRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval.StopChequeValRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stophistorychequesrch.StopChequeHistorySrchRs;
import za.co.nedbank.eqa.cheque.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.cheque.pojosoap.soappayloadgeneration.*;

public class SoapMethods extends ChequeResponseAdapters {
    private static final Logger logger = LogManager.getLogger(SoapMethods.class);
    private RestAssistance restAssistance;
    private String endpoint;
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for request {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";
    private static final String URL_PATH = "ChequeEnqDetail_endpoint";
    private static final String FAULT_STRING = "faultstring";

    public SoapMethods() {
        restAssistance = new RestAssistance();
    }


    /**
     * Description: The method is used to Get the cheque details of the client in mdm, the user should provide account number
     *
     * @param accNumber
     * @return soap GetChequeEnq response
     */
    public Response chequeEnq(String accNumber)  {
        ChequeEnqPayload chequeEnqPayload = new ChequeEnqPayload();

        String payload = chequeEnqPayload.ChequeEnq(accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest( payload,  endpoint,  URL_PATH);

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse;
            faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            ChequeEnqRs chequeEnqRs;
            chequeEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), ChequeEnqRs.class);

            assert chequeEnqRs != null;
            chequeEnqRs.setHeaders(response.getHeaders());
            chequeEnqRs.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    /**
     * Description: The method is used to post cheque details of the client in mdm, the user should provide profile number and account number
     *
     * @param accNumber
     * @param profileNum
     * @return soap PostStopCheque response
     */
    public Response stopCheque(String profileNum, String accNumber)  {
        StopChequePayload stopChequePayload = new StopChequePayload();

        String payload = stopChequePayload.stopCheque(profileNum, accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest( payload,  endpoint,  URL_PATH);

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse;
            faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            StopChequeRs stopChequeRs;
            stopChequeRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), StopChequeRs.class);

            assert stopChequeRs != null;
            stopChequeRs.setHeaders(response.getHeaders());
            stopChequeRs.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    /**
     * Description: The method is used to Get stop cheque history details of the client in mdm, the user should provide profile number and account number
     *
     * @param accNumber
     * @param profileNum
     * @return soap PostStopCheque response
     */
    public Response stopChequeHistoryEnq(String profileNum, String accNumber)  {
        StopChequeHistoryEnqPayload stopChequeHistoryEnqPayload = new StopChequeHistoryEnqPayload();

        String payload = stopChequeHistoryEnqPayload.stopChequeHistoryEnq(profileNum, accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest( payload,  endpoint,  URL_PATH);


        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse;
            faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            StopChequeHistoryEnqRs stopChequeHistoryEnqRs;
            stopChequeHistoryEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), StopChequeHistoryEnqRs.class);

            assert stopChequeHistoryEnqRs != null;
            stopChequeHistoryEnqRs.setHeaders(response.getHeaders());
            stopChequeHistoryEnqRs.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response stopChequeHistorySrch(String profileNum, String accNumber)  {
        StopChequeHistorySrchPayload stopChequeHistorySrchPayload = new StopChequeHistorySrchPayload();

        String payload = stopChequeHistorySrchPayload.stopChequeHistorySearch(profileNum, accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest( payload,  endpoint,  URL_PATH);

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse;
            faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            StopChequeHistorySrchRs stopChequeHistorySrchRs;
            stopChequeHistorySrchRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), StopChequeHistorySrchRs.class);

            assert stopChequeHistorySrchRs != null;
            stopChequeHistorySrchRs.setHeaders(response.getHeaders());
            stopChequeHistorySrchRs.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response stopChequeVal(String profileNum, String accNumber)  {
        StopChequeValPayload stopChequeValPayload = new StopChequeValPayload();

        String payload = stopChequeValPayload.stopChequeVal(profileNum, accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest( payload,  endpoint,  URL_PATH);

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse;
            faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            StopChequeValRs stopChequeValRs;
            stopChequeValRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), StopChequeValRs.class);
            assert stopChequeValRs != null;
            stopChequeValRs.setHeaders(response.getHeaders());
            stopChequeValRs.setStatusCode(response.getStatusCode());
        }
        return response;
    }
}
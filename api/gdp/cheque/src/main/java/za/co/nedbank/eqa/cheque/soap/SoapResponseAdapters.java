package za.co.nedbank.eqa.cheque.soap;

import io.restassured.response.Response;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.chequeenq.ChequeEnqRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopcheque.StopChequeRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequehistoryenq.StopChequeHistoryEnqRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stopchequeval.StopChequeValRs;
import za.co.nedbank.eqa.cheque.pojosoap.Unmarshall.stophistorychequesrch.StopChequeHistorySrchRs;
import za.co.nedbank.eqa.cheque.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;

public class SoapResponseAdapters {

private String responsechequeenqvalue = "R13";

    //private static final Logger logger = LogManager.getLogger(SoapResponseAdapters.class);

    /**
     * Description: The method is used to retrieve response details of retrieve response mdm request and return response values
     * @param response
     * @return
     * @throws JAXBException
     * @throws SOAPException
     * @throws IOException
     */

    public String [] validateChequeEnqResponse(Response response) throws JAXBException, SOAPException, IOException {

        String chequeenqresponsevalues[] = new String[0];
        ChequeEnqRs chequeEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(),ChequeEnqRs.class);

        chequeenqresponsevalues[0]= chequeEnqRs.getChequeEnqRc();
        System.out.println("Result code is: " + chequeenqresponsevalues[0]);
        if (chequeenqresponsevalues[0].equalsIgnoreCase(responsechequeenqvalue)) {
            chequeenqresponsevalues[1] = chequeEnqRs.getChequeEnqItem().getChequeAccountNumber();
        }
        return chequeenqresponsevalues;
    }

    public String [] validateStopChequeResponse(Response response) throws JAXBException, SOAPException, IOException {

        String stopchequeresponsevalues[] = new String[2];
         StopChequeRs stopChequeRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(),StopChequeRs.class);

        stopchequeresponsevalues[0]=stopChequeRs.getChequeItemRs().getResultCode();
        if (stopchequeresponsevalues[0].equalsIgnoreCase(responsechequeenqvalue)) {
            stopchequeresponsevalues[1] = stopChequeRs.getUserId();
            stopchequeresponsevalues[2] = stopChequeRs.getChequeItemRs().getChequeAccountNumber();
            stopchequeresponsevalues[3] = stopChequeRs.getChequeItemRs().getChequeReason();
            stopchequeresponsevalues[4] = stopChequeRs.getChequeItemRs().getChequeDate();
            stopchequeresponsevalues[5] = stopChequeRs.getChequeItemRs().getChequeCrossed();
        }
        return stopchequeresponsevalues;
    }

    public String [] validateStopChequeHistoryEnqResponse(Response response) throws JAXBException, SOAPException, IOException {

        String stopchequehistoryenqresponsevalues[] = new String[2];
        StopChequeHistoryEnqRs stopChequeHistoryEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(),StopChequeHistoryEnqRs.class);

        stopchequehistoryenqresponsevalues[0]=stopChequeHistoryEnqRs.getResultCode();
        if (stopchequehistoryenqresponsevalues[0].equalsIgnoreCase(responsechequeenqvalue)) {
            stopchequehistoryenqresponsevalues[1] = stopChequeHistoryEnqRs.getChequeAccountNumber();
            stopchequehistoryenqresponsevalues[2] = stopChequeHistoryEnqRs.getRequestorIdentifier().getUserId();
            stopchequehistoryenqresponsevalues[3] = stopChequeHistoryEnqRs.getStopChequeHistoryEnqItem().getChequeAmount();
            stopchequehistoryenqresponsevalues[4] = stopChequeHistoryEnqRs.getStopChequeHistoryEnqItem().getChequeBeneficiary();
            stopchequehistoryenqresponsevalues[5] = stopChequeHistoryEnqRs.getStopChequeHistoryEnqItem().getChequeReason();
        }
        return stopchequehistoryenqresponsevalues;
    }

    public String [] validateStopChequeHistorySrchResponse(Response response) throws JAXBException, SOAPException, IOException {

        String stopchequehistorysrchresponsevalues[] = new String[2];
        StopChequeHistorySrchRs stopChequeHistorySrchRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), StopChequeHistorySrchRs.class);

        stopchequehistorysrchresponsevalues[0]=stopChequeHistorySrchRs.getResultCode();
        if (stopchequehistorysrchresponsevalues[0].equalsIgnoreCase(responsechequeenqvalue)) {
            stopchequehistorysrchresponsevalues[1] = stopChequeHistorySrchRs.getStopChequeHistSrchItem().getChequeAccountNumber();
            stopchequehistorysrchresponsevalues[2] = stopChequeHistorySrchRs.getStopChequeHistSrchItem().getCLogUserId();
        }
        return stopchequehistorysrchresponsevalues;
    }

    public String [] validateStopChequeValResponse(Response response) throws JAXBException, SOAPException, IOException {

        String stopchequevalresponsevalues[] = new String[2];
        StopChequeValRs stopChequeValRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), StopChequeValRs.class);

        stopchequevalresponsevalues[0]=stopChequeValRs.getStopChequeRc();
        if (stopchequevalresponsevalues[0].equalsIgnoreCase(responsechequeenqvalue)) {
            stopchequevalresponsevalues[1] = stopChequeValRs.getChequeItemRs().getChequeAccountNumber();
            stopchequevalresponsevalues[2] = stopChequeValRs.getChequeItemRs().getChequeReason();
            stopchequevalresponsevalues[3] = stopChequeValRs.getChequeItemRs().getChequeCrossed();
        }
        return stopchequevalresponsevalues;
    }
}

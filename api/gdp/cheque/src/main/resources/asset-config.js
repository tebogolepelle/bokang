var configFunction = function (envName) {
  if (!envName) {
    envName = 'QA';
  }

//Use this config object to declare properties such as paths to various APIs that you can use in your asset.
//You can declare other appropriate properties. See below example.
  var config = {
       Authorization : "",
      //soap
      ChequeEnqDetail_endpoint: "services/ent/transactionalproductsmanagement/cheque/v3",

       //certification details for soap calls
       CERT_NAME: "AP767375.pfx",
       CERT_PASS: "oV8kkmHLpALeS121",
  };

  if(envName == 'ETE'){
    config.baseUrl = "https://ssg-e.it.nednet.co.za:443/";
  }
  else if(envName == 'QA'){
   config.baseUrl = "https://ssg-q.it.nednet.co.za:443/";
  }
    else if(envName == 'DEV'){
   config.baseUrl = "http://localhost/";
  }

  config.environment = envName;
  return config;
}

var reportConfig = function(envName,scenario) {
  if (!envName) {
    envName = "QA";
  }

  if(!scenario) {
    scenario = 'Smoke Test';
  }
   var config = {
            platform:'API',
            product : 'ME',
            project : 'GDP',
            tool : 'api-framework',
            version : '1.0.1',
            environment:envName,
            scenarioName:scenario
   };

   return config;
}
package za.co.nedbank.eqa.cheque;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.eqa.cheque.soap.SoapMethods;

public class StopChequeHistoryEnqTest {

    private Logger logger = LogManager.getLogger(StopChequeHistoryEnqTest.class);
    private static final String STATUS_MESSAGE = "API response status {}";

    private Response response = null;

    @Before
    public void setUp() {
        TestUtils.setupStub("Cheque", "StopChequeHistoryEnq.xml");
    }

    @Test
    public void validateSoapTests() {

        SoapMethods soapMethods = new SoapMethods();

        /**
         * Description : The below methods are used to validate cheque details based on the on text provided and validates the above cis is being retrieved in the response
         */


        response = soapMethods.stopChequeHistoryEnq("0000000000", "1908634707");
        if (response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Cheque Enquiry successfully executed");
        } else {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Cheque Enquiry not successfully executed");
        }
    }

}

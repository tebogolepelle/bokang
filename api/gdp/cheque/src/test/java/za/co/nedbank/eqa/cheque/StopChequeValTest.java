package za.co.nedbank.eqa.cheque;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.eqa.cheque.soap.SoapMethods;

public class StopChequeValTest {

    private Logger logger = LogManager.getLogger(StopChequeValTest.class);
    private static final String STATUS_MESSAGE = "API response status {}";

    private Response response = null;

    @Before
    public void setUp() {
        TestUtils.setupStub("Cheque", "StopChequeVal_Response.xml");
    }

    @Test
    public void validateSoapTests(){

        SoapMethods soapMethods = new SoapMethods();

        response = soapMethods.stopChequeVal("0000000000", "1908634707");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Stop Cheque Val successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Stop Cheque Val not successfully executed");
        }
    }

}

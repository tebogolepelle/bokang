package za.co.nedbank.eqa.chequebook;

import org.junit.Assert;
import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.reporter.server.kibana.model.AuditLog;

public class ChequeBookResponseAdapters {


    private static ChequeBookResponseAdapters ourInstance = new ChequeBookResponseAdapters();
//    private String responsevalidation = "R00";
//    private ResponseBody body = null;
//    private static final Logger logger = LogManager.getLogger(ChequeBookResponseAdapters.class);
//    private static String getmethodvalue =  "The method returns value..........";
//    private String reponsevalue = "SUCCESS";

    public static ChequeBookResponseAdapters getInstance() {
        return ourInstance;
    }
    protected RestUtil restUtil;
    protected JsonUtil jsonUtil;
    protected AuditLog auditLog;


    public ChequeBookResponseAdapters() {
        restUtil         = new RestUtil();
        jsonUtil         = new JsonUtil();
        auditLog  = new AuditLog();
    }

    /**
     * Description: This method is to verify only 200 response
     * @param statuscode - status code to verify whether API is up and Running or not
     */

    public void verify200Response (int statuscode)
    {
        Assert.assertEquals(200,statuscode);
    }
//
//    /**
//     * Description: The below is to extract cis value after executing the post organization method
//     * @param orgresp
//     * @return get cis validate response
//     */
//    public String [] getcis_validate_response(Response orgresp) {
//        String orgentityresponsevalue[] = new String[6];
//        body = orgresp.getBody();
//        JuristicOrgResponse juristicOrgResponse = body.as(JuristicOrgResponse.class);
//        Assert.assertEquals(reponsevalue, juristicOrgResponse.getAddOrganizationDetailRes().get(0).getResponseControl().get(0).getResultCode());
//
//        for (int i = 0; i <= 2; i++) {
//            String getidtype = juristicOrgResponse.getAddOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(1).getIdentificationType();
//            if (getidtype.equalsIgnoreCase("1041")) {
//
//                orgentityresponsevalue[0] = juristicOrgResponse.getAddOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(1).getIdentificationNumber();
//                orgentityresponsevalue[1] = juristicOrgResponse.getAddOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(0).getIdentificationNumber();
//                orgentityresponsevalue[2] = juristicOrgResponse.getAddOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getDisplayName();
//                orgentityresponsevalue[3] = juristicOrgResponse.getAddOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(1).getIdentificationIdPK();
//                orgentityresponsevalue[4] = juristicOrgResponse.getAddOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(1).getPartyId();
//                orgentityresponsevalue[5] = juristicOrgResponse.getAddOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(1).getPartyIdentificationLastUpdateTxId();
//
//            } else {
//                logger.info(getmethodvalue,"Failed to retrieve id type");}
//
//        }
//
//        return orgentityresponsevalue;
//    }
//
//    /**
//     * Description: The below method is used to retrieve the dcar values
//     * @param branchresponse
//     * @return get dcar
//     */
//
//    public String getbranchesresponse_validate(Response branchresponse)
//    {
//
//        body = branchresponse.getBody();
//        Wrapper wrapper = body.as(Wrapper.class);
//        String gdcar = wrapper.get(0).getDcar();
//        return gdcar;
//    }
//
//    /**
//     * Description: This method is used to retreived the cheque entity client epn
//     * @param arrangementsresp
//     * @return get organization entity cis
//     */
//
//    public String getarragementsresonse(Response arrangementsresp)
//    {
//        body = arrangementsresp.getBody();
//        GetRetrieveParty getRetrieveParty = body.as(GetRetrieveParty.class);
//        Assert.assertEquals(responsevalidation,getRetrieveParty.getRetrievePartyRes().get(0).getResultSet().get(0).getReasonCode());
//        String orgentitycis = getRetrieveParty.getRetrievePartyRes().get(0).getPartyResponseBObj().get(0).getOrganizationResponseBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(0).getIdentificationNumber();
//        return orgentitycis;
//
//    }
//
//    /**
//     * Description: This Method is used to retrieve cis and validate the put response with success message
//     * @param putorgreponse
//     * @param responsevaluecheck
//     * @return get org entity cis
//     */
//    public String validateputorgresponse(Response putorgreponse, String responsevaluecheck)
//    {
//        body = putorgreponse.getBody();
//        Validateputorgresponse validateputorgresponse = body.as(Validateputorgresponse.class);
//        Assert.assertEquals(responsevaluecheck,validateputorgresponse.getMaintainOrganizationDetailRes().get(0).getResponseControl().get(0).getResultCode());
//        String orgentitycis = validateputorgresponse.getMaintainOrganizationDetailRes().get(0).getOrganizationDetailBObj().get(0).getTCRMOrganizationBObj().get(0).getTCRMPartyIdentificationBObj().get(0).getIdentificationNumber();
//        return orgentitycis;
//
//    }


}

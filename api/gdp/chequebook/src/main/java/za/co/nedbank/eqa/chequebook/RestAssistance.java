package za.co.nedbank.eqa.chequebook;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;
import java.io.File;
import java.util.HashMap;

public class RestAssistance {

    private ApiConfig apiConfig;
    private RestUtil restUtil;
    private static final String CONTENT_TYPE = "text/xml";
    private String certName;
    private String certPassword;
    private HashMap<String, String> postHeaderEmpty = new HashMap<>();


    private static final Logger logger = LogManager.getLogger(RestAssistance.class);

    public RestAssistance()  {
        logger.info("class initialized {}.....", getClass().getName());

        restUtil = new RestUtil();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }

        certName = this.getApiConfig().getOtherElements().get("CERT_NAME");
        certPassword = this.getApiConfig().getOtherElements().get("CERT_PASS");

    }

    public RestUtil getRestUtil() {
        logger.info("getting RestUtil....");
        restUtil.initSpec();
        return restUtil;
    }

    public ApiConfig getApiConfig() {
        logger.info("getting apiConfig.....");
        return apiConfig;
    }

    /**
     * Description: The method is used to return the enviroment, based on the environment the required soap end point will be returned
     * @return the soapendpoint
     */

    public String getSoapEndpoint()
    {
        return getApiConfig().getEnvironmentBaseUrl();

    }

    /**
     * Description: The method is used to return the certificate loaction and user is required to pass the certificate name
     * @param certname
     * @return
     */

    public String getCertificate(String certname){

        File file  = new File(System.getProperty("user.dir") + File.separator+"src"+File.separator+"test"+File.separator+"resources"+File.separator+"certs"+File.separator+certname);
        String pathToFile = file.getAbsolutePath();
        logger.info("returns path to file");
        return pathToFile;
    }

    public Response postRequest(String strPayload, String endPoint, String path) {
        Response response = null;
        try {
            if(this.apiConfig.getEnvironment().toString().contentEquals("DEV")){
                response = this.getRestUtil().initSpec(endPoint).
                        addPayloadfromString(strPayload).
                        addContentType(CONTENT_TYPE).
                        post(this.getApiConfig().getOtherElements().get(path), postHeaderEmpty);
            }else{
                response = this.getRestUtil().initSpec(endPoint).
                        addSslCertContext(this.getCertificate(certName), certPassword).
                        addPayloadfromString(strPayload).
                        addContentType(CONTENT_TYPE).
                        post(this.getApiConfig().getOtherElements().get(path), postHeaderEmpty);
            }

        } catch (ApiException e) {
            logger.error(e.getStackTrace());
        }

        return response;

    }

}

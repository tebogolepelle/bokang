package za.co.nedbank.eqa.chequebook.pojosoap.marshall.availablebookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "AvailableBookEnqRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class AvailableBookEnqRq implements Serializable {
    @XmlElement(name = "AccountNr",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String accountNr;

    @XmlElement(name = "ChunkRequestHeader",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public ChunkRequestHeader chunkRequestHeader;

    public String getAccountNr() {
        return accountNr;
    }

    public void setAccountNr(String accountNr) {
        this.accountNr = accountNr;
    }

    public ChunkRequestHeader getChunkRequestHeader() {
        return chunkRequestHeader;
    }

    public void setChunkRequestHeader(ChunkRequestHeader chunkRequestHeader) {
        this.chunkRequestHeader = chunkRequestHeader;
    }

}

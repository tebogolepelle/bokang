package za.co.nedbank.eqa.chequebook.pojosoap.marshall.availablebookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement(name = "AvailableBookingMainClass")
@XmlAccessorType(XmlAccessType.FIELD)
public class AvailableBookingMainClass implements Serializable {
    private Envelope envelope;

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }
}

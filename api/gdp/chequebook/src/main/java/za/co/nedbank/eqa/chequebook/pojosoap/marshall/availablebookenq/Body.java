package za.co.nedbank.eqa.chequebook.pojosoap.marshall.availablebookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable{
    @XmlElement(name = "AvailableBookEnqRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public AvailableBookEnqRq availableBookEnqRq;

    public AvailableBookEnqRq getAvailableBookEnqRq() {
        return availableBookEnqRq;
    }

    public void setAvailableBookEnqRq(AvailableBookEnqRq availableBookEnqRq) {
        this.availableBookEnqRq = availableBookEnqRq;
    }
}

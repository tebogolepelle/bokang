package za.co.nedbank.eqa.chequebook.pojosoap.marshall.availablebookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ContinuationValue",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChunkRequestHeader implements Serializable {
    @XmlElement(name = "ContinuationValue",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String continuationValue;

    public String getContinuationValue() {
        return continuationValue;
    }

    public void setContinuationValue(String continuationValue) {
        this.continuationValue = continuationValue;
    }
}

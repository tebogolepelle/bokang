package za.co.nedbank.eqa.chequebook.pojosoap.marshall.chequebookorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable{
    @XmlElement(name = "ChequebookOrderRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public ChequebookOrderRq chequebookOrderRq;

    public ChequebookOrderRq getChequebookOrderRq() {
        return chequebookOrderRq;
    }

    public void setChequebookOrderRq(ChequebookOrderRq chequebookOrderRq) {
        this.chequebookOrderRq = chequebookOrderRq;
    }
}

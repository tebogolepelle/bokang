package za.co.nedbank.eqa.chequebook.pojosoap.marshall.chequebookorderenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable{
    @XmlElement(name = "ChequebookOrderEnqRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public ChequebookOrderEnqRq chequebookOrderEnqRq;

    public ChequebookOrderEnqRq getChequebookOrderEnqRq() {
        return chequebookOrderEnqRq;
    }

    public void setChequebookOrderEnqRq(ChequebookOrderEnqRq chequebookOrderEnqRq) {
        this.chequebookOrderEnqRq = chequebookOrderEnqRq;
    }
}

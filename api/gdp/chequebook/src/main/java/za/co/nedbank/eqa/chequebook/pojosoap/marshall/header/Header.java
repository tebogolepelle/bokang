package za.co.nedbank.eqa.chequebook.pojosoap.marshall.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "header", namespace = "")
@XmlAccessorType(XmlAccessType.FIELD)
public class Header implements Serializable {
    @XmlElement(name = "Security",namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    public Security security;

    @XmlElement(name = "EnterpriseContext",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public EnterpriseContext enterpriseContext;

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public EnterpriseContext getEnterpriseContext() {
        return enterpriseContext;
    }

    public void setEnterpriseContext(EnterpriseContext enterpriseContext) {
        this.enterpriseContext = enterpriseContext;
    }
}

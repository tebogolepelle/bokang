package za.co.nedbank.eqa.chequebook.pojosoap.marshall.maintchqstatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable{
    @XmlElement(name = "MaintChqBkStatusRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public MaintChqBkStatusRq maintChqBkStatusRq;


    public MaintChqBkStatusRq getMaintChqBkStatusRq() {
        return maintChqBkStatusRq;
    }

    public void setMaintChqBkStatusRq(MaintChqBkStatusRq maintChqBkStatusRq) {
        this.maintChqBkStatusRq = maintChqBkStatusRq;
    }
}

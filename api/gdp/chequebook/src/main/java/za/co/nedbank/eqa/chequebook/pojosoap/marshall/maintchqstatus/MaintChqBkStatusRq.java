package za.co.nedbank.eqa.chequebook.pojosoap.marshall.maintchqstatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "AccountNr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaintChqBkStatusRq implements Serializable {
    @XmlElement(name = "AccountNr",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String AccountNr;

    @XmlElement(name = "ChequeBookStatus",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String ChequeBookStatus;

    @XmlElement(name = "ChequebookType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String ChequebookType;

    @XmlElement(name = "OrderNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String OrderNumber;

    @XmlElement(name = "CollectionBranchNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String CollectionBranchNumber;

    public String getAccountNr() {
        return AccountNr;
    }

    public void setAccountNr(String accountNr) {
        AccountNr = accountNr;
    }

    public String getChequeBookStatus() {
        return ChequeBookStatus;
    }

    public void setChequeBookStatus(String chequeBookStatus) {
        ChequeBookStatus = chequeBookStatus;
    }

    public String getChequebookType() {
        return ChequebookType;
    }

    public void setChequebookType(String chequebookType) {
        ChequebookType = chequebookType;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getCollectionBranchNumber() {
        return CollectionBranchNumber;
    }

    public void setCollectionBranchNumber(String collectionBranchNumber) {
        CollectionBranchNumber = collectionBranchNumber;
    }
}

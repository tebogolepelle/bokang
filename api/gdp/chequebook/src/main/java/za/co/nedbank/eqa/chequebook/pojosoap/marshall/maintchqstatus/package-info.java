@XmlSchema(
        namespace = "hhttp://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3",
        xmlns = {
                @XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="ent", namespaceURI="http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext"),
                @XmlNs(prefix="v3", namespaceURI="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3"),
                @XmlNs(prefix="wsse", namespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")

        },
        elementFormDefault = XmlNsForm.QUALIFIED)
package za.co.nedbank.eqa.chequebook.pojosoap.marshall.maintchqstatus;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
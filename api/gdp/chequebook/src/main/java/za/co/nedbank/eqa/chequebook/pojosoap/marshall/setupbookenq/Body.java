package za.co.nedbank.eqa.chequebook.pojosoap.marshall.setupbookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable{
    @XmlElement(name = "SetUpBookEnquiryRq",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public SetUpBookEnquiryRq setUpBookEnquiryRq;


    public SetUpBookEnquiryRq getSetUpBookEnquiryRq() {
        return setUpBookEnquiryRq;
    }

    public void setSetUpBookEnquiryRq(SetUpBookEnquiryRq setUpBookEnquiryRq) {
        this.setUpBookEnquiryRq = setUpBookEnquiryRq;
    }
}

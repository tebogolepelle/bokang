package za.co.nedbank.eqa.chequebook.pojosoap.marshall.setupbookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "AccountNr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetUpBookEnquiryRq implements Serializable{
    @XmlElement(name = "AccountNr",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String AccountNr;

    public String getAccountNr() {
        return AccountNr;
    }

    public void setAccountNr(String accountNr) {
        AccountNr = accountNr;
    }
}

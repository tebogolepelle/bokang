package za.co.nedbank.eqa.chequebook.pojosoap.marshall.setupbooks;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "AccountNr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetUpBooksRq implements Serializable{
    @XmlElement(name = "AccountNr",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String AccountNr;

    @XmlElement(name = "NameLine1",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String NameLine1;

    @XmlElement(name = "CollectionBranchNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String CollectionBranchNumber;

    @XmlElement(name = "BlockInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String BlockInd;

    @XmlElement(name = "BookType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String BookType;

    @XmlElement(name = "SuspendChequebook",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String SuspendChequebook;

    @XmlElement(name = "PermanentChange",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String PermanentChange;

    public String getAccountNr() {
        return AccountNr;
    }

    public void setAccountNr(String accountNr) {
        AccountNr = accountNr;
    }

    public String getCollectionBranchNumber() {
        return CollectionBranchNumber;
    }

    public void setCollectionBranchNumber(String collectionBranchNumber) {
        CollectionBranchNumber = collectionBranchNumber;
    }

    public String getBlockInd() {
        return BlockInd;
    }

    public void setBlockInd(String blockInd) {
        BlockInd = blockInd;
    }

    public String getBookType() {
        return BookType;
    }

    public void setBookType(String bookType) {
        BookType = bookType;
    }

    public String getSuspendChequebook() {
        return SuspendChequebook;
    }

    public void setSuspendChequebook(String suspendChequebook) {
        SuspendChequebook = suspendChequebook;
    }

    public String getNameLine1() {
        return NameLine1;
    }

    public void setNameLine1(String nameLine1) {
        NameLine1 = nameLine1;
    }

    public String getPermanentChange() {
        return PermanentChange;
    }

    public void setPermanentChange(String permanentChange) {
        PermanentChange = permanentChange;
    }
}

package za.co.nedbank.eqa.chequebook.pojosoap.marshall.suspendchequebook;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "SuspendChequeBookRq", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class SuspendChequeBookRq implements Serializable{
    @XmlElement(name = "AccountNr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String accountNr;

    @XmlElement(name = "SuspendChequebook", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    public String suspendChequebook;

    public String getAccountNr() {
        return accountNr;
    }

    public void setAccountNr(String accountNr) {
        this.accountNr = accountNr;
    }

    public String getSuspendChequebook() {
        return suspendChequebook;
    }

    public void setSuspendChequebook(String suspendChequebook) {
        this.suspendChequebook = suspendChequebook;
    }
}

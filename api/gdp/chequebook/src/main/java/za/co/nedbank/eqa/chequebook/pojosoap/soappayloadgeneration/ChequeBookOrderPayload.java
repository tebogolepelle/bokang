package za.co.nedbank.eqa.chequebook.pojosoap.soappayloadgeneration;

import za.co.nedbank.eqa.chequebook.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.chequebook.pojosoap.marshall.header.*;
import za.co.nedbank.eqa.chequebook.pojosoap.marshall.chequebookorder.*;

import javax.xml.bind.JAXBException;

public class ChequeBookOrderPayload {
    public String chequeBookOrder(String accNum, String bookType) {

        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        ChequebookOrderRq chequebookOrderRq = new ChequebookOrderRq();
        chequebookOrderRq.setAccountNr(accNum);
        chequebookOrderRq.setChequebookType(bookType);
        chequebookOrderRq.setNumberOfBooks("1");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP433058 ");

        Security security = new Security();

        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setChequebookOrderRq(chequebookOrderRq);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

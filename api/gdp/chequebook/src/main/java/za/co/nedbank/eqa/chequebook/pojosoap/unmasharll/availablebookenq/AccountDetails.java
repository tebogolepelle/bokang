package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.availablebookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AccountDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountDetails {
    @XmlElement(name = "AccountNr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String AccountNr;

    @XmlElement(name = "Details", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private Details Details;

    public String getAccountNr() {
        return AccountNr;
    }

    public void setAccountNr(String accountNr) {
        AccountNr = accountNr;
    }

    public Details getDetails() {
        return Details;
    }

    public void setDetails(Details details) {
        Details = details;
    }

}

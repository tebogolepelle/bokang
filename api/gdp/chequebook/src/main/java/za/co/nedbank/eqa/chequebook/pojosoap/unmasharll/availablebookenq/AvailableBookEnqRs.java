package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.availablebookenq;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AvailableBookEnqRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class AvailableBookEnqRs {
    @XmlElement(name = "AccountDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private AccountDetails AccountDetails;

    @XmlElement(name = "BookResultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String BookResultCode;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public AccountDetails getAccountDetails() {
        return AccountDetails;
    }

    public void setAccountDetails(AccountDetails accountDetails) {
        AccountDetails = accountDetails;
    }

    public String getBookResultCode() {
        return BookResultCode;
    }

    public void setBookResultCode(String bookResultCode) {
        BookResultCode = bookResultCode;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "AvailableBookEnqRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", AccountDetails=" + AccountDetails +
                ", BookResultCode=" + BookResultCode +
                '}';
    }
}

package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.availablebookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private AvailableBookEnqRs AvailableBookEnqRs;

    private AvailableBookEnqRs getAvailableBookEnqRs() {
        return AvailableBookEnqRs;
    }

    private void setAvailableBookEnqRs(AvailableBookEnqRs availableBookEnqRs) {
        AvailableBookEnqRs = availableBookEnqRs;
    }
    @Override
    public String toString() {
        return "Body{" +
                "AvailableBookEnqRs='" + AvailableBookEnqRs + '\'' +
                '}';
    }
}

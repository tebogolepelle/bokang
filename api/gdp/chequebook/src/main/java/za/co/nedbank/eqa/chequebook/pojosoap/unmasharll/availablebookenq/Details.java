package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.availablebookenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Details")
@XmlAccessorType(XmlAccessType.FIELD)
public class Details {
    @XmlElement(name = "BookCardStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String BookCardStatus;

    @XmlElement(name = "BookNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String BookNumber;

    @XmlElement(name = "BookDescription", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String BookDescription;

    @XmlElement(name = "AffinityCodeKey", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String AffinityCodeKey;

    public String getBookCardStatus() {
        return BookCardStatus;
    }

    public void setBookCardStatus(String bookCardStatus) {
        BookCardStatus = bookCardStatus;
    }

    public String getBookNumber() {
        return BookNumber;
    }

    public void setBookNumber(String bookNumber) {
        BookNumber = bookNumber;
    }

    public String getBookDescription() {
        return BookDescription;
    }

    public void setBookDescription(String bookDescription) {
        BookDescription = bookDescription;
    }

    public String getAffinityCodeKey() {
        return AffinityCodeKey;
    }

    public void setAffinityCodeKeyn(String affinityCodeKey) {
        AffinityCodeKey = affinityCodeKey;
    }
}

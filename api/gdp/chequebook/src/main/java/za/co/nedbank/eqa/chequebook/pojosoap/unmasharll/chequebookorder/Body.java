package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private ChequebookOrderRs ChequebookOrderRs;

    private ChequebookOrderRs getChequebookOrderRs() {
        return ChequebookOrderRs;
    }

    private void setChequebookOrderRs(ChequebookOrderRs chequebookOrderRs) {
        ChequebookOrderRs = chequebookOrderRs;
    }

    @Override
    public String toString() {
        return "Body{" +
                "ChequebookOrderRs='" + ChequebookOrderRs + '\'' +
                '}';
    }
}

package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorder;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ChequebookOrderRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequebookOrderRs implements Serializable{
    @XmlElement(name = "AccountNr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String AccountNr;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }


    public String getAccountNr() {
        return AccountNr;
    }
    public void setAccountNr(String accountNr) {
        AccountNr = accountNr;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ChequebookOrderRs{" +
                "Headers=" + headers +
                ", AccountNr=" + AccountNr +
                '}';
    }
}

package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private ChequebookOrderEnqRs ChequebookOrderEnqRs;

    private ChequebookOrderEnqRs getChequebookOrderEnqRs() {
        return ChequebookOrderEnqRs;
    }

    private void setChequebookOrderEnqRs(ChequebookOrderEnqRs chequebookOrderEnqRs) {
        ChequebookOrderEnqRs = chequebookOrderEnqRs;
    }

    @Override
    public String toString() {
        return "Body{" +
                "ChequebookOrderEnqRs='" + ChequebookOrderEnqRs + '\'' +
                '}';
    }
}

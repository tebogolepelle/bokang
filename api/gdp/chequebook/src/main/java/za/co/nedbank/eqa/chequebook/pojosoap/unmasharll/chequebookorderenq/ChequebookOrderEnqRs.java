package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ChequebookOrderEnqRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequebookOrderEnqRs {
    @XmlElement(name = "ChequebookOrderResultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String ChequebookOrderResultCode;

    @XmlElement(name = "Info", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private Info Info;

    @XmlElement(name = "OrdersInProgress", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private OrdersInProgress OrdersInProgress;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getChequebookOrderResultCode() {
        return ChequebookOrderResultCode;
    }
    public void setChequebookOrderResultCode(String chequebookOrderResultCode) {
        ChequebookOrderResultCode = chequebookOrderResultCode;
    }

    public Info getInfo() {
        return Info;
    }
    public void setInfo(Info info) {
        Info = info;
    }

    public OrdersInProgress getOrdersInProgress() {
        return OrdersInProgress;
    }
    public void setOrdersInProgress(OrdersInProgress ordersInProgress) {
        OrdersInProgress = ordersInProgress;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ChequebookOrderEnqRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", ChequebookOrderResultCode=" + ChequebookOrderResultCode +
                ", Info=" + Info +
                ", OrdersInProgress=" + OrdersInProgress +
                '}';
    }
}

package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq;

public class Envelope {
    private String Header;
    private za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq.Body Body;

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq.Body getBody() {
        return Body;
    }

    public void setBody(za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq.Body body) {
        Body = body;
    }
}

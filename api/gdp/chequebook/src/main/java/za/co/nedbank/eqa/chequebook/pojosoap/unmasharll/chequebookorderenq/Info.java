package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Info")
@XmlAccessorType(XmlAccessType.FIELD)
public class Info {
    @XmlElement(name = "DebitAccountNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String DebitAccountNumber;

    @XmlElement(name = "DonationTrust", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String DonationTrust;

    @XmlElement(name = "ChequebookNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String ChequebookNumber;

    @XmlElement(name = "DateOrdered", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String DateOrdered;

    @XmlElement(name = "SuspendChequebook", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String SuspendChequebook;

    @XmlElement(name = "CollectionBranchNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String CollectionBranchNumber;

    public String getDebitAccountNumber() {
        return DebitAccountNumber;
    }
    public void setDebitAccountNumber(String debitAccountNumber) {
        DebitAccountNumber = debitAccountNumber;
    }

    public String getCollectionBranchNumber() {
        return CollectionBranchNumber;
    }
    public void setCollectionBranchNumber(String collectionBranchNumber) {
        CollectionBranchNumber = collectionBranchNumber;
    }

    public String getDonationTrust() {
        return DonationTrust;
    }
    public void setDonationTrust(String donationTrust) {
        DonationTrust = donationTrust;
    }

    public String getChequebookNumber() {
        return ChequebookNumber;
    }
    public void setChequebookNumber(String chequebookNumber) {
        ChequebookNumber = chequebookNumber;
    }

    public String getDateOrdered() {
        return DateOrdered;
    }
    public void setDateOrdered(String dateOrdered) {
        DateOrdered = dateOrdered;
    }

    public String getSuspendChequebook() {
        return SuspendChequebook;
    }
    public void setSuspendChequebook(String suspendChequebook) {
        SuspendChequebook = suspendChequebook;
    }
}

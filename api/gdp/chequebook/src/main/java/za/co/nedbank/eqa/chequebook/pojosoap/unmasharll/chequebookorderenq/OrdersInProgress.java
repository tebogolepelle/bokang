package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OrdersInProgress")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrdersInProgress {
        @XmlElement(name = "ChequeBookStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
        private String ChequeBookStatus;

        @XmlElement(name = "ChequebookType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
        private String ChequebookType;

        @XmlElement(name = "StatusBranch", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
        private String StatusBranch;

    public String getChequeBookStatus() {
        return ChequeBookStatus;
    }
    public void seChequeBookStatus(String chequeBookStatus) {
        ChequeBookStatus = chequeBookStatus;
    }

    public String getChequebookType() {
        return ChequebookType;
    }
    public void setChequebookType(String chequebookType) {
        ChequebookType = chequebookType;
    }

    public String getStatusBranch() {
        return StatusBranch;
    }
    public void setStatusBranch(String statusBranch) {
        StatusBranch = statusBranch;
    }
}

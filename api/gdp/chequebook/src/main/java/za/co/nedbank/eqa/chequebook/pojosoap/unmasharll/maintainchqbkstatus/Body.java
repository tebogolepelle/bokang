package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.maintainchqbkstatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private MaintChqBkStatusRs MaintChqBkStatusRs;

    private MaintChqBkStatusRs getMaintChqBkStatusRs() {
        return MaintChqBkStatusRs;
    }

    private void setMaintChqBkStatusRs(MaintChqBkStatusRs maintChqBkStatusRs) {
        MaintChqBkStatusRs = maintChqBkStatusRs;
    }

    @Override
    public String toString() {
        return "Body{" +
                "MaintChqBkStatusRs='" + MaintChqBkStatusRs + '\'' +
                '}';
    }
}

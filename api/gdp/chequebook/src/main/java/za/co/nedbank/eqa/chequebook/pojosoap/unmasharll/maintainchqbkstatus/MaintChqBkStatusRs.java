package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.maintainchqbkstatus;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MaintChqBkStatusRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaintChqBkStatusRs {
    @XmlElement(name = "MaintainChqBkRsltCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String MaintainChqBkRsltCode;

    @XmlElement(name = "ResultMessage", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String ResultMessage;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMaintainChqBkRsltCode() {
        return MaintainChqBkRsltCode;
    }
    public void setMaintainChqBkRsltCode(String maintainChqBkRsltCode) {
        MaintainChqBkRsltCode = maintainChqBkRsltCode;
    }

    public String getResultMessage() {
        return ResultMessage;
    }
    public void setResultMessage(String resultMessage) {
        ResultMessage = resultMessage;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "MaintChqBkStatusRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", MaintainChqBkRsltCode=" + MaintainChqBkRsltCode +
                ", ResultMessage=" + ResultMessage +
                '}';
    }
}

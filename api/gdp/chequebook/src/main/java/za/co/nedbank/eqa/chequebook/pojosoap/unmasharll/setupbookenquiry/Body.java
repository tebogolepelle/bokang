package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbookenquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private SetUpBookEnquiryRs SetUpBookEnquiryRs;

    private SetUpBookEnquiryRs getSetUpBookEnquiryRs() {
        return SetUpBookEnquiryRs;
    }

    private void setSetUpBookEnquiryRs(SetUpBookEnquiryRs setUpBookEnquiryRs) {
        SetUpBookEnquiryRs = setUpBookEnquiryRs;
    }

    @Override
    public String toString() {
        return "Body{" +
                "SetUpBookEnquiryRs='" + SetUpBookEnquiryRs + '\'' +
                '}';
    }
}

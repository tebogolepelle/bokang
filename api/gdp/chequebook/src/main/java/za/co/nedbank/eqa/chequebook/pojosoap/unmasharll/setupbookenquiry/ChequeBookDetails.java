package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbookenquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ChequeBookDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChequeBookDetails {
    @XmlElement(name = "ChequeBookInd", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String ChequeBookInd;

    @XmlElement(name = "ChequebookType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String ChequebookType;

    public String getChequeBookInd() {
        return ChequeBookInd;
    }
    public void setChequeBookInd(String chequeBookInd) {
        ChequeBookInd = chequeBookInd;
    }

    public String getChequebookType() {
        return ChequebookType;
    }
    public void setChequebookType(String chequebookType) {
        ChequebookType = chequebookType;
    }
}

package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbookenquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ResponseDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDetails {

    @XmlElement(name = "SetupResultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String SetupResultCode;

    @XmlElement(name = "ResultMessage", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String ResultMessage;

    public String getSetupResultCode() {
        return SetupResultCode;
    }
    public void setSetupResultCode(String setupResultCode) {
        SetupResultCode = setupResultCode;
    }

    public String setResultMessage() {
        return ResultMessage;
    }
    public void setResultMessage(String resultMessage) {
        ResultMessage = resultMessage;
    }
}

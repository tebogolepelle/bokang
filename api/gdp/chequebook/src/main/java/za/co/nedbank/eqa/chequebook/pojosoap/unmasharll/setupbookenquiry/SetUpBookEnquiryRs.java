package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbookenquiry;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SetUpBookEnquiryRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetUpBookEnquiryRs {
    @XmlElement(name = "ResponseDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private ResponseDetails ResponseDetails;

    @XmlElement(name = "SetupBookEnqDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private SetupBookEnqDetails SetupBookEnqDetails;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseDetails getResponseDetails() {
        return ResponseDetails;
    }
    public void setResponseDetails(ResponseDetails responseDetails) {
        ResponseDetails = responseDetails;
    }

    public SetupBookEnqDetails getSetupBookEnqDetails() {
        return SetupBookEnqDetails;
    }
    public void setSetupBookEnqDetails(SetupBookEnqDetails setupBookEnqDetails) {
        SetupBookEnqDetails = setupBookEnqDetails;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "SetUpBookEnquiryRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", ResponseDetails=" + ResponseDetails +
                ", SetupBookEnqDetails=" + SetupBookEnqDetails +
                '}';
    }
}

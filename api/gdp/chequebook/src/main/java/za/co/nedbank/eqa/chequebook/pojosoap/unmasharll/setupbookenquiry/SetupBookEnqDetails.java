package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbookenquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SetupBookEnqDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetupBookEnqDetails{
        @XmlElement(name = "NumberOfBooks", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
        private String NumberOfBooks;

        @XmlElement(name = "CollectionBranchNumber", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
        private String CollectionBranchNumber;

        @XmlElement(name = "ChequeBookDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
        private ChequeBookDetails ChequeBookDetails;

    public String getNumberOfBooks() {
        return NumberOfBooks;
    }
    public void setNumberOfBooks(String numberOfBooks) {
        NumberOfBooks = numberOfBooks;
    }

    public String getCollectionBranchNumber() {
        return CollectionBranchNumber;
    }
    public void setCollectionBranchNumber(String collectionBranchNumber) {
        CollectionBranchNumber = collectionBranchNumber;
    }

    public ChequeBookDetails getChequeBookDetails() {
        return ChequeBookDetails;
    }
    public void setChequeBookDetails(ChequeBookDetails chequeBookDetails) {
        ChequeBookDetails = chequeBookDetails;
    }
}

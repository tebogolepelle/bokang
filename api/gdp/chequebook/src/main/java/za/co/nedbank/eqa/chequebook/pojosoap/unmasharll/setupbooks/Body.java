package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbooks;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private SetUpBooksRs SetUpBooksRs;

    private SetUpBooksRs getSetUpBooksRs() {
        return SetUpBooksRs;
    }

    private void setSetUpBooksRs(SetUpBooksRs setUpBooksRs) {
        SetUpBooksRs = setUpBooksRs;
    }

    @Override
    public String toString() {
        return "Body{" +
                "SetUpBooksRs='" + SetUpBooksRs + '\'' +
                '}';
    }
}

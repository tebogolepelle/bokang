package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbooks;

public class Envelope {
    private String Header;
    private Body Body;

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public Body getBody() {
        return Body;
    }

    public void setBody(Body body) {
        Body = body;
    }
}

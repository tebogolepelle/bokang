package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbooks;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "SetUpBooksRs", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetUpBooksRs implements Serializable{
    @XmlElement(name = "SetupResultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String SetupResultCode;

    @XmlElement(name = "ChequeBookInd", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String ChequeBookInd;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getSetupResultCode() {
        return SetupResultCode;
    }
    public void seSetupResultCode(String setupResultCode) {
        SetupResultCode = setupResultCode;
    }

    public String getChequeBookInd() {
        return ChequeBookInd;
    }
    public void setChequeBookInd(String chequeBookInd) {
        ChequeBookInd = chequeBookInd;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "SetUpBooksRs{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", SetupResultCode=" + SetupResultCode +
                ", ChequeBookInd=" + ChequeBookInd +
                '}';
    }
}

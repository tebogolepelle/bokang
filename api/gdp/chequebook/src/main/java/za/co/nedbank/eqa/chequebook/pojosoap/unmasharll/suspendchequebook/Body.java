package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.suspendchequebook;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    private SuspendChequeBookRs SuspendChequeBookRs;

    private SuspendChequeBookRs getSuspendChequeBookRs() {
        return SuspendChequeBookRs;
    }

    private void seSuspendChequeBookRs(SuspendChequeBookRs suspendChequeBookRs) {
        SuspendChequeBookRs = suspendChequeBookRs;
    }

    @Override
    public String toString() {
        return "Body{" +
                "SuspendChequeBookRs='" + SuspendChequeBookRs + '\'' +
                '}';
    }
}

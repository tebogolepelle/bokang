package za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.suspendchequebook;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SuspendChequeBookRs")
@XmlAccessorType(XmlAccessType.FIELD)
public class SuspendChequeBookRs {
    @XmlElement(name = "SuspChqBkRsltCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String SuspChqBkRsltCode;

    @XmlElement(name = "ResultMessage", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String resultMessage;

    @XmlElement(name = "AccountNr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Chequebook/v3")
    private String accountNr;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getSuspChqBkRsltCode() {
        return SuspChqBkRsltCode;
    }
    public void seSuspChqBkRsltCode(String suspChqBkRsltCode) {
        SuspChqBkRsltCode = suspChqBkRsltCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getAccountNr() {
        return accountNr;
    }
    public void setAccountNr(String accountNr) {
        this.accountNr = accountNr;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "MaintainAccountDetailsResponse{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", SuspChqBkRsltCode=" + SuspChqBkRsltCode +
                ", ResultMessage=" + resultMessage +
                ", AccountNr=" + accountNr +
                '}';
    }
}

package za.co.nedbank.eqa.chequebook.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.chequebook.ChequeBookResponseAdapters;
import za.co.nedbank.eqa.chequebook.RestAssistance;
import za.co.nedbank.eqa.chequebook.pojosoap.FaultResponse.FaultResponse;
import za.co.nedbank.eqa.chequebook.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.chequebook.pojosoap.soappayloadgeneration.*;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.availablebookenq.AvailableBookEnqRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorder.ChequebookOrderRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq.ChequebookOrderEnqRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.maintainchqbkstatus.MaintChqBkStatusRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbookenquiry.SetUpBookEnquiryRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbooks.SetUpBooksRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.suspendchequebook.SuspendChequeBookRs;
import za.co.nedbank.execution.config.api.ApiException;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;

public class SoapMethods extends ChequeBookResponseAdapters {
    private static final Logger logger = LogManager.getLogger(SoapMethods.class);
    private RestAssistance restAssistance;
    private String endpoint;
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for request {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";
    private static final String URL_PATH = "ChequeBookDetail_endpoint";
    private static final String FAULT_STRING = "faultstring";

    private String payLoad = null;

    private AvailableBookEnqPayload availableBookEnqPayload;
    private ChequeBookOrderPayload chequeBookOrderPayload;
    private ChequeBookOrderEnqPayload chequeBookOrderEnqPayload;
    private MaintChqBkStatusPayload maintChqBkStatusPayload;
    private SetUpBookEnqPayload setUpBookEnqPayload;
    private SetUpBooksPayload setUpBooksPayload;
    private SusupendChequeBookPayload susupendChequeBookPayload;

    public SoapMethods() {
        restAssistance = new RestAssistance();
        availableBookEnqPayload = new AvailableBookEnqPayload();
        chequeBookOrderPayload = new ChequeBookOrderPayload();
        chequeBookOrderEnqPayload = new ChequeBookOrderEnqPayload();
        maintChqBkStatusPayload = new MaintChqBkStatusPayload();
        setUpBookEnqPayload = new SetUpBookEnqPayload();
        setUpBooksPayload = new SetUpBooksPayload();
        susupendChequeBookPayload = new SusupendChequeBookPayload();

    }


    /**
     * Description: The method is used to Get the cheque details of the client in mdm, the user should provide account number
     *
     * @param accNumber
     * @return soap GetChequeEnq response
     */
    public Response AvailableBookEnq(String accNumber, String continuationValue) {

        payLoad = availableBookEnqPayload.availableBookEnq(accNumber, continuationValue);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, URL_PATH);
        assert response != null;

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payLoad);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            AvailableBookEnqRs availableBookEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), AvailableBookEnqRs.class);

            assert availableBookEnqRs != null;
            availableBookEnqRs.setHeaders(response.getHeaders());
            availableBookEnqRs.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public Response ChequeBookOrder(String accNumber, String bookType) {

        payLoad = chequeBookOrderPayload.chequeBookOrder(accNumber, bookType);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, URL_PATH);

        assert response != null;


        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payLoad);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            ChequebookOrderRs chequebookOrderRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), ChequebookOrderRs.class);

            assert chequebookOrderRs != null;
            chequebookOrderRs.setHeaders(response.getHeaders());
            chequebookOrderRs.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response ChequeBookOrderEnq(String accNumber) {

        payLoad = chequeBookOrderEnqPayload.chequeBookOrderEnq(accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, URL_PATH);

        assert response != null;


        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payLoad);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            ChequebookOrderEnqRs chequebookOrderEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), ChequebookOrderEnqRs.class);

            assert chequebookOrderEnqRs != null;
            chequebookOrderEnqRs.setHeaders(response.getHeaders());
            chequebookOrderEnqRs.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public Response MaintChqBkStatus(String accNumber) {

        payLoad = maintChqBkStatusPayload.maintChqBkStatus(accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, URL_PATH);

        assert response != null;


        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payLoad);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            MaintChqBkStatusRs maintChqBkStatusRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), MaintChqBkStatusRs.class);

            assert maintChqBkStatusRs != null;
            maintChqBkStatusRs.setHeaders(response.getHeaders());
            maintChqBkStatusRs.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public Response SetUpBookEnq(String accNumber) {

        payLoad = setUpBookEnqPayload.setUpBookEnq(accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, URL_PATH);

        assert response != null;


        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payLoad);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            SetUpBookEnquiryRs setUpBookEnquiryRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), SetUpBookEnquiryRs.class);

            assert setUpBookEnquiryRs != null;
            setUpBookEnquiryRs.setHeaders(response.getHeaders());
            setUpBookEnquiryRs.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public Response SetUpBooks(String accNumber) {

        payLoad = setUpBooksPayload.setUpBooks(accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, URL_PATH);

        assert response != null;


        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payLoad);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            SetUpBooksRs setUpBooksRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), SetUpBooksRs.class);

            assert setUpBooksRs != null;
            setUpBooksRs.setHeaders(response.getHeaders());
            setUpBooksRs.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public Response SuspendChequeBook(String accNumber) {

        payLoad = susupendChequeBookPayload.suspendChequeBook(accNumber);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payLoad, endpoint, URL_PATH);

        assert response != null;


        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payLoad);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            SuspendChequeBookRs suspendChequeBookRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), SuspendChequeBookRs.class);

            assert suspendChequeBookRs != null;
            suspendChequeBookRs.setHeaders(response.getHeaders());
            suspendChequeBookRs.setStatusCode(response.getStatusCode());
        }

        return response;
    }
}
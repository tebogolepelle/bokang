package za.co.nedbank.eqa.chequebook.soap;

import io.restassured.response.Response;
import za.co.nedbank.eqa.chequebook.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.availablebookenq.AvailableBookEnqRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.chequebookorderenq.ChequebookOrderEnqRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.maintainchqbkstatus.MaintChqBkStatusRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbookenquiry.SetUpBookEnquiryRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.setupbooks.SetUpBooksRs;
import za.co.nedbank.eqa.chequebook.pojosoap.unmasharll.suspendchequebook.SuspendChequeBookRs;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;

public class SoapResponseAdapters {

    private String responsevalue = "R00";
    private String bookresulstcodevalue = "F00";
    private String bookresultscodevalue2 = "F01";
    private String bookresultscodevalue3 = "F08";

    /**
     * Description: The method is used to retrieve response details of retrieve response mdm request and return response values
     *
     * @param response
     * @return
     * @throws JAXBException
     * @throws SOAPException
     * @throws IOException
     */

    public String[] validateAvailableBookEnqResponse(Response response) throws JAXBException, SOAPException, IOException {

        String availablebookenqresponsevalues[] = new String[2];
        AvailableBookEnqRs availableBookEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), AvailableBookEnqRs.class);

        availablebookenqresponsevalues[0] = availableBookEnqRs.getBookResultCode();
        if (availablebookenqresponsevalues[0].equalsIgnoreCase(bookresulstcodevalue)) {
            availablebookenqresponsevalues[1] = availableBookEnqRs.getAccountDetails().getAccountNr();
            availablebookenqresponsevalues[2] = availableBookEnqRs.getAccountDetails().getDetails().getBookCardStatus();
            availablebookenqresponsevalues[3] = availableBookEnqRs.getAccountDetails().getDetails().getBookDescription();
            availablebookenqresponsevalues[4] = availableBookEnqRs.getAccountDetails().getDetails().getBookNumber();
            availablebookenqresponsevalues[5] = availableBookEnqRs.getAccountDetails().getDetails().getAffinityCodeKey();
        }
        return availablebookenqresponsevalues;
    }

    public String[] validateChequeBookOrderEnqResponse(Response response) throws JAXBException, SOAPException, IOException {

        String chequebookorderenqresponsevalues[] = new String[2];
        ChequebookOrderEnqRs chequebookOrderEnqRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), ChequebookOrderEnqRs.class);

        chequebookorderenqresponsevalues[0] = chequebookOrderEnqRs.getChequebookOrderResultCode();
        if (chequebookorderenqresponsevalues[0].equalsIgnoreCase(bookresulstcodevalue)) {
            chequebookorderenqresponsevalues[1] = chequebookOrderEnqRs.getInfo().getDateOrdered();
            chequebookorderenqresponsevalues[2] = chequebookOrderEnqRs.getInfo().getChequebookNumber();
            chequebookorderenqresponsevalues[3] = chequebookOrderEnqRs.getInfo().getCollectionBranchNumber();
            chequebookorderenqresponsevalues[4] = chequebookOrderEnqRs.getInfo().getDebitAccountNumber();
            chequebookorderenqresponsevalues[5] = chequebookOrderEnqRs.getInfo().getDonationTrust();
            chequebookorderenqresponsevalues[6] = chequebookOrderEnqRs.getInfo().getSuspendChequebook();
            chequebookorderenqresponsevalues[7] = chequebookOrderEnqRs.getOrdersInProgress().getChequeBookStatus();
            chequebookorderenqresponsevalues[8] = chequebookOrderEnqRs.getOrdersInProgress().getChequebookType();
            chequebookorderenqresponsevalues[9] = chequebookOrderEnqRs.getOrdersInProgress().getStatusBranch();
        }
        return chequebookorderenqresponsevalues;
    }

    public String[] validateMaintChqBkStatusResponse(Response response) throws JAXBException, SOAPException, IOException {

        String maintchqbkstatusresponsevalues[] = new String[2];
        MaintChqBkStatusRs maintChqBkStatusRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), MaintChqBkStatusRs.class);

        maintchqbkstatusresponsevalues[0] = maintChqBkStatusRs.getMaintainChqBkRsltCode();
        if (maintchqbkstatusresponsevalues[0].equalsIgnoreCase(bookresulstcodevalue)) {
            maintchqbkstatusresponsevalues[1] = maintChqBkStatusRs.getResultMessage();
        } else if (maintchqbkstatusresponsevalues[0].equalsIgnoreCase(bookresultscodevalue2)) {
            maintchqbkstatusresponsevalues[1] = maintChqBkStatusRs.getResultMessage();
        }
        return maintchqbkstatusresponsevalues;
    }

    public String[] vslidateSetUpBookEnquiryResponse(Response response) throws JAXBException, SOAPException, IOException {

        String setupbookenquiryresponsevalues[] = new String[2];
        SetUpBookEnquiryRs setUpBookEnquiryRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), SetUpBookEnquiryRs.class);

        setupbookenquiryresponsevalues[0] = setUpBookEnquiryRs.getResponseDetails().getSetupResultCode();
        if (setupbookenquiryresponsevalues[0].equalsIgnoreCase(responsevalue)) {
            setupbookenquiryresponsevalues[1] = setUpBookEnquiryRs.getResponseDetails().setResultMessage();
            setupbookenquiryresponsevalues[2] = setUpBookEnquiryRs.getSetupBookEnqDetails().getCollectionBranchNumber();
            setupbookenquiryresponsevalues[3] = setUpBookEnquiryRs.getSetupBookEnqDetails().getNumberOfBooks();
            setupbookenquiryresponsevalues[4] = setUpBookEnquiryRs.getSetupBookEnqDetails().getChequeBookDetails().getChequeBookInd();
            setupbookenquiryresponsevalues[5] = setUpBookEnquiryRs.getSetupBookEnqDetails().getChequeBookDetails().getChequebookType();
        }
        return setupbookenquiryresponsevalues;
    }

    public String[] validateSetUpBooksResponse(Response response) throws JAXBException, SOAPException, IOException {

        String setupbooksresponsevalues[] = new String[2];
        SetUpBooksRs setUpBooksRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), SetUpBooksRs.class);

        setupbooksresponsevalues[0] = setUpBooksRs.getSetupResultCode();
        if (setupbooksresponsevalues[0].equalsIgnoreCase(bookresultscodevalue3)) {
            setupbooksresponsevalues[1] = setUpBooksRs.getChequeBookInd();
        }
        return setupbooksresponsevalues;
    }

    public String[] validateSuspendChequeBookResponse(Response response) throws JAXBException, SOAPException, IOException {

        String suspendchequebookresponsevalues[] = new String[2];
        SuspendChequeBookRs suspendChequeBookRs = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), SuspendChequeBookRs.class);

        suspendchequebookresponsevalues[0] = suspendChequeBookRs.getSuspChqBkRsltCode();
        if (suspendchequebookresponsevalues[0].equalsIgnoreCase(bookresulstcodevalue)) {
            suspendchequebookresponsevalues[1] = suspendChequeBookRs.getResultMessage();
            suspendchequebookresponsevalues[2] = suspendChequeBookRs.getAccountNr();
        }
        return suspendchequebookresponsevalues;
    }

}

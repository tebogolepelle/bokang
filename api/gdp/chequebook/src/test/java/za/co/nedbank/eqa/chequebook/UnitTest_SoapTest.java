package za.co.nedbank.eqa.chequebook;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import za.co.nedbank.eqa.chequebook.soap.SoapMethods;
import za.co.nedbank.execution.config.api.ApiException;

import javax.xml.bind.JAXBException;


public class UnitTest_SoapTest {
    Logger logger = LogManager.getLogger(UnitTest_SoapTest.class);
    private static final java.lang.String STATUS_MESSAGE = "API response status {}";

    private Response response = null;
    SoapMethods soapMethods = new SoapMethods();


    @Test
    public void availableBookEnq() {
        TestUtils.setupStub("Chequebook", "AvailableBookEnq.xml");

        response = soapMethods.AvailableBookEnq("1001072006", "1");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Available Book Enq successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Available Book Enq not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore("response body xml not yet ready , Please ignore.")
    @Test
    public void chequeBookOrder() {
        TestUtils.setupStub("Chequebook", "ChequeBookOrder.xml");

        response = soapMethods.ChequeBookOrder("1001072006", "B6");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Cheque Book Order successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Cheque Book Order not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Test
    public void chequeBookOrderEnq() {
        TestUtils.setupStub("Chequebook", "ChequeBookOrderEnq.xml");

        response = soapMethods.ChequeBookOrderEnq("1001072006");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Cheque Book Order Enq successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Cheque Book Order Enq not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Test
    public void maintChqBkStatus() {
        TestUtils.setupStub("Chequebook", "MaintChqBkStatus.xml");

        response = soapMethods.MaintChqBkStatus("1001072006");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Maintain Cheque Book Status successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Maintain Cheque Book Status not successfully executed");
        }
        TestUtils.stopMockServer();
    }


    @Test
    public void setUpBookEnq() {
        TestUtils.setupStub("Chequebook", "SetUpBookEnquiry.xml");

        response = soapMethods.SetUpBookEnq("1001072006");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Set Up Book Enq successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Set Up Book Enq not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Test
    public void setUpBooks() {
        TestUtils.setupStub("Chequebook", "SetUpBooks.xml");

        response = soapMethods.SetUpBooks("1001832272");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Set Up Books successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Set Up Books not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Test
    public void suspendChequeBook() {
        TestUtils.setupStub("Chequebook", "SuspendChequeBook_Response.xml");

        response = soapMethods.SuspendChequeBook("1001832272");
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Suspend Cheque Book successfully executed");
        } else{
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Suspend Cheque Book not successfully executed");
        }
        TestUtils.stopMockServer();
    }




}
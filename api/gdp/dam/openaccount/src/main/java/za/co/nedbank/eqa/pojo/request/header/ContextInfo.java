package za.co.nedbank.eqa.pojo.request.header;


import za.co.nedbank.eqa.util.RestAssistance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ContextInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")

@XmlAccessorType(XmlAccessType.FIELD)

public class ContextInfo implements Serializable {
    @XmlElement(name="ProcessContextId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ProcessContextId;
    @XmlElement(name="ExecutionContextId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ExecutionContextId;


    public ContextInfo() {
        RestAssistance restHelper = new RestAssistance();
        String processContextId = restHelper.getApiConfig().getOtherElements().get("ProcessContextId");
        String executionContextId = restHelper.getApiConfig().getOtherElements().get("ExecutionContextId");

        this.ProcessContextId = processContextId;
        this.ExecutionContextId = executionContextId;
    }

    public ContextInfo(String processContextId, String executionContextId) {
        ProcessContextId = processContextId;
        ExecutionContextId = executionContextId;
    }

    // Getter Methods

    public String getProcessContextId() {
        return ProcessContextId;
    }

    public String getExecutionContextId() {
        return ExecutionContextId;
    }

    public void setProcessContextId(String ProcessContextIdObject) {
        this.ProcessContextId = ProcessContextIdObject;
    }

    public void setExecutionContextId(String ExecutionContextIdObject) {
        this.ExecutionContextId = ExecutionContextIdObject;
    }

}

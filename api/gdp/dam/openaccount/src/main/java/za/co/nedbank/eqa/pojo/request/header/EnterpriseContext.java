package za.co.nedbank.eqa.pojo.request.header;



import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name ="EnterpriseContext", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"ContextInfo", "RequestOriginator","InstrumentationInfo"}, namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")

public class EnterpriseContext implements Serializable {
    @XmlElement(name="ContextInfo" ,namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    ContextInfo ContextInfo;
    @XmlElement(name="RequestOriginator",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    za.co.nedbank.eqa.pojo.request.header.RequestOriginator RequestOriginator;
    @XmlElement(name="InstrumentationInfo",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    InstrumentationInfo InstrumentationInfo;

    public EnterpriseContext() {
    }


    public EnterpriseContext(ContextInfo contextInfo, RequestOriginator requestOriginator, InstrumentationInfo instrumentationInfo) {
        this.ContextInfo = contextInfo;
        this.RequestOriginator = requestOriginator;
        this.InstrumentationInfo = instrumentationInfo;
    }

    public ContextInfo getContextInfo() {
        return ContextInfo;
    }
    public void setContextInfo(ContextInfo contextInfo) {
        this.ContextInfo = contextInfo;
    }

    public RequestOriginator getRequestOriginator() {
        return RequestOriginator;
    }
    public void setRequestOriginator(RequestOriginator requestOriginator) {
        this.RequestOriginator = requestOriginator;
    }

    public InstrumentationInfo getInstrumentationInfo() {
        return InstrumentationInfo;
    }
    public void setInstrumentationInfo(InstrumentationInfo instrumentationInfo) {
        this.InstrumentationInfo = instrumentationInfo;
    }
}

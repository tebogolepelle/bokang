/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name ="Header", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)

public class Header implements Serializable {
    @XmlElement(name="EnterpriseContext",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    EnterpriseContext enterpriseContextObject;

    @XmlElement(name="Security",namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    SecurityObject securityObject;

    public Header() {
    }

    public Header(EnterpriseContext enterpriseContextObject, SecurityObject securityObject) {
        this.enterpriseContextObject = enterpriseContextObject;
        this.securityObject = securityObject;
    }

    public EnterpriseContext getEnterpriseContext() {
        return enterpriseContextObject;
    }

    public void setEnterpriseContext(EnterpriseContext EnterpriseContextObject) {
        this.enterpriseContextObject = EnterpriseContextObject;
    }


    public SecurityObject getSecurity() {
        return securityObject;
    }

    public void setSecurity(SecurityObject securityObject) {
        this.securityObject = securityObject;
    }

}

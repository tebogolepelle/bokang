/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.header;

import za.co.nedbank.eqa.util.RestAssistance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name ="InstrumentationInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
@XmlAccessorType(XmlAccessType.FIELD)

public class InstrumentationInfo implements Serializable {
    @XmlElement(name="ParentInstrumentationId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ParentInstrumentationId;
    @XmlElement(name="ChildInstrumentationId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ChildInstrumentationId;

    public InstrumentationInfo() {
        RestAssistance restHelper = new RestAssistance();
        String parentInstrumentationId = restHelper.getApiConfig().getOtherElements().get("ParentInstrumentationId");
        String childInstrumentationId = restHelper.getApiConfig().getOtherElements().get("ChildInstrumentationId");

        this.ParentInstrumentationId = parentInstrumentationId;
        this.ChildInstrumentationId = childInstrumentationId;
    }

    public InstrumentationInfo(String parentInstrumentationId, String childInstrumentationId) {
        ParentInstrumentationId = parentInstrumentationId;
        ChildInstrumentationId = childInstrumentationId;
    }

    // Getter Methods

    public String getParentInstrumentationId() {
        return ParentInstrumentationId;
    }

    public String getChildInstrumentationId() {
        return ChildInstrumentationId;
    }

    // Setter Methods
    public void setParentInstrumentationId(String ParentInstrumentationIdObject) {
        this.ParentInstrumentationId = ParentInstrumentationIdObject;
    }
    public void setChildInstrumentationId(String ChildInstrumentationIdObject) {
        this.ChildInstrumentationId = ChildInstrumentationIdObject;
    }
}


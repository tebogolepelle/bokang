/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "RequestOriginator", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
@XmlAccessorType(XmlAccessType.FIELD)

public class RequestOriginator implements Serializable {
    @XmlElement(name="MachineIPAddress",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String MachineIPAddress;

    @XmlElement(name="UserPrincipleName",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String UserPrincipleName;

    @XmlElement(name="MachineDNSName",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String MachineDNSName;

    @XmlElement(name="ChannelId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    String ChannelId;

    // Getter Methods

    public String getMachineIPAddress() {
        return MachineIPAddress;
    }

    public String getUserPrincipleName() {
        return UserPrincipleName;
    }

    public String getMachineDNSName() {
        return MachineDNSName;
    }

    public String getChannelId() {
        return ChannelId;
    }

    // Setter Methods
    public void setMachineIPAddress(String MachineIPAddressObject) {
        this.MachineIPAddress = MachineIPAddressObject;
    }
    public void setUserPrincipleName(String UserPrincipleNameObject) {
        this.UserPrincipleName = UserPrincipleNameObject;
    }
    public void setMachineDNSName(String MachineDNSNameObject) {
        this.MachineDNSName = MachineDNSNameObject;
    }
    public void setChannelId(String ChannelIdObject) {
        this.ChannelId = ChannelIdObject;
    }

}
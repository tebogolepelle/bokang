/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.header;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "Security", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
@XmlAccessorType(XmlAccessType.FIELD)

public class SecurityObject implements Serializable {
    @XmlElement(name="UsernameToken", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    UsernameToken usernameToken;

    public SecurityObject() {
    }

    public SecurityObject(UsernameToken usernameToken) {
        this.usernameToken = usernameToken;
    }

    public UsernameToken getUsernameTokenObject() {
        return usernameToken;
    }
    public void setUsernameTokenObject(UsernameToken usernameToken) {
        this.usernameToken = usernameToken;
    }
}

package za.co.nedbank.eqa.pojo.request.openaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement(name = "accountDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class AccountDetails implements Serializable {
    @XmlElement(name="accountName",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    String accountName;
    @XmlElement(name="accountType",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountType accountType;
    @XmlElement(name="accountStatus",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountStatus accountStatus;
    @XmlElement(name="accountSubStatus",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountSubStatus accountSubStatus;
    @XmlElement(name="accountOpenDate",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    String accountOpenDate;
    @XmlElement(name="accountIndustry",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountIndustry accountIndustry;

    public AccountDetails() {
    }

    public AccountDetails(String accountName, AccountType accountType, AccountStatus accountStatus, AccountSubStatus accountSubStatus, String accountOpenDate, AccountIndustry accountIndustry) {
        this.accountName = accountName;
        this.accountType = accountType;
        this.accountStatus = accountStatus;
        this.accountSubStatus = accountSubStatus;
        this.accountOpenDate = accountOpenDate;
        this.accountIndustry = accountIndustry;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public AccountSubStatus getAccountSubStatus() {
        return accountSubStatus;
    }

    public void setAccountSubStatus(AccountSubStatus accountSubStatus) {
        this.accountSubStatus = accountSubStatus;
    }

    public String getAccountOpenDate() {
        return accountOpenDate;
    }

    public void setAccountOpenDate(String accountOpenDate) {
        this.accountOpenDate = accountOpenDate;
    }

    public AccountIndustry getAccountIndustry() {
        return accountIndustry;
    }

    public void setAccountIndustry(AccountIndustry accountIndustry) {
        this.accountIndustry = accountIndustry;
    }
}

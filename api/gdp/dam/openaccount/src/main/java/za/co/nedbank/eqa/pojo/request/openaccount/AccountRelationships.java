/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement(name = "accountRelationships", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class AccountRelationships implements Serializable {
    @XmlElement(name="accountToBranchRelationships",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountToBranchRelationships accountToBranchRelationships;
    @XmlElement(name="accountToInvolvedPartyRelationships",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountToInvolvedPartyRelationships accountToInvolvedPartyRelationships;


    public AccountRelationships() {
    }
    public AccountRelationships(AccountToBranchRelationships accountToBranchRelationships, AccountToInvolvedPartyRelationships accountToInvolvedPartyRelationships) {
        this.accountToBranchRelationships = accountToBranchRelationships;

        this.accountToInvolvedPartyRelationships = accountToInvolvedPartyRelationships;
    }

    public AccountToBranchRelationships getAccountToBranchRelationships() {
        return accountToBranchRelationships;
    }

    public void setAccountToBranchRelationships(AccountToBranchRelationships accountToBranchRelationships) {
        this.accountToBranchRelationships = accountToBranchRelationships;
    }

    public AccountToInvolvedPartyRelationships getAccountToInvolvedPartyRelationships() {
        return accountToInvolvedPartyRelationships;
    }

    public void setAccountToInvolvedPartyRelationships(AccountToInvolvedPartyRelationships accountToInvolvedPartyRelationships) {
        this.accountToInvolvedPartyRelationships = accountToInvolvedPartyRelationships;
    }

}
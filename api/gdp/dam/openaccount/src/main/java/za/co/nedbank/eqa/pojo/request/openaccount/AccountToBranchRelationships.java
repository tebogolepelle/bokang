/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "accountToBranchRelationships", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class AccountToBranchRelationships implements Serializable{

        @XmlElement(name = "relationshipType",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
        RelationshipType relationshipType;
        @XmlElement(name = "branchCode",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
        String branchCode;
        @XmlElement(name = "relationshipStatus",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
        RelationshipStatus relationshipStatus;

        public AccountToBranchRelationships() {
        }

        public AccountToBranchRelationships(RelationshipType relationshipType, String branchCode, RelationshipStatus relationshipStatus) {
            this.relationshipType = relationshipType;
            this.branchCode = branchCode;
            this.relationshipStatus = relationshipStatus;
        }

        public RelationshipType relationshipType() {
            return relationshipType;
        }

        public void setRelationshipType(RelationshipType relationshipType) {
            this.relationshipType = relationshipType;
        }

        public String getBranchCode() {
            return branchCode;
        }

        public void setBranchCode(String branchCode) {
            this.branchCode = branchCode;
        }

        public RelationshipStatus getRelationshipStatus () {
            return relationshipStatus;
        }

        public void setRelationshipStatus(RelationshipStatus relationshipStatus) {
            this.relationshipStatus = relationshipStatus;
        }
}



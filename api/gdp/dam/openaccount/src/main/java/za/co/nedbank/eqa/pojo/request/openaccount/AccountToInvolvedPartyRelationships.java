/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement(name = "accountToInvolvedPartyRelationships", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class AccountToInvolvedPartyRelationships implements Serializable {
    @XmlElement(name = "involvedPartyIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    String involvedPartyIdentifier;
    @XmlElement(name = "involvedPartyRelationshipTypeName",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    String involvedPartyRelationshipTypeName;

    public AccountToInvolvedPartyRelationships() {
    }

    public AccountToInvolvedPartyRelationships(String involvedPartyIdentifier, String involvedPartyRelationshipTypeName) {
        this.involvedPartyIdentifier = involvedPartyIdentifier;
        this.involvedPartyRelationshipTypeName = involvedPartyRelationshipTypeName;
    }

    public String getInvolvedPartyIdentifier() {
        return involvedPartyIdentifier;
    }

    public void setInvolvedPartyIdentifier(String involvedPartyIdentifier) {
        this.involvedPartyIdentifier = involvedPartyIdentifier;
    }

    public String getInvolvedPartyRelationshipTypeName() {
        return involvedPartyRelationshipTypeName;
    }

    public void setInvolvedPartyRelationshipTypeName(String involvedPartyRelationshipTypeName) {
        this.involvedPartyRelationshipTypeName = involvedPartyRelationshipTypeName;
    }
}


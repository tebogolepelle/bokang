/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@XmlRootElement(name = "currentAccount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class CurrentAccount  implements Serializable {
    @XmlElement(name="attorneyTrustInd",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    String attorneyTrustInd;

    public CurrentAccount() {
    }
    public CurrentAccount(String attorneyTrustInd) {
        this.attorneyTrustInd = attorneyTrustInd;
    }

    public String getAttorneyTrustInd() {
        return attorneyTrustInd;
    }

    public void setAttorneyTrustInd(String attorneyTrustInd) {
        this.attorneyTrustInd = attorneyTrustInd;
    }
}

/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;

import za.co.nedbank.eqa.pojo.request.header.Header;
import javax.xml.bind.annotation.*;
import java.io.Serializable;


@XmlRootElement(name = "Envelope",namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)

@XmlType(propOrder = {"header","body"})
public class Envelope implements Serializable {
    @XmlElement(name="Header", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    Header header;
    @XmlElement(name="Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    Body body;

    public Envelope() {
    }

    public Envelope(Header header, Body body) {
        this.header = header;
        this.body = body;
    }


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public String toString() {
        return "Envelope{"+"Header='"+header+'\''+",Body='"+body+'\''+'}';
    }
}
/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"productInfo", "clientIdentification","accountInfo", "accountRelationships"})


public class OpenAccount implements Serializable {
    @XmlElement(name="productInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    ProductInfo productInfo;
    @XmlElement(name="clientIdentification", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    ClientIdentification clientIdentification;
    @XmlElement(name="accountInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountInfo accountInfo;
    @XmlElement(name="accountRelationships", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountRelationships accountRelationships;

    public OpenAccount(){}

    public OpenAccount(ProductInfo productInfo, ClientIdentification clientIdentification, AccountInfo accountInfo, AccountRelationships accountRelationships) {
        this.productInfo = productInfo;
        this.clientIdentification = clientIdentification;
        this.accountInfo = accountInfo;
        this.accountRelationships = accountRelationships;
    }

    public ProductInfo productInfo() {
        return productInfo;
    }

    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }


    public ClientIdentification getClientIdentification() {
        return clientIdentification;
    }

    public void setProductTargetType(ClientIdentification clientIdentification) {
        this.clientIdentification = clientIdentification;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public AccountRelationships getAccountRelationships() {
        return accountRelationships;
    }

    public void setAccountRelationships(AccountRelationships accountRelationships) {
        this.accountRelationships = accountRelationships;
    }
}

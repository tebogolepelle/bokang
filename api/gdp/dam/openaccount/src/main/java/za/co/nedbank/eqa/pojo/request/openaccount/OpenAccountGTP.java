package za.co.nedbank.eqa.pojo.request.openaccount;


import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"productInfo", "clientIdentification","accountInfo", "accountRelationships", "accountDetails"})


public class OpenAccountGTP implements Serializable {
    @XmlElement(name="productInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    ProductInfo productInfo;
    @XmlElement(name="clientIdentification", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    ClientIdentification clientIdentification;
    @XmlElement(name="accountInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountInfo accountInfo;
    @XmlElement(name="accountRelationships", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountRelationships accountRelationships;
    @XmlElement(name="accountDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountDetails accountDetails;

    public OpenAccountGTP(){}

    public OpenAccountGTP(ProductInfo productInfo, ClientIdentification clientIdentification, AccountInfo accountInfo, AccountRelationships accountRelationships, AccountDetails accountDetails) {
        this.productInfo = productInfo;
        this.clientIdentification = clientIdentification;
        this.accountInfo = accountInfo;
        this.accountRelationships = accountRelationships;
        this.accountDetails = accountDetails;
    }

    public ProductInfo productInfo() {
        return productInfo;
    }

    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }


    public ClientIdentification getClientIdentification() {
        return clientIdentification;
    }

    public void setProductTargetType(ClientIdentification clientIdentification) {
        this.clientIdentification = clientIdentification;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public AccountRelationships getAccountRelationships() {
        return accountRelationships;
    }

    public void setAccountRelationships(AccountRelationships accountRelationships) {
        this.accountRelationships = accountRelationships;
    }

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(AccountDetails accountDetails) {
        this.accountDetails = accountDetails;
    }
}

/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "productCategory", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class ProductCategory implements Serializable {
    @XmlElement(name = "productCategoryID", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    String productCategoryID;
    @XmlElement(name = "productCategory", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    ProductCateg productCategory1;

    public ProductCategory() {
    }

    public ProductCategory(String productCategoryID, ProductCateg productCateg) {
        this.productCategoryID = productCategoryID;
        this.productCategory1 = productCateg;
    }

    public String getProductCategoryID() {
        return productCategoryID;
    }

    public void setProductCategoryID (String productCategoryID){
        this.productCategoryID = productCategoryID;
    }

    public ProductCateg getProductCateg1() {
        return productCategory1;
    }

    public void setProductCateg(ProductCateg productCateg){
        this.productCategory1 = productCateg;
    }
}


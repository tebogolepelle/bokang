/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.request.openaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "RelationshipType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class RelationshipType implements Serializable {

    @XmlElement(name = "description",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    String description;

    public RelationshipType() {
    }

    public RelationshipType(String description) {
        this.description = description;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */

package za.co.nedbank.eqa.pojo.response.openaccountgdp;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class BodyRes {

    private OpenAccountResponse openAccountResponse;

    public OpenAccountResponse getOpenAccountResponse() {
        return openAccountResponse;
    }

    public void setOpenAccountResponse(OpenAccountResponse openAccountResponse) {
        this.openAccountResponse = openAccountResponse;
    }


    @Override
    public String toString() {
        return "Body{" +
                "OpenAccountResponse='" + openAccountResponse + '\'' +
                '}';
    }
}

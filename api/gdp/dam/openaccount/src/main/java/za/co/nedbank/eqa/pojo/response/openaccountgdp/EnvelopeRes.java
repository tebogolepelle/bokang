/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */

package za.co.nedbank.eqa.pojo.response.openaccountgdp;


public class EnvelopeRes {


    private BodyRes bodyResody;


    public String getHeader(String headinput) {
        if (headinput.equalsIgnoreCase( "openaccount"))
        {return Header;}


        else
        { return chequeHeader;}
    }


    public String Header = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v2=\"http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2\">\n" +
            "   <soapenv:Header>\n" +
            "      <ec2:EnterpriseContext xmlns:ec2=\"http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext\">\n" +
            "         <ec2:ContextInfo>\n" +
            "            <ec2:ProcessContextId>${=java.util.UUID.randomUUID()}</ec2:ProcessContextId>\n" +
            "            <ec2:ExecutionContextId>${=java.util.UUID.randomUUID()}</ec2:ExecutionContextId>\n" +
            "         </ec2:ContextInfo>\n" +
            "         <ec2:RequestOriginator>\n" +
            "            <ec2:MachineIPAddress>${=java.net.InetAddress.getLocalHost().getHostAddress()}</ec2:MachineIPAddress>\n" +
            "            <ec2:UserPrincipleName>${=System.getProperty(\"user.name\")}</ec2:UserPrincipleName>\n" +
            "            <ec2:MachineDNSName>${=java.net.InetAddress.getLocalHost().getHostName()}</ec2:MachineDNSName>\n" +
            "            <ec2:ChannelId>555</ec2:ChannelId>\n" +
            "         </ec2:RequestOriginator>\n" +
            "         <ec2:InstrumentationInfo>\n" +
            "            <ec2:ParentInstrumentationId>${=java.util.UUID.randomUUID()}</ec2:ParentInstrumentationId>\n" +
            "            <ec2:ChildInstrumentationId>${=java.util.UUID.randomUUID()}</ec2:ChildInstrumentationId>\n" +
            "         </ec2:InstrumentationInfo>\n" +
            "      </ec2:EnterpriseContext>\n" +
            "      <wsse:Security soapenv:mustUnderstand=\"0\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">\n" +
            "         <wsse:UsernameToken>\n" +
            "            <wsse:Username>AP767375</wsse:Username>\n" +
            "         </wsse:UsernameToken>\n" +
            "      </wsse:Security>\n" +
            "   </soapenv:Header>";

    public String chequeHeader = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ent=\"http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext\" xmlns:v2=\"http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/Cheque/v2\">\n" +
            "   <soapenv:Header>\n" +
            "      <!--VikeshN Enterprise Context version 2-->\n" +
            "      <ent:EnterpriseContext>\n" +
            "         <ent:ContextInfo>\n" +
            "            <ent:ProcessContextId>${=java.util.UUID.randomUUID()}</ent:ProcessContextId>\n" +
            "            <ent:ExecutionContextId>${=java.util.UUID.randomUUID()}</ent:ExecutionContextId>\n" +
            "         </ent:ContextInfo>\n" +
            "         <ent:RequestOriginator>\n" +
            "            <ent:MachineIPAddress>${=java.net.InetAddress.getLocalHost().getHostAddress()}</ent:MachineIPAddress>\n" +
            "            <ent:UserPrincipleName>${=System.getProperty(\"user.name\")}</ent:UserPrincipleName>\n" +
            "            <ent:MachineDNSName>${=InetAddress.getLocalHost().getHostName()}</ent:MachineDNSName>\n" +
            "            <ent:ChannelId>555</ent:ChannelId>\n" +
            "         </ent:RequestOriginator>\n" +
            "         <ent:InstrumentationInfo>\n" +
            "            <ent:ParentInstrumentationId>${=java.util.UUID.randomUUID()}</ent:ParentInstrumentationId>\n" +
            "            <ent:ChildInstrumentationId>${=java.util.UUID.randomUUID()}</ent:ChildInstrumentationId>\n" +
            "         </ent:InstrumentationInfo>\n" +
            "      </ent:EnterpriseContext>\n" +
            "      <!--VikeshN WS-Security Username Token-->\n" +
            "      <wsse:Security soapenv:mustUnderstand=\"0\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">\n" +
            "         <wsse:UsernameToken>\n" +
            "            <wsse:Username>AP767375</wsse:Username>\n" +
            "         </wsse:UsernameToken>\n" +
            "      </wsse:Security>\n" +
            "   </soapenv:Header>\n" +
            "   <soapenv:Body>\n";

    public void setHeader(String header) {
        Header = header;
    }

    public BodyRes getBody() {
        return bodyResody;
    }

    public void setBody(BodyRes body) {
        bodyResody = body;
    }

    @Override
    public String toString() {
        return "Envelope{" +
                "Header='" + Header + '\'' +
                ", Body='" + bodyResody + '\'' +
                '}';
    }
}



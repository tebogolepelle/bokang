/*
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.pojo.response.openaccountgdp;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="OpenAccountResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class OpenAccountResponse {
    @XmlElement(name="productIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    ResultSet resultSet;
    @XmlElement(name="accountIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    AccountIdentificationRes accountIdentification;


    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public ResultSet getResultSet(){
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet){
        this.resultSet = resultSet;
    }

    public AccountIdentificationRes getAccountIdentificationRes(){
        return accountIdentification;
    }

    public void setProductCategory(AccountIdentificationRes accountIdentification){
        this.accountIdentification = accountIdentification;
    }

    @Override
    public String toString() {
        return "OpenAccountResponse{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", ResultSet=" + resultSet +
                ", AccountIdentification=" + accountIdentification +
                '}';
    }
}

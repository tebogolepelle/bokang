package za.co.nedbank.eqa.soap;

import io.restassured.response.Response;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.eqa.pojo.request.header.*;
import za.co.nedbank.eqa.pojo.request.openaccount.*;
import za.co.nedbank.eqa.pojo.response.Fault.FaultResponse;
import za.co.nedbank.eqa.pojo.response.openaccountgdp.OpenAccountResponse;
import za.co.nedbank.eqa.util.MarshallHelperClass;
import za.co.nedbank.eqa.util.RestAssistance;
import za.co.nedbank.eqa.util.SoapHelper;
import za.co.nedbank.execution.config.api.ApiException;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.util.Map;

public class GTPOpenAccount {
    private static final Logger logger = LogManager.getLogger(GTPOpenAccount.class);
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for request {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";
    private final RestAssistance restAssistance = new RestAssistance();
    String certName = restAssistance.getApiConfig().getOtherElements().get("certName");
    String pass = restAssistance.getApiConfig().getOtherElements().get("certPass");
    String postPath = restAssistance.getApiConfig().getOtherElements().get("endPointOpenAccount");

    public Response openGTPAccount(Map<String, String> map) {
        Response response = null;
        String payload;
        payload = getPayload(map);
        String endpoint = restAssistance.getApiConfig().getEnvironmentBaseUrl();
        String env = System.getProperty("environment");

        RestUtil restUtil = restAssistance.getRestUtil().initSpec(endpoint);
        if (!env.equalsIgnoreCase("DEV")) {
            try {
                restUtil.addSslCertContext(restAssistance.getcertificate(certName), pass);

            } catch (ApiException e) {
                logger.error(e.getMessage(), e);
            }
        }

        try {
            response = restUtil.
                    addPayloadfromString(payload).
                    addContentType("text/xml").
                    post(postPath);
        } catch (ApiException e) {
            e.printStackTrace();
        }

        assert response != null;
        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint + postPath);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }


        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = null;
            try {
                faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            } catch (JAXBException | SOAPException | IOException e) {
                logger.error(e.getMessage(), e);
            }
            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            OpenAccountResponse openAccountResponse = null;
            try {
                openAccountResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), OpenAccountResponse.class);
            } catch (JAXBException | SOAPException | IOException e) {
                logger.error(e.getMessage(), e);
            }
            assert openAccountResponse != null;
            openAccountResponse.setHeaders(response.getHeaders());
            openAccountResponse.setStatusCode(response.getStatusCode());
        }
        return response;

    }

    public String getPayload(Map<String, String> map) {
        Header header = new Header();
        BodyGTP body = new BodyGTP();
        OpenAccountGTP openAccount = new OpenAccountGTP();

        JSONObject json = new JSONObject(map);

        //Header
        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("bb424bfe-127c-497f-96fb-089558cb7177");
        instrumentationInfo.setChildInstrumentationId("afdb4ccf-78a9-4b36-bbd2-5cd7a2052189");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("10.74.77.44");
        requestOriginator.setUserPrincipleName("CC321816");
        requestOriginator.setMachineDNSName("V105P10PRA2218");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setProcessContextId("444083a8-dfa2-4916-82da-88ee387153b9");
        contextInfo.setExecutionContextId("a259c9db-3677-4931-9f8d-e630f5ea177c");


        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");


        SecurityObject security = new SecurityObject();
        security.setUsernameTokenObject(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);

        //body
        ProductCateg productCateg1 = new ProductCateg();
        productCateg1.setProductCategID(json.get("productCategID").toString());

        ProductCategory productCategory = new ProductCategory();
        productCategory.setProductCateg(productCateg1);
        productCategory.setProductCategoryID(json.get("productCategoryID").toString());

        ProductIdentification productIdentification = new ProductIdentification();
        productIdentification.setProductIdentifier(json.get("productIdentifier").toString());
        productIdentification.setProductIDType(json.get("productIDType").toString());

        ProductCategory productCategory1 = new ProductCategory();
        productCategory1.setProductCategoryID(json.get("productCategoryID").toString());
        productCategory1.setProductCateg(productCateg1);

        RelationshipStatus relationshipStatus1 = new RelationshipStatus();
        relationshipStatus1.setDescription(json.get("descriptionRs").toString());

        RelationshipType relationshipType1 = new RelationshipType();
        relationshipType1.setDescription(json.get("descriptionRt").toString());

        AccountToBranchRelationships accountToBranchRelationships = new AccountToBranchRelationships();
        accountToBranchRelationships.setRelationshipType(relationshipType1);
        accountToBranchRelationships.setBranchCode(json.get("branchCode").toString());
        accountToBranchRelationships.setRelationshipStatus(relationshipStatus1);

        AccountToInvolvedPartyRelationships accountToInvolvedPartyRelationships = new AccountToInvolvedPartyRelationships();
        accountToInvolvedPartyRelationships.setInvolvedPartyIdentifier(json.get("involvedPartyIdentifier").toString());
        accountToInvolvedPartyRelationships.setInvolvedPartyRelationshipTypeName(json.get("involvedPartyRelationshipTypeName").toString());

        AccountRelationships accountRelationships = new AccountRelationships();
        accountRelationships.setAccountToInvolvedPartyRelationships(accountToInvolvedPartyRelationships);
        accountRelationships.setAccountToBranchRelationships(accountToBranchRelationships);


        ClientIDType clientIDType = new ClientIDType();
        clientIDType.setCode(json.get("code").toString());
        clientIDType.setDescription(json.get("description").toString());

        ClientIdentification clientIdentification = new ClientIdentification();
        clientIdentification.setClientIdentifier(json.get("clientIdentifier").toString());
        clientIdentification.setClientIDType(clientIDType);

        CurrentAccount currentAccount = new CurrentAccount();
        currentAccount.setAttorneyTrustInd(json.get("attorneyTrustInd").toString());

        AccountIdentification accountIdentification = new AccountIdentification();
        accountIdentification.setAccountIdentification(null);

        TransactionalAccount transactionalAccount = new TransactionalAccount();
        transactionalAccount.setCurrentAccount(currentAccount);
        transactionalAccount.setAccountIdentification(accountIdentification);

        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setTransactionalAccount(transactionalAccount);

        ProductTargetType productTargetType1 = new ProductTargetType();
        productTargetType1.setDescription(json.get("codeAt").toString());

        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductTargetType(productTargetType1);
        productInfo.setProductIdentification(productIdentification);
        productInfo.setProductCategory(productCategory);

        openAccount.setAccountRelationships(accountRelationships);
        openAccount.setProductInfo(productInfo);
        openAccount.setAccountInfo(accountInfo);
        openAccount.setProductTargetType(clientIdentification);

        AccountType accountType = new AccountType();
        accountType.setCode(json.get("descriptionPtt").toString());
        accountType.setDescription(json.get("descriptionAt").toString());

        AccountSubStatus accountSubStatus = new AccountSubStatus();
        accountSubStatus.setValue1(json.get("valueAss").toString());

        AccountStatus accountStatus = new AccountStatus();
        accountStatus.setValue(json.get("valueAs").toString());

        AccountIndustry accountIndustry = new AccountIndustry();
        accountIndustry.setCode(json.get("codeInd").toString());

        AccountDetails accountDetails = new AccountDetails();
        accountDetails.setAccountName(json.get("accountName").toString());
        accountDetails.setAccountType(accountType);
        accountDetails.setAccountStatus(accountStatus);
        accountDetails.setAccountSubStatus(accountSubStatus);
        accountDetails.setAccountOpenDate(json.get("accountOpenDate").toString());
        accountDetails.setAccountIndustry(accountIndustry);


        openAccount.setAccountDetails(accountDetails);
        body.setOpenAccount(openAccount);


        EnvelopeGTP envelope = new EnvelopeGTP(header, body);

        //marshall the POJO to xml and return
        return MarshallHelperClass.RequestMarshaller(EnvelopeGTP.class, envelope);
    }

    public void verify200Response(int statuscode) {

        Assert.assertEquals(200, statuscode);
    }
}

package za.co.nedbank.eqa.soap;

import io.restassured.response.Response;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.eqa.pojo.request.header.InstrumentationInfo;
import za.co.nedbank.eqa.pojo.request.openaccount.ClientIDType;
import za.co.nedbank.eqa.pojo.request.openaccount.ClientIdentification;
import za.co.nedbank.eqa.pojo.request.openaccount.Envelope;
import za.co.nedbank.eqa.pojo.response.Fault.FaultResponse;
import za.co.nedbank.eqa.pojo.response.openaccountgdp.OpenAccountResponse;
import za.co.nedbank.eqa.util.RestAssistance;
import za.co.nedbank.eqa.util.SoapHelper;
import za.co.nedbank.execution.config.api.ApiException;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.util.Map;

public class LegacyOpenAccount {
    private static final Logger logger = LogManager.getLogger(LegacyOpenAccount.class);
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for request {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";
    private final RestAssistance restAssistance = new RestAssistance();
    String certName = restAssistance.getApiConfig().getOtherElements().get("certName");
    String pass = restAssistance.getApiConfig().getOtherElements().get("certPass");
    String postPath = restAssistance.getApiConfig().getOtherElements().get("endPointOpenAccount");


    public Response openLegacyAccount(Map<String, String> map) {

        Response response = null;
        String payload = getPayload(map);
        String endpoint = restAssistance.getApiConfig().getEnvironmentBaseUrl();
        String env = System.getProperty("environment");

        RestUtil restUtil = restAssistance.getRestUtil().initSpec(endpoint);
        if (!env.equalsIgnoreCase("DEV")) {
            try {
                restUtil.addSslCertContext(restAssistance.getcertificate(certName), pass);
            } catch (ApiException e) {
                logger.error(e.getMessage(), e);
            }
        }

        try {
            response = restUtil.
                    addPayloadfromString(payload).
                    addContentType("text/xml").
                    post(postPath);
        } catch (ApiException e) {
            e.printStackTrace();
        }

        assert response != null;
        verify200Response(response.getStatusCode());
        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint + postPath);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = null;
            try {
                faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            } catch (JAXBException | SOAPException | IOException e) {
                logger.error(e.getMessage(), e);
            }
            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            OpenAccountResponse openAccountResponse = null;
            try {
                openAccountResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), OpenAccountResponse.class);
            } catch (JAXBException | SOAPException | IOException e) {
                logger.error(e.getMessage(), e);
            }
            assert openAccountResponse != null;
            openAccountResponse.setHeaders(response.getHeaders());
            openAccountResponse.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public String getPayload(Map<String, String> map) {

        JSONObject json = new JSONObject(map);
        za.co.nedbank.eqa.pojo.request.header.Header header = new za.co.nedbank.eqa.pojo.request.header.Header();
        za.co.nedbank.eqa.pojo.request.openaccount.Body body = new za.co.nedbank.eqa.pojo.request.openaccount.Body();
        za.co.nedbank.eqa.pojo.request.openaccount.OpenAccount openAccount = new za.co.nedbank.eqa.pojo.request.openaccount.OpenAccount();

        //Header
        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("bb424bfe-127c-497f-96fb-089558cb7177");
        instrumentationInfo.setChildInstrumentationId("afdb4ccf-78a9-4b36-bbd2-5cd7a2052189");

        za.co.nedbank.eqa.pojo.request.header.RequestOriginator requestOriginator = new za.co.nedbank.eqa.pojo.request.header.RequestOriginator();
        requestOriginator.setMachineIPAddress("10.74.77.44");
        requestOriginator.setUserPrincipleName("CC321816");
        requestOriginator.setMachineDNSName("V105P10PRA2218");
        requestOriginator.setChannelId("555");

        za.co.nedbank.eqa.pojo.request.header.ContextInfo contextInfo = new za.co.nedbank.eqa.pojo.request.header.ContextInfo();
        contextInfo.setProcessContextId("444083a8-dfa2-4916-82da-88ee387153b9");
        contextInfo.setExecutionContextId("a259c9db-3677-4931-9f8d-e630f5ea177c");

        za.co.nedbank.eqa.pojo.request.header.EnterpriseContext enterpriseContext = new za.co.nedbank.eqa.pojo.request.header.EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        za.co.nedbank.eqa.pojo.request.header.UsernameToken usernameToken = new za.co.nedbank.eqa.pojo.request.header.UsernameToken();
        usernameToken.setUsername("AP767375");


        za.co.nedbank.eqa.pojo.request.header.SecurityObject security = new za.co.nedbank.eqa.pojo.request.header.SecurityObject();
        security.setUsernameTokenObject(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);

        //body
        za.co.nedbank.eqa.pojo.request.openaccount.ProductCateg productCateg1 = new za.co.nedbank.eqa.pojo.request.openaccount.ProductCateg();
        productCateg1.setProductCategID(json.get("productCategID").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.ProductCategory productCategory = new za.co.nedbank.eqa.pojo.request.openaccount.ProductCategory();
        productCategory.setProductCateg(productCateg1);
        productCategory.setProductCategoryID(json.get("productCategoryID").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.ProductIdentification productIdentification = new za.co.nedbank.eqa.pojo.request.openaccount.ProductIdentification();
        productIdentification.setProductIdentifier(json.get("productIdentifier").toString());
        productIdentification.setProductIDType(json.get("productIDType").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.ProductCategory productCategory1 = new za.co.nedbank.eqa.pojo.request.openaccount.ProductCategory();
        productCategory1.setProductCategoryID(json.get("productCategoryID").toString());
        productCategory1.setProductCateg(productCateg1);

        za.co.nedbank.eqa.pojo.request.openaccount.RelationshipStatus relationshipStatus1 = new za.co.nedbank.eqa.pojo.request.openaccount.RelationshipStatus();
        relationshipStatus1.setDescription(json.get("descriptionRs").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.RelationshipType relationshipType1 = new za.co.nedbank.eqa.pojo.request.openaccount.RelationshipType();
        relationshipType1.setDescription(json.get("descriptionRt").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.AccountToBranchRelationships accountToBranchRelationships = new za.co.nedbank.eqa.pojo.request.openaccount.AccountToBranchRelationships();
        accountToBranchRelationships.setRelationshipType(relationshipType1);
        accountToBranchRelationships.setBranchCode(json.get("branchCode").toString());
        accountToBranchRelationships.setRelationshipStatus(relationshipStatus1);

        za.co.nedbank.eqa.pojo.request.openaccount.AccountToInvolvedPartyRelationships accountToInvolvedPartyRelationships = new za.co.nedbank.eqa.pojo.request.openaccount.AccountToInvolvedPartyRelationships();
        accountToInvolvedPartyRelationships.setInvolvedPartyIdentifier(json.get("involvedPartyIdentifier").toString());
        accountToInvolvedPartyRelationships.setInvolvedPartyRelationshipTypeName(json.get("involvedPartyRelationshipTypeName").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.AccountRelationships accountRelationships = new za.co.nedbank.eqa.pojo.request.openaccount.AccountRelationships();
        accountRelationships.setAccountToInvolvedPartyRelationships(accountToInvolvedPartyRelationships);
        accountRelationships.setAccountToBranchRelationships(accountToBranchRelationships);


        ClientIDType clientIDType = new ClientIDType();
        clientIDType.setCode(json.get("code").toString());
        clientIDType.setDescription(json.get("description").toString());

        ClientIdentification clientIdentification = new ClientIdentification();
        clientIdentification.setClientIdentifier(json.get("clientIdentifier").toString());
        clientIdentification.setClientIDType(clientIDType);

        za.co.nedbank.eqa.pojo.request.openaccount.CurrentAccount currentAccount = new za.co.nedbank.eqa.pojo.request.openaccount.CurrentAccount();
        currentAccount.setAttorneyTrustInd(json.get("attorneyTrustInd").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.AccountIdentification accountIdentification = new za.co.nedbank.eqa.pojo.request.openaccount.AccountIdentification();
        accountIdentification.setAccountIdentification(null);

        za.co.nedbank.eqa.pojo.request.openaccount.TransactionalAccount transactionalAccount = new za.co.nedbank.eqa.pojo.request.openaccount.TransactionalAccount();
        transactionalAccount.setCurrentAccount(currentAccount);
        transactionalAccount.setAccountIdentification(accountIdentification);

        za.co.nedbank.eqa.pojo.request.openaccount.AccountInfo accountInfo = new za.co.nedbank.eqa.pojo.request.openaccount.AccountInfo();
        accountInfo.setTransactionalAccount(transactionalAccount);

        za.co.nedbank.eqa.pojo.request.openaccount.ProductTargetType productTargetType1 = new za.co.nedbank.eqa.pojo.request.openaccount.ProductTargetType();
        productTargetType1.setDescription(json.get("descriptionPtt").toString());

        za.co.nedbank.eqa.pojo.request.openaccount.ProductInfo productInfo = new za.co.nedbank.eqa.pojo.request.openaccount.ProductInfo();
        productInfo.setProductTargetType(productTargetType1);
        productInfo.setProductIdentification(productIdentification);
        productInfo.setProductCategory(productCategory);

        openAccount.setAccountRelationships(accountRelationships);
        openAccount.setProductInfo(productInfo);
        openAccount.setAccountInfo(accountInfo);
        openAccount.setProductTargetType(clientIdentification);


        body.setOpenAccount(openAccount);


        Envelope envelope = new Envelope(header, body);

        //marshall the POJO to xml and return
        return  za.co.nedbank.eqa.util.MarshallHelperClass.RequestMarshaller(Envelope.class, envelope);

    }

    public void verify200Response(int statuscode) {

        Assert.assertEquals(200, statuscode);
    }

}

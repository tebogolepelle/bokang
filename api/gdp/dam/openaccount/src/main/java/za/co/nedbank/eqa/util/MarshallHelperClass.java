/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.util;

import javax.xml.bind.*;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MarshallHelperClass {

    private MarshallHelperClass() {
    }

    /**
     * Description: The below method is used for marshalling of request and converts the object to webservice payload
     *
     * @return
     * @throws JAXBException
     */

    public static String RequestMarshaller(Class parentclass, Object parentobject) {

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(parentclass);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Marshaller marshaller = null;
        try {
            assert jaxbContext != null;
            marshaller = jaxbContext.createMarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        try {
            assert marshaller != null;
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (PropertyException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        } catch (PropertyException e) {
            e.printStackTrace();
        }
        try {
            marshaller.marshal(parentobject, outputStream);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        String payload = new String(outputStream.toByteArray());
        String requestpayload = String.format(payload);
        return requestpayload;

    }

    /**
     * Description: The method is used to unmarshall the soap response and returns the values as per the user input
     *
     * @param xml
     * @param clazz
     * @param <T>
     * @return
     * @throws JAXBException
     * @throws SOAPException
     * @throws IOException
     */

    public static <T> T unmarshall(InputStream xml, Class<T> clazz) throws JAXBException, SOAPException, IOException {
        T obj = null;
        SOAPMessage message = null;

        message = MessageFactory.newInstance().createMessage(null, xml);
        JAXBContext jaxbContext = null;
        jaxbContext = JAXBContext.newInstance(clazz);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        obj = clazz.cast(jaxbUnmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument()));

        return obj;
    }


}
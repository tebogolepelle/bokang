/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.util;

import za.co.nedbank.eqa.api.rest.RestUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;
import za.co.nedbank.reporter.server.kibana.model.AuditLog;
import java.io.File;


public class RestAssistance {

    private ApiConfig apiConfig;
    private RestUtil restUtil;
    protected AuditLog auditLog;


    private static final Logger logger = LogManager.getLogger(RestAssistance.class);


    public RestAssistance()  {
        logger.info("class initialized {}.....", getClass().getName());

        auditLog = new AuditLog();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }

    }

    public RestUtil getRestUtil() {
        logger.info("getting RestUtil....");
        restUtil = new RestUtil();
        return restUtil;
    }

    public ApiConfig getApiConfig() {
        logger.info("getting apiConfig.....");
        return apiConfig;
    }

    /**
     * Description: The method is used to return the enviroment, based on the environment the required soap end point will be returned
     * @return the soapendpoint
     */

    public String  getsopendpoint()
    {
        String soapendpoint =null;
        if (System.getProperty("environment").equalsIgnoreCase("ete")){

            soapendpoint = getApiConfig().getOtherElements().get("SOAP_END_POINT");
        }

        else if(System.getProperty("environment").equalsIgnoreCase("qa")) {

            soapendpoint = getApiConfig().getOtherElements().get("SOAP_END_POINT1");

        }
        else if(System.getProperty("environment").equalsIgnoreCase("dev")) {

            soapendpoint = getApiConfig().getOtherElements().get("DEV_SOAP_END_POINT");

        }
        return soapendpoint;
    }

    /**
     * Description: The method is used to return the certificate loaction and user is required to pass the certificate name
     * @param certname
     * @return
     */

    public String getcertificate(String certname){

        File file  = new File(System.getProperty("user.dir") + File.separator+"src"+File.separator+"test"+File.separator+"resources"+File.separator+certname);
        String pathtofile = file.getAbsolutePath();
        logger.info("returns path to file");
        return pathtofile;
    }
}
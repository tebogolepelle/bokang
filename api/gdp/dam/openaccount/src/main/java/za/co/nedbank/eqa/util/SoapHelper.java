/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.util;


import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.EncoderConfig;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.pojo.request.header.Header;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;

import static io.restassured.RestAssured.given;

public class SoapHelper {
    private static final Logger logger = LogManager.getLogger(SoapHelper.class);
    private static final String READING_FILE_ERROR = "Error reading file {}";
    private ApiConfig apiConfig;
    private static final String PROXYHOST = "172.17.2.9";


    public SoapHelper() {
        logger.info("Initialling class {}.....", getClass().getName());
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
    }

    private String getBody(String strOperationName) throws IOException {
        logger.info("getting xml body started {}.....", SoapHelper.class.getName());

        String strFileName = strOperationName + ".xml";
        StringBuilder body = new StringBuilder();

        InputStream is = SoapHelper.class.getResourceAsStream("/soap_body/" + strFileName);
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        String l;
        while ((l = r.readLine()) != null) {
            body.append(l);
        }

        logger.info("Body: {}", body);

        logger.info("getting xml body complete {}.....", SoapHelper.class.getName());

        return body.toString();
    }

    private String getHeader(String strServicePath) throws IOException {
        logger.info("getting xml header started {}.....", SoapHelper.class.getName());
        String xmlnsVersion = strServicePath.substring(strServicePath.lastIndexOf('/') + 1);
        StringBuilder strHeader = new StringBuilder();


        InputStream is = SoapHelper.class.getResourceAsStream("/soap_body/header.xml");
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        String l;
        while ((l = r.readLine()) != null) {
            strHeader.append(l);
        }


        strHeader = new StringBuilder(strHeader.toString().replace("{xmlns_version}", xmlnsVersion));
        logger.info("header: {}", strHeader);
        logger.info("getting xml header complete {}.....", SoapHelper.class.getName());

        return strHeader.toString();
    }

    private FileInputStream getCertificate() throws IOException {
        logger.info("getting Certificate  started {}.....", SoapHelper.class.getName());
        FileInputStream fileInputStream = null;

        File tempFile;

        tempFile = File.createTempFile("prefix", "suffix");


        tempFile.deleteOnExit();

        InputStream is = SoapHelper.class.getResourceAsStream("/" + apiConfig.getOtherElements().get("cert_name"));
        FileUtils.copyInputStreamToFile(is, tempFile);
        fileInputStream = new FileInputStream(tempFile);


        logger.info("getting Certificate  complete {}.....", SoapHelper.class.getName());
        return fileInputStream;
    }

    private RequestSpecification configureRestAssuredAuthentication() throws CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException, KeyStoreException, KeyManagementException {
        logger.info("configure Rest Assured Authentication started {}.....", SoapHelper.class.getName());
        System.setProperty("javax.net.ssl.trustStore", "C:/Program Files/Java/jdk1.8.0_251/jre/lib/security");

        KeyStore keyStore = null;
        String keyStorePassword = apiConfig.getOtherElements().get("cert_password");

        try {
            keyStore = KeyStore.getInstance("PKCS12");
        } catch (KeyStoreException e) {
            logger.error(e.getMessage());
        }


        assert keyStore != null;
        keyStore.load(this.getCertificate(), keyStorePassword.toCharArray());


        X509HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SSLSocketFactory lSchemeSocketFactory;

        lSchemeSocketFactory = new SSLSocketFactory(keyStore, keyStorePassword);


        lSchemeSocketFactory.setHostnameVerifier(hostnameVerifier);
        RequestSpecification soapSpec = new RequestSpecBuilder().setConfig(RestAssured.config().sslConfig(new SSLConfig().with().sslSocketFactory(lSchemeSocketFactory).and().allowAllHostnames()).encoderConfig(EncoderConfig.encoderConfig().encodeContentTypeAs(" multipart/related; type=\"application/xop+xml\"; " +
                "start=\"<rootpart@soapui.org>\"; start-info=\"text/xml\"; " +
                "boundary=\"----=_Part_8_1547798259.1570440458254\"", ContentType.XML))).build();

        logger.info("configure Rest Assured Authentication complete {}.....", SoapHelper.class.getName());
        return soapSpec;
    }


    private void clearProxyProperties() {
        logger.info("clearing Proxy properties started {}.....", getClass().getName());

        System.clearProperty("http.proxyHost");
        System.clearProperty("http.proxyPort");
        System.clearProperty("http.proxyUser");
        System.clearProperty("http.proxyPassword");
        System.clearProperty("https.proxyHost");
        System.clearProperty("https.proxyPort");
        System.clearProperty("https.proxyUser");
        System.clearProperty("https.proxyPassword");
        System.clearProperty("java.net.useSystemProxies");

    }

    public Response postRequest(String strPayload, String endPoint, String cert, String certpassword) throws CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException, KeyStoreException, KeyManagementException {
        logger.info("posting Request started {}.....", SoapHelper.class.getName());

        RequestSpecification spec;
        spec = configureRestAssuredAuthentication();


        Response response = given().spec(spec)
                .body(strPayload)
                .when()
                .post(endPoint.toLowerCase());

        if (logger.isInfoEnabled()) {
            logger.info("Soap end point used {}", endPoint);
            logger.info("payload: {}", strPayload);
            logger.info("Response body for response {}", response.getBody().prettyPrint());
            logger.info("posting request finished for class {}.....", SoapHelper.class.getName());
        }
        return response;

    }

    public Response postRequestFramework(String strPayload, String endPoint) {
        logger.info("posting Request started {}.....", SoapHelper.class.getName());
        za.co.nedbank.eqa.util.RestAssistance restHelper = new za.co.nedbank.eqa.util.RestAssistance();

        Response response = null;
        restHelper.getRestUtil().addPayloadfromString(strPayload);
        try {
            response = restHelper.getRestUtil().post(endPoint.toLowerCase());
        } catch (ApiException e) {
            e.printStackTrace();
        }

        if (logger.isInfoEnabled()) {
            logger.info("Soap end point used {}", endPoint);
            logger.info("payload: {}", strPayload);
            logger.info("Response body for response {}", response.getBody().prettyPrint());
            logger.info("posting request finished for class {}.....", SoapHelper.class.getName());
        }
        return response;

    }

    public String createPayload(String strOperationName, String strServicePath) throws IOException {
        logger.info("creating Payload started {}.....", SoapHelper.class.getName());

        logger.info("operation name: {}", strOperationName);
        logger.info("Service path: {}", strServicePath);

        String strHeader = getHeader(strServicePath);
        String strBody = getBody(strOperationName);
        strHeader = strHeader.replace("{service_path}", strServicePath);
        String payload = strHeader + strBody + "</soapenv:Envelope>";
        logger.info("payload: {}", payload);

        logger.info("creating Payload complete {}.....", SoapHelper.class.getName());

        return payload;
    }

    public String replaceParameter(String strXML, String strParameterName, String strParameterValue) {
        logger.info("replacing parameter started {}.....", SoapHelper.class.getName());
        logger.info("XML: {}.....", strXML);
        logger.info("Parameter name to replace: {}.....", strParameterName);
        logger.info("New Value: {}.....", strParameterValue);

        String newXML = strXML.replace("{" + strParameterName + "}", strParameterValue);

        logger.debug("Updated XML: {}.....", newXML);
        logger.info("replacing parameter complete {}.....", SoapHelper.class.getName());

        return newXML;
    }

    public Header getHeader() {
        za.co.nedbank.eqa.pojo.request.header.InstrumentationInfo instrumentationInfo = new za.co.nedbank.eqa.pojo.request.header.InstrumentationInfo();
        za.co.nedbank.eqa.pojo.request.header.RequestOriginator requestOriginator = new za.co.nedbank.eqa.pojo.request.header.RequestOriginator();
        za.co.nedbank.eqa.pojo.request.header.ContextInfo contextInfo = new za.co.nedbank.eqa.pojo.request.header.ContextInfo();
        za.co.nedbank.eqa.pojo.request.header.EnterpriseContext enterpriseContext = new za.co.nedbank.eqa.pojo.request.header.EnterpriseContext(contextInfo, requestOriginator, instrumentationInfo);

        //Header Security
        za.co.nedbank.eqa.pojo.request.header.UsernameToken usernameToken = new za.co.nedbank.eqa.pojo.request.header.UsernameToken();
        za.co.nedbank.eqa.pojo.request.header.SecurityObject securityObject = new za.co.nedbank.eqa.pojo.request.header.SecurityObject(usernameToken);
        return new Header(enterpriseContext, securityObject);
    }

    public static <T> T unmarshall(InputStream xml, Class<T> clazz) throws JAXBException, SOAPException, IOException {
        T obj = null;
        SOAPMessage message = null;

        message = MessageFactory.newInstance().createMessage(null, xml);
        JAXBContext jaxbContext = null;
        jaxbContext = JAXBContext.newInstance(clazz);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        obj = clazz.cast(jaxbUnmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument()));

        return obj;
    }


    public static String marshallEnvelopeObject(Class parentclass, Object parentobject) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(parentclass);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        marshaller.marshal(parentobject, outputStream);
        String payload = new String(outputStream.toByteArray());
        String requestpayload = String.format(payload);
        return requestpayload;

    }
}

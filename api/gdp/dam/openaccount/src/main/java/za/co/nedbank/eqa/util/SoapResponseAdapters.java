/**
 * Project Name : DAM - Open account
 * Author : Michael Ratema
 * Version : V1.0
 * Reviewed By : ........
 * Date of Creation : August 24, 2020
 * Modification History :
 * Date of change : 24-Aug-2020
 * Version : V1.1
 * changed function : Asset initial commit(Open account)
 * change description :
 * Modified By : Michael Ratema
 */
package za.co.nedbank.eqa.util;


import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.pojo.response.openaccountgdp.OpenAccountResponse;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;


public class SoapResponseAdapters {

    private String responsevalue = "SUCCESS";
    private static final Logger logger = LogManager.getLogger(SoapResponseAdapters.class);


    /**
     * Description : the method is used to retireve open account response and retruns the value of the status code and response values
     *
     * @param response
     * @return
     * @throws SOAPException
     * @throws JAXBException
     * @throws IOException
     */

    public String[] validateOpenAccountResponse(Response response) {

        String[] validateOpenAccountResponsevalues = new String[2];
        try {
            OpenAccountResponse openAccRes = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), OpenAccountResponse.class);
        } catch (JAXBException | SOAPException | IOException e) {
            e.printStackTrace();
        }

        String code = response.getStatusLine();
        if (code.contains("200")) {
            System.out.println(response.getBody().prettyPrint());

        } else {
            logger.info("validate open account response did not find the values");
        }

        return validateOpenAccountResponsevalues;
    }


}
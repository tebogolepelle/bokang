var configFunction = function (envName) {
  if (!envName) {
    envName = 'QA';
  }

//Use this config object to declare properties such as paths to various APIs that you can use in your asset.
//You can declare other appropriate properties. See below example.
  var config = {

    endPointOpenAccount:"/services/ent/transactionalproductsmanagement/depositaccountsmanagement/v2",

    certName: "AP767375.pfx",
    certPass: "oV8kkmHLpALeS121",
    //UsernameToken
    usernameToken: "AP169024"
  };

//config.baseUrl is the property that holds the base URL for all the requests.
  if(envName == 'ETE'){
    config.baseUrl = 'https://ssg-e.it.nednet.co.za:443';
  }

  else if(envName == 'QA'){
             config.baseUrl = 'https://ssg-q.it.nednet.co.za:443';
  }
  else if(envName == 'DEV') {
    config.baseUrl = 'http://localhost:8080';
  }

  config.environment = envName;
  return config;
}

var reportConfig = function(envName,scenario) {
  if (!envName) {
    envName = 'QA';
  }

  if(!scenario) {
    scenario = 'Smoke Test';
  }
       var config = {
           platform: 'API',
           product: 'dam',
           project: 'openaccount',
           tool: 'api-framework',
           version: '1.0.0',
           environment: envName,
           scenarioName: scenario
       };
   return config;
}
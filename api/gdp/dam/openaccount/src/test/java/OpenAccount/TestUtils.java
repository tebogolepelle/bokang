package OpenAccount;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.util.RestAssistance;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class TestUtils {
    private static WireMockServer wireMockServer = new WireMockServer(options().httpsPort(8080));
    private static final Logger logger = LogManager.getLogger(TestUtils.class);

    public static void setupStub(String serviceSubString,String mockResponseFile) {
        RestAssistance restHelper  = new RestAssistance();
        String env = restHelper.getApiConfig().getEnvironment().toString();
        if(serviceSubString.equalsIgnoreCase("/services/ent/transactionalproductsmanagement/depositaccountsmanagement/v2")) {
            if (env.contentEquals("DEV")) {
                logger.info("Setting up the stub for service...");
                try {
                    wireMockServer = new WireMockServer();
                    wireMockServer.start();
                    logger.info("Mock server started");

                } catch (Exception exception) {
                    logger.error(exception.getMessage());
                }
                String contentType;
                if(mockResponseFile.contains("xml")){
                    contentType = "application/xml";

                }else{
                    contentType = "application/json";
                }
                wireMockServer.stubFor(post(urlPathMatching(serviceSubString.toLowerCase()))
                        .willReturn(aResponse().withHeader("Content-Type", contentType)
                                .withStatus(200)
                                .withBodyFile(mockResponseFile)));
            }
        }
    }

    public static void stopMockServer() {
        if(wireMockServer != null){
            wireMockServer.stop();
            logger.info("Mock server stopped successfully");
        }else{
            logger.info("Mock server is not running");
        }
    }

}

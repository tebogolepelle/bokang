package OpenAccount;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.eqa.soap.GTPOpenAccount;
import za.co.nedbank.eqa.util.SoapResponseAdapters;
import java.util.HashMap;
import java.util.Map;


public class gtpSoapTest {
    private static final Logger logger = LogManager.getLogger(gtpSoapTest.class);
    private static final java.lang.String STATUS_MESSAGE = "API response status {}";

    @Before
    public void setUp() {
        TestUtils.setupStub("/services/ent/transactionalproductsmanagement/depositaccountsmanagement/v2","Mock_Respose_OpenAccount.xml");
    }

    @Test
    public void gtpOpenAccountSoapTests() {

        GTPOpenAccount gtp_openAccountTest = new GTPOpenAccount();
        new SoapResponseAdapters();

        Map<String, String> map = new HashMap<>();
        map.put("productIdentifier", "1100");
        map.put("productIDType", "CA");
        map.put("productCategoryID", "19");
        map.put("productCategID", "0");
        map.put("descriptionPtt", "MP");
        map.put("clientIdentifier", "151000127032");
        map.put("code", "1041");
        map.put("description", "EnterprisePartyNumber");
        map.put("attorneyTrustInd", "1");
        map.put("descriptionRt", "DomicileBranch");
        map.put("branchCode", "347");
        map.put("descriptionRs", "ACTIVE");
        map.put("involvedPartyIdentifier", "12345");
        map.put("involvedPartyRelationshipTypeName", "RelationshipManager");
        map.put("accountName", "MyAccName");
        map.put("accountOpenDate", "2020-08-24");
        map.put("codeAt", "CA");
        map.put("descriptionAt", "CurrentAccount");
        map.put("valueAs", "05");
        map.put("valueAss", "0");
        map.put("codeInd", "1");

        Response response = gtp_openAccountTest.openGTPAccount(map);

        int expectedStatusCode = 200;
        int actualStatusCode = response.getStatusCode();
        Assert.assertEquals(expectedStatusCode, actualStatusCode);

        logger.info(STATUS_MESSAGE, response.getStatusLine());
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

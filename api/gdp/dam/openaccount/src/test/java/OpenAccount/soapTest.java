package OpenAccount;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.eqa.soap.LegacyOpenAccount;
import za.co.nedbank.eqa.util.SoapResponseAdapters;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import java.util.HashMap;
import java.util.Map;

public class soapTest {
    private static final Logger logger = LogManager.getLogger(soapTest.class);
    private static final String STATUS_MESSAGE = "API response status {}";

    @Before
    public void setUp() {
        try {
            ApiConfig apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }

        TestUtils.setupStub("/services/ent/transactionalproductsmanagement/depositaccountsmanagement/v2","Mock_Respose_OpenAccount.xml");
    }

    @Test
    public void casaOpenAccountSoapTests() {
        LegacyOpenAccount legacyOpenAccount = new LegacyOpenAccount();
        new SoapResponseAdapters();

        Map<String, String> map = new HashMap<>();
        map.put("productIdentifier", "1100");
        map.put("productIDType", "CA");
        map.put("productCategoryID", "19");
        map.put("productCategID", "0");
        map.put("descriptionPtt", "CA");
        map.put("clientIdentifier", "600003273416");
        map.put("code", "1041");
        map.put("description", "EnterprisePartyNumber");
        map.put("attorneyTrustInd", "2");
        map.put("descriptionRt", "DomicileBranch");
        map.put("branchCode", "347");
        map.put("descriptionRs", "ACTIVE");
        map.put("involvedPartyIdentifier", "12345");
        map.put("involvedPartyRelationshipTypeName", "RelationshipManager");

        Response response = legacyOpenAccount.openLegacyAccount(map);

        int expectedStatusCode = 200;
        int actualStatusCode = response.getStatusCode();
        Assert.assertEquals(expectedStatusCode, actualStatusCode);
        logger.info(STATUS_MESSAGE, response.getStatusLine());
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

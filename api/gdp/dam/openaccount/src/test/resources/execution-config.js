var configFunction = function (envName) {
  if (!envName) {
    envName = 'ete';
  }
  print('conf env'+envName);

  var config = {
      Authorization :null };
   config.environment = envName;
  return config;
}

var reportConfig = function(envName) {
  if (!envName) {
    envName = 'ete';
  }
   var config = {
            platform:'API',
            product : 'DAM',
            project : 'OpenAccount',
            tool : 'api-framework',
            version : '1.0.0',
            environment:envName,
            scenario: 'openaccount_scenario'
   };

   return config;
}
package za.co.nedbank.eqa.investments;

import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.reporter.server.kibana.model.AuditLog;

public class InvesetmentsMainResAdapters {


    private static InvesetmentsMainResAdapters ourInstance = new InvesetmentsMainResAdapters();
    public static InvesetmentsMainResAdapters getInstance() {
        return ourInstance;
    }

    protected RestUtil restUtil;
    protected JsonUtil jsonUtil;
    protected AuditLog auditLog;


    public InvesetmentsMainResAdapters() {
        restUtil = new RestUtil();
        jsonUtil = new JsonUtil();
        auditLog = new AuditLog();
    }

}
package za.co.nedbank.eqa.investments.pojosoap.Marshall.InvestmentsCloseAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "CloseAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CloseAccount closeAccount;

    public CloseAccount getCloseAccount() {
        return closeAccount;
    }
    public void setCloseAccount(CloseAccount closeAccount) {
        this.closeAccount = closeAccount;
    }
}

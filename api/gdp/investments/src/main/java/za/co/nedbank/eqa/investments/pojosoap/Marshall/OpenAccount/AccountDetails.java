package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.AccountSubStatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "accountDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountDetails implements Serializable {
    @XmlElement(name = "accountName",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountName;

    @XmlElement(name = "accountSubStatus",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountSubStatus accountSubStatus;

    @XmlElement(name = "accountUnit",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountUnit;

    @XmlElement(name = "accountOpenDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountOpenDate;

    public String getAccountName() {
        return AccountName;
    }
    public void setAccountName(String accountName) {
        AccountName = accountName;
    }

    public AccountSubStatus getAccountSubStatus() {
        return accountSubStatus;
    }
    public void setAccountSubStatus(AccountSubStatus accountSubStatus) {
        this.accountSubStatus = accountSubStatus;
    }

    public String getAccountUnit() {
        return AccountUnit;
    }
    public void setAccountUnit(String accountUnit) {
        AccountUnit = accountUnit;
    }

    public String getAccountOpenDate() {
        return AccountOpenDate;
    }
    public void setAccountOpenDate(String accountOpenDate) {
        AccountOpenDate = accountOpenDate;
    }
}

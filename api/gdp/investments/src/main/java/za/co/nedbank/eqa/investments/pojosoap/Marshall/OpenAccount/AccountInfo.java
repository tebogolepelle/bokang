package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "OpenAccount")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountInfo implements Serializable {
    @XmlElement(name = "investmentAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public InvestmentAccount investmentAccount;

    public InvestmentAccount getInvestmentAccount() {
        return investmentAccount;
    }

    public void setInvestmentAccount(InvestmentAccount investmentAccount) {
        this.investmentAccount = investmentAccount;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "accountToBranchRelationships")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountToBranchRelationships implements Serializable {
    @XmlElement(name = "branchCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String BranchCode;

    @XmlElement(name = "relationshipType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public RelationshipType relationshipType;

    @XmlElement(name = "relationshipStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public RelationshipStatus relationshipStatus;

    public String getBranchCode() {
        return BranchCode;
    }
    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;}

    public RelationshipType getRelationshipType() {
        return relationshipType;
    }
    public void setRelationshipType(RelationshipType relationshipType) {
        this.relationshipType = relationshipType;
    }

    public RelationshipStatus getRelationshipStatus() {
        return relationshipStatus;
    }
    public void setRelationshipStatus(RelationshipStatus relationshipStatus) {
        this.relationshipStatus = relationshipStatus;
    }

}

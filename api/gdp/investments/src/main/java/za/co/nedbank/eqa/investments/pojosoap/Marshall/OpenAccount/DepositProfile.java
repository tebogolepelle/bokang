package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "depositProfile")
@XmlAccessorType(XmlAccessType.FIELD)
public class DepositProfile implements Serializable {
    @XmlElement(name = "annualLimit",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AnnualLimit annualLimit;

    @XmlElement(name = "lifetimeLimit",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public LifetimeLimit lifetimeLimit;

    @XmlElement(name = "lifetimeContributions",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public LifetimeContributions lifetimeContributions;

    @XmlElement(name = "annualContributions",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AnnualContributions annualContributions;

    public AnnualLimit getAnnualLimit() {
        return annualLimit;
    }
    public void setAnnualLimit(AnnualLimit annualLimit) {
        this.annualLimit = annualLimit;
    }

    public LifetimeLimit getRatesLifetimeLimit() {
        return lifetimeLimit;
    }
    public void setLifetimeLimit(LifetimeLimit lifetimeLimit) {
        this.lifetimeLimit = lifetimeLimit;
    }

    public LifetimeContributions getLifetimeContributions() {
        return lifetimeContributions;
    }
    public void setLifetimeContributions(LifetimeContributions lifetimeContributions) {
        this.lifetimeContributions = lifetimeContributions;
    }

    public AnnualContributions getAnnualContributions() {
        return annualContributions;
    }
    public void setAnnualContributions(AnnualContributions annualContributions) {
        this.annualContributions = annualContributions;
    }
}

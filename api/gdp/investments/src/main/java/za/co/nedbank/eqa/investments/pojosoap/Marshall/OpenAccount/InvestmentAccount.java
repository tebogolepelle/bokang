package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.Rates;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "investmentAccount")
@XmlAccessorType(XmlAccessType.FIELD)
public class InvestmentAccount implements Serializable {
    @XmlElement(name = "accountIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountIdentification;

    @XmlElement(name = "termDeposit",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TermDeposit termDeposit;

    @XmlElement(name = "termDepositSummary",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String TermDepositSummary;

    @XmlElement(name = "taxFreeSavings",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TaxFreeSavings taxFreeSavings;

    @XmlElement(name = "depositProfile",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public DepositProfile depositProfile;

    @XmlElement(name = "rates",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Rates rates;

    public String getAccountIdentification() {
        return AccountIdentification;
    }
    public void setAccountIdentification(String accountIdentification) {
        AccountIdentification = accountIdentification;
    }

    public TermDeposit getTermDeposit() {
        return termDeposit;
    }
    public void setTermDeposit(TermDeposit termDeposit) {
        this.termDeposit = termDeposit;
    }

    public String getTermDepositSummary() {
        return TermDepositSummary;
    }
    public void setTermDepositSummary(String termDepositSummary) {
        TermDepositSummary = termDepositSummary;
    }

    public TaxFreeSavings getTaxFreeSavings() {
        return taxFreeSavings;
    }
    public void setTaxFreeSavings(TaxFreeSavings taxFreeSavings) {
        this.taxFreeSavings = taxFreeSavings;
    }

    public DepositProfile getDepositProfile() {
        return depositProfile;
    }
    public void setDepositProfile(DepositProfile depositProfile) {
        this.depositProfile = depositProfile;
    }

    public Rates getRates() {
        return rates;
    }
    public void setRates(Rates rates) {
        this.rates = rates;
    }

}

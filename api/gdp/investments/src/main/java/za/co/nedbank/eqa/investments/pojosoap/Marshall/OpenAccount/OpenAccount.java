package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.ProductInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "OpenAccount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class OpenAccount implements Serializable {
    @XmlElement(name = "productInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ProductInfo productInfo;

    @XmlElement(name = "personIdentification", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public List<PersonIdentification> personIdentification;

    @XmlElement(name = "accountInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountInfo accountInfo;

    @XmlElement(name = "accountRelationships", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountRelationships accountRelationships;

    @XmlElement(name = "accountDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountDetails accountDetails;

    public ProductInfo getProductInfo() {
        return productInfo;
    }
    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public List<PersonIdentification> getPersonIdentification() {
        return personIdentification;
    }
    public void setPersonIdentification(List<PersonIdentification> personIdentification) {
        this.personIdentification = personIdentification;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }
    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public AccountRelationships getAccountRelationships() {
        return accountRelationships;
    }
    public void setAccountRelationships(AccountRelationships accountRelationships) {
        this.accountRelationships = accountRelationships;
    }

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }
    public void setAccountDetails(AccountDetails accountDetails) {
        this.accountDetails = accountDetails;
    }
}
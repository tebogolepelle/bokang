package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "taxFreeSavings")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaxFreeSavings implements Serializable {
    @XmlElement(name = "transferInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String TransferInd;

    @XmlElement(name = "transferredAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TransferredAmount transferredAmount;

    public String getTransferInd() {
        return TransferInd;
    }
    public void setTransferInd(String transferInd) {
        TransferInd = transferInd;
    }

    public TransferredAmount getTransferredAmount() {
        return transferredAmount;
    }
    public void setTransferredAmount(TransferredAmount transferredAmount) {
        this.transferredAmount = transferredAmount;
    }
}

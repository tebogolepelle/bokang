package za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "term")
@XmlAccessorType(XmlAccessType.FIELD)
public class Term implements Serializable {
    @XmlElement(name = "value",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String Value;

    @XmlElement(name = "indicator",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String Indicator;

    public String getValue() {
        return Value;
    }
    public void setValue(String value) {
        Value = value;
    }

    public String getIndicator() {
        return Indicator;
    }
    public void setIndicator(String indicator) {
        Indicator = indicator;
    }
}

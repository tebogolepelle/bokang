package za.co.nedbank.eqa.investments.pojosoap.Marshall.RetireveQualifiedAccounts;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.retrieveinstructions.ChunkRequestHdr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "RetrieveQualifiedAccounts", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveQualifiedAccounts implements Serializable {
    @XmlElement(name = "personIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PersonIdentification personIdentification;

    @XmlElement(name = "instructionType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InstructionType;

    @XmlElement(name = "chunkRequestHdr",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ChunkRequestHdr chunkRequestHdr;

    public PersonIdentification getPersonIdentification() {
        return personIdentification;
    }
    public void setPersonIdentification(PersonIdentification personIdentification) {
        this.personIdentification = personIdentification;
    }

    public String getInstructionType() {
        return InstructionType;
    }
    public void setInstructionType(String instructionType) {
        InstructionType = instructionType;
    }

    public ChunkRequestHdr getChunkRequestHdr() {
        return chunkRequestHdr;
    }
    public void setChunkRequestHdr(ChunkRequestHdr chunkRequestHdr) {
        this.chunkRequestHdr = chunkRequestHdr;
    }
}

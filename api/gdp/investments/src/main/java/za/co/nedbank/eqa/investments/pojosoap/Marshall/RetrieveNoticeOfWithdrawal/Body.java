package za.co.nedbank.eqa.investments.pojosoap.Marshall.RetrieveNoticeOfWithdrawal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "RetrieveNoticeOfWithdrawal",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public RetrieveNoticeOfWithdrawal retrieveNoticeOfWithdrawal;

    public RetrieveNoticeOfWithdrawal getRetrieveNoticeOfWithdrawal() {
        return retrieveNoticeOfWithdrawal;
    }
    public void setRetrieveNoticeOfWithdrawal(RetrieveNoticeOfWithdrawal retrieveNoticeOfWithdrawal) {
        this.retrieveNoticeOfWithdrawal = retrieveNoticeOfWithdrawal;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.RetrieveNoticeOfWithdrawal;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.retrieveinstructions.ChunkRequestHdr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "RetrieveNoticeOfWithdrawal", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveNoticeOfWithdrawal implements Serializable {
    @XmlElement(name = "accountIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String AccountIdentifier;

    @XmlElement(name = "instructionId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String InstructionId;

    @XmlElement(name = "chunkRequestHdr",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public ChunkRequestHdr chunkRequestHdr;

    public String getAccountIdentifier() {
        return AccountIdentifier;
    }
    public void setAccountIdentifier(String accountIdentifier) {
        AccountIdentifier = accountIdentifier;
    }

    public String getInstructionId() {
        return InstructionId;
    }
    public void setInstructionId(String instructionId) {
        InstructionId = instructionId;
    }

    public ChunkRequestHdr getChunkRequestHdr() {
        return chunkRequestHdr;
    }
    public void setChunkRequestHdr(ChunkRequestHdr chunkRequestHdr) {
        this.chunkRequestHdr = chunkRequestHdr;
    }
}

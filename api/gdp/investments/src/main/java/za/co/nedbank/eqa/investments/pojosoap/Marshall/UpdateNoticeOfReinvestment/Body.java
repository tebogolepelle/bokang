package za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfReinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "UpdateNoticeOfReinvestment",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public UpdateNoticeOfReinvestment updateNoticeOfReinvestment;

    public UpdateNoticeOfReinvestment getUpdateNoticeOfReinvestment() {
        return updateNoticeOfReinvestment;
    }

    public void setUpdateNoticeOfReinvestment(UpdateNoticeOfReinvestment updateNoticeOfReinvestment) {
        this.updateNoticeOfReinvestment = updateNoticeOfReinvestment;
    }
}

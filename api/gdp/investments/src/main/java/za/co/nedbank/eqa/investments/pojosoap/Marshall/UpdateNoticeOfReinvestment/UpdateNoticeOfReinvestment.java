package za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfReinvestment;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment.Instruction;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment.ReinvestAndConversionInstruction;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.ProductIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "UpdateNoticeOfReinvestment", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateNoticeOfReinvestment implements Serializable {
    @XmlElement(name = "accountIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String AccountIdentifier;

    @XmlElement(name = "instruction",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public Instruction instruction;

    @XmlElement(name = "reinvestAndConversionInstruction",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public ReinvestAndConversionInstruction reinvestAndConversionInstruction;

    @XmlElement(name = "termDeposit",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public TermDeposit termDeposit;

    @XmlElement(name = "personIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public PersonIdentification personIdentification;

    @XmlElement(name = "controlConditions",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String ControlConditions;

    @XmlElement(name = "productIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public ProductIdentification productIdentification;

    public String getAccountIdentifier() {
        return AccountIdentifier;
    }
    public void setAccountIdentifier(String accountIdentifier) {
        AccountIdentifier = accountIdentifier;
    }

    public Instruction getInstruction() {
        return instruction;
    }
    public void setInstruction(Instruction instruction) {
        this.instruction = instruction;
    }

    public ReinvestAndConversionInstruction getReinvestAndConversionInstruction() {
        return reinvestAndConversionInstruction;
    }
    public void setReinvestAndConversionInstruction(ReinvestAndConversionInstruction reinvestAndConversionInstruction) {
        this.reinvestAndConversionInstruction = reinvestAndConversionInstruction;
    }

    public TermDeposit getTermDeposit() {
        return termDeposit;
    }
    public void setTermDeposit(TermDeposit termDeposit) {
        this.termDeposit = termDeposit;
    }

    public PersonIdentification getPersonIdentification() {
        return personIdentification;
    }
    public void setPersonIdentification(PersonIdentification personIdentification) {
        this.personIdentification = personIdentification;
    }

    public String getControlConditions() {
        return ControlConditions;
    }
    public void setControlConditions(String controlConditions) {
        ControlConditions = controlConditions;
    }

    public ProductIdentification getProductIdentification() {
        return productIdentification;
    }
    public void setProductIdentification(ProductIdentification productIdentification) {
        this.productIdentification = productIdentification;
    }

}

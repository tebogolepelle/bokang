package za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfTaxFreeTransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "UpdateNoticeOfTaxFreeTransfer",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public UpdateNoticeOfTaxFreeTransfer updateNoticeOfTaxFreeTransfer;

    public UpdateNoticeOfTaxFreeTransfer getUpdateNoticeOfReinvestment() {
        return updateNoticeOfTaxFreeTransfer;
    }

    public void setUpdateNoticeOfTaxFreeTransfer(UpdateNoticeOfTaxFreeTransfer updateNoticeOfTaxFreeTransfer) {
        this.updateNoticeOfTaxFreeTransfer = updateNoticeOfTaxFreeTransfer;
    }
}

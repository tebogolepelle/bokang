package za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfWithdrawal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "UpdateNoticeOfWithdrawal",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public UpdateNoticeOfWithdrawal updateNoticeOfWithdrawal;

    public UpdateNoticeOfWithdrawal getUpdateNoticeOfWithdrawal() {
        return updateNoticeOfWithdrawal;
    }

    public void setUpdateNoticeOfWithdrawal(UpdateNoticeOfWithdrawal updateNoticeOfWithdrawal) {
        this.updateNoticeOfWithdrawal = updateNoticeOfWithdrawal;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.UpliftAccountDormacy;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.AccountIdentification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "UpliftAccountDormancy", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpliftAccountDormancy implements Serializable {
    @XmlElement(name = "status",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Status status;

    @XmlElement(name = "accountIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountIdentification accountIdentification;

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public AccountIdentification getAccountIdentification() {
        return accountIdentification;
    }
    public void setAccountIdentification(AccountIdentification accountIdentification) {
        this.accountIdentification = accountIdentification;
    }

}

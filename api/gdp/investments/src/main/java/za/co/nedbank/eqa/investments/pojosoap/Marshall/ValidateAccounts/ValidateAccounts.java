package za.co.nedbank.eqa.investments.pojosoap.Marshall.ValidateAccounts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ValidateAccounts", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidateAccounts implements Serializable {
    @XmlElement(name = "accountIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountIdentifier;

    @XmlElement(name = "newAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String NewAccount;

    public String getAccountIdentifier() {
        return AccountIdentifier;
    }
    public void setAccountIdentifier(String accountIdentifier) {
        AccountIdentifier = accountIdentifier;
    }

    public String getNewAccount() {
        return NewAccount;
    }
    public void setNewAccount(String newAccount) {
        NewAccount = newAccount;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofwithdrawal.InstructionAmount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "instruction", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)
public class Instruction implements Serializable {
    @XmlElement(name = "instructionTypeId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public InstructionTypeId instructionTypeId;

    @XmlElement(name = "instructionDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String InstructionDate;

    @XmlElement(name = "originatingChannelId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String OriginatingChannelId;

    @XmlElement(name = "instructionId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String InstructionId;

    @XmlElement(name = "referenceNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String ReferenceNumber;

    @XmlElement(name = "instructionAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public InstructionAmount instructionAmount;

    @XmlElement(name = "instructionEffectiveDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String InstructionEffectiveDate;

    @XmlElement(name = "interestReinvestmentInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String InterestReinvestmentInd;

    @XmlElement(name = "beneficiaryInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String BeneficiaryInd;

    @XmlElement(name = "recurringInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String RecurringInd;

    public InstructionTypeId getInstructionTypeId() {
        return instructionTypeId;
    }
    public void setInstructionTypeId(InstructionTypeId instructionTypeId) {
        this.instructionTypeId = instructionTypeId;
    }

    public String getInstructionDate() {
        return InstructionDate;
    }
    public void setInstructionDate(String instructionDate) {
        InstructionDate = instructionDate;
    }

    public String getOriginatingChannelId() {
        return OriginatingChannelId;
    }
    public void setOriginatingChannelId(String originatingChannelId) {
        OriginatingChannelId = originatingChannelId;
    }

    public String getInstructionId() {
        return InstructionId;
    }
    public void setInstructionId(String instructionId) {
        InstructionId = instructionId;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }
    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }

    public InstructionAmount getInstructionAmount() {
        return instructionAmount;
    }
    public void setInstructionAmount(InstructionAmount instructionAmount) {
        this.instructionAmount = instructionAmount;
    }

    public String getInstructionEffectiveDate() {
        return InstructionEffectiveDate;
    }
    public void setInstructionEffectiveDate(String instructionEffectiveDate) {
        InstructionEffectiveDate = instructionEffectiveDate;
    }

    public String getInterestReinvestmentInd() {
        return InterestReinvestmentInd;
    }
    public void setInterestReinvestmentInd(String interestReinvestmentInd) {
        InterestReinvestmentInd = interestReinvestmentInd;
    }

    public String getBeneficiaryInd() {
        return BeneficiaryInd;
    }
    public void setBeneficiaryInd(String beneficiaryInd) {
        BeneficiaryInd = beneficiaryInd;
    }

    public String getRecurringInd() {
        return RecurringInd;
    }
    public void setRecurringInd(String recurringInd) {
        RecurringInd = recurringInd;
    }
}

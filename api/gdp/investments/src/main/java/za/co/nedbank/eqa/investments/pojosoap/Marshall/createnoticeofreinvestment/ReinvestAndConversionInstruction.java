package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.CapitalDisposalAccount;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.InterestDisposalAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "reinvestAndConversionInstruction", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReinvestAndConversionInstruction implements Serializable {
    @XmlElement(name = "reinvestmentInstructionType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ReinvestmentInstructionType reinvestmentInstructionType;

    @XmlElement(name = "reinvestmentTerm",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ReinvestmentTerm;

    @XmlElement(name = "interestFrequencyTypeId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public InterestFrequencyTypeId interestFrequencyTypeId;

    @XmlElement(name = "totalReinvestmentAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TotalReinvestmentAmount totalReinvestmentAmount;

    @XmlElement(name = "partInterestAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PartInterestAmount partInterestAmount;

    @XmlElement(name = "capitalDisposalAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CapitalDisposalAccount capitalDisposalAccount;

    @XmlElement(name = "interestDisposalAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public InterestDisposalAccount interestDisposalAccount;

    public ReinvestmentInstructionType getReinvestmentInstructionType() {
        return reinvestmentInstructionType;
    }
    public void setReinvestmentInstructionType(ReinvestmentInstructionType reinvestmentInstructionType) {
        this.reinvestmentInstructionType = reinvestmentInstructionType;
    }

    public String getReinvestmentTerm() {
        return ReinvestmentTerm;
    }
    public void setReinvestmentTerm(String reinvestmentTerm) {
        ReinvestmentTerm = reinvestmentTerm;
    }

    public InterestFrequencyTypeId getInterestFrequencyTypeId() {
        return interestFrequencyTypeId;
    }
    public void setInterestFrequencyTypeId(InterestFrequencyTypeId interestFrequencyTypeId) {
        this.interestFrequencyTypeId = interestFrequencyTypeId;
    }

    public TotalReinvestmentAmount getTotalReinvestmentAmount() {
        return totalReinvestmentAmount;
    }
    public void setTotalReinvestmentAmount(TotalReinvestmentAmount totalReinvestmentAmount) {
        this.totalReinvestmentAmount = totalReinvestmentAmount;
    }

    public PartInterestAmount getPartInterestAmount() {
        return partInterestAmount;
    }
    public void setPartInterestAmount(PartInterestAmount partInterestAmount) {
        this.partInterestAmount = partInterestAmount;
    }

    public CapitalDisposalAccount getCapitalDisposalAccount() {
        return capitalDisposalAccount;
    }
    public void setCapitalDisposalAccount(CapitalDisposalAccount capitalDisposalAccount) {
        this.capitalDisposalAccount = capitalDisposalAccount;
    }

    public InterestDisposalAccount getInterestDisposalAccount() {
        return interestDisposalAccount;
    }
    public void setInterestDisposalAccount(InterestDisposalAccount interestDisposalAccount) {
        this.interestDisposalAccount = interestDisposalAccount;
    }
}

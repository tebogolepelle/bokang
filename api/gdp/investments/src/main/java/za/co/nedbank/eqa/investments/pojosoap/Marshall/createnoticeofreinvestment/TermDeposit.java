package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "termDeposit", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class TermDeposit implements Serializable {
    @XmlElement(name = "interestOption",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestOption;

    @XmlElement(name = "interestDay",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestDay;

    public String getInterestOption() {
        return InterestOption;
    }
    public void setInterestOption(String interestOption) {
        InterestOption = interestOption;
    }

    public String getInterestDay() {
        return InterestDay;
    }
    public void setInterestDay(String interestDay) {
        InterestDay = interestDay;
    }
}

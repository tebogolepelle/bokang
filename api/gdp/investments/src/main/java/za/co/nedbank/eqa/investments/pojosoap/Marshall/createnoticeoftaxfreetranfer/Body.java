package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "CreateNoticeOfTaxfreeTransfer",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CreateNoticeOfTaxfreeTransfer createNoticeOfTaxfreeTransfer;

    public CreateNoticeOfTaxfreeTransfer getCreateNoticeOfTaxfreeTransfer() {
        return createNoticeOfTaxfreeTransfer;
    }
    public void setCreateNoticeOfTaxfreeTransfer(CreateNoticeOfTaxfreeTransfer createNoticeOfTaxfreeTransfer) {
        this.createNoticeOfTaxfreeTransfer = createNoticeOfTaxfreeTransfer;
    }
}

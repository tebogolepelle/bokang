package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "CreateNoticeOfTaxfreeTransfer", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateNoticeOfTaxfreeTransfer implements Serializable {
    @XmlElement(name = "accountIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountIdentifier;

    @XmlElement(name = "instruction",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Instruction instruction;

    @XmlElement(name = "transferInstruction",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TransferInstruction transferInstruction;

    @XmlElement(name = "depositProfile",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String DepositProfile;

    @XmlElement(name = "taxFreeSavings",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String TaxFreeSavings;

    @XmlElement(name = "termDeposit",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TermDeposit termDeposit;

    @XmlElement(name = "personIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PersonIdentification personIdentification;

    @XmlElement(name = "controlConditions",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ControlConditions;

    public String getAccountIdentifier() {
        return AccountIdentifier;
    }
    public void setAccountIdentifier(String accountIdentifier) {
        AccountIdentifier = accountIdentifier;
    }

    public Instruction getInstruction() {
        return instruction;
    }
    public void setInstruction(Instruction instruction) {
        this.instruction = instruction;
    }

    public TransferInstruction getTransferInstruction() {
        return transferInstruction;
    }
    public void setTransferInstruction(TransferInstruction transferInstruction) {
        this.transferInstruction = transferInstruction;
    }

    public String getDepositProfile() {
        return DepositProfile;
    }
    public void setDepositProfile(String depositProfile) {
        DepositProfile = depositProfile;
    }

    public String getTaxFreeSavings() {
        return TaxFreeSavings;
    }
    public void setTaxFreeSavings(String taxFreeSavings) {
        TaxFreeSavings = taxFreeSavings;
    }

    public TermDeposit getTermDeposit() {
        return termDeposit;
    }
    public void setTermDeposit(TermDeposit termDeposit) {
        this.termDeposit = termDeposit;
    }

    public PersonIdentification getPersonIdentification() {
        return personIdentification;
    }
    public void setPersonIdentification(PersonIdentification personIdentification) {
        this.personIdentification = personIdentification;
    }

    public String getControlConditions() {
        return ControlConditions;
    }
    public void setControlConditions(String controlConditions) {
        ControlConditions = controlConditions;
    }
}

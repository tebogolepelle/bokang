package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "organizationDetail", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationDetail implements Serializable {
    @XmlElement(name = "organizationName",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String OrganizationName;

    @XmlElement(name = "orgRegistrationNo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String OrgRegistrationNo;

    @XmlElement(name = "vatRegistrationNo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String VatRegistrationNo;

    @XmlElement(name = "email",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String Email;

    public String getOrganizationName() {
        return OrganizationName;
    }
    public void setOrganizationName(String organizationName) {
        OrganizationName = organizationName;
    }

    public String getOrgRegistrationNo() {
        return OrgRegistrationNo;
    }
    public void setOrgRegistrationNo(String orgRegistrationNo) {
        OrgRegistrationNo = orgRegistrationNo;
    }

    public String getVatRegistrationNo() {
        return VatRegistrationNo;
    }
    public void setVatRegistrationNo(String vatRegistrationNo) {
        VatRegistrationNo = vatRegistrationNo;
    }

    public String getEmail() {
        return Email;
    }
    public void setEmail(String email) {
        Email = email;
    }
}

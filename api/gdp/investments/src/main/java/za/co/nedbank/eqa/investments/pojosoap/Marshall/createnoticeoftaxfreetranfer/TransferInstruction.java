package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "transferInstruction", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransferInstruction implements Serializable {
    @XmlElement(name = "transferAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TransferAmount transferAmount;

    @XmlElement(name = "transferEffectiveDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String TransferEffectiveDate;

    @XmlElement(name = "organizationDetail",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public OrganizationDetail organizationDetail;

    @XmlElement(name = "rejectionInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String RejectionInd;

    public TransferAmount getTransferAmount() {
        return transferAmount;
    }
    public void setTransferAmount(TransferAmount transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTransferEffectiveDate() {
        return TransferEffectiveDate;
    }
    public void setTransferEffectiveDate(String transferEffectiveDate) {
        TransferEffectiveDate = transferEffectiveDate;
    }

    public OrganizationDetail getTrOrganizationDetail() {
        return organizationDetail;
    }
    public void setOrganizationDetail(OrganizationDetail organizationDetail) {
        this.organizationDetail = organizationDetail;
    }

    public String getRejectionInd() {
        return RejectionInd;
    }
    public void setRejectionInd(String rejectionInd) {
        RejectionInd = rejectionInd;
    }
}

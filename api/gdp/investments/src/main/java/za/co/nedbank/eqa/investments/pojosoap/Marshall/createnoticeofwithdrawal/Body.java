package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofwithdrawal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "CreateNoticeOfWithdrawal",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CreateNoticeOfWithdrawal createNoticeOfWithdrawal;

    public CreateNoticeOfWithdrawal getCreateNoticeOfWithdrawal() {
        return createNoticeOfWithdrawal;
    }
    public void setCreateNoticeOfWithdrawal(CreateNoticeOfWithdrawal createNoticeOfWithdrawal) {
        this.createNoticeOfWithdrawal = createNoticeOfWithdrawal;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofwithdrawal;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfWithdrawal.ClosureReasonType;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfWithdrawal.EarlyReleaseReason;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfWithdrawal.FundsDestination;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "noticeInstruction", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class NoticeInstruction implements Serializable {
    @XmlElement(name = "noticeInstructionType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public NoticeInstructionType noticeInstructionType;

    @XmlElement(name = "transferOutInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String TransferOutInd;

    @XmlElement(name = "fundsDestination",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public FundsDestination fundsDestination;

    @XmlElement(name = "destinationProductCode",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String DestinationProductCode;

    @XmlElement(name = "closureReasonType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ClosureReasonType closureReasonType;

    @XmlElement(name = "earlyReleaseReason",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public EarlyReleaseReason earlyReleaseReason;

    @XmlElement(name = "recurringNoticeFrequency",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public RecurringNoticeFrequency recurringNoticeFrequency;

    @XmlElement(name = "recurringStartDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String RecurringStartDate;

    @XmlElement(name = "recurringEndDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String RecurringEndDate;

    public NoticeInstructionType getNoticeInstructionType() {
        return noticeInstructionType;
    }
    public void setNoticeInstructionType(NoticeInstructionType noticeInstructionType) {
        this.noticeInstructionType = noticeInstructionType;
    }

    public String getTransferOutInd() {
        return TransferOutInd;
    }
    public void setTransferOutInd(String transferOutInd) {
        TransferOutInd = transferOutInd;
    }

    public RecurringNoticeFrequency getRecurringNoticeFrequency() {
        return recurringNoticeFrequency;
    }
    public void setRecurringNoticeFrequency(RecurringNoticeFrequency recurringNoticeFrequency) {
        this.recurringNoticeFrequency = recurringNoticeFrequency;
    }

    public String getRecurringStartDate() {
        return RecurringStartDate;
    }
    public void setRecurringStartDate(String recurringStartDate) {
        RecurringStartDate = recurringStartDate;
    }

    public String getRecurringEndDate() {
        return RecurringEndDate;
    }
    public void setRecurringEndDate(String recurringEndDate) {
        RecurringEndDate = recurringEndDate;
    }

    public FundsDestination getFundsDestination() {
        return fundsDestination;
    }
    public void setFundsDestination(FundsDestination fundsDestination) {
        this.fundsDestination = fundsDestination;
    }

    public String getDestinationProductCode() {
        return DestinationProductCode;
    }
    public void setDestinationProductCode(String destinationProductCode) {
        DestinationProductCode = destinationProductCode;
    }

    public ClosureReasonType getClosureReasonType() {
        return closureReasonType;
    }
    public void setClosureReasonType(ClosureReasonType closureReasonType) {
        this.closureReasonType = closureReasonType;
    }

    public EarlyReleaseReason getEarlyReleaseReason() {
        return earlyReleaseReason;
    }
    public void setEarlyReleaseReason(EarlyReleaseReason earlyReleaseReason) {
        this.earlyReleaseReason = earlyReleaseReason;
    }

}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofwithdrawal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "noticeInstructionType", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class NoticeInstructionType implements Serializable{
        @XmlElement(name = "code",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
        public String Code;

        public String getCode() {
            return Code;
        }
        public void setCode(String code) {
            Code = code;
        }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeofreinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "DeleteNoticeOfReinvestment",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public DeleteNoticeOfReinvestment deleteNoticeOfReinvestment;

    public DeleteNoticeOfReinvestment getDeleteNoticeOfReinvestment() {
        return deleteNoticeOfReinvestment;
    }
    public void setDeleteNoticeOfReinvestment(DeleteNoticeOfReinvestment deleteNoticeOfReinvestment) {
        this.deleteNoticeOfReinvestment = deleteNoticeOfReinvestment;
    }
}

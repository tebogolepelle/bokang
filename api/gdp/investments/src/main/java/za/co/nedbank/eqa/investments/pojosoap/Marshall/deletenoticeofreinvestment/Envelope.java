package za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeofreinvestment;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.Header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Envelope", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Envelope implements Serializable {
    private static final long serialVersionUID = 5641077979266998859L;

    @XmlElement(name = "Header",namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    public Header header;

    @XmlElement(name = "Body",namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    public Body body;

    public Body getBody() {
        return body;
    }
    public void setBody(Body body) {
        this.body = body;
    }

    public Header getHeader() {
        return header;
    }
    public void setHeader(Header header) {
        this.header = header;
    }
}

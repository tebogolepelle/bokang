package za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeoftaxfreetransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "DeleteNoticeOfTaxFreeTransfer",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public DeleteNoticeOfTaxFreeTransfer deleteNoticeOfTaxFreeTransfer;

    public DeleteNoticeOfTaxFreeTransfer getDeleteNoticeOfTaxFreeTransfer() {
        return deleteNoticeOfTaxFreeTransfer;
    }
    public void setDeleteNoticeOfTaxFreeTransfer(DeleteNoticeOfTaxFreeTransfer deleteNoticeOfTaxFreeTransfer) {
        this.deleteNoticeOfTaxFreeTransfer = deleteNoticeOfTaxFreeTransfer;
    }
}

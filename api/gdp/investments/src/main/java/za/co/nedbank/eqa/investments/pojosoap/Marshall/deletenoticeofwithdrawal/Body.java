package za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeofwithdrawal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "DeleteNoticeOfWithdrawal",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public DeleteNoticeOfWithdrawal deleteNoticeOfWithdrawal;

    public DeleteNoticeOfWithdrawal getDeleteNoticeOfWithdrawal() {
        return deleteNoticeOfWithdrawal;
    }
    public void setDeleteNoticeOfWithdrawal(DeleteNoticeOfWithdrawal deleteNoticeOfWithdrawal) {
        this.deleteNoticeOfWithdrawal = deleteNoticeOfWithdrawal;
    }
}

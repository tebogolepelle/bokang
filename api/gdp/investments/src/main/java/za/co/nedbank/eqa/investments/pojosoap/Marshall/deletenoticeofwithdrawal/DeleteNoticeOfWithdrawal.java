package za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeofwithdrawal;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "DeleteNoticeOfWithdrawal", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeleteNoticeOfWithdrawal implements Serializable {
    @XmlElement(name = "accountIdentifier",namespace = "hhttp://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String AccountIdentifier;

    @XmlElement(name = "instructionId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String InstructionId;

    @XmlElement(name = "personIdentification",namespace = "hhttp://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public PersonIdentification personIdentification;

    public String getAccountIdentifier() {
        return AccountIdentifier;
    }
    public void setAccountIdentifier(String accountIdentifier) {
        AccountIdentifier = accountIdentifier;
    }

    public String getInstructionId() {
        return InstructionId;
    }
    public void setInstructionId(String instructionId) {
        InstructionId = instructionId;
    }

    public PersonIdentification getPersonIdentification() {
        return personIdentification;
    }
    public void setPersonIdentification(PersonIdentification personIdentification) {
        this.personIdentification = personIdentification;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.getmaintainableaccountdetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "productCategory")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductCategory implements Serializable {
    @XmlElement(name = "productCategoryID",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ProductCategoryID;

    @XmlElement(name = "productCategoryName",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ProductCategoryName;

    @XmlElement(name = "productCategoryLevel",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ProductCategoryLevel;

    public String getProductCategoryID() {
        return ProductCategoryID;
    }
    public void setProductCategoryID(String productCategoryID) {
        ProductCategoryID = productCategoryID;
    }

    public String getProductCategoryName() {
        return ProductCategoryName;
    }
    public void setProductCategoryName(String productCategoryName) {
        ProductCategoryName = productCategoryName;
    }

    public String getproductCategoryLevel() {
        return ProductCategoryLevel;
    }
    public void setproductCategoryLevel(String productCategoryLevel) {
        ProductCategoryLevel = productCategoryLevel;
    }
}

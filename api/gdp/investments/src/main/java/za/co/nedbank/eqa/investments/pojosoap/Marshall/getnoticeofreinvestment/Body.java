package za.co.nedbank.eqa.investments.pojosoap.Marshall.getnoticeofreinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "GetNoticeOfReinvestment",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public GetNoticeOfReinvestment getNoticeOfReinvestment;

    public GetNoticeOfReinvestment getGetNoticeOfReinvestment() {
        return getNoticeOfReinvestment;
    }
    public void setGetNoticeOfReinvestment(GetNoticeOfReinvestment getNoticeOfReinvestment) {
        this.getNoticeOfReinvestment = getNoticeOfReinvestment;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.getnoticeofreinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "GetNoticeOfReinvestment", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetNoticeOfReinvestment implements Serializable {
    @XmlElement(name = "accountIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountIdentifier;

    @XmlElement(name = "instructionId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InstructionId;

    public String getAccountIdentifier() {
        return AccountIdentifier;
    }
    public void setAccountIdentifier(String accountIdentifier) {
        AccountIdentifier = accountIdentifier;}

    public String getInstructionId() {
        return InstructionId;
    }
    public void setInstructionId(String instructionId) {
        InstructionId = instructionId;}
}

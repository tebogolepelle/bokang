package za.co.nedbank.eqa.investments.pojosoap.Marshall.getnoticeoftaxfreetransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "GetNoticeOfTaxFreeTransfer",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public GetNoticeOfTaxFreeTransfer getNoticeOfTaxFreeTransfer;

    public GetNoticeOfTaxFreeTransfer getGetNoticeOfTaxFreeTransfer() {
        return getNoticeOfTaxFreeTransfer;
    }
    public void setGetNoticeOfTaxFreeTransfer(GetNoticeOfTaxFreeTransfer getNoticeOfTaxFreeTransfer) {
        this.getNoticeOfTaxFreeTransfer = getNoticeOfTaxFreeTransfer;
    }
}

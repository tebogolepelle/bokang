package za.co.nedbank.eqa.investments.pojosoap.Marshall.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "ContextInfo", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContextInfo implements Serializable {
    @XmlElement(name = "ProcessContextId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String ProcessContextId;

    @XmlElement(name = "ExecutionContextId",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public String ExecutionContextId;


    public String getProcessContextId() {
        return ProcessContextId;
    }

    public void setProcessContextId(String processContextId) {
        ProcessContextId = processContextId;
    }

    public String getExecutionContextId() {
        return ExecutionContextId;
    }

    public void setExecutionContextId(String executionContextId) {
        ExecutionContextId = executionContextId;
    }
}

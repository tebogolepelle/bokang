package za.co.nedbank.eqa.investments.pojosoap.Marshall.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "EnterpriseContext", namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnterpriseContext implements Serializable {
    @XmlElement(name = "ContextInfo",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public ContextInfo contextInfo;
    @XmlElement(name = "RequestOriginator",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public RequestOriginator requestOriginator;
    @XmlElement(name = "InstrumentationInfo",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    public InstrumentationInfo instrumentationInfo;

    public ContextInfo getContextInfo() {
        return contextInfo;
    }

    public void setContextInfo(ContextInfo contextInfo) {
        this.contextInfo = contextInfo;
    }

    public RequestOriginator getRequestOriginator() {
        return requestOriginator;
    }

    public void setRequestOriginator(RequestOriginator requestOriginator) {
        this.requestOriginator = requestOriginator;
    }

    public InstrumentationInfo getInstrumentationInfo() {
        return instrumentationInfo;
    }

    public void setInstrumentationInfo(InstrumentationInfo instrumentationInfo) {
        this.instrumentationInfo = instrumentationInfo;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Security", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
@XmlAccessorType(XmlAccessType.FIELD)
public class Security implements Serializable {
    @XmlElement(name = "UsernameToken",namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    public UsernameToken usernameToken;

    public UsernameToken getUsernameToken() {
        return usernameToken;
    }

    public void setUsernameToken(UsernameToken usernameToken) {
        this.usernameToken = usernameToken;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "interestDisposalAccount")
public class InterestDisposalAccount implements Serializable {
    @XmlElement(name = "accountType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountType;

    @XmlElement(name = "accountNumber",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountNumber;

    @XmlElement(name = "accountSortCode",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AccountSortCode;

    public String getAccountType() {
        return AccountType;
    }
    public void setAccountType(String accountType) {
        AccountType = accountType;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getAccountSortCode() {
        return AccountSortCode;
    }
    public void setAccountSortCode(String accountSortCode) {
        AccountSortCode = accountSortCode;
    }
}

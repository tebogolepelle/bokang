package za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "investmentAccount")
@XmlAccessorType(XmlAccessType.FIELD)
public class InvestmentAccount implements Serializable {
    @XmlElement(name = "accountIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountIdentification accountIdentification;

    @XmlElement(name = "termDeposit",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TermDeposit termDeposit;

    @XmlElement(name = "termDepositSummary",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String TermDepositSummary;

    @XmlElement(name = "taxFreeSavings",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String TaxFreeSavings;

    @XmlElement(name = "depositProfile",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String DepositProfile;

    @XmlElement(name = "rates",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Rates rates;

    public AccountIdentification getAccountIdentification() {
        return accountIdentification;
    }
    public void setAccountIdentification(AccountIdentification accountIdentification) {
        this.accountIdentification = accountIdentification;
    }

    public TermDeposit getTermDeposit() {
        return termDeposit;
    }
    public void setTermDeposit(TermDeposit termDeposit) {
        this.termDeposit = termDeposit;
    }

    public String getTermDepositSummary() {
        return TermDepositSummary;
    }
    public void setTermDepositSummary(String termDepositSummary) {
        TermDepositSummary = termDepositSummary;
    }

    public String getTaxFreeSavings() {
        return TaxFreeSavings;
    }
    public void setTaxFreeSavings(String taxFreeSavings) {
        TaxFreeSavings = taxFreeSavings;
    }

    public String getDepositProfile() {
        return DepositProfile;
    }
    public void setDepositProfile(String depositProfile) {
        DepositProfile = depositProfile;
    }

    public Rates getRates() {
        return rates;
    }
    public void setRates(Rates rates) {
        this.rates = rates;
    }
}

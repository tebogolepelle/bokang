package za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "MaintainAccountDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class MaintainAccountDetails implements Serializable {
    @XmlElement(name = "productInfo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ProductInfo productInfo;

    @XmlElement(name = "personIdentification",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public List<PersonIdentification> personIdentification;

    @XmlElement(name = "accountInfo",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountInfo accountInfo;

    @XmlElement(name = "accountDetails",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountDetails accountDetails;

    public ProductInfo getProductInfo() {
        return productInfo;
    }
    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public List<PersonIdentification> getPersonIdentification() {
        return personIdentification;
    }
    public void setPersonIdentification(List<PersonIdentification> personIdentification) {
        this.personIdentification = personIdentification;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }
    public void setAccountInfo(AccountInfo accountInfo){
        this.accountInfo = accountInfo;
    }

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }
    public void setAccountDetails(AccountDetails accountDetails){
        this.accountDetails = accountDetails;
    }
}

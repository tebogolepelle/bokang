package za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "personIdentification", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonIdentification implements Serializable {
    @XmlElement(name = "personIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String PersonIdentifier;

    @XmlElement(name = "personIDType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public List<PersonIDType> personIDType;

    public String getPersonIdentifier() {
        return PersonIdentifier;
    }
    public void setPersonIdentifier(String personIdentifier) {
        PersonIdentifier = personIdentifier;
    }

    public List<PersonIDType> getPersonIDType() {
        return personIDType;
    }
    public void setPersonIDType(List<PersonIDType> personIDType) {
        this.personIDType = personIDType;
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "rates")
@XmlAccessorType(XmlAccessType.FIELD)
public class Rates implements Serializable {
    @XmlElement(name = "rateType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public RateType rateType;

    @XmlElement(name = "rateValue",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String RateValue;

    @XmlElement(name = "ratePeriod",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public RatePeriod ratePeriod;

    public RateType getRateType() {
        return rateType;
    }
    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public String getRateValue() {
        return RateValue;
    }
    public void setRateValue(String rateValue) {
        RateValue = rateValue;
    }

    public RatePeriod getRatePeriod() {
        return ratePeriod;
    }
    public void setRatePeriod(RatePeriod ratePeriod) {
        this.ratePeriod = ratePeriod;
    }
}

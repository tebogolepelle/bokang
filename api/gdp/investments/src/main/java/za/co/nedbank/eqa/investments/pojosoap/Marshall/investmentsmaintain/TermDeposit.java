package za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "termDeposit")
@XmlAccessorType(XmlAccessType.FIELD)
public class TermDeposit implements Serializable {
    @XmlElement(name = "capitalDisposalAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CapitalDisposalAccount capitalDisposalAccount;

    @XmlElement(name = "interestDisposalAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public InterestDisposalAccount interestDisposalAccount;

    @XmlElement(name = "originatingChannelId",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String OriginatingChannelId;

    @XmlElement(name = "pledgedCode",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PledgedCode pledgedCode;

    @XmlElement(name = "pledgeAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String PledgeAccount;

    @XmlElement(name = "interestDay",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestDay;

    @XmlElement(name = "goalCapturedInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String GoalCapturedInd;

    @XmlElement(name = "optimumInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String OptimumInd;

    @XmlElement(name = "term",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Term term;

    @XmlElement(name = "originalIntendedCapitalAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public OriginalIntendedCapitalAmount originalIntendedCapitalAmount;

    @XmlElement(name = "openReason",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public OpenReason openReason;

    @XmlElement(name = "sourceOfFundsType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public SourceOfFundsType sourceOfFundsType;

    @XmlElement(name = "penaltyInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PenaltyInd penaltyInd;

    @XmlElement(name = "penaltyAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PenaltyAmount penaltyAmount;

    @XmlElement(name = "initialDepositAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public InitialDepositAccount initialDepositAccount;

    @XmlElement(name = "interestReinvestmentInd",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestReinvestmentInd;

    @XmlElement(name = "maturityDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String MaturityDate;

    @XmlElement(name = "otherStatus",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String OtherStatus;

    @XmlElement(name = "otherDescription",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String OtherDescription;

    @XmlElement(name = "interestOption",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestOption;

    @XmlElement(name = "interestRateCalculationRatio",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestRateCalculationRatio;

    public CapitalDisposalAccount getCapitalDisposalAccount() {
        return capitalDisposalAccount;
    }
    public void setCapitalDisposalAccount(CapitalDisposalAccount capitalDisposalAccount) {
        this.capitalDisposalAccount = capitalDisposalAccount;
    }

    public InterestDisposalAccount getInterestDisposalAccount() {
        return interestDisposalAccount;
    }
    public void setInterestDisposalAccount(InterestDisposalAccount interestDisposalAccount) {
        this.interestDisposalAccount = interestDisposalAccount;
    }

    public String getOriginatingChannelId() {
        return OriginatingChannelId;
    }
    public void setOriginatingChannelId(String originatingChannelId) {
        OriginatingChannelId = originatingChannelId;
    }

    public PledgedCode getPledgedCode() {
        return pledgedCode;
    }
    public void setPledgedCode(PledgedCode pledgedCode) {
        this.pledgedCode = pledgedCode;
    }

    public String getPledgeAccount() {
        return PledgeAccount;
    }
    public void setPledgeAccount(String pledgeAccount) {
        PledgeAccount = pledgeAccount;
    }

    public String getInterestDay() {
        return InterestDay;
    }
    public void setInterestDay(String interestDay) {
        InterestDay = interestDay;
    }

    public String getOptimumInd() {
        return OptimumInd;
    }
    public void setOptimumInd(String optimumInd) {
        OptimumInd = optimumInd;
    }

    public String getInterestRateCalculationRatio() {
        return InterestRateCalculationRatio;
    }
    public void setInterestRateCalculationRatio(String interestRateCalculationRatio) {
        InterestRateCalculationRatio = interestRateCalculationRatio;
    }

    public OpenReason getOpenReason() {
        return openReason;
    }
    public void setOpenReason(OpenReason openReason) {
        this.openReason = openReason;
    }

    public SourceOfFundsType getSourceOfFundsType() {
        return sourceOfFundsType;
    }
    public void setSourceOfFundsType(SourceOfFundsType sourceOfFundsType) {
        this.sourceOfFundsType = sourceOfFundsType;
    }

    public PenaltyInd getPenaltyInd() {
        return penaltyInd;
    }
    public void setPenaltyInd(PenaltyInd penaltyInd) {
        this.penaltyInd = penaltyInd;
    }

    public PenaltyAmount getPenaltyAmount() {
        return penaltyAmount;
    }
    public void setPenaltyAmount(PenaltyAmount penaltyAmount) {
        this.penaltyAmount = penaltyAmount;
    }

    public OriginalIntendedCapitalAmount getOriginalIntendedCapitalAmount() {
        return originalIntendedCapitalAmount;
    }
    public void setOriginalIntendedCapitalAmount(OriginalIntendedCapitalAmount originalIntendedCapitalAmount) {
        this.originalIntendedCapitalAmount = originalIntendedCapitalAmount;
    }

    public Term getTerm() {
        return term;
    }
    public void setTerm(Term term) {
        this.term = term;
    }

    public InitialDepositAccount getInitialDepositAccount() {
        return initialDepositAccount;
    }
    public void setInitialDepositAccount(InitialDepositAccount term) {
        this.initialDepositAccount = initialDepositAccount;
    }

    public String getInterestReinvestmentInd() {
        return InterestReinvestmentInd;
    }
    public void setInterestReinvestmentInd(String interestReinvestmentInd) {
        InterestReinvestmentInd = interestReinvestmentInd;
    }

    public String getMaturityDate() {
        return MaturityDate;
    }
    public void setMaturityDate(String maturityDate) {
        MaturityDate = maturityDate;
    }

    public String getOtherStatus() {
        return OtherStatus;
    }
    public void setOtherStatus(String otherStatus) {
        OtherStatus = otherStatus;
    }

    public String getOtherDescription() {
        return OtherDescription;
    }
    public void setOtherDescription(String otherDescription) {
        OtherDescription = otherDescription;
    }

    public String getInterestOption() {
        return InterestOption;
    }
    public void setInterestOption(String interestOption) {
        InterestOption = interestOption;
    }

    public String getGoalCapturedInd() {
        return GoalCapturedInd;
    }
    public void setGoalCapturedInd(String goalCapturedInd) {
        GoalCapturedInd = goalCapturedInd;
    }

}

package za.co.nedbank.eqa.investments.pojosoap.Marshall.retrieveinstructions;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "Body")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body implements Serializable {
    @XmlElement(name = "RetrieveInstructions",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public RetrieveInstructions retrieveInstructions;

    public RetrieveInstructions getRetrieveInstructions() {
        return retrieveInstructions;
    }

    public void setRetrieveInstructions(RetrieveInstructions retrieveInstructions) {
        this.retrieveInstructions = retrieveInstructions;
    }
}

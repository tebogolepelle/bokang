package za.co.nedbank.eqa.investments.pojosoap.Marshall.retrieveinstructions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "chunkRequestHdr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChunkRequestHdr implements Serializable {
    @XmlElement(name = "continuationValue",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String ContinuationValue;

    @XmlElement(name = "maxRecords",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v2")
    public String MaxRecords;

    public String getMaxRecords() {
        return MaxRecords;
    }
    public void setMaxRecords(String maxRecords) {
        MaxRecords = maxRecords;
    }

    public String getContinuationValue() {
        return ContinuationValue;
    }
    public void setContinuationValue(String continuationValue) {
        ContinuationValue = continuationValue;
    }
}

@XmlSchema(
        namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1",
        xmlns = {
                @XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="ent", namespaceURI="http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext"),
                @XmlNs(prefix="v1", namespaceURI="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1"),
                @XmlNs(prefix="ec2", namespaceURI = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext"),
                @XmlNs(prefix = "wsse", namespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
        },
        elementFormDefault = XmlNsForm.QUALIFIED)
package za.co.nedbank.eqa.investments.pojosoap.Marshall.retrieveinstructions;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment.Envelope;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment.TermDeposit;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.*;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class CreateNoticeOfReinvestmentPayLoad {
    public String createNoticeOfReinvestment() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");
        personIDType.setDescription("Enterprise Party Number");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("110019678118");
        personIdentification.setPersonIDType(personIDTypeList);

        ProductIdentification productIdentification = new ProductIdentification();
        productIdentification.setProductIdentifier("1126");
        productIdentification.setproductIDType("Investments");

        TermDeposit termDeposit = new TermDeposit();
        termDeposit.setInterestOption("1");
        termDeposit.setInterestDay("1");

        InterestDisposalAccount interestDisposalAccount = new InterestDisposalAccount();
        interestDisposalAccount.setAccountType("SA");
        interestDisposalAccount.setAccountNumber("2102225186");
        interestDisposalAccount.setAccountSortCode("0");

        CapitalDisposalAccount capitalDisposalAccount = new CapitalDisposalAccount();
        capitalDisposalAccount.setAccountType("SA");
        capitalDisposalAccount.setAccountNumber("2102225186");
        capitalDisposalAccount.setAccountSortCode("0");

        PartInterestAmount partInterestAmount = new PartInterestAmount();
        partInterestAmount.setAmount("0");
        partInterestAmount.setCurrency("ZAR");

        TotalReinvestmentAmount totalReinvestmentAmount = new TotalReinvestmentAmount();
        totalReinvestmentAmount.setAmount("6587.36");
        totalReinvestmentAmount.setCurrency("ZAR");

        InterestFrequencyTypeId interestFrequencyTypeId = new InterestFrequencyTypeId();
        interestFrequencyTypeId.setCode("");
        interestFrequencyTypeId.setDescription("M");

        ReinvestmentInstructionType reinvestmentInstructionType = new ReinvestmentInstructionType();
        reinvestmentInstructionType.setCode("");
        reinvestmentInstructionType.setDescription("Fulli");

        ReinvestAndConversionInstruction reinvestAndConversionInstruction = new ReinvestAndConversionInstruction();
        reinvestAndConversionInstruction.setReinvestmentInstructionType(reinvestmentInstructionType);
        reinvestAndConversionInstruction.setReinvestmentTerm("01");
        reinvestAndConversionInstruction.setInterestFrequencyTypeId(interestFrequencyTypeId);
        reinvestAndConversionInstruction.setTotalReinvestmentAmount(totalReinvestmentAmount);
        reinvestAndConversionInstruction.setPartInterestAmount(partInterestAmount);
        reinvestAndConversionInstruction.setCapitalDisposalAccount(capitalDisposalAccount);
        reinvestAndConversionInstruction.setInterestDisposalAccount(interestDisposalAccount);

        InstructionTypeId instructionTypeId = new InstructionTypeId();
        instructionTypeId.setDescription("Same");

        Instruction instruction = new Instruction();
        instruction.setInstructionTypeId(instructionTypeId);
        instruction.setInstructionDate("2020-02-05T01:30:21");
        instruction.setOriginatingChannelId("B");

        CreateNoticeOfReinvestment createNoticeOfReinvestment = new CreateNoticeOfReinvestment();
        createNoticeOfReinvestment.setAccountIdentifier("39586439993");
        createNoticeOfReinvestment.setInstruction(instruction);
        createNoticeOfReinvestment.setReinvestAndConversionInstruction(reinvestAndConversionInstruction);
        createNoticeOfReinvestment.setTermDeposit(termDeposit);
        createNoticeOfReinvestment.setProductIdentification(productIdentification);
        createNoticeOfReinvestment.setPersonIdentification(personIdentification);
        createNoticeOfReinvestment.setControlConditions("");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setCreateNoticeOfReinvestment(createNoticeOfReinvestment);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

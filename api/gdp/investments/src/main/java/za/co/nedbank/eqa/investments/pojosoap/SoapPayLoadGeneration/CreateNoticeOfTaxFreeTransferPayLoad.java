package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.CapitalDisposalAccount;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIDType;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class CreateNoticeOfTaxFreeTransferPayLoad {
    public String createNoticeOfTaxFreeTransfer() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("600000303724");
        personIdentification.setPersonIDType(personIDTypeList);

        CapitalDisposalAccount capitalDisposalAccount = new CapitalDisposalAccount();
        capitalDisposalAccount.setAccountType("");
        capitalDisposalAccount.setAccountNumber("478059996");
        capitalDisposalAccount.setAccountSortCode("250655");

        TermDeposit termDeposit = new TermDeposit();
        termDeposit.setCapitalDisposalAccount(capitalDisposalAccount);

        OrganizationDetail organizationDetail = new OrganizationDetail();
        organizationDetail.setOrganizationName("CAPITEC");
        organizationDetail.setOrgRegistrationNo("1234/12345/123");
        organizationDetail.setVatRegistrationNo("9876543210");
        organizationDetail.setEmail("NBHEKISISA@GMAIL.COM");

        TransferAmount transferAmount = new TransferAmount();
        transferAmount.setAmount("500");

        TransferInstruction transferInstruction = new TransferInstruction();
        transferInstruction.setTransferAmount(transferAmount);
        transferInstruction.setTransferEffectiveDate("2020-03-04");
        transferInstruction.setOrganizationDetail(organizationDetail);
        transferInstruction.setRejectionInd("N");

        Instruction instruction = new Instruction();
        instruction.setInstructionId("TO");
        instruction.setReferenceNumber("0123456789");

        CreateNoticeOfTaxfreeTransfer createNoticeOfTaxfreeTransfer = new CreateNoticeOfTaxfreeTransfer();
        createNoticeOfTaxfreeTransfer.setAccountIdentifier("448579881");
        createNoticeOfTaxfreeTransfer.setInstruction(instruction);
        createNoticeOfTaxfreeTransfer.setTransferInstruction(transferInstruction);
        createNoticeOfTaxfreeTransfer.setDepositProfile("");
        createNoticeOfTaxfreeTransfer.setTaxFreeSavings("");
        createNoticeOfTaxfreeTransfer.setTermDeposit(termDeposit);
        createNoticeOfTaxfreeTransfer.setPersonIdentification(personIdentification);
        createNoticeOfTaxfreeTransfer.setControlConditions("");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setCreateNoticeOfTaxfreeTransfer(createNoticeOfTaxfreeTransfer);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

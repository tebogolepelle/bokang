package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeofreinvestment.Body;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeofreinvestment.DeleteNoticeOfReinvestment;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.deletenoticeofreinvestment.Envelope;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIDType;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class DeleteNoticeOfReinvestmentPayLoad {
    public String deleteNoticeOfReinvestment() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("600000303724");
        personIdentification.setPersonIDType(personIDTypeList);

        DeleteNoticeOfReinvestment deleteNoticeOfReinvestment = new DeleteNoticeOfReinvestment();
        deleteNoticeOfReinvestment.setAccountIdentifier("155556749997");
        deleteNoticeOfReinvestment.setInstructionId("Same");
        deleteNoticeOfReinvestment.setPersonIdentification(personIdentification);

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setDeleteNoticeOfReinvestment(deleteNoticeOfReinvestment);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

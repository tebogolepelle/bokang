package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.body.Body;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.*;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class InvestmentsAccountMaintainPayLoad {
    public String investmentsAccountMaintain() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        AccountSubStatus accountSubStatus = new AccountSubStatus();
        accountSubStatus.setValue("98");
        accountSubStatus.setRemark("FICA update");

        AccountDetails accountDetails = new AccountDetails();
        accountDetails.setAccountName("My New name for maintain");
        accountDetails.setAccountSubStatus(accountSubStatus);

        RatePeriod ratePeriod = new RatePeriod();
        ratePeriod.setCode("M");
        ratePeriod.setDescription("Monthly");

        RateType rateType = new RateType();
        rateType.setDescription("Credit Interest Rate");

        Rates rates = new Rates();
        rates.setRateType(rateType);
        rates.setRatePeriod(ratePeriod);

        PledgedCode pledgedCode = new PledgedCode();
        pledgedCode.setCode("98");
        pledgedCode.setDescription("");

        InterestDisposalAccount interestDisposalAccount = new InterestDisposalAccount();
        interestDisposalAccount.setAccountType("SA");
        interestDisposalAccount.setAccountNumber("2102225186");
        interestDisposalAccount.setAccountType("0");

        CapitalDisposalAccount capitalDisposalAccount = new CapitalDisposalAccount();
        capitalDisposalAccount.setAccountType("SA");
        capitalDisposalAccount.setAccountNumber("2102225186");
        capitalDisposalAccount.setAccountType("0");

        TermDeposit termDeposit = new TermDeposit();
        termDeposit.setCapitalDisposalAccount(capitalDisposalAccount);
        termDeposit.setInterestDisposalAccount(interestDisposalAccount);
        termDeposit.setOriginatingChannelId("B");
        termDeposit.setPledgedCode(pledgedCode);
        termDeposit.setPledgeAccount("Pledges Nedbank AAA");
        termDeposit.setInterestDay("1");

        AccountIdentification accountIdentification = new AccountIdentification();
        accountIdentification.setAccountIdentifier("85770679990");

        InvestmentAccount investmentAccount = new InvestmentAccount();
        investmentAccount.setAccountIdentification(accountIdentification);
        investmentAccount.setTermDeposit(termDeposit);
        investmentAccount.setTermDepositSummary("");
        investmentAccount.setTaxFreeSavings("");
        investmentAccount.setDepositProfile("");
        investmentAccount.setRates(rates);

        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setInvestmentAccount(investmentAccount);

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");
        personIDType.setDescription("Enterprise Party Number");

        PersonIDType personIDType1 = new PersonIDType();
        personIDType1.setCode("0000");
        personIDType1.setDescription("InvestorNumber");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        List<PersonIDType> personIDTypeList1 = new ArrayList<PersonIDType>();
        personIDTypeList1.add(personIDType1);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("600000268549");
        personIdentification.setPersonIDType(personIDTypeList);

        PersonIdentification personIdentification1 = new PersonIdentification();
        personIdentification1.setPersonIdentifier("37729973");
        personIdentification1.setPersonIDType(personIDTypeList1);

        List<PersonIdentification> personIdentificationList = new ArrayList<PersonIdentification>();
        personIdentificationList.add(personIdentification);
        personIdentificationList.add(personIdentification1);

        ProductCategory productCategory = new ProductCategory();
        productCategory.setProductCategoryID("1011");
        productCategory.setProductCategoryName("INVESTMENTS");
        productCategory.setproductCategoryLevel("1");

        ProductIdentification productIdentification = new ProductIdentification();
        productIdentification.setProductIdentifier("1134");
        productIdentification.setproductIDType("Channel Product Identifier");

        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductIdentification(productIdentification);
        productInfo.setProductCategory(productCategory);

        MaintainAccountDetails maintainAccountDetails = new MaintainAccountDetails();
        maintainAccountDetails.setProductInfo(productInfo);
        maintainAccountDetails.setPersonIdentification(personIdentificationList);
        maintainAccountDetails.setAccountInfo(accountInfo);
        maintainAccountDetails.setAccountDetails(accountDetails);

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setMaintainAccountDetails(maintainAccountDetails);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

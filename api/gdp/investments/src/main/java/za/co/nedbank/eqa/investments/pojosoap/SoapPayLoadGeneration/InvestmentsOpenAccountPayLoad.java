package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount.AccountDetails;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount.AccountInfo;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount.Envelope;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.OpenAccount.InvestmentAccount;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.*;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class InvestmentsOpenAccountPayLoad {
    public String investmentsOpenAccount() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        ProductCategory productCategory = new ProductCategory();
        productCategory.setProductCategoryID("1011");
        productCategory.setProductCategoryName("INVESTMENTS");
        productCategory.setproductCategoryLevel("1");

        ProductIdentification productIdentification = new ProductIdentification();
        productIdentification.setProductIdentifier("1134");
        productIdentification.setproductIDType("Channel Product Identifier");

        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductIdentification(productIdentification);
        productInfo.setProductCategory(productCategory);

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");
        personIDType.setDescription("Enterprise Party Number");

        PersonIDType personIDType1 = new PersonIDType();
        personIDType1.setCode("1048");
        personIDType1.setDescription("Profile Number");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        List<PersonIDType> personIDTypeList1 = new ArrayList<PersonIDType>();
        personIDTypeList1.add(personIDType1);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("191625223401");
        personIdentification.setPersonIDType(personIDTypeList);

        PersonIdentification personIdentification1 = new PersonIdentification();
        personIdentification1.setPersonIdentifier("3059295755");
        personIdentification1.setPersonIDType(personIDTypeList1);

        List<PersonIdentification> personIdentificationList = new ArrayList<PersonIdentification>();
        personIdentificationList.add(personIdentification);
        personIdentificationList.add(personIdentification1);

        OpenReason openReason = new OpenReason();
        openReason.setCode("01");
        openReason.setDescription("Salary");

        SourceOfFundsType sourceOfFundsType = new SourceOfFundsType();
        sourceOfFundsType.setCode("00");
        sourceOfFundsType.setDescription("SALARIES OR BUSINESS PROCEEDS");

        PenaltyInd penaltyInd = new PenaltyInd();
        penaltyInd.setCode("0");
        penaltyInd.setDescription("Penalty fee on full/partial withdrawal. Values 0 or 1");

        PenaltyAmount penaltyAmount = new PenaltyAmount();
        penaltyAmount.setAmount("0");
        penaltyAmount.setCurrency("ZAR");

        OriginalIntendedCapitalAmount originalIntendedCapitalAmount = new OriginalIntendedCapitalAmount();
        originalIntendedCapitalAmount.setAmount("10000");
        originalIntendedCapitalAmount.setCurrency("ZAR");

        CapitalDisposalAccount capitalDisposalAccount = new CapitalDisposalAccount();
        capitalDisposalAccount.setAccountType("CA");
        capitalDisposalAccount.setAccountNumber("1001004086");
        capitalDisposalAccount.setAccountSortCode("0");

        InterestDisposalAccount interestDisposalAccount = new InterestDisposalAccount();
        interestDisposalAccount.setAccountType("CA");
        interestDisposalAccount.setAccountNumber("1001004086");
        interestDisposalAccount.setAccountSortCode("0");

        PledgedCode pledgedCode = new PledgedCode();
        pledgedCode.setCode("0");
        pledgedCode.setDescription("");

        Term term = new Term();
        term.setValue("12");
        term.setIndicator("M");

        InitialDepositAccount initialDepositAccount = new InitialDepositAccount();
        initialDepositAccount.setAccountType("CA");
        initialDepositAccount.setAccountNumber("1001007328");
        initialDepositAccount.setAccountSortCode("0");

        TermDeposit termDeposit = new TermDeposit();
        termDeposit.setOptimumInd("0");
        termDeposit.setInterestRateCalculationRatio("0");
        termDeposit.setOpenReason(openReason);
        termDeposit.setSourceOfFundsType(sourceOfFundsType);
        termDeposit.setPenaltyInd(penaltyInd);
        termDeposit.setPenaltyAmount(penaltyAmount);
        termDeposit.setInterestReinvestmentInd("0");
        termDeposit.setMaturityDate("1111-11-11");
        termDeposit.setOtherStatus("00");
        termDeposit.setOtherDescription("Free format text for non residents");
        termDeposit.setInterestOption("1");
        termDeposit.setOriginalIntendedCapitalAmount(originalIntendedCapitalAmount);
        termDeposit.setCapitalDisposalAccount(capitalDisposalAccount);
        termDeposit.setInterestDisposalAccount(interestDisposalAccount);
        termDeposit.setOriginatingChannelId("I");
        termDeposit.setPledgedCode(pledgedCode);
        termDeposit.setPledgeAccount("Free format text entry");
        termDeposit.setGoalCapturedInd("N");
        termDeposit.setInterestDay("1");
        termDeposit.setTerm(term);
        termDeposit.setInitialDepositAccount(initialDepositAccount);

        TransferredAmount transferredAmount = new TransferredAmount();
        transferredAmount.setAmount("0");
        transferredAmount.setCurrency("ZAR");

        TaxFreeSavings taxFreeSavings = new TaxFreeSavings();
        taxFreeSavings.setTransferInd("N");
        taxFreeSavings.setTransferredAmount(transferredAmount);

        AnnualLimit annualLimit = new AnnualLimit();
        annualLimit.setAmount("0");
        annualLimit.setCurrency("ZAR");

        LifetimeLimit lifetimeLimit = new LifetimeLimit();
        lifetimeLimit.setAmount("0");
        lifetimeLimit.setCurrency("ZAR");

        LifetimeContributions lifetimeContributions = new LifetimeContributions();
        lifetimeContributions.setAmount("0");
        lifetimeContributions.setCurrency("ZAR");

        AnnualContributions annualContributions = new AnnualContributions();
        annualContributions.setAmount("0");
        annualContributions.setCurrency("ZAR");

        DepositProfile depositProfile = new DepositProfile();
        depositProfile.setAnnualLimit(annualLimit);
        depositProfile.setLifetimeLimit(lifetimeLimit);
        depositProfile.setLifetimeContributions(lifetimeContributions);
        depositProfile.setAnnualContributions(annualContributions);

        RateType rateType = new RateType();
        rateType.setDescription("Credit Interest Rate");

        RatePeriod ratePeriod = new RatePeriod();
        ratePeriod.setCode("Q");
        ratePeriod.setDescription("Quarterly");

        Rates rates = new Rates();
        rates.setRateType(rateType);
        rates.setRateValue("8.5");
        rates.setRatePeriod(ratePeriod);

        InvestmentAccount investmentAccount = new InvestmentAccount();
        investmentAccount.setAccountIdentification("");
        investmentAccount.setTermDeposit(termDeposit);
        investmentAccount.setTermDepositSummary("");
        investmentAccount.setTaxFreeSavings(taxFreeSavings);
        investmentAccount.setDepositProfile(depositProfile);
        investmentAccount.setRates(rates);

        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setInvestmentAccount(investmentAccount);

        RelationshipStatus relationshipStatus = new RelationshipStatus();
        relationshipStatus.setCode("");
        relationshipStatus.setDescription("");

        AccountToBranchRelationships accountToBranchRelationships = new AccountToBranchRelationships();
        accountToBranchRelationships.setBranchCode("702");
        accountToBranchRelationships.setRelationshipStatus(relationshipStatus);

        AccountRelationships accountRelationships = new AccountRelationships();
        accountRelationships.setAccountToBranchRelationships(accountToBranchRelationships);

        AccountSubStatus accountSubStatus = new AccountSubStatus();
        accountSubStatus.setValue("0");
        accountSubStatus.setRemark("Free text FICA description");

        AccountDetails accountDetails = new AccountDetails();
        accountDetails.setAccountName("My New");
        accountDetails.setAccountSubStatus(accountSubStatus);
        accountDetails.setAccountUnit("ZAR");
        accountDetails.setAccountOpenDate("2020-02-24");

        OpenAccount openAccount = new OpenAccount();
        openAccount.setProductInfo(productInfo);
        openAccount.setPersonIdentification(personIdentificationList);
        openAccount.setAccountInfo(accountInfo);
        openAccount.setAccountRelationships(accountRelationships);
        openAccount.setAccountDetails(accountDetails);

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setOpenAccount(openAccount);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

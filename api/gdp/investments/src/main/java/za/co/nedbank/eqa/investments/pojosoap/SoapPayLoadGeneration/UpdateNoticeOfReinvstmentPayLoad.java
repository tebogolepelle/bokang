package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfReinvestment.Body;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfReinvestment.Envelope;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfReinvestment.UpdateNoticeOfReinvestment;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class UpdateNoticeOfReinvstmentPayLoad {
    public String updateNoticeOfReinvstment() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        InstructionTypeId instructionTypeId = new InstructionTypeId();
        instructionTypeId.setDescription("Diff");

        Instruction instruction = new Instruction();
        instruction.setInstructionTypeId(instructionTypeId);
        instruction.setInstructionDate("2020-01-15T01:30:21");
        instruction.setOriginatingChannelId("B");

        ReinvestmentInstructionType reinvestmentInstructionType = new ReinvestmentInstructionType();
        reinvestmentInstructionType.setCode("");
        reinvestmentInstructionType.setDescription("Partn");

        InterestFrequencyTypeId interestFrequencyTypeId = new InterestFrequencyTypeId();
        interestFrequencyTypeId.setCode("");
        interestFrequencyTypeId.setDescription("M");

        TotalReinvestmentAmount totalReinvestmentAmount = new TotalReinvestmentAmount();
        totalReinvestmentAmount.setAmount("1002.6");
        totalReinvestmentAmount.setCurrency("ZAR");

        PartInterestAmount partInterestAmount = new PartInterestAmount();
        partInterestAmount.setAmount("0");
        partInterestAmount.setCurrency("ZAR");

        CapitalDisposalAccount capitalDisposalAccount = new CapitalDisposalAccount();
        capitalDisposalAccount.setAccountType("CA");
        capitalDisposalAccount.setAccountNumber("1998210022");
        capitalDisposalAccount.setAccountSortCode("0");

        InterestDisposalAccount interestDisposalAccount = new InterestDisposalAccount();
        interestDisposalAccount.setAccountType("CA");
        interestDisposalAccount.setAccountNumber("1998210022");
        interestDisposalAccount.setAccountSortCode("0");

        ReinvestAndConversionInstruction reinvestAndConversionInstruction = new ReinvestAndConversionInstruction();
        reinvestAndConversionInstruction.setReinvestmentInstructionType(reinvestmentInstructionType);
        reinvestAndConversionInstruction.setReinvestmentTerm("1");
        reinvestAndConversionInstruction.setInterestFrequencyTypeId(interestFrequencyTypeId);
        reinvestAndConversionInstruction.setTotalReinvestmentAmount(totalReinvestmentAmount);
        reinvestAndConversionInstruction.setPartInterestAmount(partInterestAmount);
        reinvestAndConversionInstruction.setCapitalDisposalAccount(capitalDisposalAccount);
        reinvestAndConversionInstruction.setInterestDisposalAccount(interestDisposalAccount);

        CapitalDisposalAccount capitalDisposalAccount1 = new CapitalDisposalAccount();
        capitalDisposalAccount1.setAccountType("SA");
        capitalDisposalAccount1.setAccountNumber("2102225186");
        capitalDisposalAccount1.setAccountSortCode("0");

        InterestDisposalAccount interestDisposalAccount1 = new InterestDisposalAccount();
        interestDisposalAccount1.setAccountType("SA");
        interestDisposalAccount1.setAccountNumber("2102225186");
        interestDisposalAccount1.setAccountSortCode("0");

        TermDeposit termDeposit = new TermDeposit();
        termDeposit.setInterestOption("1");
        termDeposit.setCapitalDisposalAccount(capitalDisposalAccount1);
        termDeposit.setInterestDisposalAccount(interestDisposalAccount1);
        termDeposit.setInterestDay("5");

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");
        personIDType.setDescription("Enterprise Party Number");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("600000268549");
        personIdentification.setPersonIDType(personIDTypeList);

        ProductIdentification productIdentification = new ProductIdentification();
        productIdentification.setProductIdentifier("1391");
        productIdentification.setproductIDType("Investments");

        UpdateNoticeOfReinvestment updateNoticeOfReinvestment = new UpdateNoticeOfReinvestment();
        updateNoticeOfReinvestment.setAccountIdentifier("155556749997");
        updateNoticeOfReinvestment.setInstruction(instruction);
        updateNoticeOfReinvestment.setReinvestAndConversionInstruction(reinvestAndConversionInstruction);
        updateNoticeOfReinvestment.setTermDeposit(termDeposit);
        updateNoticeOfReinvestment.setPersonIdentification(personIdentification);
        updateNoticeOfReinvestment.setControlConditions("");
        updateNoticeOfReinvestment.setProductIdentification(productIdentification);

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setUpdateNoticeOfReinvestment(updateNoticeOfReinvestment);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

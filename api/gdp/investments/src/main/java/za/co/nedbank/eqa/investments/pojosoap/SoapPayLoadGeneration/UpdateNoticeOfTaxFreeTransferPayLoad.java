package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfTaxFreeTransfer.Body;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfTaxFreeTransfer.Envelope;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfTaxFreeTransfer.Instruction;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfTaxFreeTransfer.UpdateNoticeOfTaxFreeTransfer;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer.OrganizationDetail;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer.TransferAmount;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeoftaxfreetranfer.TransferInstruction;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.CapitalDisposalAccount;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIDType;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class UpdateNoticeOfTaxFreeTransferPayLoad {
    public String updateNoticeOfTaxFreeTransfer() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        Instruction instruction = new Instruction();
        instruction.setInstructionId("TO");
        instruction.setReferenceNumber("NR1987652001150011675499050003");

        TransferAmount transferAmount = new TransferAmount();
        transferAmount.setAmount("1500");

        OrganizationDetail organizationDetail = new OrganizationDetail();
        organizationDetail.setOrganizationName("CAPITECa");
        organizationDetail.setOrgRegistrationNo("9999/12345/123");
        organizationDetail.setVatRegistrationNo("9876543290");
        organizationDetail.setEmail("NBHEKISISAAA@Google.COM");

        TransferInstruction transferInstruction = new TransferInstruction();
        transferInstruction.setTransferAmount(transferAmount);
        transferInstruction.setTransferEffectiveDate("2020-03-04");
        transferInstruction.setOrganizationDetail(organizationDetail);
        transferInstruction.setRejectionInd("N");

        CapitalDisposalAccount capitalDisposalAccount = new CapitalDisposalAccount();
        capitalDisposalAccount.setAccountType("");
        capitalDisposalAccount.setAccountNumber("51350010223");
        capitalDisposalAccount.setAccountSortCode("250655");

        TermDeposit termDeposit = new TermDeposit();
        termDeposit.setCapitalDisposalAccount(capitalDisposalAccount);

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("600000303724");
        personIdentification.setPersonIDType(personIDTypeList);

        UpdateNoticeOfTaxFreeTransfer updateNoticeOfTaxFreeTransfer = new UpdateNoticeOfTaxFreeTransfer();
        updateNoticeOfTaxFreeTransfer.setAccountIdentifier("302765259994");
        updateNoticeOfTaxFreeTransfer.setInstruction(instruction);
        updateNoticeOfTaxFreeTransfer.setTransferInstruction(transferInstruction);
        updateNoticeOfTaxFreeTransfer.setTaxFreeSavings("");
        updateNoticeOfTaxFreeTransfer.setDepositProfile("");
        updateNoticeOfTaxFreeTransfer.setTermDeposit(termDeposit);
        updateNoticeOfTaxFreeTransfer.setPersonIdentification(personIdentification);
        updateNoticeOfTaxFreeTransfer.setControlConditions("");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setUpdateNoticeOfTaxFreeTransfer(updateNoticeOfTaxFreeTransfer);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

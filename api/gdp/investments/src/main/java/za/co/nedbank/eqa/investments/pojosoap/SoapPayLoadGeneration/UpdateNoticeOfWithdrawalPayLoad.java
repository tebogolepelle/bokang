package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfWithdrawal.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfWithdrawal.Body;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpdateNoticeOfWithdrawal.Envelope;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofreinvestment.Instruction;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.createnoticeofwithdrawal.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.CapitalDisposalAccount;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIDType;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.PersonIdentification;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

public class UpdateNoticeOfWithdrawalPayLoad {
    public String UpdateNoticeOfWithdrawal() throws JAXBException{
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        InstructionAmount instructionAmount = new InstructionAmount();
        instructionAmount.setAmount("507.11");
        instructionAmount.setCurrency("ZAR");

        Instruction instruction = new Instruction();
        instruction.setInstructionId("NW20191220008");
        instruction.setReferenceNumber("NW20191220008");
        instruction.setInstructionAmount(instructionAmount);
        instruction.setInstructionDate("2019-11-05T01:30:21");
        instruction.setInstructionEffectiveDate("2019-12-20");
        instruction.setInterestReinvestmentInd("0");
        instruction.setBeneficiaryInd("N");
        instruction.setRecurringInd("N");
        instruction.setOriginatingChannelId("B");

        NoticeInstructionType noticeInstructionType = new NoticeInstructionType();
        noticeInstructionType.setCode("UPD");

        FundsDestination fundsDestination = new FundsDestination();
        fundsDestination.setCode("0");

        ClosureReasonType closureReasonType = new ClosureReasonType();
        closureReasonType.setCode("0");

        EarlyReleaseReason earlyReleaseReason = new EarlyReleaseReason();
        earlyReleaseReason.setCode("0");

        RecurringNoticeFrequency recurringNoticeFrequency = new RecurringNoticeFrequency();
        recurringNoticeFrequency.setCode("M");
        recurringNoticeFrequency.setDescription("Monthly");

        NoticeInstruction noticeInstruction = new NoticeInstruction();
        noticeInstruction.setNoticeInstructionType(noticeInstructionType);
        noticeInstruction.setTransferOutInd("N");
        noticeInstruction.setFundsDestination(fundsDestination);
        noticeInstruction.setDestinationProductCode("0");
        noticeInstruction.setClosureReasonType(closureReasonType);
        noticeInstruction.setEarlyReleaseReason(earlyReleaseReason);
        noticeInstruction.setRecurringNoticeFrequency(recurringNoticeFrequency);
        noticeInstruction.setRecurringStartDate("2019-12-20");
        noticeInstruction.setRecurringEndDate("2020-11-30");

        CapitalDisposalAccount capitalDisposalAccount = new CapitalDisposalAccount();
        capitalDisposalAccount.setAccountType("SA");
        capitalDisposalAccount.setAccountNumber("2102225186");
        capitalDisposalAccount.setAccountSortCode("0");

        TermDeposit termDeposit = new TermDeposit();
        termDeposit.setCapitalDisposalAccount(capitalDisposalAccount);

        PersonIDType personIDType = new PersonIDType();
        personIDType.setCode("1041");
        personIDType.setDescription("Enterprise Party Number");

        List<PersonIDType> personIDTypeList = new ArrayList<PersonIDType>();
        personIDTypeList.add(personIDType);

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setPersonIdentifier("600000268549");
        personIdentification.setPersonIDType(personIDTypeList);

        UpdateNoticeOfWithdrawal updateNoticeOfWithdrawal = new UpdateNoticeOfWithdrawal();
        updateNoticeOfWithdrawal.setAccountIdentifier("117004869986");
        updateNoticeOfWithdrawal.setInstruction(instruction);
        updateNoticeOfWithdrawal.setNoticeInstruction(noticeInstruction);
        updateNoticeOfWithdrawal.setTermDeposit(termDeposit);
        updateNoticeOfWithdrawal.setPersonIdentification(personIdentification);
        updateNoticeOfWithdrawal.setControlConditions("");

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setUpdateNoticeOfWithdrawal(updateNoticeOfWithdrawal);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

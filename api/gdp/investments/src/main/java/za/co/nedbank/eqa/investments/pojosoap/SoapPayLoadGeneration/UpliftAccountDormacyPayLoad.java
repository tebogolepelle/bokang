package za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.UpliftAccountDormacy.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.header.*;
import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.AccountIdentification;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

import javax.xml.bind.JAXBException;

public class UpliftAccountDormacyPayLoad {
    public String upliftAccountDormacy() {
        Envelope envelope = new Envelope();
        Header header = new Header();
        Body body = new Body();

        AccountIDType accountIDType = new AccountIDType();
        accountIDType.setCode("1041");
        accountIDType.setDescription("Enterprise Party Number");

        AccountIdentification accountIdentification = new AccountIdentification();
        accountIdentification.setAccountIdentifier("TO INPUT DATA");
        accountIdentification.setAccountIDType(accountIDType);

        Status status = new Status();
        status.setStatusCode("TO INPUT DATA");
        status.setStatusDescription("TO INPUT DATA");

        UpliftAccountDormancy upliftAccountDormancy = new UpliftAccountDormancy();
        upliftAccountDormancy.setStatus(status);
        upliftAccountDormancy.setAccountIdentification(accountIdentification);

        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        instrumentationInfo.setParentInstrumentationId("1235648");
        instrumentationInfo.setChildInstrumentationId("135864");

        RequestOriginator requestOriginator = new RequestOriginator();
        requestOriginator.setMachineIPAddress("172.28.62.62");
        requestOriginator.setUserPrincipleName("NB313260");
        requestOriginator.setMachineDNSName("L0610053276");
        requestOriginator.setChannelId("555");

        ContextInfo contextInfo = new ContextInfo();
        contextInfo.setExecutionContextId("e4964ce8-53c9-461d-a93b-83ab3432b5f3");
        contextInfo.setProcessContextId("8ab025db-4c3c-4893-a441-50bf3fb035a5");

        EnterpriseContext enterpriseContext = new EnterpriseContext();
        enterpriseContext.setInstrumentationInfo(instrumentationInfo);
        enterpriseContext.setRequestOriginator(requestOriginator);
        enterpriseContext.setContextInfo(contextInfo);

        UsernameToken usernameToken = new UsernameToken();
        usernameToken.setUsername("AP767375");

        Security security = new Security();
        security.setUsernameToken(usernameToken);

        header.setEnterpriseContext(enterpriseContext);
        header.setSecurity(security);
        body.setUpliftAccountDormancy(upliftAccountDormancy);

        envelope.setHeader(header);
        envelope.setBody(body);

        return MarshallHelperClass.RequestMarshaller(Envelope.class,envelope);
    }
}

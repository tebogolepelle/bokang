package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.CreateNoticeOfReinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public CreateNoticeOfReinvestmentResponse createNoticeOfReinvestmentResponse;

    public CreateNoticeOfReinvestmentResponse getCreateNoticeOfReinvestmentResponse() {
        return createNoticeOfReinvestmentResponse;
    }
    public void setCreateNoticeOfReinvestmentResponse(CreateNoticeOfReinvestmentResponse createNoticeOfReinvestmentResponse) {
        this.createNoticeOfReinvestmentResponse = createNoticeOfReinvestmentResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "CreateNoticeOfReinvestmentResponse='" + createNoticeOfReinvestmentResponse + '\'' +
                '}';
    }
}

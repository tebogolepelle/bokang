package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.CreateNoticeOfWithdrawal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public CreateNoticeOfWithdrawalResponse createNoticeOfWithdrawalResponse;

    public CreateNoticeOfWithdrawalResponse getCreateNoticeOfWithdrawalResponse() {
        return createNoticeOfWithdrawalResponse;
    }
    public void setCreateNoticeOfWithdrawalResponse(CreateNoticeOfWithdrawalResponse createNoticeOfWithdrawalResponse) {
        this.createNoticeOfWithdrawalResponse = createNoticeOfWithdrawalResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "CreateNoticeOfWithdrawalResponse='" + createNoticeOfWithdrawalResponse + '\'' +
                '}';
    }
}

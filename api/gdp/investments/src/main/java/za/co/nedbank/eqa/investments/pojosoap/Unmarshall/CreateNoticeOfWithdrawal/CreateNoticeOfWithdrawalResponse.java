package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.CreateNoticeOfWithdrawal;

import io.restassured.http.Headers;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreateNoticeOfWithdrawalResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateNoticeOfWithdrawalResponse {
    @XmlElement(name = "resultSet", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ResultSet resultSet;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }
    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "CreateNoticeOfWithdrawalResponse{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", resultSet=" + resultSet +
                '}';
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.DeleteNoticeOfReinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public DeleteNoticeOfReinvestmentResponse deleteNoticeOfReinvestmentResponse;

    public DeleteNoticeOfReinvestmentResponse getDeleteNoticeOfReinvestmentResponse() {
        return deleteNoticeOfReinvestmentResponse;
    }
    public void setDeleteNoticeOfReinvestmentResponse(DeleteNoticeOfReinvestmentResponse deleteNoticeOfReinvestmentResponse) {
        this.deleteNoticeOfReinvestmentResponse = deleteNoticeOfReinvestmentResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "DeleteNoticeOfReinvestmentResponse='" + deleteNoticeOfReinvestmentResponse + '\'' +
                '}';
    }
}

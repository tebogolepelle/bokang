package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.DeleteNoticeOfTaxFreeTransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public DeleteNoticeOfTaxFreeTransferResponse deleteNoticeOfTaxFreeTransferResponse;

    public DeleteNoticeOfTaxFreeTransferResponse getDeleteNoticeOfTaxFreeTransferResponse() {
        return deleteNoticeOfTaxFreeTransferResponse;
    }
    public void setDeleteNoticeOfTaxFreeTransferResponse(DeleteNoticeOfTaxFreeTransferResponse deleteNoticeOfTaxFreeTransferResponse) {
        this.deleteNoticeOfTaxFreeTransferResponse = deleteNoticeOfTaxFreeTransferResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "DeleteNoticeOfTaxFreeTransferResponse='" + deleteNoticeOfTaxFreeTransferResponse + '\'' +
                '}';
    }
}

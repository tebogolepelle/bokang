package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.DeleteNoticeOfTaxFreeTransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "resultSet", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResultSet {
    @XmlElement(name = "resultCode", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private String ResultCode;

    @XmlElement(name = "resultDescription", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private String ResultDescription;

    public String getResultCode() {
        return ResultCode;
    }
    public void setResultCode(String ResultCode) {
        this.ResultCode = ResultCode;
    }

    public String getResultDescription() {
        return ResultDescription;
    }
    public void setResultDescription(String resultDescription) {
        ResultDescription = resultDescription;
    }

    @Override
    public String toString() {
        return "resultSet{" +
                "resultCode='" + ResultCode + '\'' +
                ", resultDescription='" + ResultDescription + '\'' +
                '}';
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.DeleteNoticeOfWithdrawal;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public DeleteNoticeOfWithdrawalResponse deleteNoticeOfWithdrawalResponse;

    public DeleteNoticeOfWithdrawalResponse getDeleteNoticeOfWithdrawalResponse() {
        return deleteNoticeOfWithdrawalResponse;
    }
    public void setDeleteNoticeOfWithdrawalResponse(DeleteNoticeOfWithdrawalResponse deleteNoticeOfWithdrawalResponse) {
        this.deleteNoticeOfWithdrawalResponse = deleteNoticeOfWithdrawalResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "DeleteNoticeOfWithdrawalResponse='" + deleteNoticeOfWithdrawalResponse + '\'' +
                '}';
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountDetails implements Serializable {
    @XmlElement(name = "accountSubStatus", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private AccountSubStatus accountSubStatus;

    @XmlElement(name = "accountOpenDate", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private String AccountOpenDate;

    public AccountSubStatus getAccountSubStatus() {
        return accountSubStatus;
    }
    public void setAccountSubStatus(AccountSubStatus accountSubStatus) {
        this.accountSubStatus = accountSubStatus;
    }

    public String getAccountOpenDate() {
        return AccountOpenDate;
    }
    public void setAccountOpenDate(String accountOpenDate) {
        AccountOpenDate = accountOpenDate;
    }

}

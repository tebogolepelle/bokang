package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountRelationships implements Serializable {
    @XmlElement(name = "accountToAffinityRelationships",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountToAffinityRelationships accountToAffinityRelationships;

    @XmlElement(name = "accountToBranchRelationships",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountToBranchRelationships accountToBranchRelationships;

    public AccountToAffinityRelationships getAccountToAffinityRelationships() {
        return accountToAffinityRelationships;
    }
    public void setAccountToAffinityRelationships(AccountToAffinityRelationships accountToAffinityRelationships) {
        this.accountToAffinityRelationships = accountToAffinityRelationships;
    }

    public AccountToBranchRelationships getAccountToBranchRelationships() {
        return accountToBranchRelationships;
    }
    public void setAccountToBranchRelationships(AccountToBranchRelationships accountToBranchRelationships) {
        this.accountToBranchRelationships = accountToBranchRelationships;
    }
}

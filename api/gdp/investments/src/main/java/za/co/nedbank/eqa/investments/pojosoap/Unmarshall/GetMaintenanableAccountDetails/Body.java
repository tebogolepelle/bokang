package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public GetMaintenanableAccountDetailsResponse getMaintenanableAccountDetailsResponse;

    public GetMaintenanableAccountDetailsResponse getGetMaintenanableAccountDetailsResponse() {
        return getMaintenanableAccountDetailsResponse;
    }
    public void setGetMaintenanableAccountDetailsResponse(GetMaintenanableAccountDetailsResponse getMaintenanableAccountDetailsResponse) {
        this.getMaintenanableAccountDetailsResponse = getMaintenanableAccountDetailsResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "GetMaintenanableAccountDetailsResponse='" + getMaintenanableAccountDetailsResponse + '\'' +
                '}';
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="GetMaintenanableAccountDetailsResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)


public class GetMaintenanableAccountDetailsResponse {
    @XmlElement(name = "resultSet", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ResultSet resultSet;

    @XmlElement(name = "productInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ProductInfo productInfo;

    @XmlElement(name = "personIdentification", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PersonIdentification personIdentification;

    @XmlElement(name = "accountInfo", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountInfo accountInfo;

    @XmlElement(name = "preferences", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Preferences preferences;

    @XmlElement(name = "accountRelationships", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountRelationships accountRelationships;

    @XmlElement(name = "accountDetails", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountDetails accountDetails;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }
    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public ProductInfo getProductInfo() {
        return productInfo;
    }
    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public PersonIdentification getPersonIdentification() {
        return personIdentification;
    }
    public void setPersonIdentification(PersonIdentification personIdentification) {
        this.personIdentification = personIdentification;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }
    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public Preferences getPreferences() {
        return preferences;
    }
    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public AccountRelationships getAccountRelationships() {
        return accountRelationships;
    }
    public void setAccountRelationships(AccountRelationships accountRelationships) {
        this.accountRelationships = accountRelationships;
    }

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }
    public void setAccountDetails(AccountDetails accountDetails) {
        this.accountDetails = accountDetails;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "GetMaintenanableAccountDetailsResponse{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", resultSet=" + resultSet +
                ", ProductInfo=" + productInfo +
                ", PersonIdentification=" + personIdentification +
                ", AccountInfo=" + accountInfo +
                ", Preferences=" + preferences +
                ", AccountRelationships=" + accountRelationships +
                ", AccountDetails=" + accountDetails +
                '}';
    }
}

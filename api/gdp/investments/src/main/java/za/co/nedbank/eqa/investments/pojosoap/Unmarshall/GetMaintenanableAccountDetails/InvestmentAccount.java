package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import za.co.nedbank.eqa.investments.pojosoap.Marshall.investmentsmaintain.TermDeposit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class InvestmentAccount implements Serializable {
    @XmlElement(name = "accountIdentification", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccountIdentification accountIdentification;

    @XmlElement(name = "termDeposit", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TermDeposit termDeposit;

    @XmlElement(name = "termDepositSummary", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public TermDepositSummary termDepositSummary;

    @XmlElement(name = "rates", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Rates rates;

    public AccountIdentification getAccountIdentification() {
        return accountIdentification;
    }
    public void setAccountIdentification(AccountIdentification accountIdentification) {
        this.accountIdentification = accountIdentification;
    }

    public TermDeposit getTermDeposit() {
        return termDeposit;
    }
    public void setTermDeposit(TermDeposit termDeposit) {
        this.termDeposit = termDeposit;
    }

    public TermDepositSummary getTermDepositSummary() {
        return termDepositSummary;
    }
    public void setTermDepositSummary(TermDepositSummary termDepositSummary) {
        this.termDepositSummary = termDepositSummary;
    }

    public Rates getRates() {
        return rates;
    }
    public void setRates(Rates rates) {
        this.rates = rates;
    }
}
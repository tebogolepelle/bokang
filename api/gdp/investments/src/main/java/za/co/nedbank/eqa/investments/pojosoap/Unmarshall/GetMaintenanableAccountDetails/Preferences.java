package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Preferences implements Serializable {
    @XmlElement(name = "statementPreferences", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private StatementPreferences statementPreferences;

    public StatementPreferences getStatementPreferences() {
        return statementPreferences;
    }
    public void setStatementPreferences(StatementPreferences statementPreferences) {
        this.statementPreferences = statementPreferences;
    }

}

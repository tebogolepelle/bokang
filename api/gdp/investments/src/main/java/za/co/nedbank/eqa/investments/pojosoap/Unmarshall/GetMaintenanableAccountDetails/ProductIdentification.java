package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductIdentification implements Serializable {
    @XmlElement(name = "productIdentifier",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ProductIdentifier;

    @XmlElement(name = "productIDType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String ProductIDType;

    public String getProductIdentifier() {
        return ProductIdentifier;
    }
    public void setProductIdentifier(String productIdentifier) {
        ProductIdentifier = productIdentifier;}

    public String getProductIDType() {
        return ProductIDType;
    }
    public void setProductIDType(String productIDType) {
        ProductIDType = productIDType;}
}

package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Term implements Serializable {
    @XmlElement(name = "value",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String Value;

    public String getValue() {
        return Value;
    }
    public void setValue(String value) {
        Value = value;
    }
}

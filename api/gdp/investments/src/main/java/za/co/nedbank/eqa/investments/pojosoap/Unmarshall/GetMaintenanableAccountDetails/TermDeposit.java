package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TermDeposit implements Serializable {
    @XmlElement(name = "interestRateCalculationRatio",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestRateCalculationRatio;

    @XmlElement(name = "interestDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestDate;

    @XmlElement(name = "openReason",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public OpenReason openReason;

    @XmlElement(name = "sourceOfFundsType",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public SourceOfFundsType sourceOfFundsType;

    @XmlElement(name = "interestEffectiveTillDate",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestEffectiveTillDate;

    @XmlElement(name = "partialWithdrawalAmount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PartialWithdrawalAmount partialWithdrawalAmount;

    @XmlElement(name = "otherStatus",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String OtherStatus;

    @XmlElement(name = "interestOption",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestOption;

    @XmlElement(name = "capitalDisposalAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CapitalDisposalAccount capitalDisposalAccount;

    @XmlElement(name = "interestDisposalAccount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public InterestDisposalAccount interestDisposalAccount;

    @XmlElement(name = "pledgedCode",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public PledgedCode pledgedCode;

    @XmlElement(name = "allowedWithdrawalsCount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AllowedWithdrawalsCount;

    @XmlElement(name = "interestDay",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String InterestDay;

    @XmlElement(name = "allowedDepositsCount",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public String AllowedDepositsCount;

    @XmlElement(name = "term",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public Term term;

    public String getInterestRateCalculationRatio() {
        return InterestRateCalculationRatio;
    }
    public void setInterestRateCalculationRatio(String interestRateCalculationRatio) {
        InterestRateCalculationRatio = interestRateCalculationRatio;
    }

    public String getInterestDate() {
        return InterestDate;
    }
    public void setInterestDate(String interestDate) {
        InterestDate = interestDate;
    }

    public OpenReason getOpenReason() {
        return openReason;
    }
    public void setOpenReason(OpenReason openReason) {
        this.openReason = openReason;
    }

    public SourceOfFundsType getSourceOfFundsType() {
        return sourceOfFundsType;
    }
    public void setSourceOfFundsType(SourceOfFundsType sourceOfFundsType) {
        this.sourceOfFundsType = sourceOfFundsType;
    }

    public String getInterestEffectiveTillDate() {
        return InterestEffectiveTillDate;
    }
    public void setInterestEffectiveTillDate(String interestEffectiveTillDate) {
        InterestEffectiveTillDate = interestEffectiveTillDate;
    }

    public PartialWithdrawalAmount getPartialWithdrawalAmount() {
        return partialWithdrawalAmount;
    }
    public void setPartialWithdrawalAmount(PartialWithdrawalAmount partialWithdrawalAmount) {
        this.partialWithdrawalAmount = partialWithdrawalAmount;
    }

    public String getOtherStatus() {
        return OtherStatus;
    }
    public void setOtherStatus(String otherStatus) {
        OtherStatus = otherStatus;
    }

    public String getInterestOption() {
        return InterestOption;
    }
    public void setInterestOption(String interestOption) {
        InterestOption = interestOption;
    }

    public CapitalDisposalAccount getCapitalDisposalAccount() {
        return capitalDisposalAccount;
    }
    public void setCapitalDisposalAccount(CapitalDisposalAccount capitalDisposalAccount) {
        this.capitalDisposalAccount = capitalDisposalAccount;
    }

    public InterestDisposalAccount getInterestDisposalAccount() {
        return interestDisposalAccount;
    }
    public void setInterestDisposalAccount(InterestDisposalAccount interestDisposalAccount) {
        this.interestDisposalAccount = interestDisposalAccount;
    }

    public PledgedCode getPledgedCode() {
        return pledgedCode;
    }
    public void setPledgedCode(PledgedCode pledgedCode) {
        this.pledgedCode = pledgedCode;
    }

    public String getAllowedWithdrawalsCount() {
        return AllowedWithdrawalsCount;
    }
    public void setAllowedWithdrawalsCount(String allowedWithdrawalsCount) {
        AllowedWithdrawalsCount = allowedWithdrawalsCount;
    }

    public String getInterestDay() {
        return InterestDay;
    }
    public void setInterestDay(String interestDay) {
        InterestDay = interestDay;
    }

    public String getAllowedDepositsCount() {
        return AllowedDepositsCount;
    }
    public void setAllowedDepositsCount(String allowedDepositsCount) {
        AllowedDepositsCount = allowedDepositsCount;
    }

    public Term getTerm() {
        return term;
    }
    public void setTerm(Term term) {
        this.term = term;
    }
}

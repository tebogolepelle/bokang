package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TermDepositSummary implements Serializable {
    @XmlElement(name = "capitalBalance",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public CapitalBalance capitalBalance;

    @XmlElement(name = "accruedBonusInterest",namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public AccruedBonusInterest accruedBonusInterest;

    public CapitalBalance getCapitalBalance() {
        return capitalBalance;
    }
    public void setTaxFreeSavingsCapitalBalance(CapitalBalance capitalBalance) {
        this.capitalBalance = capitalBalance;
    }

    public AccruedBonusInterest getAccruedBonusInterest() {
        return accruedBonusInterest;
    }
    public void setAccruedBonusInterest(AccruedBonusInterest accruedBonusInterest) {
        this.accruedBonusInterest = accruedBonusInterest;
    }
}

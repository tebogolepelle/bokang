package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetNoticeOfReinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public GetNoticeOfReinvestmentResponse getNoticeOfReinvestmentResponse;

    public GetNoticeOfReinvestmentResponse getGetNoticeOfReinvestmentResponse() {
        return getNoticeOfReinvestmentResponse;
    }
    public void setGetNoticeOfReinvestmentResponse(GetNoticeOfReinvestmentResponse getNoticeOfReinvestmentResponse) {
        this.getNoticeOfReinvestmentResponse = getNoticeOfReinvestmentResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "GetNoticeOfReinvestmentResponse='" + getNoticeOfReinvestmentResponse + '\'' +
                '}';
    }
}

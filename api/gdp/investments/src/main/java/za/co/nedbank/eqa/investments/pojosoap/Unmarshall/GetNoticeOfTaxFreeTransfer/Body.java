package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetNoticeOfTaxFreeTransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public GetNoticeOfTaxFreeTransferResponse getNoticeOfTaxFreeTransferResponse;

    public GetNoticeOfTaxFreeTransferResponse getGetNoticeOfTaxFreeTransferResponse() {
        return getNoticeOfTaxFreeTransferResponse;
    }
    public void setGetNoticeOfTaxFreeTransferResponse(GetNoticeOfTaxFreeTransferResponse getNoticeOfTaxFreeTransferResponse) {
        this.getNoticeOfTaxFreeTransferResponse = getNoticeOfTaxFreeTransferResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "GetNoticeOfTaxFreeTransferResponse='" + getNoticeOfTaxFreeTransferResponse + '\'' +
                '}';
    }
}

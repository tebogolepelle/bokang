package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.InvestmentsOpenAccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public OpenAccountResponse openAccountResponse;

    public OpenAccountResponse getOpenAccountResponse() {
        return openAccountResponse;
    }
    public void setOpenAccountResponse(OpenAccountResponse openAccountResponse) {
        this.openAccountResponse = openAccountResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "OpenAccountResponse='" + openAccountResponse + '\'' +
                '}';
    }
}

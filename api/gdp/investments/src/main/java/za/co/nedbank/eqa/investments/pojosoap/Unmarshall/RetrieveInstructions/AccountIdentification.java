package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.RetrieveInstructions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountIdentification implements Serializable {
    @XmlElement(name = "accountIdentifier", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private String AccountIdentifier;

    public String getAccountIdentifier() {
        return AccountIdentifier;
    }
    public void setAccountIdentifier(String accountIdentifier) {
        AccountIdentifier = accountIdentifier;
    }

}

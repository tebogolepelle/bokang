package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.RetrieveInstructions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public RetrieveInstructionsResponse retrieveInstructionsResponse;

    public RetrieveInstructionsResponse getRetrieveInstructionsResponse() {
        return retrieveInstructionsResponse;
    }
    public void setRetrieveInstructionsResponse(RetrieveInstructionsResponse retrieveInstructionsResponse) {
        this.retrieveInstructionsResponse = retrieveInstructionsResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "RetrieveInstructionsResponse='" + retrieveInstructionsResponse + '\'' +
                '}';
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.RetrieveInstructions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ChunkResponseHdr implements Serializable {
    @XmlElement(name = "continuationValue", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private String ContinuationValue;

    @XmlElement(name = "remainingItemCount", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private String RemainingItemCount;

    public String getContinuationValue() {
        return ContinuationValue;
    }
    public void setContinuationValue(String continuationValue) {
        ContinuationValue = continuationValue;
    }

    public String getRemainingItemCount() {
        return RemainingItemCount;
    }
    public void setRemainingItemCount(String remainingItemCount) {
        RemainingItemCount = remainingItemCount;
    }
}

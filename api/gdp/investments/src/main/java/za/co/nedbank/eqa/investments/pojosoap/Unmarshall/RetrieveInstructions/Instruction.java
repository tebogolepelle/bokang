package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.RetrieveInstructions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Instruction implements Serializable {
    @XmlElement(name = "instructionTypeId", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private InstructionTypeId instructionTypeId;

    @XmlElement(name = "status", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private Status status;

    public InstructionTypeId getInstructionTypeId() {
        return instructionTypeId;
    }
    public void setInstructionTypeId(InstructionTypeId instructionTypeId) {
        this.instructionTypeId = instructionTypeId;
    }

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

}

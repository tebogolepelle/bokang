package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.RetrieveInstructions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class InstructionsList implements Serializable {
    @XmlElement(name = "instruction", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private List<Instruction> instruction;

    @XmlElement(name = "accountIdentification", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    private List<AccountIdentification> accountIdentification;

    public List<Instruction> getInstruction() {
        return instruction;
    }
    public void setInstruction(List<Instruction> instruction) {
        this.instruction = instruction;
    }

    public List<AccountIdentification> getAccountIdentification() {
        return accountIdentification;
    }
    public void setAccountIdentification(List<AccountIdentification> accountIdentification) {
        this.accountIdentification = accountIdentification;
    }

}

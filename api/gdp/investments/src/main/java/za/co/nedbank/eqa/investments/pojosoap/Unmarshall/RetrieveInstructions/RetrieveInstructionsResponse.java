package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.RetrieveInstructions;

import io.restassured.http.Headers;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RetrieveInstructionsResponse",namespace="http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveInstructionsResponse {
    @XmlElement(name = "resultSet", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ResultSet resultSet;

    @XmlElement(name = "chunkResponseHdr", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public ChunkResponseHdr chunkResponseHdr;

    @XmlElement(name = "instructionsList", namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
    public InstructionsList instructionsList;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }
    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public ChunkResponseHdr getChunkResponseHdr() {
        return chunkResponseHdr;
    }
    public void setChunkResponseHdr(ChunkResponseHdr chunkResponseHdr) {
        this.chunkResponseHdr = chunkResponseHdr;
    }

    public InstructionsList getInstructionsList() {
        return instructionsList;
    }
    public void setInstructionsList(InstructionsList instructionsList) {
        this.instructionsList = instructionsList;
    }

    public Headers getHeaders() {
        return headers;
    }
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "RetrieveInstructionsResponse{" +
                "Headers=" + headers +
                ", StatusCode=" + statusCode +
                ", resultSet=" + resultSet +
                ", ChunkResponseHdr=" + chunkResponseHdr +
                ", InstructionsList=" + instructionsList +
                '}';
    }
}

package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpdateNoticeOfReinvestment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public UpdateNoticeOfReinvestmentResponse updateNoticeOfReinvestmentResponse;

    public UpdateNoticeOfReinvestmentResponse getUpdateNoticeOfReinvestmentResponse() {
        return updateNoticeOfReinvestmentResponse;
    }
    public void setUpdateNoticeOfReinvestmentResponse(UpdateNoticeOfReinvestmentResponse updateNoticeOfReinvestmentResponse) {
        this.updateNoticeOfReinvestmentResponse = updateNoticeOfReinvestmentResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "UpdateNoticeOfReinvestmentResponse='" + updateNoticeOfReinvestmentResponse + '\'' +
                '}';
    }
}

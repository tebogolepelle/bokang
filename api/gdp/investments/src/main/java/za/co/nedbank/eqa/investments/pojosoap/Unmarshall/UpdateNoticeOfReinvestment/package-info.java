@XmlSchema(
        namespace = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1",
        xmlns = {
                @XmlNs(prefix = "soapenv", namespaceURI = "http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix = "wsse", namespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"),
                @XmlNs(prefix = "ent",namespaceURI = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext"),
                @XmlNs(prefix = "kd4", namespaceURI = "http://www.ibm.com/KD4Soap"),
                @XmlNs(prefix = "saml2", namespaceURI = "urn:oasis:names:tc:SAML:2.0:assertion"),
                @XmlNs(prefix = "soap12", namespaceURI = "http://www.w3.org/2003/05/soap-envelope"),
                @XmlNs(prefix = "soap", namespaceURI = "http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
                @XmlNs(prefix = "nsi", namespaceURI = "commonj.sdo"),
                @XmlNs(prefix = "ns0", namespaceURI = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1"),
                @XmlNs(prefix = "xci0", namespaceURI = "http://contracts.it.nednet.co.za/services/ent/transactionalproductsmanagement/DepositAccountsManagement/v1")
        },
        elementFormDefault = XmlNsForm.QUALIFIED)
package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpdateNoticeOfReinvestment;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpdateNoticeOfTaxFreeTransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public UpdateNoticeOfTaxFreeTransferResponse updateNoticeOfTaxFreeTransferResponse;

    public UpdateNoticeOfTaxFreeTransferResponse getUpdateNoticeOfTaxFreeTransferResponse() {
        return updateNoticeOfTaxFreeTransferResponse;
    }
    public void setUpdateNoticeOfTaxFreeTransferResponse(UpdateNoticeOfTaxFreeTransferResponse updateNoticeOfTaxFreeTransferResponse) {
        this.updateNoticeOfTaxFreeTransferResponse = updateNoticeOfTaxFreeTransferResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "UpdateNoticeOfTaxFreeTransferResponse='" + updateNoticeOfTaxFreeTransferResponse + '\'' +
                '}';
    }
}

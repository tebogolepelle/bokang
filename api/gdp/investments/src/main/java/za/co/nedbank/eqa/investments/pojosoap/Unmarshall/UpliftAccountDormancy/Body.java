package za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpliftAccountDormancy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Body",namespace="http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {
    public UpliftAccountDormancyResponse upliftAccountDormancyResponse;

    public UpliftAccountDormancyResponse getUpliftAccountDormancyResponse() {
        return upliftAccountDormancyResponse;
    }
    public void setUpliftAccountDormancyResponse(UpliftAccountDormancyResponse upliftAccountDormancyResponse) {
        this.upliftAccountDormancyResponse = upliftAccountDormancyResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "UpliftAccountDormancyResponse='" + upliftAccountDormancyResponse + '\'' +
                '}';
    }
}

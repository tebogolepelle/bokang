package za.co.nedbank.eqa.investments.pojosoap.eqahelperclass;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MarshallHelperClass {

    private MarshallHelperClass()
    { }

    /**
     * Description: The below method is used for marshalling of request and converts the object to webservice payload
     *
     * @param parentclass
     * @param parentobject
     */

    public static String RequestMarshaller(Class parentclass, Object parentobject) {


        String payload = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(parentclass);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
            marshaller.marshal(parentobject, outputStream);
            payload  = new String(outputStream.toByteArray());
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return payload;

    }

    /**
     * Description: The method is used to Unmarshall the soap response and returns the values as per the user input
     * @param xml
     * @param clazz
     * @param <T>

     */

    public static <T> T unmarshall(InputStream xml, Class<T> clazz){
        T obj = null;
        SOAPMessage message;

        try {
            message = MessageFactory.newInstance().createMessage(null, xml);
            JAXBContext jaxbContext;
            jaxbContext = JAXBContext.newInstance(clazz);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            obj = clazz.cast(jaxbUnmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument()));
        } catch (IOException | SOAPException | JAXBException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
package za.co.nedbank.eqa.investments.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import za.co.nedbank.eqa.investments.InvesetmentsMainResAdapters;
import za.co.nedbank.eqa.investments.RestAssistance;
import za.co.nedbank.eqa.investments.pojosoap.FaultResponse.FaultResponse;
import za.co.nedbank.eqa.investments.pojosoap.SoapPayLoadGeneration.*;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.CreateNoticeOfReinvestment.CreateNoticeOfReinvestmentResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.CreateNoticeOfTaxFreeTransfer.CreateNoticeOfTaxfreeTransferResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.CreateNoticeOfWithdrawal.CreateNoticeOfWithdrawalResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.DeleteNoticeOfReinvestment.DeleteNoticeOfReinvestmentResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.DeleteNoticeOfTaxFreeTransfer.DeleteNoticeOfTaxFreeTransferResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.DeleteNoticeOfWithdrawal.DeleteNoticeOfWithdrawalResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails.GetMaintenanableAccountDetailsResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetNoticeOfReinvestment.GetNoticeOfReinvestmentResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetNoticeOfTaxFreeTransfer.GetNoticeOfTaxFreeTransferResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.InvestmentsMaintailnAccountDetails.MaintainAccountDetailsResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.InvestmentsOpenAccount.OpenAccountResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpdateNoticeOfReinvestment.UpdateNoticeOfReinvestmentResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpdateNoticeOfTaxFreeTransfer.UpdateNoticeOfTaxFreeTransferResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpliftAccountDormancy.UpliftAccountDormancyResponse;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;

public class SoapMethods extends InvesetmentsMainResAdapters {
    private static final Logger logger = LogManager.getLogger(SoapMethods.class);
    private RestAssistance restAssistance;
    private String endpoint;
    private String payload = null;
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for request {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";
    private static final String FAULT_STRING = "faultstring";

    private String path;

    private InvestmentsAccountMaintainPayLoad investmentsAccountMaintainPayLoad;
    private CreateNoticeOfReinvestmentPayLoad createNoticeOfReinvestmentPayLoad;
    private CreateNoticeOfTaxFreeTransferPayLoad createNoticeOfTaxFreeTransferPayLoad;
    private CreateNoticeOfWithdrawalPayLoad createNoticeOfWithdrawalPayLoad;
    private DeleteNoticeOfReinvestmentPayLoad deleteNoticeOfReinvestmentPayLoad;
    private DeleteNotiveOfTaxFreeTransferPayLoad deleteNotiveOfTaxFreeTransferPayLoad;
    private DeleteNoticeOfWithdrawalPayLoad deleteNoticeOfWithdrawalPayLoad;
    private InvestmentsOpenAccountPayLoad investmentsOpenAccountPayLoad;
    private UpdateNoticeOfReinvstmentPayLoad updateNoticeOfReinvstmentPayLoad;
    private UpdateNoticeOfTaxFreeTransferPayLoad updateNoticeOfTaxFreeTransferPayLoad;
    private UpliftAccountDormacyPayLoad upliftAccountDormacyPayLoad;
    private GetMaintainableAccountDetailsPayLoad getMaintainableAccountDetailsPayLoad;
    private GetNoticeOfReinvestmentPayLoad getNoticeOfReinvestmentPayLoad;
    private GetNoticeOfTaxFreeTransferPayLoad getNoticeOfTaxFreeTransferPayLoad;

    public SoapMethods() {
        restAssistance = new RestAssistance();
        path = restAssistance.getApiConfig().getOtherElements().get("Investments_Maintain_endpoint");
        investmentsAccountMaintainPayLoad = new InvestmentsAccountMaintainPayLoad();
        createNoticeOfReinvestmentPayLoad = new CreateNoticeOfReinvestmentPayLoad();
        createNoticeOfTaxFreeTransferPayLoad = new CreateNoticeOfTaxFreeTransferPayLoad();
        createNoticeOfWithdrawalPayLoad = new CreateNoticeOfWithdrawalPayLoad();
        deleteNoticeOfReinvestmentPayLoad = new DeleteNoticeOfReinvestmentPayLoad();
        deleteNotiveOfTaxFreeTransferPayLoad = new DeleteNotiveOfTaxFreeTransferPayLoad();
        deleteNoticeOfWithdrawalPayLoad = new DeleteNoticeOfWithdrawalPayLoad();
        investmentsOpenAccountPayLoad = new InvestmentsOpenAccountPayLoad();
        updateNoticeOfReinvstmentPayLoad = new UpdateNoticeOfReinvstmentPayLoad();
        updateNoticeOfTaxFreeTransferPayLoad = new UpdateNoticeOfTaxFreeTransferPayLoad();
        upliftAccountDormacyPayLoad = new UpliftAccountDormacyPayLoad();
        getMaintainableAccountDetailsPayLoad = new GetMaintainableAccountDetailsPayLoad();
        getNoticeOfReinvestmentPayLoad = new GetNoticeOfReinvestmentPayLoad();
        getNoticeOfTaxFreeTransferPayLoad = new GetNoticeOfTaxFreeTransferPayLoad();

    }

    public Response InvestmentMaintain() {

        payload = investmentsAccountMaintainPayLoad.investmentsAccountMaintain();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            MaintainAccountDetailsResponse maintainAccountDetailsResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), MaintainAccountDetailsResponse.class);

            assert maintainAccountDetailsResponse != null;
            maintainAccountDetailsResponse.setHeaders(response.getHeaders());
            maintainAccountDetailsResponse.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public Response InvestmentOpenAccount() {

        payload = investmentsOpenAccountPayLoad.investmentsOpenAccount();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            OpenAccountResponse openAccountResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), OpenAccountResponse.class);

            assert openAccountResponse != null;
            openAccountResponse.setHeaders(response.getHeaders());
            openAccountResponse.setStatusCode(response.getStatusCode());
        }

        return response;
    }

    public Response CreateNoticeOfReinvestment() {

        payload = createNoticeOfReinvestmentPayLoad.createNoticeOfReinvestment();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);

        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains(FAULT_STRING)) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            CreateNoticeOfReinvestmentResponse createNoticeOfReinvestmentResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), CreateNoticeOfReinvestmentResponse.class);

            assert createNoticeOfReinvestmentResponse != null;
            createNoticeOfReinvestmentResponse.setHeaders(response.getHeaders());
            createNoticeOfReinvestmentResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response CreateNoticeOfTaxFreeTransfer() {

        payload = createNoticeOfTaxFreeTransferPayLoad.createNoticeOfTaxFreeTransfer();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            CreateNoticeOfTaxfreeTransferResponse createNoticeOfTaxfreeTransferResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), CreateNoticeOfTaxfreeTransferResponse.class);

            assert createNoticeOfTaxfreeTransferResponse != null;
            createNoticeOfTaxfreeTransferResponse.setHeaders(response.getHeaders());
            createNoticeOfTaxfreeTransferResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response CreateNoticeOfWithdrawal() {

        payload = createNoticeOfWithdrawalPayLoad.createNoticeOfWithdrawal();
        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            CreateNoticeOfWithdrawalResponse createNoticeOfWithdrawalResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), CreateNoticeOfWithdrawalResponse.class);

            assert createNoticeOfWithdrawalResponse != null;
            createNoticeOfWithdrawalResponse.setHeaders(response.getHeaders());
            createNoticeOfWithdrawalResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response DeleteNoticeOfReinvestment() {

        payload = deleteNoticeOfReinvestmentPayLoad.deleteNoticeOfReinvestment();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            DeleteNoticeOfReinvestmentResponse deleteNoticeOfReinvestmentResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), DeleteNoticeOfReinvestmentResponse.class);

            assert deleteNoticeOfReinvestmentResponse != null;
            deleteNoticeOfReinvestmentResponse.setHeaders(response.getHeaders());
            deleteNoticeOfReinvestmentResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response DeleteNoticeOfTaxFreeTransfer() {

        payload = deleteNotiveOfTaxFreeTransferPayLoad.deleteNotiveOfTaxFreeTransfer();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            DeleteNoticeOfTaxFreeTransferResponse deleteNoticeOfTaxFreeTransferResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), DeleteNoticeOfTaxFreeTransferResponse.class);

            assert deleteNoticeOfTaxFreeTransferResponse != null;
            deleteNoticeOfTaxFreeTransferResponse.setHeaders(response.getHeaders());
            deleteNoticeOfTaxFreeTransferResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response DeleteNoticeOfWithdrawal() {

        payload = deleteNoticeOfWithdrawalPayLoad.deleteNoticeOfWithdrawal();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            DeleteNoticeOfWithdrawalResponse deleteNoticeOfWithdrawalResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), DeleteNoticeOfWithdrawalResponse.class);

            assert deleteNoticeOfWithdrawalResponse != null;
            deleteNoticeOfWithdrawalResponse.setHeaders(response.getHeaders());
            deleteNoticeOfWithdrawalResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response UpdateNoticeOfReinvestment() {

        payload = updateNoticeOfReinvstmentPayLoad.updateNoticeOfReinvstment();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            UpdateNoticeOfReinvestmentResponse updateNoticeOfReinvestmentResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), UpdateNoticeOfReinvestmentResponse.class);

            assert updateNoticeOfReinvestmentResponse != null;
            updateNoticeOfReinvestmentResponse.setHeaders(response.getHeaders());
            updateNoticeOfReinvestmentResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response UpdateNoticeOfTaxFreeTransfer() {

        payload = updateNoticeOfTaxFreeTransferPayLoad.updateNoticeOfTaxFreeTransfer();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            UpdateNoticeOfTaxFreeTransferResponse updateNoticeOfTaxFreeTransferResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), UpdateNoticeOfTaxFreeTransferResponse.class);

            assert updateNoticeOfTaxFreeTransferResponse != null;
            updateNoticeOfTaxFreeTransferResponse.setHeaders(response.getHeaders());
            updateNoticeOfTaxFreeTransferResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response UpliftAccountDormacy() {

        payload = upliftAccountDormacyPayLoad.upliftAccountDormacy();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);

        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            UpliftAccountDormancyResponse upliftAccountDormancyResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), UpliftAccountDormancyResponse.class);

            assert upliftAccountDormancyResponse != null;
            upliftAccountDormancyResponse.setHeaders(response.getHeaders());
            upliftAccountDormancyResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response GetMaintainableAccountDetails() {

        payload = getMaintainableAccountDetailsPayLoad.getMaintainableAccountDetails();

        System.out.println(payload);

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);

        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            GetMaintenanableAccountDetailsResponse getMaintenanableAccountDetailsResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), GetMaintenanableAccountDetailsResponse.class);

            assert getMaintenanableAccountDetailsResponse != null;
            getMaintenanableAccountDetailsResponse.setHeaders(response.getHeaders());
            getMaintenanableAccountDetailsResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response GetNoticeOfReinvestment() {

        payload = getNoticeOfReinvestmentPayLoad.getNoticeOfReinvestment();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            GetNoticeOfReinvestmentResponse getNoticeOfReinvestmentResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), GetNoticeOfReinvestmentResponse.class);

            assert getNoticeOfReinvestmentResponse != null;
            getNoticeOfReinvestmentResponse.setHeaders(response.getHeaders());
            getNoticeOfReinvestmentResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public Response GetNoticeOfTaxFreeTransfer() {

        payload = getNoticeOfTaxFreeTransferPayLoad.getNoticeOfTaxFreeTransfer();

        endpoint = restAssistance.getSoapEndpoint();
        Response response = restAssistance.postRequest(payload, endpoint, path);


        assert response != null;

        verify200Response(response.getStatusCode());

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, endpoint);
            logger.info(PAYLOAD_MESSAGE, payload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), FaultResponse.class);

            assert faultResponse != null;
            logger.info(ENDING_METHOD_MESSAGE, faultResponse.getFaultString());
        } else {
            GetNoticeOfTaxFreeTransferResponse getNoticeOfTaxFreeTransferResponse = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), GetNoticeOfTaxFreeTransferResponse.class);

            assert getNoticeOfTaxFreeTransferResponse != null;
            getNoticeOfTaxFreeTransferResponse.setHeaders(response.getHeaders());
            getNoticeOfTaxFreeTransferResponse.setStatusCode(response.getStatusCode());
        }
        return response;
    }

    public void verify200Response(int statuscode) {
        Assert.assertEquals(200, statuscode);
    }
}
package za.co.nedbank.eqa.investments.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.investments.pojosoap.eqahelperclass.MarshallHelperClass;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.GetMaintenanableAccountDetails.GetMaintenanableAccountDetailsResponse;
import za.co.nedbank.eqa.investments.pojosoap.Unmarshall.UpliftAccountDormancy.UpliftAccountDormancyResponse;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.IOException;

public class SoapResponseAdapters {

    private static final Logger logger = LogManager.getLogger(SoapResponseAdapters.class);
    String statusCode, statusDescription = null;
    private static final String SOAP_STATUS_CODE = "Response status code returned....{}";
    private static final String SOAP_STATUS_DESCRIPTION = "Response status description returned....{}";
    /**
     * Description: The method is used to retrieve response details of retrieve response mdm request and return response values
     * @param response
     * @return
     * @throws JAXBException
     * @throws SOAPException
     * @throws IOException
     */

    public String[] validateGetMaintainableAccountDetailsResponse(Response response) {
        GetMaintenanableAccountDetailsResponse getMaintainableRes = null;
        String[] validateGetMaintainableAccountDetailsValues = new String[2];
        getMaintainableRes = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), GetMaintenanableAccountDetailsResponse.class);


        String code = response.getStatusLine();
        if (code.contains("200")) {
            statusCode = getMaintainableRes.getResultSet().getResultCode();
            statusDescription = getMaintainableRes.getResultSet().getResultDescription();

            logger.info(SOAP_STATUS_CODE,statusCode);
            logger.info(SOAP_STATUS_DESCRIPTION,statusDescription);

        } else {
            logger.info("validate open account response did not find the values");
        }
        return validateGetMaintainableAccountDetailsValues;
    }

    public String[] validateUpliftAccountDormacyResponse(Response response) {
        UpliftAccountDormancyResponse upliftAccDormacyRes = null;
        String[] validateUpliftAccountDormacyValues = new String[2];
        upliftAccDormacyRes = MarshallHelperClass.unmarshall(response.getBody().asInputStream(), UpliftAccountDormancyResponse.class);


        String code = response.getStatusLine();
        if (code.contains("200")) {
            statusCode = upliftAccDormacyRes.getResultSet().getResultCode();
            statusDescription = upliftAccDormacyRes.getResultSet().getResultDescription();

            logger.info(SOAP_STATUS_CODE,statusCode);
            logger.info(SOAP_STATUS_DESCRIPTION,statusDescription);
        } else {
            logger.info("validate open account response did not find the values");
        }
        return validateUpliftAccountDormacyValues;
    }

}

package za.co.nedbank.eqa.investments;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import za.co.nedbank.eqa.investments.soap.SoapMethods;

public class UnitTest_SoapTest {
    Logger logger = LogManager.getLogger(UnitTest_SoapTest.class);
    private static final java.lang.String STATUS_MESSAGE = "API response status {}";

    private Response response = null;
    SoapMethods soapMethods;


    @Test
    public void investmentMaintain() {
        TestUtils.setupStub("depositaccountsmanagement", "Investments_Maintain_Responses.xml");
        soapMethods = new SoapMethods();
        response = soapMethods.InvestmentMaintain();
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Maintain successfully executed");
        } else{
            logger.info("Investments Maintain not successfully executed");
        }
    }

    @Ignore
    @Test
    public void investmentOpenAccount() {

        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.InvestmentOpenAccount();
        if(response != null){
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Open Account successfully executed");
        } else{
            logger.info("Investments Open Account not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Test
    public void getMaintainableAccountDetails() {
        TestUtils.setupStub("depositaccountsmanagement", "GetMaintenanableAccountDetails_Response.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.GetMaintainableAccountDetails();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Get Maintainable Account Details successfully executed");
        } else{
            logger.info("Investments Get Maintainable Account Details not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void getNoticeOfReinvestment() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.GetNoticeOfReinvestment();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Get Notice Of Reinvestment successfully executed");
        } else{
            logger.info("Investments Get Notice Of Reinvestment not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void getNoticeOfTaxFreeTransfer() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.GetNoticeOfTaxFreeTransfer();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Get Notice Of Tax Free Transfer successfully executed");
        } else{
            logger.info("Investments Get Notice Of Tax Free Transfer not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void createNoticeOfReinvestment() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.CreateNoticeOfReinvestment();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Create Notice Of Reinvestment successfully executed");
        } else{
            logger.info("Investments Create Notice Of Reinvestment not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void createNoticeOfTaxFreeTransfer() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.CreateNoticeOfTaxFreeTransfer();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Create Notice Of Tax Free Transfer successfully executed");
        } else{
            logger.info("Investments Create Notice Of Tax Free Transfer not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Test
    public void createNoticeOfWithdrawal() {
        TestUtils.setupStub("depositaccountsmanagement", "CreateNoticeOfWithdrawal_Response.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.CreateNoticeOfWithdrawal();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Create Notice Of Withdrawal successfully executed");
        } else{
            logger.info("Investments Create Notice Of Withdrawal not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void deleteNoticeOfReinvestment() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.DeleteNoticeOfReinvestment();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Delete Notice Of Reinvestment successfully executed");
        } else{
            logger.info("Investments Delete Notice Of Reinvestment not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void deleteNoticeOfTaxFreeTransfer() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.DeleteNoticeOfTaxFreeTransfer();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Delete Notice Of Tax Free Transfer successfully executed");
        } else{
            logger.info("Investments Delete Notice Of Tax Free Transfer not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Test
    public void deleteNoticeOfWithdrawal() {
        TestUtils.setupStub("depositaccountsmanagement", "DeleteNoticeOfWithdrawal_Response.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.DeleteNoticeOfWithdrawal();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Delete Notice Of Withdrawal successfully executed");
        } else{
            logger.info("Investments Delete Notice Of Withdrawal not successfully executed");
        }
        TestUtils.stopMockServer();

    }

    @Ignore
    @Test
    public void updateNoticeOfReinvestment() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.UpdateNoticeOfReinvestment();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Update Notice Of Reinvestment successfully executed");
        } else{
            logger.info("Investments Update Notice Of Reinvestment not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void updateNoticeOfTaxFreeTransfer() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.UpdateNoticeOfTaxFreeTransfer();
        if(response != null) {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Update Notice Of Tax Free Transfer successfully executed");
        } else{
            logger.info("Investments Update Notice Of Tax Free Transfer not successfully executed");
        }
        TestUtils.stopMockServer();
    }

    @Ignore
    @Test
    public void upliftAccountDormacy() {
        TestUtils.setupStub("depositaccountsmanagement", "investments.xml");
        soapMethods = new SoapMethods();

        response = soapMethods.UpliftAccountDormacy();
        if(response != null)
        {
            logger.info(STATUS_MESSAGE, response.getStatusLine());
            logger.info("Investments Uplift Account Dormacy successfully executed");
        } else{
            logger.info("Investments Uplift Account Dormacy not successfully executed");
        }
        TestUtils.stopMockServer();
    }

}

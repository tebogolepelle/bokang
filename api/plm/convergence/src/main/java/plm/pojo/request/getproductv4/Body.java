package plm.pojo.request.getproductv4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="GetProduct")
    GetProduct getProduct;

    public Body() {
    }

    public Body(GetProduct getProduct) {
        this.getProduct = getProduct;
    }

    public GetProduct getGetProduct() {
        return getProduct;
    }

    public void setGetProduct(GetProduct getProduct) {
        this.getProduct = getProduct;
    }
}

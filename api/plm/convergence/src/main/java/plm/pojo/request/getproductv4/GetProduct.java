package plm.pojo.request.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetProduct {
    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productIdentifier;

    public GetProduct() {
    }

    public GetProduct(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }
}

package plm.pojo.request.header;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class Header {
    @XmlElement(name="EnterpriseContext",namespace = "http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext")
    EnterpriseContext EnterpriseContextObject;

    @XmlElement(name="Security",namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    plm.pojo.request.header.SecurityObject SecurityObject;

    public Header() {
    }

    public Header(EnterpriseContext enterpriseContextObject, SecurityObject securityObject) {
        EnterpriseContextObject = enterpriseContextObject;
        SecurityObject = securityObject;
    }

    public EnterpriseContext getEnterpriseContext() {
        return EnterpriseContextObject;
    }

    public SecurityObject getSecurity() {
        return SecurityObject;
    }

    public void setEnterpriseContext(EnterpriseContext EnterpriseContextObject) {
        this.EnterpriseContextObject = EnterpriseContextObject;
    }

    public void setSecurity(SecurityObject SecurityObject) {
        this.SecurityObject = SecurityObject;
    }

}

package plm.pojo.request.header;

import javax.xml.bind.annotation.*;

@XmlRootElement(namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
public class SecurityObject {
    @XmlElement(name="UsernameToken")
    UsernameToken UsernameTokenObject;

    public SecurityObject() {
    }

    public SecurityObject(UsernameToken usernameTokenObject) {
        UsernameTokenObject = usernameTokenObject;
    }

    public UsernameToken getUsernameTokenObject() {
        return UsernameTokenObject;
    }
    public void setUsernameTokenObject(UsernameToken usernameTokenObject) {
        UsernameTokenObject = usernameTokenObject;
    }
}

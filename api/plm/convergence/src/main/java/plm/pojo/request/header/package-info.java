@XmlSchema(
        xmlns = {
                @XmlNs(prefix="ent", namespaceURI="http://contracts.it.nednet.co.za/Infrastructure/2008/09/EnterpriseContext"),
                @XmlNs(prefix="wsse", namespaceURI="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")},
        elementFormDefault = XmlNsForm.QUALIFIED)

package plm.pojo.request.header;

        import javax.xml.bind.annotation.XmlNs;
        import javax.xml.bind.annotation.XmlNsForm;
        import javax.xml.bind.annotation.XmlSchema;
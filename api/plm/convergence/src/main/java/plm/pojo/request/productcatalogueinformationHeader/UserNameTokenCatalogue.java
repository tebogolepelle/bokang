package plm.pojo.request.productcatalogueinformationHeader;

import plm.util.RestHelper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserNameTokenCatalogue {

    @XmlElement(name = "Username",namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
     String Username;

    public UserNameTokenCatalogue(){
        RestHelper restHelper = new RestHelper();
        String username = restHelper.getApiConfig().getOtherElements().get("UsernameToken");
        this.setUsername(username);
    }



    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

}

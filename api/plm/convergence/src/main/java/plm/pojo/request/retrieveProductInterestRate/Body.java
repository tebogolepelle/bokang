package plm.pojo.request.retrieveProductInterestRate;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveProductInterestRates")
    RetrieveProductInterestRates retrieveProductInterestRates;

    public Body() {
    }

    public Body(RetrieveProductInterestRates retrieveProductInterestRates){
        this.retrieveProductInterestRates = retrieveProductInterestRates;
    }

    public RetrieveProductInterestRates getRetrieveProductInterestRates() {
        return retrieveProductInterestRates;
    }

    public void setRetrieveProductInterestRates(RetrieveProductInterestRates retrieveProductInterestRates) {
        this.retrieveProductInterestRates = retrieveProductInterestRates;
    }

}

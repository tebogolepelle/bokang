package plm.pojo.request.retrieveProductInterestRate;

import plm.pojo.request.header.Header;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Envelope",namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"Header", "Body"})
public class Envelope {
    @XmlElement(name="Header", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    plm.pojo.request.header.Header Header;
    @XmlElement(name="Body", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    plm.pojo.request.retrieveProductInterestRate.Body Body;

    public Envelope() {
    }

    public Envelope(plm.pojo.request.header.Header header, Body body) {
        this.Header = header;
        this.Body = body;
    }

    public Header getHeader() {
        return Header;
    }

    public void setHeader(Header header) {
        this.Header = header;
    }

    public Body getBody() {
        return Body;
    }

    public void setBody(Body body) {
        this.Body = body;
    }

    public String toString() {
        return "Envelope{" +
                "Header='" + Header + '\'' +
                ", Body='" + Body + '\'' +
                '}';
    }
}
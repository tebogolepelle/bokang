package plm.pojo.request.retrieveProductInterestRate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRate {
    String rateId;
    String rateName;

    public InterestRate() {
    }

    public InterestRate(String rateId, String rateName) {
        this.rateId = rateId;
        this.rateName = rateName;
    }

// Getter Methods

    public String getRateId() {
        return rateId;
    }

    public String getRateName() {
        return rateName;
    }


    // Setter Methods
    public void setRateId(String rateIdObject) {
        this.rateId = rateIdObject;
    }
    public void setRateName(String rateNameObject) {
        this.rateName = rateNameObject;
    }

}


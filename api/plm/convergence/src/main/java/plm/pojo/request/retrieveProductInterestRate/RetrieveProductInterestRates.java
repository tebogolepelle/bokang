package plm.pojo.request.retrieveProductInterestRate;

import javax.xml.bind.annotation.*;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"interestRates", "productIdentifier","featureIdentifier","productLine"})

public class RetrieveProductInterestRates {
    InterestRates interestRates;

    String productIdentifier;

    String featureIdentifier;

    String productLine;

    public RetrieveProductInterestRates() {
    }

    public RetrieveProductInterestRates(InterestRates interestRates, String productIdentifier, String featureIdentifier, String ProductLine) {
        this.interestRates = interestRates;
        this.productIdentifier = productIdentifier;
        this.featureIdentifier = featureIdentifier;
        this.productLine = ProductLine;
    }

// Getter Methods

    public String getProductLine() {
        return productLine;
    }

    public InterestRates getInterestRates() {
        return interestRates;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    // Setter Methods
    public void setInterestRates(InterestRates interestRatesObject) {
        this.interestRates = interestRatesObject;
    }
    public void setProductIdentifier(String productIdentifierObject) {
        this.productIdentifier = productIdentifierObject;
    }
    public void setFeatureIdentifier(String featureIdentifierObject) {
        this.featureIdentifier = featureIdentifierObject;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }
}


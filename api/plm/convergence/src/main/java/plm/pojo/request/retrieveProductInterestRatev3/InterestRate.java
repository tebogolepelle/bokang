package plm.pojo.request.retrieveProductInterestRatev3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRate {
    String rateName;

    public InterestRate() {
    }

    public InterestRate( String rateName) {
        this.rateName = rateName;
    }

// Getter Methods

    public String getRateName() {
        return rateName;
    }

    // Setter Methods
    public void setRateName(String rateNameObject) {
        this.rateName = rateNameObject;
    }

}


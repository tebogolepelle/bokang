package plm.pojo.request.retrieveProductInterestRatev3;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRates {
    InterestRate interestRate;

    public InterestRates() {
    }

    public InterestRates(InterestRate interestRateObject) {
        interestRate = interestRateObject;
    }
// Getter Methods

    public InterestRate getInterestRate() {
        return interestRate;
    }

    // Setter Methods
    public void setInterestRate(InterestRate interestRateObject) {
        this.interestRate = interestRateObject;
    }

}

package plm.pojo.request.retrieveProductInterestRatev3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"interestRates", "productIdentification","featureIdentifier","productLine"})

public class RetrieveProductInterestRates {
    InterestRates interestRates;

    List<ProductIdentification> productIdentification;
    String featureIdentifier;

    String productLine;

    public RetrieveProductInterestRates() {
    }

    public RetrieveProductInterestRates(InterestRates interestRates, List<ProductIdentification> productIdentification, String featureIdentifier, String ProductLine) {
        this.interestRates = interestRates;
        this.productIdentification = productIdentification;
        this.featureIdentifier = featureIdentifier;
        this.productLine = ProductLine;
    }

// Getter Methods

    public String getProductLine() {
        return productLine;
    }

    public InterestRates getInterestRates() {
        return interestRates;
    }

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    // Setter Methods
    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }


    public void setInterestRates(InterestRates interestRatesObject) {
        this.interestRates = interestRatesObject;
    }

    public void setFeatureIdentifier(String featureIdentifierObject) {
        this.featureIdentifier = featureIdentifierObject;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

}


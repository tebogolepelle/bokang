package plm.pojo.request.retrieveProductInterestRatev4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveProductInterestRates")
    RetrieveProductInterestRate retrieveProductInterestRate;

    public Body() {
    }

    public Body(RetrieveProductInterestRate retrieveProductInterestRate){
        this.retrieveProductInterestRate = retrieveProductInterestRate;
    }

    public RetrieveProductInterestRate getRetrieveProductInterestRate() {
        return retrieveProductInterestRate;
    }

    public void setRetrieveProductInterestRate(RetrieveProductInterestRate retrieveProductInterestRate) {
        this.retrieveProductInterestRate = retrieveProductInterestRate;
    }

}

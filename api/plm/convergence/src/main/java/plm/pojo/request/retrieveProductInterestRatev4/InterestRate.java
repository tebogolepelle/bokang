package plm.pojo.request.retrieveProductInterestRatev4;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRate {
    String rateId;
    String rateName;

    public InterestRate() {
    }

    public InterestRate(String rateId, String rateName) {
        this.rateId = rateId;
        this.rateName = rateName;
    }

    public String getRateId() {
        return rateId;
    }

    public void setRateId(String rateId) {
        this.rateId = rateId;
    }

    public String getRateName() {
        return rateName;
    }

    public void setRateName(String rateName) {
        this.rateName = rateName;
    }
}

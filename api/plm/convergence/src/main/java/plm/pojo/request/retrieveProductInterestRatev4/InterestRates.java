package plm.pojo.request.retrieveProductInterestRatev4;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRates {
    InterestRate interestRate;

    public InterestRates() {
    }

    public InterestRates(InterestRate interestRate) {
        this.interestRate = interestRate;
    }

    public InterestRate getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(InterestRate interestRate) {
        this.interestRate = interestRate;
    }
}

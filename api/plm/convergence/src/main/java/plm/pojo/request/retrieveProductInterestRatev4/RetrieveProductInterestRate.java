package plm.pojo.request.retrieveProductInterestRatev4;

import plm.pojo.request.retrieveproductofferinformationv4.ChannelType;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"interestRates", "productIdentification","productChannel","additionalIdentifiers","productLine","pricingPlanIdentifier","jurisdiction","discriminators"})

public class RetrieveProductInterestRate {
    InterestRates interestRates;
    List<ProductIdentification> productIdentification;
    List<AdditionalIdentifiers> additionalIdentifiers;
    ChannelType productChannel;
    String pricingPlanIdentifier;
    String productLine;
    Jurisdiction jurisdiction;
    List<Discriminators> discriminators;

    public RetrieveProductInterestRate() {
    }

    public RetrieveProductInterestRate(InterestRates interestRates, List<ProductIdentification> productIdentification, List<AdditionalIdentifiers> additionalIdentifiers, ChannelType productChannel, String pricingPlanIdentifier, String productLine, Jurisdiction jurisdiction, List<Discriminators> discriminators) {
        this.interestRates = interestRates;
        this.productIdentification = productIdentification;
        this.additionalIdentifiers = additionalIdentifiers;
        this.productChannel = productChannel;
        this.pricingPlanIdentifier = pricingPlanIdentifier;
        this.productLine = productLine;
        this.jurisdiction = jurisdiction;
        this.discriminators = discriminators;
    }

    public InterestRates getInterestRates() {
        return interestRates;
    }

    public void setInterestRates(InterestRates interestRates) {
        this.interestRates = interestRates;
    }

    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }

    public List<AdditionalIdentifiers> getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(List<AdditionalIdentifiers> additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public ChannelType getProductChannel() {
        return productChannel;
    }

    public void setProductChannel(ChannelType productChannel) {
        this.productChannel = productChannel;
    }

    public String getPricingPlanIdentifier() {
        return pricingPlanIdentifier;
    }

    public void setPricingPlanIdentifier(String pricingPlanIdentifier) {
        this.pricingPlanIdentifier = pricingPlanIdentifier;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public Jurisdiction getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Jurisdiction jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public List<Discriminators> getDiscriminators() {
        return discriminators;
    }

    public void setDiscriminators(List<Discriminators> discriminators) {
        this.discriminators = discriminators;
    }
}


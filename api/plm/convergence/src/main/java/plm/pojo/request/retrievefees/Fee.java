package plm.pojo.request.retrievefees;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Fee {
    FeeIdentifier feeIdentifier;
    ArrayList<FeeParameters> feeParameters;
    String requestedTerm;
    RequestedLimit requestedLimit;

    public Fee() {
    }

    public Fee(FeeIdentifier feeIdentifier, ArrayList<FeeParameters> feeParameters, String requestedTerm, RequestedLimit requestedLimit) {
        this.feeIdentifier = feeIdentifier;
        this.feeParameters = feeParameters;
        this.requestedTerm = requestedTerm;
        this.requestedLimit = requestedLimit;
    }

    public FeeIdentifier getFeeIdentifier() {
        return feeIdentifier;
    }

    public void setFeeIdentifier(FeeIdentifier feeIdentifier) {
        this.feeIdentifier = feeIdentifier;
    }

    public ArrayList<FeeParameters> getFeeParameters() {
        return feeParameters;
    }

    public void setFeeParameters(ArrayList<FeeParameters> feeParameters) {
        this.feeParameters = feeParameters;
    }

    public String getRequestedTerm() {
        return requestedTerm;
    }

    public void setRequestedTerm(String requestedTerm) {
        this.requestedTerm = requestedTerm;
    }

    public RequestedLimit getRequestedLimit() {
        return requestedLimit;
    }

    public void setRequestedLimit(RequestedLimit requestedLimit) {
        this.requestedLimit = requestedLimit;
    }
}

@XmlSchema(
        namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v2",
        xmlns = {@XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="v2", namespaceURI="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v2")
        },
        elementFormDefault = XmlNsForm.QUALIFIED)

package plm.pojo.request.retrievefeesv2;

        import javax.xml.bind.annotation.XmlNs;
        import javax.xml.bind.annotation.XmlNsForm;
        import javax.xml.bind.annotation.XmlSchema;
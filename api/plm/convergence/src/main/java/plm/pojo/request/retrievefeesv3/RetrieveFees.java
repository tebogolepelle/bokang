package plm.pojo.request.retrievefeesv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"fee",  "productLine","channelType", "productIdentifier","featureIdentifier"})


public class RetrieveFees {
    Fee fee;
    String productIdentifier;
    String featureIdentifier;
    String productLine;
    ChannelType channelType;

    public RetrieveFees() {
    }

    public RetrieveFees(Fee fee, String productIdentifier, String featureIdentifier, String productLine, ChannelType channelType) {
        this.fee = fee;
        this.productIdentifier = productIdentifier;
        this.featureIdentifier = featureIdentifier;
        this.productLine = productLine;
        this.channelType = channelType;
    }

    public Fee getFee() {
        return fee;
    }

    public void setFee(Fee fee) {
        this.fee = fee;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public void setFeatureIdentifier(String featureIdentifier) {
        this.featureIdentifier = featureIdentifier;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }
}

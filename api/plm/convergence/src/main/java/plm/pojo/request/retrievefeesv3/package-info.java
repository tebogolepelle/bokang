@XmlSchema(
        namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3",
        xmlns = {@XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="v3", namespaceURI="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
                },
        elementFormDefault = XmlNsForm.QUALIFIED)

package plm.pojo.request.retrievefeesv3;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
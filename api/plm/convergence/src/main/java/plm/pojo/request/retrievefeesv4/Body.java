package plm.pojo.request.retrievefeesv4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveFees")
    RetrieveFees retrieveFees;

    public Body() {
    }

    public Body(RetrieveFees retrieveFees){
        this.retrieveFees = retrieveFees;
    }

    public RetrieveFees getRetrieveFees() {
        return retrieveFees;
    }

    public void setRetrieveFees(RetrieveFees retrieveFees) {
        this.retrieveFees = retrieveFees;
    }
}

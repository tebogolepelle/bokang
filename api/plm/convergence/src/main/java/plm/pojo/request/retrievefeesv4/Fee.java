package plm.pojo.request.retrievefeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Fee {
    FeeIdentifier feeIdentifier;
    ArrayList<FeeParameters> feeParameters;

    String tierType;
    String tierValue;

    public Fee() {
    }

    public Fee(FeeIdentifier feeIdentifier, ArrayList<FeeParameters> feeParameters, String tierType, String tierValue) {
        this.feeIdentifier = feeIdentifier;
        this.feeParameters = feeParameters;
        this.tierType = tierType;
        this.tierValue = tierValue;
    }

    public Fee(FeeIdentifier feeIdentifier, ArrayList<FeeParameters> feeParameters) {
        this.feeIdentifier = feeIdentifier;
        this.feeParameters = feeParameters;
    }

    public String getTierType() {
        return tierType;
    }

    public void setTierType(String tierType) {
        this.tierType = tierType;
    }

    public String getTierValue() {
        return tierValue;
    }

    public void setTierValue(String tierValue) {
        this.tierValue = tierValue;
    }

    public FeeIdentifier getFeeIdentifier() {
        return feeIdentifier;
    }

    public void setFeeIdentifier(FeeIdentifier feeIdentifier) {
        this.feeIdentifier = feeIdentifier;
    }

    public ArrayList<FeeParameters> getFeeParameters() {
        return feeParameters;
    }

    public void setFeeParameters(ArrayList<FeeParameters> feeParameters) {
        this.feeParameters = feeParameters;
    }

}

package plm.pojo.request.retrievefeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FeeIdentifier {
    String feeID;
    String feeName;

    public FeeIdentifier() {
    }

    public FeeIdentifier(String feeID, String feeName) {
        this.feeID = feeID;
        this.feeName = feeName;
    }

    public FeeIdentifier(String feeName) {
        this.feeName = feeName;
    }

    public String getFeeID() {
        return feeID;
    }

    public void setFeeID(String feeID) {
        this.feeID = feeID;
    }
}

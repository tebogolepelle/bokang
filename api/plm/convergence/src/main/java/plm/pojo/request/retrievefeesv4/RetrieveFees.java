package plm.pojo.request.retrievefeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"fee", "productLine", "channelType", "productIdentification", "featureIdentifier"})


public class RetrieveFees {
    Fee fee;
    List<ProductIdentification> productIdentification;
    String featureIdentifier;
    String productLine;
    ChannelType channelType;

    public RetrieveFees() {
    }

    public RetrieveFees(Fee fee, List<ProductIdentification> productIdentification, String featureIdentifier, String productLine, ChannelType channelType) {
        this.fee = fee;
        this.productIdentification = productIdentification;
        this.featureIdentifier = featureIdentifier;
        this.productLine = productLine;
        this.channelType = channelType;
    }

    public Fee getFee() {
        return fee;
    }

    public void setFee(Fee fee) {
        this.fee = fee;
    }

    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public void setFeatureIdentifier(String featureIdentifier) {
        this.featureIdentifier = featureIdentifier;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }
}

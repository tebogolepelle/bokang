package plm.pojo.request.retrieveinterestrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveInterestRates")
    plm.pojo.request.retrieveinterestrate.RetrieveInterestRates retrieveInterestRates;

    public Body() {
    }

    public Body(plm.pojo.request.retrieveinterestrate.RetrieveInterestRates retrieveInterestRates) {
        this.retrieveInterestRates = retrieveInterestRates;
    }

    public plm.pojo.request.retrieveinterestrate.RetrieveInterestRates getRetrieveInterestRates() {
        return retrieveInterestRates;
    }

    public void setRetrieveInterestRates(plm.pojo.request.retrieveinterestrate.RetrieveInterestRates retrieveInterestRates) {
        this.retrieveInterestRates = retrieveInterestRates;
    }
}

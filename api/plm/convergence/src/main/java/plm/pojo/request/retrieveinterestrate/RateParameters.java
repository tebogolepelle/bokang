package plm.pojo.request.retrieveinterestrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateParameters {
    RateParameterType type;
    String value;

    public RateParameters() {
    }

    public RateParameters(RateParameterType type, String value) {
        this.type = type;
        this.value = value;
    }

    public RateParameterType getType() {
        return type;
    }

    public void setType(RateParameterType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

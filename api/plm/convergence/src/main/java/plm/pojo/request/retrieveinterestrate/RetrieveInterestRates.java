package plm.pojo.request.retrieveinterestrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"interestRate",  "productLine","channelType", "productIdentifier","featureIdentifier"})

public class RetrieveInterestRates {
    InterestRate interestRate;
    String productLine;
    ChannelType channelType;
    String productIdentifier;
    String featureIdentifier;

    public RetrieveInterestRates() {
    }

    public RetrieveInterestRates(InterestRate interestRate, String productLine, ChannelType channelType, String productIdentifier, String featureIdentifier) {
        this.interestRate = interestRate;
        this.productLine = productLine;
        this.channelType = channelType;
        this.productIdentifier = productIdentifier;
        this.featureIdentifier = featureIdentifier;
    }

    public InterestRate getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(InterestRate interestRate) {
        this.interestRate = interestRate;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public void setFeatureIdentifier(String featureIdentifier) {
        this.featureIdentifier = featureIdentifier;
    }
}

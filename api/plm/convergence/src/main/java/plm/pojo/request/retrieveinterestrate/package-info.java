@XmlSchema(
        namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1",
        xmlns = {@XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="v1", namespaceURI="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
                },
        elementFormDefault = XmlNsForm.QUALIFIED)

package plm.pojo.request.retrieveinterestrate;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
package plm.pojo.request.retrieveinterestratev4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveInterestRates")
    RetrieveInterestRates retrieveInterestRates;

    public Body() {
    }

    public Body(RetrieveInterestRates retrieveInterestRates) {
        this.retrieveInterestRates = retrieveInterestRates;
    }

    public RetrieveInterestRates getRetrieveInterestRates() {
        return retrieveInterestRates;
    }

    public void setRetrieveInterestRates(RetrieveInterestRates retrieveInterestRates) {
        this.retrieveInterestRates = retrieveInterestRates;
    }
}

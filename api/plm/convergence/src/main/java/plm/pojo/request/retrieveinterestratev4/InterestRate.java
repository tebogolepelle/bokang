package plm.pojo.request.retrieveinterestratev4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v4")
@XmlAccessorType(XmlAccessType.FIELD)
public class InterestRate {
    RateIdentifier rateIdentifier;
    ArrayList<RateParameters> rateParameters;
    String requestedTerm;
    RequestedAmount requestedAmount;

    public InterestRate() {
    }

    public InterestRate(RateIdentifier rateIdentifier, ArrayList<RateParameters> rateParameters, String requestedTerm, RequestedAmount requestedAmount) {
        this.rateIdentifier = rateIdentifier;
        this.rateParameters = rateParameters;
        this.requestedTerm = requestedTerm;
        this.requestedAmount = requestedAmount;
    }

    public RateIdentifier getRateIdentifier() {
        return rateIdentifier;
    }

    public void setRateIdentifier(RateIdentifier rateIdentifier) {
        this.rateIdentifier = rateIdentifier;
    }

    public ArrayList<RateParameters> getRateParameters() {
        return rateParameters;
    }

    public void setRateParameters(ArrayList<RateParameters> rateParameters) {
        this.rateParameters = rateParameters;
    }

    public String getRequestedTerm() {
        return requestedTerm;
    }

    public void setRequestedTerm(String requestedTerm) {
        this.requestedTerm = requestedTerm;
    }

    public RequestedAmount getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(RequestedAmount requestedAmount) {
        this.requestedAmount = requestedAmount;
    }
}

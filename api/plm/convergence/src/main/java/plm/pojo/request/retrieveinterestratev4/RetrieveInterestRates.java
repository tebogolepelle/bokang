package plm.pojo.request.retrieveinterestratev4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"interestRate",  "productLine","channelType", "productIdentification","featureIdentifier"})

public class RetrieveInterestRates {
    InterestRate interestRate;
    String productLine;
    ChannelType channelType;
    List<ProductIdentification> productIdentification;
    String featureIdentifier;

    public RetrieveInterestRates() {
    }

    public RetrieveInterestRates(InterestRate interestRate, String productLine, ChannelType channelType, List<ProductIdentification> productIdentification, String featureIdentifier) {
        this.interestRate = interestRate;
        this.productLine = productLine;
        this.channelType = channelType;
        this.productIdentification = productIdentification;
        this.featureIdentifier = featureIdentifier;
    }

    public InterestRate getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(InterestRate interestRate) {
        this.interestRate = interestRate;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public void setFeatureIdentifier(String featureIdentifier) {
        this.featureIdentifier = featureIdentifier;
    }
}

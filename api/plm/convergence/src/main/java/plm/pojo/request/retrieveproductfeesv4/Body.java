package plm.pojo.request.retrieveproductfeesv4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveProductFees")
    RetrieveProductFees retrieveProductFees;

    public Body() {
    }

    public Body(RetrieveProductFees retrieveProductFees){
        this.retrieveProductFees = retrieveProductFees;
    }

    public RetrieveProductFees getRetrieveProductFees() {
        return retrieveProductFees;
    }

    public void setRetrieveProductFees(RetrieveProductFees retrieveProductFees) {
        this.retrieveProductFees = retrieveProductFees;
    }

}

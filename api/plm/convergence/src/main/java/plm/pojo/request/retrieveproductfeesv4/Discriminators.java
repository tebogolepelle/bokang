package plm.pojo.request.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"discriminator", "discriminatorTypeCode", "discriminatorValue"})

public class Discriminators {
    String discriminator;
    String discriminatorTypeCode;
    String discriminatorValue;

    public Discriminators() {

    }

    public Discriminators(String discriminator, String discriminatorTypeCode, String discriminatorValue) {
        this.discriminator = discriminator;
        this.discriminatorTypeCode = discriminatorTypeCode;
        this.discriminatorValue = discriminatorValue;
    }


    public String getDiscriminator() {
        return discriminator;
    }

    public void setDiscriminator(String discriminator) {
        this.discriminator = discriminator;
    }

    public String getDiscriminatorTypeCode() {
        return discriminatorTypeCode;
    }

    public void setDiscriminatorTypeCode(String discriminatorTypeCode) {
        this.discriminatorTypeCode = discriminatorTypeCode;
    }

    public String getDiscriminatorValue() {
        return discriminatorValue;
    }

    public void setDiscriminatorValue(String discriminatorValue) {
        this.discriminatorValue = discriminatorValue;
    }
}

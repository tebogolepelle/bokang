package plm.pojo.request.retrieveproductfeesv4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"fees", "productIdentification","additionalIdentifiers","productChannel", "pricingPlanIdentifier","productLine","jurisdiction","discriminators"})

public class RetrieveProductFees {
    List<Fees> fees;
    List<ProductIdentification> productIdentification;
    List<AdditionalIdentifiers> additionalIdentifiers;
    ChannelType productChannel;
    String pricingPlanIdentifier;
    String productLine;
    Jurisdiction jurisdiction;
    List<Discriminators> discriminators;

    public RetrieveProductFees() {
    }

    public RetrieveProductFees(List<Fees> fees, List<ProductIdentification> productIdentification, List<AdditionalIdentifiers> additionalIdentifiers, ChannelType productChannel, String pricingPlanIdentifier, String productLine, Jurisdiction jurisdiction, List<Discriminators> discriminators) {
        this.fees = fees;
        this.productIdentification = productIdentification;
        this.additionalIdentifiers = additionalIdentifiers;
        this.productChannel = productChannel;
        this.pricingPlanIdentifier = pricingPlanIdentifier;
        this.productLine = productLine;
        this.jurisdiction = jurisdiction;
        this.discriminators = discriminators;
    }


    public RetrieveProductFees(List<Fees> fees, List<ProductIdentification> productIdentification, Jurisdiction jurisdiction, List<Discriminators> discriminators) {
        this.fees = fees;
        this.productIdentification = productIdentification;
        this.jurisdiction = jurisdiction;
        this.discriminators = discriminators;
    }

    public ChannelType getProductChannel() {
        return productChannel;
    }

    public void setProductChannel(ChannelType productChannel) {
        this.productChannel = productChannel;
    }


    // Getter Methods

    public List<AdditionalIdentifiers> getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(List<AdditionalIdentifiers> additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public Jurisdiction getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Jurisdiction jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public List<Discriminators> getDiscriminators() {
        return discriminators;
    }

    public void setDiscriminators(List<Discriminators> discriminators) {
        this.discriminators = discriminators;
    }

    public String getProductLine() {
        return productLine;
    }


    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    // Setter Methods
    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public List<Fees> getFees() {
        return fees;
    }

    public void setFees(List<Fees> fees) {
        this.fees = fees;
    }

    public String getPricingPlanIdentifier() {
        return pricingPlanIdentifier;
    }

    public void setPricingPlanIdentifier(String pricingPlanIdentifier) {
        this.pricingPlanIdentifier = pricingPlanIdentifier;
    }
}


package plm.pojo.request.retrieveproductofferinformation;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveProductOfferInformation")
    RetrieveProductOfferInformation retrieveProductOfferInformation;

    public Body() {
    }

    public Body(RetrieveProductOfferInformation retrieveProductInterestRates){
        this.retrieveProductOfferInformation = retrieveProductInterestRates;
    }

    public RetrieveProductOfferInformation getRetrieveProductOfferInformation() {
        return retrieveProductOfferInformation;
    }

    public void setRetrieveProductOfferInformation(RetrieveProductOfferInformation retrieveProductOfferInformation) {
        this.retrieveProductOfferInformation = retrieveProductOfferInformation;
    }
}

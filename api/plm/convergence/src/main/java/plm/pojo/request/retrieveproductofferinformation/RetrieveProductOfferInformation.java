package plm.pojo.request.retrieveproductofferinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveProductOfferInformation {
    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    String productIdentifier;
    @XmlElement(name = "channelType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    ChannelType channelType;
    @XmlElement(name = "productLine",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    String productLine;
    @XmlElement(name = "characteristicFilters",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    ArrayList<CharacteristicFilters> characteristicFilters;

    public RetrieveProductOfferInformation() {
    }

    public RetrieveProductOfferInformation(String productIdentifier, ChannelType channelType, String productLine, ArrayList<CharacteristicFilters> characteristicFilters) {
        this.productIdentifier = productIdentifier;
        this.channelType = channelType;
        this.productLine = productLine;
        this.characteristicFilters = characteristicFilters;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public ArrayList<CharacteristicFilters> getCharacteristicFilters() {
        return characteristicFilters;
    }

    public void setCharacteristicFilters(ArrayList<CharacteristicFilters> characteristicFilters) {
        this.characteristicFilters = characteristicFilters;
    }
}

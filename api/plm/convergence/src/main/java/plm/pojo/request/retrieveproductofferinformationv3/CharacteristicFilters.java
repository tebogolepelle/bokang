package plm.pojo.request.retrieveproductofferinformationv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class CharacteristicFilters {
    @XmlElement(name = "type",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    CharacteristicFiltersType type;
    @XmlElement(name = "value",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    String value;

    public CharacteristicFilters() {
    }

    public CharacteristicFilters(CharacteristicFiltersType type, String value) {
        this.type = type;
        this.value = value;
    }

    public CharacteristicFiltersType getType() {
        return type;
    }

    public void setType(CharacteristicFiltersType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

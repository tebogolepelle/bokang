package plm.pojo.request.retrieveproductofferinformationv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveProductOfferInformation {
    @XmlElement(name = "productIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    List<ProductIdentification> productIdentification;
    @XmlElement(name = "channelType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    ChannelType channelType;
    @XmlElement(name = "productLine",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    String productLine;
    @XmlElement(name = "characteristicFilters",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    List<CharacteristicFilters> characteristicFilters;

    public RetrieveProductOfferInformation() {
    }

    public RetrieveProductOfferInformation(List<ProductIdentification> productIdentification, ChannelType channelType, String productLine, List<CharacteristicFilters> characteristicFilters) {
        this.productIdentification = productIdentification;
        this.channelType = channelType;
        this.productLine = productLine;
        this.characteristicFilters = characteristicFilters;
    }

    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public List<CharacteristicFilters> getCharacteristicFilters() {
        return characteristicFilters;
    }

    public void setCharacteristicFilters(List<CharacteristicFilters> characteristicFilters) {
        this.characteristicFilters = characteristicFilters;
    }
}

package plm.pojo.request.retrieveproductofferinformationv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"productIDType", "productKeys", "sourceSystem"})

public class AdditionalIdentifiers {
    Productidtype productIDType;
    ProductKeys productKeys;
    SourceSystem sourceSystem;

    public AdditionalIdentifiers() {

    }

    public AdditionalIdentifiers(Productidtype productIDType, ProductKeys productKeys, SourceSystem sourceSystem) {
        this.productIDType = productIDType;
        this.productKeys = productKeys;
        this.sourceSystem = sourceSystem;
    }

    public Productidtype getProductIDType() {
        return productIDType;
    }

    public void setProductIDType(Productidtype productIDType) {
        this.productIDType = productIDType;
    }

    public ProductKeys getProductKeys() {
        return productKeys;
    }

    public void setProductKeys(ProductKeys productKeys) {
        this.productKeys = productKeys;
    }

    public SourceSystem getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(SourceSystem sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
}

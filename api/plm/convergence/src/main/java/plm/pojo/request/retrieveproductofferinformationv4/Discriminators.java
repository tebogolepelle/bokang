package plm.pojo.request.retrieveproductofferinformationv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"discriminator", "discriminatortypecode", "discriminatorvalue"})

public class Discriminators {
    String discriminator;
    String discriminatortypecode;
    String discriminatorvalue;

    public Discriminators() {

    }

    public Discriminators(String discriminator, String discriminatortypecode, String discriminatorvalue) {
        this.discriminator = discriminator;
        this.discriminatortypecode = discriminatortypecode;
        this.discriminatorvalue = discriminatorvalue;
    }


    public String getDiscriminator() {
        return discriminator;
    }

    public void setDiscriminator(String discriminator) {
        this.discriminator = discriminator;
    }

    public String getDiscriminatortypecode() {
        return discriminatortypecode;
    }

    public void setDiscriminatortypecode(String discriminatortypecode) {
        this.discriminatortypecode = discriminatortypecode;
    }

    public String getDiscriminatorvalue() {
        return discriminatorvalue;
    }

    public void setDiscriminatorvalue(String discriminatorvalue) {
        this.discriminatorvalue = discriminatorvalue;
    }
}

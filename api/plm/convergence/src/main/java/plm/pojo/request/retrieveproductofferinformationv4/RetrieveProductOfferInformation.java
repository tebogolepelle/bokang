package plm.pojo.request.retrieveproductofferinformationv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveProductOfferInformation {
    @XmlElement(name = "productIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<ProductIdentification> productIdentification;
    @XmlElement(name = "additionalIdentifiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<AdditionalIdentifiers> additionalIdentifiers;
    @XmlElement(name = "channelType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ChannelType productChannel;
    @XmlElement(name = "productLine",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String productLine;
    @XmlElement(name = "jurisdiction",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    Jurisdiction jurisdiction;
    @XmlElement(name = "discriminators",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<Discriminators> discriminators;

    public RetrieveProductOfferInformation() {
    }

    public RetrieveProductOfferInformation(List<ProductIdentification> productIdentification, List<AdditionalIdentifiers> additionalIdentifiers, ChannelType productChannel, String productLine, Jurisdiction jurisdiction, List<Discriminators> discriminators) {
        this.productIdentification = productIdentification;
        this.additionalIdentifiers = additionalIdentifiers;
        this.productChannel = productChannel;
        this.productLine = productLine;
        this.jurisdiction = jurisdiction;
        this.discriminators = discriminators;
    }


    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }

    public List<AdditionalIdentifiers> getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(List<AdditionalIdentifiers> additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public ChannelType getProductChannel() {
        return productChannel;
    }

    public void setProductChannel(ChannelType productChannel) {
        this.productChannel = productChannel;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public Jurisdiction getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Jurisdiction jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public List<Discriminators> getDiscriminators() {
        return discriminators;
    }

    public void setDiscriminators(List<Discriminators> discriminators) {
        this.discriminators = discriminators;
    }
}

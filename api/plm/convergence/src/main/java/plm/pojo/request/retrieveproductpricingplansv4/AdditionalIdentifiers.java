package plm.pojo.request.retrieveproductpricingplansv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"productIDType", "productKeys", "sourceSystem"})

public class AdditionalIdentifiers {
    ProductIDType productIDType;
    List<ProductKeys> productKeys;
    SourceSystem sourceSystem;

    public AdditionalIdentifiers() {

    }

    public AdditionalIdentifiers(ProductIDType productIDType, List<ProductKeys> productKeys, SourceSystem sourceSystem) {
        this.productIDType = productIDType;
        this.productKeys = productKeys;
        this.sourceSystem = sourceSystem;
    }

    public ProductIDType getProductIDType() {
        return productIDType;
    }

    public void setProductIDType(ProductIDType productIDType) {
        this.productIDType = productIDType;
    }

    public List<ProductKeys> getProductKeys() {
        return productKeys;
    }

    public void setProductKeys(List<ProductKeys> productKeys) {
        this.productKeys = productKeys;
    }

    public SourceSystem getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(SourceSystem sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
}

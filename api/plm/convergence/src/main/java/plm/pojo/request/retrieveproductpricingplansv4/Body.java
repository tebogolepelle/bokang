package plm.pojo.request.retrieveproductpricingplansv4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveProductPricingPlans")
    RetrieveProductPricingPlans retrieveProductOfferInformation;

    public Body() {
    }

    public Body(RetrieveProductPricingPlans retrieveProductInterestRates){
        this.retrieveProductOfferInformation = retrieveProductInterestRates;
    }

    public RetrieveProductPricingPlans getRetrieveProductOfferInformation() {
        return retrieveProductOfferInformation;
    }

    public void setRetrieveProductOfferInformation(RetrieveProductPricingPlans retrieveProductOfferInformation) {
        this.retrieveProductOfferInformation = retrieveProductOfferInformation;
    }
}

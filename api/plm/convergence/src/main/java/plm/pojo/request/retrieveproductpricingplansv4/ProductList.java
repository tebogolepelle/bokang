package plm.pojo.request.retrieveproductpricingplansv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductList {
    @XmlElement(name = "productIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<ProductIdentification> productIdentification;
    @XmlElement(name = "additionalIdentifiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<AdditionalIdentifiers> additionalIdentifiers;
    @XmlElement(name = "jurisdiction",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<Jurisdiction> jurisdiction;
    @XmlElement(name = "discriminators",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<Discriminators> discriminators;
    @XmlElement(name = "channelType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ChannelType productChannel;
    @XmlElement(name = "productLine",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String productLine;


    public ProductList() {
    }

    public ProductList(List<ProductIdentification> productIdentification,  List<Jurisdiction> jurisdiction, List<Discriminators> discriminators, ChannelType productChannel, String productLine) {
        this.productIdentification = productIdentification;
        this.jurisdiction = jurisdiction;
        this.discriminators = discriminators;
        this.productChannel = productChannel;
        this.productLine = productLine;
    }



    public ProductList(List<ProductIdentification> productIdentification, List<AdditionalIdentifiers> additionalIdentifiers, List<Jurisdiction> jurisdiction, List<Discriminators> discriminators, ChannelType productChannel, String productLine) {
        this.productIdentification = productIdentification;
        this.additionalIdentifiers = additionalIdentifiers;
        this.jurisdiction = jurisdiction;
        this.discriminators = discriminators;
        this.productChannel = productChannel;
        this.productLine = productLine;
    }

    public List<Jurisdiction> getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(List<Jurisdiction> jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public List<ProductIdentification> getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(List<ProductIdentification> productIdentification) {
        this.productIdentification = productIdentification;
    }

    public List<AdditionalIdentifiers> getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(List<AdditionalIdentifiers> additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }


    public List<Discriminators> getDiscriminators() {
        return discriminators;
    }

    public void setDiscriminators(List<Discriminators> discriminators) {
        this.discriminators = discriminators;
    }

    public ChannelType getProductChannel() {
        return productChannel;
    }

    public void setProductChannel(ChannelType productChannel) {
        this.productChannel = productChannel;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }
}

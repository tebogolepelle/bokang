package plm.pojo.request.retrieveproductsv4;


import plm.pojo.request.getproductv4.GetProduct;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(namespace = "http://schemas.xmlsoap.org/soap/envelope/soapenv")
@XmlAccessorType(XmlAccessType.FIELD)

public class Body {
    @XmlElement(name="RetrieveProducts")
    RetrieveProducts retrieveProducts;

    public Body() {
    }

    public Body(RetrieveProducts retrieveProducts) {
        this.retrieveProducts = retrieveProducts;
    }

    public RetrieveProducts getRetrieveProducts() {
        return retrieveProducts;
    }

    public void setRetrieveProducts(RetrieveProducts retrieveProducts) {
        this.retrieveProducts = retrieveProducts;
    }
}

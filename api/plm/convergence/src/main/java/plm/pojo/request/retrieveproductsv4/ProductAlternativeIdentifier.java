package plm.pojo.request.retrieveproductsv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductAlternativeIdentifier {
    @XmlElement(name = "productIDType", namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductIDType productIDType;
    @XmlElement(name = "productKeys", namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductKeys productKeys;
    @XmlElement(name = "sourceSystem", namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    SourceSystem sourceSystem;

    public ProductAlternativeIdentifier() {
    }

    public ProductAlternativeIdentifier(ProductIDType productIDType, ProductKeys productKeys, SourceSystem sourceSystem) {
        this.productIDType = productIDType;
        this.productKeys = productKeys;
        this.sourceSystem = sourceSystem;
    }

    public ProductKeys getProductKeys() {
        return productKeys;
    }

    public void setProductKeys(ProductKeys productKeys) {
        this.productKeys = productKeys;
    }

    public SourceSystem getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(SourceSystem sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public ProductIDType getProductIDType() {
        return productIDType;
    }

    public void setProductIDType(ProductIDType productIDType) {
        this.productIDType = productIDType;
    }
}

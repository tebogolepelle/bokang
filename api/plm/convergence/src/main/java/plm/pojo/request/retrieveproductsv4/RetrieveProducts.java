package plm.pojo.request.retrieveproductsv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveProducts {
    @XmlElement(name = "productAlternativeIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductAlternativeIdentifier productAlternativeIdentifier;

    public RetrieveProducts() {
    }

    public RetrieveProducts(ProductAlternativeIdentifier productAlternativeIdentifier) {
        this.productAlternativeIdentifier = productAlternativeIdentifier;
    }

    public ProductAlternativeIdentifier getProductAlternativeIdentifier() {
        return productAlternativeIdentifier;
    }

    public void setProductAlternativeIdentifier(ProductAlternativeIdentifier productAlternativeIdentifier) {
        this.productAlternativeIdentifier = productAlternativeIdentifier;
    }
}

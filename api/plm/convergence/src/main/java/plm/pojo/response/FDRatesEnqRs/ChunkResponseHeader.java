package plm.pojo.response.FDRatesEnqRs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ChunkResponseHeader {
    @XmlElement(name = "ContinuationValue",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    String ContinuationValue;

    public String getContinuationValue() {
        return ContinuationValue;
    }

    public void setContinuationValue(String continuationValue) {
        ContinuationValue = continuationValue;
    }
}

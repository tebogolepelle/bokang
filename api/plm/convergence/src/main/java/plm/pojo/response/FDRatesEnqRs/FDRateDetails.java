package plm.pojo.response.FDRatesEnqRs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FDRateDetails {
    @XmlElement(name = "ProductType",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    String ProductType;

    @XmlElement(name = "InterestFrequency",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    String InterestFrequency;

    @XmlElement(name = "InvestmentPeriod",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    String InvestmentPeriod;

    @XmlElement(name = "PartRate",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    String PartRate;

    @XmlElement(name = "LowerBand",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    String LowerBand;

    @XmlElement(name = "UpperBand",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    String UpperBand;


    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getInterestFrequency() {
        return InterestFrequency;
    }

    public void setInterestFrequency(String interestFrequency) {
        InterestFrequency = interestFrequency;
    }

    public String getInvestmentPeriod() {
        return InvestmentPeriod;
    }

    public void setInvestmentPeriod(String investmentPeriod) {
        InvestmentPeriod = investmentPeriod;
    }

    public String getPartRate() {
        return PartRate;
    }

    public void setPartRate(String partRate) {
        PartRate = partRate;
    }

    public String getLowerBand() {
        return LowerBand;
    }

    public void setLowerBand(String lowerBand) {
        LowerBand = lowerBand;
    }

    public String getUpperBand() {
        return UpperBand;
    }

    public void setUpperBand(String upperBand) {
        UpperBand = upperBand;
    }
}

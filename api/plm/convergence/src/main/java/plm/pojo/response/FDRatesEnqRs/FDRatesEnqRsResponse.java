package plm.pojo.response.FDRatesEnqRs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="FDRatesEnqRs",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
@XmlAccessorType(XmlAccessType.FIELD)

public class FDRatesEnqRsResponse {
    //@XmlElement(name = "FDRateDetails",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    //ArrayList<FDRateDetails> fDRateDetails;

    @XmlElement(name = "chunkResponseHeader",namespace="http://contracts.it.nednet.co.za/services/biz/arrangementmanagement/InvestmentMaintenance/v15")
    ChunkResponseHeader chunkResponseHeader;


    public ChunkResponseHeader getChunkResponseHeader() {
        return chunkResponseHeader;
    }

    public void setChunkResponseHeader(ChunkResponseHeader chunkResponseHeader) {
        this.chunkResponseHeader = chunkResponseHeader;
    }
}

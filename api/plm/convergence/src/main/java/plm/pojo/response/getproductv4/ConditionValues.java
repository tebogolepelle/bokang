package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConditionValues {
    @XmlElement(name = "conditionValue",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ArrayList<ConditionValue> conditionValue;

    public ConditionValues() {
    }

    public ArrayList<ConditionValue> getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(ArrayList<ConditionValue> conditionValue) {
        this.conditionValue = conditionValue;
    }
}

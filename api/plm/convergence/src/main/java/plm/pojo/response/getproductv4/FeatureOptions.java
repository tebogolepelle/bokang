package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FeatureOptions {
    @XmlElement(name = "featureOption",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String featureOption;
    @XmlElement(name = "optionValues",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ArrayList<OptionValues> optionValues;

    public FeatureOptions() {
    }

    public String getFeatureOption() {
        return featureOption;
    }

    public void setFeatureOption(String featureOption) {
        this.featureOption = featureOption;
    }

    public ArrayList<OptionValues> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(ArrayList<OptionValues> optionValues) {
        this.optionValues = optionValues;
    }
}

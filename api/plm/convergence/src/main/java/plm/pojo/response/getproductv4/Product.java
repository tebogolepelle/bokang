package plm.pojo.response.getproductv4;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Product {
    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productIdentifier;
    @XmlElement(name = "shortDescription",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String shortDescription;
    @XmlElement(name = "description",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String description;
    @XmlElement(name = "productNames",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductNames productNames;
    @XmlElement(name = "productAlternativeIdentifiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductAlternativeIdentifiers productAlternativeIdentifiers;
    @XmlElement(name = "productStructure",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductStructure productStructure;
    @XmlElement(name = "productPricing",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductPricing productPricing;
    @XmlElement(name = "selectivelyAvailable",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String selectivelyAvailable;
    @XmlElement(name = "productLine",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductLine productLine;
    @XmlElement(name = "productCatalogue",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductCatalogue productCatalogue;
    @XmlElement(name = "productFeature",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ArrayList<ProductFeature> productFeature;
    @XmlElement(name = "productCondition",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ArrayList<ProductCondition> productCondition;


    public Product() {
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductNames getProductNames() {
        return productNames;
    }

    public void setProductNames(ProductNames productNames) {
        this.productNames = productNames;
    }

    public ProductAlternativeIdentifiers getProductAlternativeIdentifiers() {
        return productAlternativeIdentifiers;
    }

    public void setProductAlternativeIdentifiers(ProductAlternativeIdentifiers productAlternativeIdentifiers) {
        this.productAlternativeIdentifiers = productAlternativeIdentifiers;
    }

    public ProductStructure getProductStructure() {
        return productStructure;
    }

    public void setProductStructure(ProductStructure productStructure) {
        this.productStructure = productStructure;
    }

    public ProductPricing getProductPricing() {
        return productPricing;
    }

    public void setProductPricing(ProductPricing productPricing) {
        this.productPricing = productPricing;
    }

    public String getSelectivelyAvailable() {
        return selectivelyAvailable;
    }

    public void setSelectivelyAvailable(String selectivelyAvailable) {
        this.selectivelyAvailable = selectivelyAvailable;
    }

    public ProductLine getProductLine() {
        return productLine;
    }

    public void setProductLine(ProductLine productLine) {
        this.productLine = productLine;
    }

        public ProductCatalogue getProductCatalogue() {
        return productCatalogue;
    }

    public void setProductCatalogue(ProductCatalogue productCatalogue) {
        this.productCatalogue = productCatalogue;
    }

    public ArrayList<ProductFeature> getProductFeature() {
        return productFeature;
    }

    public void setProductFeature(ArrayList<ProductFeature> productFeature) {
        this.productFeature = productFeature;
    }

    public ArrayList<ProductCondition> getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(ArrayList<ProductCondition> productCondition) {
        this.productCondition = productCondition;
    }
}

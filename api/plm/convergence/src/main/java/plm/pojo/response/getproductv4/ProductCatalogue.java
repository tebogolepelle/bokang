package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductCatalogue {
    @XmlElement(name = "productCatalogueName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productCatalogueName;
    @XmlElement(name = "productHierarchy",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductHierarchy productHierarchy;

    public ProductCatalogue() {
    }

    public String getProductCatalogueName() {
        return productCatalogueName;
    }

    public void setProductCatalogueName(String productCatalogueName) {
        this.productCatalogueName = productCatalogueName;
    }

    public ProductHierarchy getProductHierarchy() {
        return productHierarchy;
    }

    public void setProductHierarchy(ProductHierarchy productHierarchy) {
        this.productHierarchy = productHierarchy;
    }
}

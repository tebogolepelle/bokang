package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductCondition {
    @XmlElement(name = "name",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String name;
    @XmlElement(name = "conditionValues",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ArrayList<ConditionValues> conditionValues;

    public ProductCondition() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ConditionValues> getConditionValues() {
        return conditionValues;
    }

    public void setConditionValues(ArrayList<ConditionValues> conditionValues) {
        this.conditionValues = conditionValues;
    }
}

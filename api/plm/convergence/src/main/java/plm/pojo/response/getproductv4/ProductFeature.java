package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductFeature {
    @XmlElement(name = "featureName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String featureName;
    @XmlElement(name = "featureDescription",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String featureDescription;
    @XmlElement(name = "featureOptions",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ArrayList<FeatureOptions> featureOptions;

    public ProductFeature() {
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getFeatureDescription() {
        return featureDescription;
    }

    public void setFeatureDescription(String featureDescription) {
        this.featureDescription = featureDescription;
    }

    public ArrayList<FeatureOptions> getFeatureOptions() {
        return featureOptions;
    }

    public void setFeatureOptions(ArrayList<FeatureOptions> featureOptions) {
        this.featureOptions = featureOptions;
    }
}

package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductHierarchy {
    @XmlElement(name = "productHierarchyName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productHierarchyName;
    @XmlElement(name = "productCatalogueName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productCatalogueName;
    @XmlElement(name = "productHierarchyDescription",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productHierarchyDescription;
    @XmlElement(name = "productCategory",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductCategory productCategory;

    public ProductHierarchy() {
    }

    public String getProductHierarchyName() {
        return productHierarchyName;
    }

    public void setProductHierarchyName(String productHierarchyName) {
        this.productHierarchyName = productHierarchyName;
    }

    public String getProductCatalogueName() {
        return productCatalogueName;
    }

    public void setProductCatalogueName(String productCatalogueName) {
        this.productCatalogueName = productCatalogueName;
    }

    public String getProductHierarchyDescription() {
        return productHierarchyDescription;
    }

    public void setProductHierarchyDescription(String productHierarchyDescription) {
        this.productHierarchyDescription = productHierarchyDescription;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }
}


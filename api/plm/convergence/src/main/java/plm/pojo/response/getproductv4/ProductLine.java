package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductLine {
    @XmlElement(name = "productLineID",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productLineID;
    @XmlElement(name = "productLineName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productLineName;

    public ProductLine() {
    }

    public String getProductLineID() {
        return productLineID;
    }

    public void setProductLineID(String productLineID) {
        this.productLineID = productLineID;
    }

    public String getProductLineName() {
        return productLineName;
    }

    public void setProductLineName(String productLineName) {
        this.productLineName = productLineName;
    }
}

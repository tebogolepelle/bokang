package plm.pojo.response.getproductv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductPricing {

    @XmlElement(name = "pricingSystemID",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String pricingSystemID;
    @XmlElement(name = "pricingSystemName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String pricingSystemName;
    @XmlElement(name = "productPricingPlan",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productPricingPlan;

    public ProductPricing() {
    }

    public String getProductPricingPlan() {
        return productPricingPlan;
    }

    public void setProductPricingPlan(String productPricingPlan) {
        this.productPricingPlan = productPricingPlan;
    }

    public String getPricingSystemID() {
        return pricingSystemID;
    }

    public void setPricingSystemID(String pricingSystemID) {
        this.pricingSystemID = pricingSystemID;
    }

    public String getPricingSystemName() {
        return pricingSystemName;
    }

    public void setPricingSystemName(String pricingSystemName) {
        this.pricingSystemName = pricingSystemName;
    }
}

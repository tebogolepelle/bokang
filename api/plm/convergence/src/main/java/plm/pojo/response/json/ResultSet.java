package plm.pojo.response.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultSet {
    @JsonProperty("ReasonCode")
    private String reasonCode;

    @JsonProperty("ReasonCode")
    public String getReasonCode() {
        return reasonCode;
    }

    @JsonProperty("ReasonCode")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

}

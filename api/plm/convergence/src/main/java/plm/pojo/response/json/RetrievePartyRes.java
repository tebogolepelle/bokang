package plm.pojo.response.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ResultSet"
})
public class RetrievePartyRes {

    @JsonProperty("ResultSet")
    private List<ResultSet> resultSet = null;

    @JsonProperty("ResultSet")
    public List<ResultSet> getResultSet() {
        return resultSet;
    }

    @JsonProperty("ResultSet")
    public void setResultSet(List<ResultSet> resultSet) {
        this.resultSet = resultSet;
    }

}

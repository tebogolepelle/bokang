package plm.pojo.response.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "RetrievePartyRes"
})
public class RetrievePartyResRoot {
    @JsonProperty("RetrievePartyRes")
    private List<RetrievePartyRes> retrievePartyRes = null;

    @JsonProperty("RetrievePartyRes")
    public List<RetrievePartyRes> getRetrievePartyRes() {
        return retrievePartyRes;
    }

    @JsonProperty("RetrievePartyRes")
    public void setRetrievePartyRes(List<RetrievePartyRes> retrievePartyRes) {
        this.retrievePartyRes = retrievePartyRes;
    }
}

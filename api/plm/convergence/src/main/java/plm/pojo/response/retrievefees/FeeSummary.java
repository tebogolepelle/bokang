package plm.pojo.response.retrievefees;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class FeeSummary {
    @XmlElement(name="featureIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
    String featureIdentifier;
    @XmlElement(name="feeIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
    FeeIdentifier feeIdentifier;
    @XmlElement(name="calculatedFee",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
    CalculatedFee calculatedFee;

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public void setFeatureIdentifier(String featureIdentifier) {
        this.featureIdentifier = featureIdentifier;
    }

    public FeeIdentifier getFeeIdentifier() {
        return feeIdentifier;
    }

    public void setFeeIdentifier(FeeIdentifier feeIdentifier) {
        this.feeIdentifier = feeIdentifier;
    }

    public CalculatedFee getCalculatedFee() {
        return calculatedFee;
    }

    public void setCalculatedFee(CalculatedFee calculatedFee) {
        this.calculatedFee = calculatedFee;
    }
}

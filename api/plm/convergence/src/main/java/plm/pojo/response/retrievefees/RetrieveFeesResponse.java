package plm.pojo.response.retrievefees;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RetrieveFeesResponse",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveFeesResponse {
    @XmlElement(name="feeSummary",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
    FeeSummary feeSummary;
    @XmlElement(name="resultSet",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
    ResultSet resultSet;

    Headers headers;
    int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public FeeSummary getFeeSummary() {
        return feeSummary;
    }

    public void setFeeSummary(FeeSummary feeSummary) {
        this.feeSummary = feeSummary;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    @Override
    public String toString() {
        return "RetrieveFeesResponse{" +
                "feeSummary=" + feeSummary +
                ", resultSet=" + resultSet +
                ", headers=" + headers +
                ", statusCode=" + statusCode +
                '}';
    }
}

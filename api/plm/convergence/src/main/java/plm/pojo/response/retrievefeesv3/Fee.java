package plm.pojo.response.retrievefeesv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class Fee {
    @XmlElement(name="amount",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    String amount;
    @XmlElement(name="currency",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    String currency;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}

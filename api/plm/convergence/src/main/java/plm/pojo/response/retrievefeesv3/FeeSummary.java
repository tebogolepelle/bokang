package plm.pojo.response.retrievefeesv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class FeeSummary {
    @XmlElement(name="featureIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    String featureIdentifier;
    @XmlElement(name="productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    String productIdentifier;
    @XmlElement(name="feeIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    FeeIdentifier feeIdentifier;
    @XmlElement(name="calculatedFee",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    CalculatedFee calculatedFee;
    @XmlElement(name="feeDetail",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    FeeDetail feeDetail;
    @XmlElement(name="referenceRateType",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    ReferenceRateType referenceRateType;

    public FeeDetail getFeeDetail() {
        return feeDetail;
    }

    public void setFeeDetail(FeeDetail feeDetail) {
        this.feeDetail = feeDetail;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public void setFeatureIdentifier(String featureIdentifier) {
        this.featureIdentifier = featureIdentifier;
    }

    public FeeIdentifier getFeeIdentifier() {
        return feeIdentifier;
    }

    public void setFeeIdentifier(FeeIdentifier feeIdentifier) {
        this.feeIdentifier = feeIdentifier;
    }

    public CalculatedFee getCalculatedFee() {
        return calculatedFee;
    }

    public void setCalculatedFee(CalculatedFee calculatedFee) {
        this.calculatedFee = calculatedFee;
    }

    public ReferenceRateType getReferenceRateType() {
        return referenceRateType;
    }

    public void setReferenceRateType(ReferenceRateType referenceRateType) {
        this.referenceRateType = referenceRateType;
    }


}

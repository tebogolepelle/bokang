package plm.pojo.response.retrievefeesv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class ReferenceRateType {
    @XmlElement(name="rateName",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    String rateName;
    @XmlElement(name="rateValue",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
    String rateValue;

    public String getRateName() {
        return rateName;
    }

    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    @Override
    public String toString() {
        return "ReferenceRateType{" +
                "rateName='" + rateName + '\'' +
                ", rateValue='" + rateValue + '\'' +
                '}';
    }
}

package plm.pojo.response.retrievefeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class FeeDetail {
    @XmlElement(name="feeBaseAmount",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v4")
    String feeBaseAmount;

    @XmlElement(name="feeUnit",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v4")
    String feeUnit;

    @XmlElement(name="feeMinimumLimit",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v4")
    String feeMinimumLimit;


    @XmlElement(name="feePercentage", namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v4")
    String feePercentage;


    public String getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(String feePercentage) {
        this.feePercentage = feePercentage;
    }

    public String getFeeBaseAmount() {
        return feeBaseAmount;
    }

    public void setFeeBaseAmount(String feeBaseAmount) {
        this.feeBaseAmount = feeBaseAmount;
    }

    public String getFeeUnit() {
        return feeUnit;
    }

    public void setFeeUnit(String feeUnit) {
        this.feeUnit = feeUnit;
    }

    public String getFeeMinimumLimit() {
        return feeMinimumLimit;
    }

    public void setFeeMinimumLimit(String feeMinimumLimit) {
        this.feeMinimumLimit = feeMinimumLimit;
    }
}

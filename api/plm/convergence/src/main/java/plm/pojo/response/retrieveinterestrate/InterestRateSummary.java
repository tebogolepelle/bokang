package plm.pojo.response.retrieveinterestrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)


public class InterestRateSummary {
    @XmlElement(name="rateIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
    RateIdentifier rateIdentifier;
    @XmlElement(name="effectiveInterestRate",namespace="http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v1")
    String effectiveInterestRate;

    public RateIdentifier getRateIdentifier() {
        return rateIdentifier;
    }

    public void setRateIdentifier(RateIdentifier rateIdentifier) {
        this.rateIdentifier = rateIdentifier;
    }

    public String getEffectiveInterestRate() {
        return effectiveInterestRate;
    }

    public void setEffectiveInterestRate(String effectiveInterestRate) {
        this.effectiveInterestRate = effectiveInterestRate;
    }
}

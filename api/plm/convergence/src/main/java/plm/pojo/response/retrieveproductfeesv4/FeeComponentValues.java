package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FeeComponentValues {
    @XmlElement(name = "feeValueType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String feeValueType;
    @XmlElement(name = "frequency",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    Frequency frequency;
    @XmlElement(name = "amountWithoutVAT",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    AmountWithoutVAT amountWithoutVAT;

    @XmlElement(name = "value",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String value;

    @XmlElement(name = "unitOfMeasure",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String unitOfMeasure;

    @XmlElement(name = "feeValueParameter",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String feeValueParameter;

    @XmlElement(name = "displayValue",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String displayValue;


    public String getFeeValueType() {
        return feeValueType;
    }

    public void setFeeValueType(String feeValueType) {
        this.feeValueType = feeValueType;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public AmountWithoutVAT getAmountWithoutVAT() {
        return amountWithoutVAT;
    }

    public void setAmountWithoutVAT(AmountWithoutVAT amountWithoutVAT) {
        this.amountWithoutVAT = amountWithoutVAT;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getFeeValueParameter() {
        return feeValueParameter;
    }

    public void setFeeValueParameter(String feeValueParameter) {
        this.feeValueParameter = feeValueParameter;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }
}

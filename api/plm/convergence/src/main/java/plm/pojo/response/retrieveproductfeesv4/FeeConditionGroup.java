package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FeeConditionGroup {
    @XmlElement(name = "conditionStructureType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ConditionStructureType conditionStructureType;
    @XmlElement(name = "numericRangeCondition",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<NumericRangeCondition> numericRangeCondition;
    @XmlElement(name = "controlCondition",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ControlCondition controlCondition;

    @XmlElement(name = "canBeOverridden",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String canBeOverridden;

    public ConditionStructureType getConditionStructureType() {
        return conditionStructureType;
    }

    public void setConditionStructureType(ConditionStructureType conditionStructureType) {
        this.conditionStructureType = conditionStructureType;
    }

    public List<NumericRangeCondition> getNumericRangeCondition() {
        return numericRangeCondition;
    }

    public void setNumericRangeCondition(List<NumericRangeCondition> numericRangeCondition) {
        this.numericRangeCondition = numericRangeCondition;
    }

    public ControlCondition getControlCondition() {
        return controlCondition;
    }

    public void setControlCondition(ControlCondition controlCondition) {
        this.controlCondition = controlCondition;
    }

    public String getCanBeOverridden() {
        return canBeOverridden;
    }

    public void setCanBeOverridden(String canBeOverridden) {
        this.canBeOverridden = canBeOverridden;
    }
}

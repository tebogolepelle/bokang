package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class FeeDetail {
    @XmlElement(name="feeIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    FeeIdentifier feeIdentifier;

    @XmlElement(name="isMandatory",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String isMandatory;

    @XmlElement(name="chargeTriggerType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ChargeTriggerType chargeTriggerType;

    @XmlElement(name="chargingBasis", namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ChargingBasis chargingBasis;

    @XmlElement(name="calculationBasis",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    CalculationBasis calculationBasis;

    @XmlElement(name="effectiveDate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String effectiveDate;

    @XmlElement(name="effectiveDate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String endDate;

    public FeeIdentifier getFeeIdentifier() {
        return feeIdentifier;
    }

    public void setFeeIdentifier(FeeIdentifier feeIdentifier) {
        this.feeIdentifier = feeIdentifier;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public ChargeTriggerType getChargeTriggerType() {
        return chargeTriggerType;
    }

    public void setChargeTriggerType(ChargeTriggerType chargeTriggerType) {
        this.chargeTriggerType = chargeTriggerType;
    }

    public ChargingBasis getChargingBasis() {
        return chargingBasis;
    }

    public void setChargingBasis(ChargingBasis chargingBasis) {
        this.chargingBasis = chargingBasis;
    }

    public CalculationBasis getCalculationBasis() {
        return calculationBasis;
    }

    public void setCalculationBasis(CalculationBasis calculationBasis) {
        this.calculationBasis = calculationBasis;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}

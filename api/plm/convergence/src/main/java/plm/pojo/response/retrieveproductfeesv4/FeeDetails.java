package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FeeDetails {
    @XmlElement(name = "feeDetail",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    FeeDetail feeDetail;
    @XmlElement(name = "relatedProduct",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    RelatedProduct relatedProduct;
    @XmlElement(name = "feature",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    FeatureIdentification feature;
    @XmlElement(name = "feeValueSets",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<FeeValueSets> feeValueSets;

    public FeeDetails() {
    }

    public FeeDetail getFeeDetail() {
        return feeDetail;
    }

    public void setFeeDetail(FeeDetail feeDetail) {
        this.feeDetail = feeDetail;
    }

    public List<FeeValueSets> getFeeValueSets() {
        return feeValueSets;
    }

    public void setFeeValueSets(List<FeeValueSets> feeValueSets) {
        this.feeValueSets = feeValueSets;
    }

    public RelatedProduct getRelatedProduct() {
        return relatedProduct;
    }

    public void setRelatedProduct(RelatedProduct relatedProduct) {
        this.relatedProduct = relatedProduct;
    }

    public FeatureIdentification getFeature() {
        return feature;
    }

    public void setFeature(FeatureIdentification feature) {
        this.feature = feature;
    }

}

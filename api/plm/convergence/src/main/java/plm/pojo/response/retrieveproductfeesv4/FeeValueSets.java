package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FeeValueSets {
    @XmlElement(name = "feeConditionGroup",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<FeeConditionGroup> feeConditionGroup;
    @XmlElement(name = "feeComponentValues",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<FeeComponentValues> feeComponentValues;

    public List<FeeConditionGroup> getFeeConditionGroup() {
        return feeConditionGroup;
    }

    public void setFeeConditionGroup(List<FeeConditionGroup> feeConditionGroup) {
        this.feeConditionGroup = feeConditionGroup;
    }

    public List<FeeComponentValues> getFeeComponentValues() {
        return feeComponentValues;
    }

    public void setFeeComponentValues(List<FeeComponentValues> feeComponentValues) {
        this.feeComponentValues = feeComponentValues;
    }
}

package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRateDetails {
    @XmlElement(name = "additionalIdentifiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    AdditionalIdentifiers additionalIdentifiers;

    @XmlElement(name = "jurisdictions",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    Jurisdiction jurisdictions;

    @XmlElement(name = "discriminators",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    Discriminators discriminators;

    @XmlElement(name = "interestRateDetail",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    InterestRateDetail interestRateDetail;

    public AdditionalIdentifiers getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(AdditionalIdentifiers additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public Jurisdiction getJurisdictions() {
        return jurisdictions;
    }

    public void setJurisdictions(Jurisdiction jurisdictions) {
        this.jurisdictions = jurisdictions;
    }

    public Discriminators getDiscriminators() {
        return discriminators;
    }

    public void setDiscriminators(Discriminators discriminators) {
        this.discriminators = discriminators;
    }

    public InterestRateDetail getInterestRateDetail() {
        return interestRateDetail;
    }

    public void setInterestRateDetail(InterestRateDetail interestRateDetail) {
        this.interestRateDetail = interestRateDetail;
    }
}

package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class ProductIdentification {
    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String productIdentifier;
    @XmlElement(name = "productIDType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String productIDType;
    @XmlElement(name = "productStructureType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ProductStructureType productStructureType;

    public ProductIdentification() {

    }

    public ProductIdentification(String productIdentifier, String productIDType, ProductStructureType productStructureType) {
        this.productIdentifier = productIdentifier;
        this.productIDType = productIDType;
        this.productStructureType = productStructureType;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getProductIDType() {
        return productIDType;
    }

    public void setProductIDType(String productIDType) {
        this.productIDType = productIDType;
    }

    public ProductStructureType getProductStructureType() {
        return productStructureType;
    }

    public void setProductStructureType(ProductStructureType productStructureType) {
        this.productStructureType = productStructureType;
    }
}

package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductKeys {

    @XmlElement(name = "fieldName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String fieldName;
    @XmlElement(name = "fieldValue",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String fieldValue;
    @XmlElement(name = "sequence",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String sequence;


    public ProductKeys() {
    }

    public ProductKeys(String fieldName, String fieldValue, String sequence) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.sequence = sequence;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
}

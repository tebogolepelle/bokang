package plm.pojo.response.retrieveproductfeesv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class ProductStructureType {
    @XmlElement(name = "productStructureTypeName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String productStructureTypeName;

    public ProductStructureType() {
    }

    public ProductStructureType(String productStructureTypeName) {
        this.productStructureTypeName = productStructureTypeName;
    }

    public String getProductStructureTypeName() {
        return productStructureTypeName;
    }

    public void setProductStructureTypeName(String productStructureTypeName) {
        this.productStructureTypeName = productStructureTypeName;
    }
}

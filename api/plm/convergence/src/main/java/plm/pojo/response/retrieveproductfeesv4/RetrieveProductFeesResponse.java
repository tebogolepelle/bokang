package plm.pojo.response.retrieveproductfeesv4;

import io.restassured.http.Headers;
import plm.pojo.request.retrieveproductofferinformationv4.Discriminators;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="RetrieveProductFeesResponse",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveProductFeesResponse {
    @XmlElement(name = "productPricing",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<ProductPricing> productPricing;

    @XmlElement(name = "productIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ProductIdentification productIdentification;

    @XmlElement(name="resultSet",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ResultSet resultSet;

    Headers headers;
    int statusCode;

    public RetrieveProductFeesResponse() {

    }

    public RetrieveProductFeesResponse(List<ProductPricing> productPricing, ProductIdentification productIdentification, ResultSet resultSet, Headers headers, int statusCode) {
        this.productPricing = productPricing;
        this.productIdentification = productIdentification;
        this.resultSet = resultSet;
        this.headers = headers;
        this.statusCode = statusCode;
    }

    public List<ProductPricing> getProductPricing() {
        return productPricing;
    }

    public void setProductPricing(List<ProductPricing> productPricing) {
        this.productPricing = productPricing;
    }

    public ProductIdentification getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(ProductIdentification productIdentification) {
        this.productIdentification = productIdentification;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}

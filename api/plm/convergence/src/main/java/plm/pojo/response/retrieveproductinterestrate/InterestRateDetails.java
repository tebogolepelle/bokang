package plm.pojo.response.retrieveproductinterestrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRateDetails {
    @XmlElement(name = "interestRate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    InterestRate interestRate;
    @XmlElement(name = "interestTiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    InterestTiers interestTiers;

    public InterestRate getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(InterestRate interestRate) {
        this.interestRate = interestRate;
    }

    public InterestTiers getInterestTiers() {
        return interestTiers;
    }

    public void setInterestTiers(InterestTiers interestTiers) {
        this.interestTiers = interestTiers;
    }
}

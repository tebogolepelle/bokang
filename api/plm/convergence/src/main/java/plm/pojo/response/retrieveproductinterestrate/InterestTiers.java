package plm.pojo.response.retrieveproductinterestrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestTiers {
    @XmlElement(name = "interestTierType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    String interestTierType;
    @XmlElement(name = "interestTierFrom",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    String interestTierFrom;
    @XmlElement(name = "interestTierTo",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    String interestTierTo;

    public String getInterestTierType() {
        return interestTierType;
    }

    public void setInterestTierType(String interestTierType) {
        this.interestTierType = interestTierType;
    }

    public String getInterestTierFrom() {
        return interestTierFrom;
    }

    public void setInterestTierFrom(String interestTierFrom) {
        this.interestTierFrom = interestTierFrom;
    }

    public String getInterestTierTo() {
        return interestTierTo;
    }

    public void setInterestTierTo(String interestTierTo) {
        this.interestTierTo = interestTierTo;
    }
}

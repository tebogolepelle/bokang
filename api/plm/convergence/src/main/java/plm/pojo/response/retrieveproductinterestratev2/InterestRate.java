package plm.pojo.response.retrieveproductinterestratev2;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRate {
    @XmlElement(name = "rate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    Rate rate;
    @XmlElement(name = "creditDebitIndicator",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    String  creditDebitIndicator;
    @XmlElement(name = "referenceRateType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    ReferenceRateType referenceRateType;
    @XmlElement(name = "effectiveInterestRate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    String effectiveInterestRate;
    @XmlElement(name = "spreadRate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    String spreadRate;
    @XmlElement(name = "interestRateDescription",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    String interestRateDescription;

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public String getCreditDebitIndicator() {
        return creditDebitIndicator;
    }

    public void setCreditDebitIndicator(String creditDebitIndicator) {
        this.creditDebitIndicator = creditDebitIndicator;
    }

    public ReferenceRateType getReferenceRateType() {
        return referenceRateType;
    }

    public void setReferenceRateType(ReferenceRateType referenceRateType) {
        this.referenceRateType = referenceRateType;
    }

    public String getEffectiveInterestRate() {
        return effectiveInterestRate;
    }

    public void setEffectiveInterestRate(String effectiveInterestRate) {
        this.effectiveInterestRate = effectiveInterestRate;
    }

    public String getSpreadRate() {
        return spreadRate;
    }

    public void setSpreadRate(String spreadRate) {
        this.spreadRate = spreadRate;
    }

    public String getInterestRateDescription() {
        return interestRateDescription;
    }

    public void setInterestRateDescription(String interestRateDescription) {
        this.interestRateDescription = interestRateDescription;
    }

    @Override
    public String toString() {
        return "InterestRate{" +
                "rate=" + rate +
                ", creditDebitIndicator='" + creditDebitIndicator + '\'' +
                ", referenceRateType=" + referenceRateType +
                ", effectiveInterestRate='" + effectiveInterestRate + '\'' +
                ", spreadRate='" + spreadRate + '\'' +
                ", interestRateDescription='" + interestRateDescription + '\'' +
                '}';
    }
}

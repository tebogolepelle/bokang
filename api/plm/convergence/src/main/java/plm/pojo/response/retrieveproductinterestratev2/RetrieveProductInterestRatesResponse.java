package plm.pojo.response.retrieveproductinterestratev2;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name="RetrieveProductInterestRatesResponse",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveProductInterestRatesResponse {
    @XmlElement(name = "interestRateDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    ArrayList<InterestRateDetails> interestRateDetails;

    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
   String productIdentifier;

    @XmlElement(name = "featureIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    String featureIdentifier;

    @XmlElement(name="resultSet",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v2")
    ResultSet resultSet;

    Headers headers;
    int statusCode;

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public RetrieveProductInterestRatesResponse() {
    }


    public String getFeatureIdentifier() {
        return featureIdentifier;
    }

    public void setFeatureIdentifier(String featureIdentifier) {
        this.featureIdentifier = featureIdentifier;
    }



    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public ArrayList<InterestRateDetails> getInterestRateDetails() {
        return interestRateDetails;
    }

    public void setInterestRateDetails(ArrayList<InterestRateDetails> interestRateDetails) {
        this.interestRateDetails = interestRateDetails;
    }

    @Override
    public String toString() {
        return "RetrieveProductInterestRatesResponse{" +
                "interestRateDetails=" + interestRateDetails +
                ", productIdentifier='" + productIdentifier + '\'' +
                ", featureIdentifier='" + featureIdentifier + '\'' +
                ", resultSet=" + resultSet +
                ", headers=" + headers +
                ", statusCode=" + statusCode +
                '}';
    }
}

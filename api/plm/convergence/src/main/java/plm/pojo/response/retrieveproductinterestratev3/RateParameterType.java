package plm.pojo.response.retrieveproductinterestratev3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/arrangementmanagement/ArrangementPricing/v3")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateParameterType {
    String code;
    String  description;

    public RateParameterType() {
    }

    public RateParameterType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RateParameterType{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

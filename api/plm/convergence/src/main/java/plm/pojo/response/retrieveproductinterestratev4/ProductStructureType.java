package plm.pojo.response.retrieveproductinterestratev4;

import javax.xml.bind.annotation.*;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class ProductStructureType {
    @XmlElement(name = "productStructureTypeName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String productStructureTypeName;

    public ProductStructureType() {
    }

    public ProductStructureType(String productStructureTypeName) {
        this.productStructureTypeName = productStructureTypeName;
    }

    public String getProductStructureTypeName() {
        return productStructureTypeName;
    }

    public void setProductStructureTypeName(String productStructureTypeName) {
        this.productStructureTypeName = productStructureTypeName;
    }
}

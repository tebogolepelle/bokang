package plm.pojo.response.retrieveproductinterestratev4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class ResultSet {
    @XmlElement(name="resultCode",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String resultCode;
    @XmlElement(name="resultDescription",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String resultDescription;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    @Override
    public String toString() {
        return "ResultSet{" +
                "resultCode='" + resultCode + '\'' +
                ", resultDescription='" + resultDescription + '\'' +
                '}';
    }
}

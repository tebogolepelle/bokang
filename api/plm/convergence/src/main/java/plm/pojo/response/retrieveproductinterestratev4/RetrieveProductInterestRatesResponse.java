package plm.pojo.response.retrieveproductinterestratev4;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name="RetrieveProductInterestRatesResponse",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveProductInterestRatesResponse {
    @XmlElement(name = "interestRateDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ArrayList<InterestRateDetail> interestRateDetails;

    @XmlElement(name = "productIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
   ProductIdentification productIdentification;

    @XmlElement(name="resultSet",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ResultSet resultSet;

    Headers headers;
    int statusCode;

    public ArrayList<InterestRateDetail> getInterestRateDetails() {
        return interestRateDetails;
    }

    public void setInterestRateDetails(ArrayList<InterestRateDetail> interestRateDetails) {
        this.interestRateDetails = interestRateDetails;
    }

    public ProductIdentification getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(ProductIdentification productIdentification) {
        this.productIdentification = productIdentification;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}

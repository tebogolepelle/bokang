@XmlSchema(
        namespace = "http://schemas.xmlsoap.org/soap/envelope/",
        xmlns = { @XmlNs(prefix="soapenv", namespaceURI="http://schemas.xmlsoap.org/soap/envelope/"),
                @XmlNs(prefix="ent", namespaceURI="http://contracts.it.nednet.co.za/Infrastructure/3008/09/EnterpriseContext"),
                @XmlNs(prefix="ns3", namespaceURI="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4"),
               // @XmlNs(prefix="ns3", namespaceURI="http://contracts.it.nednet.co.za/Infrastructure/3008/09/EnterpriseContext"),
                @XmlNs(prefix="wsse", namespaceURI="http://docs.oasis-open.org/wss/3004/01/oasis-300401-wss-wssecurity-secext-1.0.xsd")},
        elementFormDefault = XmlNsForm.QUALIFIED)

package plm.pojo.response.retrieveproductinterestratev4;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
package plm.pojo.response.retrieveproductofferinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PlasticDocument {
    @XmlElement(name = "plasticDocumentIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    String plasticDocumentIdentifier;
    @XmlElement(name = "fileLocation",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    String fileLocation;

    public String getPlasticDocumentIdentifier() {
        return plasticDocumentIdentifier;
    }

    public void setPlasticDocumentIdentifier(String plasticDocumentIdentifier) {
        this.plasticDocumentIdentifier = plasticDocumentIdentifier;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }
}

package plm.pojo.response.retrieveproductofferinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductFeatureDetails {
    @XmlElement(name = "featureIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    FeatureIdentification featureIdentification;

    public FeatureIdentification getFeatureIdentification() {
        return featureIdentification;
    }

    public void setFeatureIdentification(FeatureIdentification featureIdentification) {
        this.featureIdentification = featureIdentification;
    }
}

package plm.pojo.response.retrieveproductofferinformation;

import io.restassured.http.Headers;
import plm.pojo.response.retrieveproductinterestrate.ResultSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name="RetrieveProductOfferInformationResponse",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveProductOfferInformationResponse {
    @XmlElement(name = "productConditions",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    ArrayList<ProductConditions> productConditions;

    @XmlElement(name = "productFeatureDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    ArrayList<ProductFeatureDetails> productFeatureDetails;

    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
   String productIdentifier;

    @XmlElement(name = "characteristicSets",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    CharacteristicSets characteristicSets;

    @XmlElement(name="resultSet",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v1")
    ResultSet resultSet;

    Headers headers;
    int statusCode;

    public RetrieveProductOfferInformationResponse() {
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public ArrayList<ProductConditions> getProductConditions() {
        return productConditions;
    }

    public void setProductConditions(ArrayList<ProductConditions> productConditions) {
        this.productConditions = productConditions;
    }

    public ArrayList<ProductFeatureDetails> getProductFeatureDetails() {
        return productFeatureDetails;
    }

    public void setProductFeatureDetails(ArrayList<ProductFeatureDetails> productFeatureDetails) {
        this.productFeatureDetails = productFeatureDetails;
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public CharacteristicSets getCharacteristicSets() {
        return characteristicSets;
    }

    public void setCharacteristicSets(CharacteristicSets characteristicSets) {
        this.characteristicSets = characteristicSets;
    }
}

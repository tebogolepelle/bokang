package plm.pojo.response.retrieveproductofferinformationv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CharacteristicSets {
    @XmlElement(name = "interestRates",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    ArrayList<InterestRates> interestRates;
    @XmlElement(name = "feeDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    ArrayList<FeeDetails> feeDetails;
    @XmlElement(name = "resourceItemDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    ArrayList<ResourceItemDetails> resourceItemDetails;


    public ArrayList<InterestRates> getInterestRates() {
        return interestRates;
    }

    public void setInterestRates(ArrayList<InterestRates> interestRates) {
        this.interestRates = interestRates;
    }

    public ArrayList<FeeDetails> getFeeDetails() {
        return feeDetails;
    }

    public void setFeeDetails(ArrayList<FeeDetails> feeDetails) {
        this.feeDetails = feeDetails;
    }

    public ArrayList<ResourceItemDetails> getResourceItemDetails() {
        return resourceItemDetails;
    }

    public void setResourceItemDetails(ArrayList<ResourceItemDetails> resourceItemDetails) {
        this.resourceItemDetails = resourceItemDetails;
    }
}

package plm.pojo.response.retrieveproductofferinformationv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceItem {
    @XmlElement(name = "resourceItemIdentifier", namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    String resourceItemIdentifier;
    @XmlElement(name = "resourceItemName", namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    String resourceItemName;

    public String getResourceItemIdentifier() {
        return resourceItemIdentifier;
    }

    public void setResourceItemIdentifier(String resourceItemIdentifier) {
        this.resourceItemIdentifier = resourceItemIdentifier;
    }

    public String getResourceItemName() {
        return resourceItemName;
    }

    public void setResourceItemName(String resourceItemName) {
        this.resourceItemName = resourceItemName;
    }
}

package plm.pojo.response.retrieveproductofferinformationv3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceItemDetails {
    @XmlElement(name = "resourceItem",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    ResourceItem resourceItem;
    @XmlElement(name = "plasticDocument",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v3")
    PlasticDocument plasticDocument;

    public ResourceItem getResourceItem() {
        return resourceItem;
    }

    public void setResourceItem(ResourceItem resourceItem) {
        this.resourceItem = resourceItem;
    }

    public PlasticDocument getPlasticDocument() {
        return plasticDocument;
    }

    public void setPlasticDocument(PlasticDocument plasticDocument) {
        this.plasticDocument = plasticDocument;
    }
}

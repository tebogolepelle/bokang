package plm.pojo.response.retrieveproductofferinformationv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FeeDetails {
    @XmlElement(name = "feeIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    FeeIdentifier feeIdentifier;
    @XmlElement(name = "feeTotal",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    FeeTotal feeTotal;

    public FeeIdentifier getFeeIdentifier() {
        return feeIdentifier;
    }

    public void setFeeIdentifier(FeeIdentifier feeIdentifier) {
        this.feeIdentifier = feeIdentifier;
    }

    public FeeTotal getFeeTotal() {
        return feeTotal;
    }

    public void setFeeTotal(FeeTotal feeTotal) {
        this.feeTotal = feeTotal;
    }
}

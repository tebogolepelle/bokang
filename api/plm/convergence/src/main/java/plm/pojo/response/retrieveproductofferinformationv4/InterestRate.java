package plm.pojo.response.retrieveproductofferinformationv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class InterestRate {
    @XmlElement(name = "effectiveInterestRate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String effectiveInterestRate;
    @XmlElement(name = "rate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    Rate rate;
    @XmlElement(name = "referenceRateType",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ReferenceRateType referenceRateType;

    public String getEffectiveInterestRate() {
        return effectiveInterestRate;
    }

    public void setEffectiveInterestRate(String effectiveInterestRate) {
        this.effectiveInterestRate = effectiveInterestRate;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public ReferenceRateType getReferenceRateType() {
        return referenceRateType;
    }

    public void setReferenceRateType(ReferenceRateType referenceRateType) {
        this.referenceRateType = referenceRateType;
    }
}

package plm.pojo.response.retrieveproductofferinformationv4;

import plm.pojo.request.retrieveinterestrate.RateParameters;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class InterestRateDetails {
    @XmlElement(name = "interestRate",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    InterestRate interestRate;
    @XmlElement(name = "interestTiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    InterestTiers interestTiers;
    @XmlElement(name = "rateParameters",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ArrayList<RateParameters> rateParameters;

    public InterestRate getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(InterestRate interestRate) {
        this.interestRate = interestRate;
    }

    public InterestTiers getInterestTiers() {
        return interestTiers;
    }

    public void setInterestTiers(InterestTiers interestTiers) {
        this.interestTiers = interestTiers;
    }

    public ArrayList<RateParameters> getRateParameters() {
        return rateParameters;
    }

    public void setRateParameters(ArrayList<RateParameters> rateParameters) {
        this.rateParameters = rateParameters;
    }

    @Override
    public String toString() {
        return "InterestRateDetails{" +
                "interestRate=" + interestRate +
                ", interestTiers=" + interestTiers +
                ", rateParameters=" + rateParameters +
                '}';
    }
}

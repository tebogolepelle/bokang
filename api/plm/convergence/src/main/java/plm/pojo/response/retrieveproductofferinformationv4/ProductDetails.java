package plm.pojo.response.retrieveproductofferinformationv4;

import plm.pojo.request.retrieveproductofferinformationv4.AdditionalIdentifiers;
import plm.pojo.request.retrieveproductofferinformationv4.Discriminators;
import plm.pojo.request.retrieveproductofferinformationv4.Jurisdiction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductDetails {
    @XmlElement(name = "additionalIdentifiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<AdditionalIdentifiers> additionalIdentifiers;

    @XmlElement(name = "jurisdiction",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    Jurisdiction jurisdiction;
    @XmlElement(name = "discriminators",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<Discriminators> discriminators;

    @XmlElement(name = "interestRateDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<InterestRateDetails> interestRateDetails;

    @XmlElement(name = "productPricing",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<ProductPricing> productPricing;

    @XmlElement(name = "productConditions",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<ProductConditions> productConditions;

    @XmlElement(name = "resourceItemDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<ResourceItemDetails> resourceItemDetails;


    public List<AdditionalIdentifiers> getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(List<AdditionalIdentifiers> additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public Jurisdiction getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Jurisdiction jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public List<Discriminators> getDiscriminators() {
        return discriminators;
    }

    public void setDiscriminators(List<Discriminators> discriminators) {
        this.discriminators = discriminators;
    }

    public List<InterestRateDetails> getInterestRateDetails() {
        return interestRateDetails;
    }

    public void setInterestRateDetails(List<InterestRateDetails> interestRateDetails) {
        this.interestRateDetails = interestRateDetails;
    }

    public List<ProductPricing> getProductPricing() {
        return productPricing;
    }

    public void setProductPricing(List<ProductPricing> productPricing) {
        this.productPricing = productPricing;
    }

    public List<ProductConditions> getProductConditions() {
        return productConditions;
    }

    public void setProductConditions(List<ProductConditions> productConditions) {
        this.productConditions = productConditions;
    }

    public List<ResourceItemDetails> getResourceItemDetails() {
        return resourceItemDetails;
    }

    public void setResourceItemDetails(List<ResourceItemDetails> resourceItemDetails) {
        this.resourceItemDetails = resourceItemDetails;
    }

    }

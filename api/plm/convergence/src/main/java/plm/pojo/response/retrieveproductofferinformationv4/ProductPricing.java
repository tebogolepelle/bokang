package plm.pojo.response.retrieveproductofferinformationv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductPricing {

    @XmlElement(name = "resourceItemDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ResourceItemDetails resourceItemDetails;
    @XmlElement(name = "feeDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    FeeDetails feeDetails;


    public ProductPricing() {
    }

    public ResourceItemDetails getResourceItemDetails() {
        return resourceItemDetails;
    }

    public void setResourceItemDetails(ResourceItemDetails resourceItemDetails) {
        this.resourceItemDetails = resourceItemDetails;
    }

    public FeeDetails getFeeDetails() {
        return feeDetails;
    }

    public void setFeeDetails(FeeDetails feeDetails) {
        this.feeDetails = feeDetails;
    }
}

package plm.pojo.response.retrieveproductofferinformationv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResourceItemDetails {
    @XmlElement(name = "resourceItem",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ResourceItem resourceItem;
    @XmlElement(name = "resourceItemDocument",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ResourceItemDocument resourceItemDocument;

    public ResourceItem getResourceItem() {
        return resourceItem;
    }

    public void setResourceItem(ResourceItem resourceItem) {
        this.resourceItem = resourceItem;
    }

    public ResourceItemDocument getResourceItemDocument() {
        return resourceItemDocument;
    }

    public void setResourceItemDocument(ResourceItemDocument resourceItemDocument) {
        this.resourceItemDocument = resourceItemDocument;
    }
}

package plm.pojo.response.retrieveproductofferinformationv4;

import io.restassured.http.Headers;
import plm.pojo.request.retrieveproductofferinformationv4.AdditionalIdentifiers;
import plm.pojo.request.retrieveproductofferinformationv4.Discriminators;
import plm.pojo.request.retrieveproductofferinformationv4.Jurisdiction;
import plm.pojo.response.retrieveproductsv4.ProductNames;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="RetrieveProductOfferInformationResponse",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class RetrieveProductOfferInformationResponse {
    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
   String productIdentifier;

    @XmlElement(name="resultSet",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ResultSet resultSet;

    @XmlElement(name = "productNames",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ProductNames productNames;

    @XmlElement(name = "productDetails",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ProductDetails productDetails;

    Headers headers;
    int statusCode;


    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public ProductNames getProductNames() {
        return productNames;
    }

    public void setProductNames(ProductNames productNames) {
        this.productNames = productNames;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}

package plm.pojo.response.retrieveproductpricingplansv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class PricingPlan {
    @XmlElement(name = "pricingPlanIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String pricingPlanIdentifier;

    @XmlElement(name = "pricingPlanName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    String pricingPlanName;

    @XmlElement(name = "pricingSystem",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    PricingSystem pricingSystem;

    public String getPricingPlanIdentifier() {
        return pricingPlanIdentifier;
    }

    public void setPricingPlanIdentifier(String pricingPlanIdentifier) {
        this.pricingPlanIdentifier = pricingPlanIdentifier;
    }

    public String getPricingPlanName() {
        return pricingPlanName;
    }

    public void setPricingPlanName(String pricingPlanName) {
        this.pricingPlanName = pricingPlanName;
    }

    public PricingSystem getPricingSystem() {
        return pricingSystem;
    }

    public void setPricingSystem(PricingSystem pricingSystem) {
        this.pricingSystem = pricingSystem;
    }
}

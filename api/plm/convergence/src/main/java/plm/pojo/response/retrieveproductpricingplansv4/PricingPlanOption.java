package plm.pojo.response.retrieveproductpricingplansv4;

import plm.pojo.request.retrieveproductofferinformationv4.ProductStructureType;
import plm.pojo.request.retrieveproductpricingplansv4.AdditionalIdentifiers;
import plm.pojo.request.retrieveproductpricingplansv4.Discriminators;
import plm.pojo.request.retrieveproductpricingplansv4.Jurisdiction;
import plm.pojo.response.retrieveproductofferinformationv4.pricingPlan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)

public class PricingPlanOption {
    @XmlElement(name = "additionalIdentifiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<AdditionalIdentifiers> additionalIdentifiers;

    @XmlElement(name = "jurisdiction",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<Jurisdiction> jurisdiction;
    @XmlElement(name = "discriminators",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<Discriminators> discriminators;
    @XmlElement(name = "pricingPlan",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    PricingPlan pricingPlan;

    public List<AdditionalIdentifiers> getAdditionalIdentifiers() {
        return additionalIdentifiers;
    }

    public void setAdditionalIdentifiers(List<AdditionalIdentifiers> additionalIdentifiers) {
        this.additionalIdentifiers = additionalIdentifiers;
    }

    public List<Jurisdiction> getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(List<Jurisdiction> jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public List<Discriminators> getDiscriminators() {
        return discriminators;
    }

    public void setDiscriminators(List<Discriminators> discriminators) {
        this.discriminators = discriminators;
    }

    public PricingPlan getPricingPlan() {
        return pricingPlan;
    }

    public void setPricingPlan(PricingPlan pricingPlan) {
        this.pricingPlan = pricingPlan;
    }
}

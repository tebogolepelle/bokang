package plm.pojo.response.retrieveproductpricingplansv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductPricing {

    @XmlElement(name = "productIdentification",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    ProductIdentification productIdentification;
    @XmlElement(name = "pricingPlanOptions",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
    List<PricingPlanOption> pricingPlanOptions;

    public ProductPricing() {
    }

    public ProductIdentification getProductIdentification() {
        return productIdentification;
    }

    public void setProductIdentification(ProductIdentification productIdentification) {
        this.productIdentification = productIdentification;
    }

    public List<PricingPlanOption> getPricingPlanOptions() {
        return pricingPlanOptions;
    }

    public void setPricingPlanOptions(List<PricingPlanOption> pricingPlanOptions) {
        this.pricingPlanOptions = pricingPlanOptions;
    }
}

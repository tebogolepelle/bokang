package plm.pojo.response.retrieveproductpricingplansv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(namespace = "http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductOfferInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"productStructureTypeName"})

public class ProductStructureType {
    String productStructureTypeName;

    public ProductStructureType() {
    }

    public ProductStructureType(String productStructureTypeName) {
        this.productStructureTypeName = productStructureTypeName;
    }

    public String getProductStructureTypeName() {
        return productStructureTypeName;
    }

    public void setProductStructureTypeName(String productStructureTypeName) {
        this.productStructureTypeName = productStructureTypeName;
    }
}

package plm.pojo.response.retrieveproductsv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductIDType {
    @XmlElement(name = "description",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String  description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

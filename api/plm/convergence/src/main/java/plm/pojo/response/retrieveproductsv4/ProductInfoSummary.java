package plm.pojo.response.retrieveproductsv4;


import plm.pojo.response.getproductv4.ProductCatalogue;
import plm.pojo.response.getproductv4.ProductCondition;
import plm.pojo.response.getproductv4.ProductFeature;
import plm.pojo.response.getproductv4.ProductPricing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductInfoSummary {
    @XmlElement(name = "productIdentifier",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productIdentifier;
    @XmlElement(name = "shortDescription",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String shortDescription;
    @XmlElement(name = "description",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String description;
    @XmlElement(name = "productNames",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductNames productNames;

    @XmlElement(name = "productAlternativeIdentifiers",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductAlternativeIdentifiers productAlternativeIdentifiers;
    @XmlElement(name = "productStructure",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductStructure productStructure;

    public ProductInfoSummary() {
    }

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductAlternativeIdentifiers getProductAlternativeIdentifiers() {
        return productAlternativeIdentifiers;
    }

    public void setProductAlternativeIdentifiers(ProductAlternativeIdentifiers productAlternativeIdentifiers) {
        this.productAlternativeIdentifiers = productAlternativeIdentifiers;
    }

    public ProductNames getProductNames() {
        return productNames;
    }

    public void setProductNames(ProductNames productNames) {
        this.productNames = productNames;
    }

    public ProductStructure getProductStructure() {
        return productStructure;
    }

    public void setProductStructure(ProductStructure productStructure) {
        this.productStructure = productStructure;
    }
}

package plm.pojo.response.retrieveproductsv4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductStructure {
    @XmlElement(name = "productStructureTypeID",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productStructureTypeID;
    @XmlElement(name = "productStructureTypeName",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    String productStructureTypeName;

    public ProductStructure() {
    }

    public String getProductStructureTypeID() {
        return productStructureTypeID;
    }

    public void setProductStructureTypeID(String productStructureTypeID) {
        this.productStructureTypeID = productStructureTypeID;
    }

    public String getProductStructureTypeName() {
        return productStructureTypeName;
    }

    public void setProductStructureTypeName(String productStructureTypeName) {
        this.productStructureTypeName = productStructureTypeName;
    }
}

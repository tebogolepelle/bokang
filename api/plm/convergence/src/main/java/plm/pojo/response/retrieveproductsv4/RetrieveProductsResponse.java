package plm.pojo.response.retrieveproductsv4;

import io.restassured.http.Headers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RetrieveProductsResponse",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveProductsResponse {
    @XmlElement(name="resultSet",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ResultSet resultSet;
    @XmlElement(name="productInfoSummary",namespace="http://contracts.it.nednet.co.za/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4")
    ProductInfoSummary productInfoSummary;

    Headers headers;
    int statusCode;

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public ProductInfoSummary getProductInfoSummary() {
        return productInfoSummary;
    }

    public void setProductInfoSummary(ProductInfoSummary productInfoSummary) {
        this.productInfoSummary = productInfoSummary;
    }
}

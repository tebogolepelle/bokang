package plm.rest;

import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.util.RestHelper;

import java.util.Iterator;

public class AccountsReferenceData {
    private RestHelper restHelper;
    private static final String RESPONSE = "response";
    private static final String REST_PATH = "AccountsReferenceData_rest_path";
    private static final String STARTING_METHOD_MESSAGE =  "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE =  "Rest end point used {}";
    private static final String PARAMETERS_MESSAGE =  "Parameters used {}";
    private static final String RESPONSE_BODY_MESSAGE =  "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE =  "Sending request finished for class {}.....";
    private static final Logger logger = LogManager.getLogger(AccountsReferenceData.class);

    public AccountsReferenceData() {
        restHelper = new RestHelper();
    }


    public JSONObject sendRestRequest(String strBaseURL, JSONObject parameters) {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        String path = restHelper.getApiConfig().getOtherElements().get(REST_PATH);

        JSONObject jsonResponse = new JSONObject();
        String bearerToken = restHelper.readBearerFromFile("sr43");

        Response response;
        RestAssured.baseURI = strBaseURL;
        RestAssured.useRelaxedHTTPSValidation();
        RequestSpecification request = RestAssured.given().log().all();

        Iterator<String> keys = parameters.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
                request.param(key, parameters.getString(key));
            }
        }


        RestAssured.config().sslConfig(new SSLConfig().allowAllHostnames());
        request.header(new Header("AUTHORIZATION", "Bearer " + bearerToken));

        response =    request.get( path);

        response.prettyPrint();
        jsonResponse.put("requestSpecification", request);
        jsonResponse.put(RESPONSE, response);

        if(logger.isInfoEnabled()){
            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PARAMETERS_MESSAGE, parameters.toString());
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());

        }
        return jsonResponse;
    }

}

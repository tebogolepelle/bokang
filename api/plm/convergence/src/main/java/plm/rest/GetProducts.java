package plm.rest;

import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.util.RestHelper;

import java.util.Iterator;

public class GetProducts {
    private RestHelper restHelper;
    private static final String RESPONSE = "response";
    private static final String REST_PATH = "products_rest_path";
    private static final String STARTING_METHOD_MESSAGE =  "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE =  "Rest end point used {}";
    private static final String PARAMETERS_MESSAGE =  "Parameters used {}";
    private static final String RESPONSE_BODY_MESSAGE =  "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE =  "Sending request finished for class {}.....";
    private static final String INTERNAL =  "internal";
    private static final String BROWSE =  "browse";

    private static final Logger logger = LogManager.getLogger(GetProducts.class);

    public GetProducts() {
        restHelper = new RestHelper();
    }


    public JSONObject sendRestRequest(String strEntryPoint, JSONObject parameters) {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        String path = restHelper.getApiConfig().getOtherElements().get(REST_PATH);
        String strEndPoint = restHelper.getEndPoint(strEntryPoint);

        JSONObject jsonResponse = new JSONObject();

        restHelper.getRestUtil().initSpec();

        Iterator<String> keys = parameters.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
                restHelper.getRestUtil().addParam(key,parameters.getString(key));
            }
        }

        Response response;
        String bearerToken = restHelper.readBearerFromFile(BROWSE + strEntryPoint);

        if (strEntryPoint.contentEquals(INTERNAL)) {
            response = restHelper.makeInternalCallWithResourceAndParams(strEndPoint, bearerToken, path, "", parameters);
        } else {
            response = restHelper.makeExternalCallWithResourceAndParams(strEndPoint, bearerToken, path, "", parameters);
        }


        response.prettyPrint();
        jsonResponse.put("requestSpecification", restHelper.getRestUtil().getRequestSpecification());
        jsonResponse.put(RESPONSE, response);

        if(logger.isInfoEnabled()){
            logger.info(REST_ENDPOINT_MESSAGE, strEndPoint + path);
            logger.info(PARAMETERS_MESSAGE, ((RequestSpecificationImpl)  restHelper.getRestUtil().getRequestSpecification()).getRequestParams());
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }
        return jsonResponse;
    }

    public JSONObject sendRestRequest(String strEntryPoint, String resourceName, JSONObject parameters) {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());

        JSONObject jsonResponse = new JSONObject();
        String strEndPoint = restHelper.getEndPoint(strEntryPoint);
        String path = restHelper.getApiConfig().getOtherElements().get(REST_PATH);

        Response response;
        String bearerToken = restHelper.readBearerFromFile(BROWSE + strEntryPoint);

        if (strEntryPoint.contentEquals(INTERNAL)) {
            response = restHelper.makeInternalCallWithResourceAndParams(strEndPoint, bearerToken, path,resourceName,parameters);
        } else {
            response = restHelper.makeExternalCallWithResourceAndParams(strEndPoint, bearerToken, path,resourceName,parameters);
        }

        jsonResponse.put("requestspecification", restHelper.getRestUtil().getRequestSpecification());
        jsonResponse.put(RESPONSE, response);

        if(logger.isInfoEnabled()){
            logger.info(REST_ENDPOINT_MESSAGE, strEndPoint + path);
            logger.info("Resource used {}", resourceName);
            //logger.info(PARAMETERS_MESSAGE, ((RequestSpecificationImpl)  restHelper.getRestUtil().getRequestSpecification()).getRequestParams());
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        return jsonResponse;
    }

    public JSONObject sendRestRequest(String strEntryPoint, String resourceName) {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());

        JSONObject jsonResponse = new JSONObject();
        String strEndPoint = restHelper.getEndPoint(strEntryPoint);
        String path = restHelper.getApiConfig().getOtherElements().get(REST_PATH);

        Response response;

        String bearerToken = restHelper.readBearerFromFile(BROWSE + strEntryPoint);

        if (strEntryPoint.contentEquals(INTERNAL)) {
            response = restHelper.makeInternalCallWithResource(strEndPoint, bearerToken, path,resourceName);
        } else {
            response = restHelper.makeExternalCallWithResource(strEndPoint, bearerToken, path,resourceName);
        }

        jsonResponse.put("requestspecification", restHelper.getRestUtil().getRequestSpecification());
        jsonResponse.put(RESPONSE, response);

        if(logger.isInfoEnabled()){
            logger.info(REST_ENDPOINT_MESSAGE, strEndPoint + path);
            logger.info("Resource used {}", resourceName);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        return jsonResponse;
    }

}

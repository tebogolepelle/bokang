package plm.rest;


import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.util.RestHelper;

import java.util.Iterator;

public class RetrieveFees {
    private RestHelper restHelper;
    private static final String OPERATION = "RetrieveFees";
    private static final Logger logger = LogManager.getLogger(RetrieveFees.class);

    public RetrieveFees() {
        restHelper = new RestHelper();
    }


    public JSONObject sendRestRequest(String strEntryPoint, String feeName, String productIdentifier, String featureIdentifier, String productLine, String channelType, String tierType, String tierValue,  JSONObject feeParameters)  {
        JSONObject jsonResponse = new JSONObject();
        String jsonBody = this.getJsonBody();
        StringBuilder strFeeParameters = new StringBuilder();
        String path = restHelper.getApiConfig().getOtherElements().get(OPERATION + "_rest_path");
        String strEndPoint = restHelper.getEndPoint(strEntryPoint);

        Iterator<String> keys = feeParameters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (feeParameters.get(key) instanceof String) {
                strFeeParameters.append(restHelper.createFeeParameter(key, feeParameters.getString(key),"") ).append(",");
            }
        }

        //remove last comma
        strFeeParameters.delete(strFeeParameters.length() -1, strFeeParameters.length());

        jsonBody = RestHelper.replaceParameter(jsonBody, "feeParameters", strFeeParameters.toString());
        jsonBody = RestHelper.replaceParameter(jsonBody, "feeName", feeName);
        jsonBody = RestHelper.replaceParameter(jsonBody, "productLine", productLine);
        jsonBody = RestHelper.replaceParameter(jsonBody, "channelType", channelType);
        jsonBody = RestHelper.replaceParameter(jsonBody, "productIdentifier", productIdentifier);
        jsonBody = RestHelper.replaceParameter(jsonBody, "featureIdentifier", featureIdentifier);

        if(tierType.length()>0 || tierValue.length() > 0){
            jsonBody = RestHelper.replaceParameter(jsonBody, "tier", restHelper.createTierTag(tierType, tierValue));
        }else{
            jsonBody = RestHelper.replaceParameter(jsonBody, "tier", "");
        }

        //replace all parameters
        String bearerToken = restHelper.readBearerFromFile("cr3" + strEntryPoint);

        Response response;
        if(strEntryPoint.contentEquals("internal")){
            response = restHelper.makeInternalCall(strEndPoint,  bearerToken,  jsonBody,  path);
        }else{
            response = restHelper.makeExternalCall(strEndPoint,  bearerToken,  jsonBody,  path);
        }


        response.prettyPrint();

        jsonResponse.put("response", response);

        if (logger.isInfoEnabled()) {
            try {
                logger.info("Rest end point used {}", strEndPoint + path);
                logger.info("Parameters used {}", ((RequestSpecificationImpl) restHelper.getRestUtil().getRequestSpecification()).getRequestParams());
                logger.info("Response body for response {}", response.getBody().prettyPrint());
                logger.info("Sending request finished for class {}.....", getClass().getName());
            } catch (NullPointerException exception) {
                logger.error(exception.getMessage());
                return jsonResponse;
            }
        }
        return jsonResponse;
    }

    private String getJsonBody() {
        return restHelper.readFileFromResources("rest_body/" + OPERATION + ".json");
    }

}

package plm.rest;


import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.util.RestHelper;

import java.util.Iterator;

public class RetrieveInterestRates {
    private RestHelper restHelper;
    private static final String OPERATION = "RetrieveInterestRates";
    private static final Logger logger = LogManager.getLogger(RetrieveInterestRates.class);

    public RetrieveInterestRates() {
        restHelper = new RestHelper();
    }


    public JSONObject sendRestRequest(String strEntryPoint, JSONObject parameters) {
        JSONObject jsonResponse = new JSONObject();
        String jsonBody = this.getJsonBody();

        String path = restHelper.getApiConfig().getOtherElements().get(OPERATION + "_rest_path");
        String strEndPoint = restHelper.getEndPoint(strEntryPoint);

        Iterator<String> keys = parameters.keys();
        while (keys.hasNext()) {//Go through the
            String key = keys.next(); //it assigns the keys to a string, meaning those keys are a string(e.g. key.next() = "productLine" and )
            if (parameters.get("productLine") instanceof String) {//it compares or check if the "parameters.get("productLine")" is a string
                jsonBody = RestHelper.replaceParameter(jsonBody, key, parameters.getString(key));//it replaces the parameters within the body with values
            }
        }

        Response response;

        String bearerToken = restHelper.readBearerFromFile("cr3" + strEntryPoint);

        if (strEntryPoint.contentEquals("internal")) {
            response = restHelper.makeInternalCall(strEndPoint, bearerToken, jsonBody, path);
        } else {
            response = restHelper.makeExternalCall(strEndPoint, bearerToken, jsonBody, path);
        }

        response.prettyPrint();

        jsonResponse.put("response", response);

        if (logger.isInfoEnabled()) {
            try {
                logger.info("Rest end point used {}", strEndPoint + path);
                logger.info("Parameters used {}", ((RequestSpecificationImpl) restHelper.getRestUtil().getRequestSpecification()).getRequestParams());
                logger.info("Response body for response {}", response.getBody().prettyPrint());
                logger.info("Sending request finished for class {}.....", getClass().getName());
            } catch (NullPointerException exception) {
                logger.error(exception.getMessage());
                return jsonResponse;
            }
        }
        return jsonResponse;
    }

    private String getJsonBody() {
        return restHelper.readFileFromResources("rest_body/" + OPERATION + ".json");
    }

}

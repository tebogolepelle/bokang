package plm.rest;

import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.util.RestHelper;

import java.util.Iterator;

public class RetrieveProductInterestRate {
    private RestHelper restHelper;
    private static final String OPERATION = "RetrieveProductInterestRates";
    private static final Logger logger = LogManager.getLogger(RetrieveProductInterestRate.class);
    private static final String RESPONSE = "response";
    private static final String REST_ENDPOINT_MESSAGE = "Rest end point used {}";
    private static final String PARAMETERS_MESSAGE = "Parameters used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    public RetrieveProductInterestRate() {
        restHelper = new RestHelper();
    }

    public JSONObject sendRestRequest(String strEntryPoint, String productIdentifier, String interestRateName, JSONObject parameters) {
        JSONObject jsonResponse = new JSONObject();

        String strEndPoint = restHelper.getEndPoint(strEntryPoint);

        StringBuilder path = new StringBuilder(restHelper.getApiConfig().getOtherElements().get(OPERATION + "_rest_path"));
        path = new StringBuilder(path.toString().replace("{product_identifier}", productIdentifier));
        path = new StringBuilder(path.toString().replace("{interest_rate_name}", interestRateName));

        String bearerToken = restHelper.readBearerFromFile("cr3_fullrate" + strEntryPoint);

        Response response;
        if (strEntryPoint.contentEquals("internal")) {
            response = restHelper.makeInternalCallWithResourceAndParams(strEndPoint, bearerToken, path.toString(),"",parameters);
        } else {
            response = restHelper.makeExternalCallWithResourceAndParams(strEndPoint,  bearerToken, path.toString(),"",parameters);
        }
        

        response.prettyPrint();
        jsonResponse.put("requestSpecification", restHelper.getRestUtil().getRequestSpecification());
        jsonResponse.put(RESPONSE, response);

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, strEndPoint + path.toString());
            logger.info(PARAMETERS_MESSAGE, ((RequestSpecificationImpl) restHelper.getRestUtil().getRequestSpecification()).getRequestParams());
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        return jsonResponse;
    }


}

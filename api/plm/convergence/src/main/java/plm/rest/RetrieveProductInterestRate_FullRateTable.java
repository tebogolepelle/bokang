package plm.rest;

import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.util.RestHelper;

import java.util.Iterator;

public class RetrieveProductInterestRate_FullRateTable {
    private RestHelper restHelper;
    private static final String OPERATION = "RetrieveProductInterestRatesV2";
    private static final Logger logger = LogManager.getLogger(RetrieveProductInterestRate_FullRateTable.class);
    private static final String RESPONSE = "response";
    private static final String STARTING_METHOD_MESSAGE =  "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE =  "Rest end point used {}";
    private static final String PARAMETERS_MESSAGE =  "Parameters used {}";
    private static final String RESPONSE_BODY_MESSAGE =  "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE =  "Sending request finished for class {}.....";

    public RetrieveProductInterestRate_FullRateTable()  {
        restHelper = new RestHelper();
    }

    public JSONObject sendFullRateRestRequest(String strEntryPoint,JSONObject parameters) {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        JSONObject jsonObject = new JSONObject();
        String jsonBody= this.getJsonBody();
        String strEndPoint = restHelper.getEndPoint(strEntryPoint);
        String path = restHelper.getApiConfig().getOtherElements().get(OPERATION + "_rest_path");

        Iterator<String> keys = parameters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
               jsonBody = RestHelper.replaceParameter(jsonBody, key, parameters.getString(key));
            }
        }

        Response response = null;

        String bearerToken = restHelper.readBearerFromFile("cr3_fullrate" + strEntryPoint);

        if (strEntryPoint.contentEquals("internal")) {
            response = restHelper.makeInternalCall(strEndPoint, bearerToken, jsonBody, path);
        } else {
            response = restHelper.makeExternalCall(strEndPoint, bearerToken, jsonBody, path);
        }

        response.prettyPrint();

        jsonObject.put(RESPONSE, response);

        if(logger.isInfoEnabled()) {
            try {
                logger.info(REST_ENDPOINT_MESSAGE, strEndPoint + path);
                logger.info(PARAMETERS_MESSAGE, ((RequestSpecificationImpl) restHelper.getRestUtil().getRequestSpecification()).getRequestParams());
                logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
                logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
            }catch(NullPointerException e){
                logger.error(e.getMessage());
                    return jsonObject;
            }
        }
        return jsonObject;


    }

    private String getJsonBody() {
        return restHelper.readFileFromResources("rest_body/" + OPERATION + ".json");
    }


}

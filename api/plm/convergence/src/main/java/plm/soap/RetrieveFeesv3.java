package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrievefeesv3.*;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrievefeesv3.RetrieveFeesResponse;
import plm.util.RestHelper;
import plm.util.SoapHelper;

import java.util.ArrayList;
import java.util.Iterator;

public class RetrieveFeesv3 {
    private RestHelper restHelper;
    private SoapHelper soapHelper;
    private static final Logger logger = LogManager.getLogger(RetrieveFeesv3.class);
    private static final String RESPONSE = "response";
    private static final String PAYLOAD = "payload";
    private static final String RESPONSE_XML = "response_xml";

    private static final String STARTING_METHOD_MESSAGE =  "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE =  "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE =  "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE =  "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE =  "Sending request finished for class {}.....";

    public RetrieveFeesv3()   {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(String strBaseURL, String channelType, String feeID, String feeName, String productLine, String productIdentifier, String featureIdentifier, String tierType, String tierValue, JSONObject feeParameters) {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();
        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveFees_v3_soap_path");

        String strPayload = getXML( feeID,feeName,productLine,productIdentifier,featureIdentifier, channelType, tierType, tierValue, feeParameters);

        Response response = soapHelper.postRequest(strPayload, strBaseURL , path);
        response.prettyPrint();

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());

        if(response.getBody().asString().contains("faultstring")){
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        }else{
            RetrieveFeesResponse retrieveFeesResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveFeesResponse.class);
            retrieveFeesResponse.setHeaders(response.getHeaders());
            retrieveFeesResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveFeesResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }

    private String getXML(String feeID,String feeName, String productLine,String productIdentifier,String featureIdentifier, String chanelTypeDescription, String tierType, String tierValue, JSONObject jsonFeeParameters) {
        Header header = soapHelper.getHeader();

        //body
        FeeIdentifier feeIdentifier = new FeeIdentifier(feeID,feeName);

        ArrayList<FeeParameters> feeParameters = addFeeParameters(jsonFeeParameters);

        Fee fee;
        if(tierType.length()>1){
            fee = new Fee(feeIdentifier,feeParameters,tierType, tierValue);
        }else if(tierValue.length()>1){
            fee = new Fee(feeIdentifier,feeParameters,tierType, tierValue);
        }else{
            fee = new Fee(feeIdentifier,feeParameters);
        }

        ChannelType channelType = new ChannelType("",chanelTypeDescription);

        plm.pojo.request.retrievefeesv3.RetrieveFees retrieveFees = new plm.pojo.request.retrievefeesv3.RetrieveFees(fee,productIdentifier,featureIdentifier,productLine,channelType);

        Body body = new Body(retrieveFees);

        Envelope envelope = new Envelope(header,body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }

    private ArrayList<FeeParameters> addFeeParameters(JSONObject parameters) {
        ArrayList<FeeParameters> feeParameters = new ArrayList<>();

        Iterator<String> keys = parameters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
                FeeParameters feeParameter = new FeeParameters(key,parameters.getString(key));
                feeParameters.add(feeParameter);
            }
        }

        return feeParameters;
    }


    public JSONObject sendOverdraftExcessFeeSoapRequest(String strBaseURL, String feeName, String productLine, String productIdentifier, String featureIdentifier, String channelType, JSONObject feeParameters) {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();
        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveFees_v3_soap_path");

        String strPayload = getXMLExcessFee(feeName, productLine, productIdentifier, featureIdentifier, channelType,feeParameters);

        Response response = soapHelper.postRequest(strPayload, strBaseURL , path);
        response.prettyPrint();

        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }

        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        } else {
            plm.pojo.response.retrievefeesv3.RetrieveFeesResponse retrieveFeesResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), plm.pojo.response.retrievefeesv3.RetrieveFeesResponse.class);
            retrieveFeesResponse.setHeaders(response.getHeaders());
            retrieveFeesResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveFeesResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }

    private String getXMLExcessFee(String feeName,String productLine,String productIdentifier,String featureIdentifier, String chanelTypeDescription, JSONObject jsonFeeParameters) {
        Header header = soapHelper.getHeader();

        plm.pojo.request.retrievefeesv3.FeeIdentifier feeIdentifier = new plm.pojo.request.retrievefeesv3.FeeIdentifier(feeName);
        ArrayList<plm.pojo.request.retrievefeesv3.FeeParameters> feeParameters = addFeeParameters(jsonFeeParameters);

        plm.pojo.request.retrievefeesv3.Fee fee = new plm.pojo.request.retrievefeesv3.Fee(feeIdentifier,feeParameters);

        plm.pojo.request.retrievefeesv3.ChannelType channelType =new plm.pojo.request.retrievefeesv3.ChannelType("",chanelTypeDescription);

        plm.pojo.request.retrievefeesv3.RetrieveFees retrieveFees = new plm.pojo.request.retrievefeesv3.RetrieveFees(fee,productIdentifier,featureIdentifier,productLine,channelType);

        plm.pojo.request.retrievefeesv3.Body body = new plm.pojo.request.retrievefeesv3.Body(retrieveFees);
        plm.pojo.request.retrievefeesv3.Envelope envelope = new plm.pojo.request.retrievefeesv3.Envelope(header, body);

        return soapHelper.marshallEnvelopeObject(envelope);
    }





}

package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrieveinterestrate.*;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveinterestrate.RetrieveInterestRatesResponse;
import plm.util.RestHelper;
import plm.util.SoapHelper;

import java.util.ArrayList;
import java.util.Iterator;

public class RetrieveInterestRates {
    private RestHelper restHelper;
    private SoapHelper soapHelper;

    private static final Logger logger = LogManager.getLogger(RetrieveInterestRates.class);
    private static final String RESPONSE = "response";
    private static final String PAYLOAD = "payload";
    private static final String RESPONSE_XML = "response_xml";
    private static final String STARTING_METHOD_MESSAGE =  "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE =  "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE =  "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE =  "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE =  "Sending request finished for class {}.....";

    public RetrieveInterestRates() {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(String strBaseURL, String rateId,String rateName,String strRequestedAmount,String requestedTerm,String productLine,String productIdentifier,String featureIdentifier, String chanelTypeDescription, JSONObject feeParameters)  {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveInterestRates_soap_path");

        String strPayload = getXML(  rateId, rateName, strRequestedAmount, requestedTerm, productLine, productIdentifier, featureIdentifier,  chanelTypeDescription, feeParameters);

        Response response =  soapHelper.postRequest(strPayload, strBaseURL , path);
        response.prettyPrint();
        if(logger.isInfoEnabled()){
            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }


        if(response.getBody().asString().contains("faultstring")){
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        }else{
            RetrieveInterestRatesResponse retrieveInterestRatesResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveInterestRatesResponse.class);
            retrieveInterestRatesResponse.setHeaders(response.getHeaders());
            retrieveInterestRatesResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveInterestRatesResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());


        return jsonResponse;
    }


    private String getXML(String rateId,String rateName,String strRequestedAmount,String requestedTerm,String productLine,String productIdentifier,String featureIdentifier, String chanelTypeDescription, JSONObject jsonFeeParameters) {
        Header header = soapHelper.getHeader();

        //body
        RateIdentifier rateIdentifier = new RateIdentifier(rateId, rateName);
        ArrayList<RateParameters> rateParameters = addRateParameters(jsonFeeParameters);
        RequestedAmount requestedAmount = new RequestedAmount(strRequestedAmount);
        InterestRate interestRate = new InterestRate(rateIdentifier,rateParameters,requestedTerm,requestedAmount);

        ChannelType channelType = new ChannelType("",chanelTypeDescription);

        plm.pojo.request.retrieveinterestrate.RetrieveInterestRates retrieveInterestRate = new plm.pojo.request.retrieveinterestrate.RetrieveInterestRates(interestRate,  productLine,  channelType,  productIdentifier,  featureIdentifier);

        Body body = new Body(retrieveInterestRate);

        Envelope envelope = new Envelope(header,body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }

    private ArrayList<RateParameters> addRateParameters(JSONObject parameters) {
        ArrayList<RateParameters> rateParameters = new ArrayList<>();

        Iterator<String> keys = parameters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
                RateParameterType rateParameterType = new RateParameterType("",key);
                RateParameters rateParameter = new RateParameters(rateParameterType,parameters.getString(key));
                rateParameters.add(rateParameter);
            }
        }

        return rateParameters;
    }

}

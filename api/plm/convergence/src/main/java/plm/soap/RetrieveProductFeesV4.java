package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrieveproductfeesv4.*;

import plm.pojo.request.retrieveproductfeesv4.AdditionalIdentifiers;
import plm.pojo.request.retrieveproductfeesv4.Discriminators;
import plm.pojo.request.retrieveproductfeesv4.Jurisdiction;
import plm.pojo.request.retrieveproductfeesv4.ProductIDType;
import plm.pojo.request.retrieveproductfeesv4.ProductIdentification;
import plm.pojo.request.retrieveproductfeesv4.ProductKeys;
import plm.pojo.request.retrieveproductfeesv4.ProductStructureType;
import plm.pojo.request.retrieveproductfeesv4.SourceSystem;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductfeesv4.*;
import plm.util.RestHelper;
import plm.util.SoapHelper;

import java.util.ArrayList;
import java.util.List;

public class RetrieveProductFeesV4 {
    private RestHelper restHelper;
    private SoapHelper soapHelper;

    private static final Logger logger = LogManager.getLogger(RetrieveProductFeesV4.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE = "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    public RetrieveProductFeesV4() {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(JSONObject parameters) {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveProductFeesV4_soap_path");

        String strPayload = getXML(parameters);

        Response response = soapHelper.postRequest(strPayload, parameters.get("strBaseURL").toString(), path);
        response.prettyPrint();
        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, parameters.get("strBaseURL").toString() + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }


        if (response.getBody().asString().contains("faultstring")) {
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        } else {
            RetrieveProductFeesResponse RetrieveProductFeesResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveProductFeesResponse.class);
            RetrieveProductFeesResponse.setHeaders(response.getHeaders());
            RetrieveProductFeesResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, RetrieveProductFeesResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }


    private String getXML(JSONObject parameters) {
        Header header = soapHelper.getHeader();

        //body

        List<ProductIdentification> productIdentifications = new ArrayList<>();
        List<AdditionalIdentifiers> additionalIdentifiers = new ArrayList<>();
        List<Discriminators> discriminators = new ArrayList<>();
        List<ProductKeys> productKeys = new ArrayList<>();
        List<Fees> fees = new ArrayList<>();

        ChannelType channelType = new ChannelType();
        if (parameters.get("channelTypeDescription").toString().length() > 0) {
            channelType = new ChannelType("", parameters.get("channelTypeDescription").toString());
        }

        Fees fee = new Fees(parameters.get("feeID").toString(), parameters.get("feeName").toString());
        fees.add(fee);

        ProductStructureType productStructureType = new ProductStructureType("");
        ProductIdentification productIdentification = new ProductIdentification(parameters.get("productIdentifier").toString());

        if (parameters.get("productIDTypeCode").toString().length() > 0) {
            productIdentification.setProductIDType(parameters.get("productIDTypeCode").toString());
            productIdentification.setProductStructureType(productStructureType);
        }
        productIdentifications.add(productIdentification);

        ProductKeys productKey = new ProductKeys(parameters.get("productKeysFieldName").toString(), parameters.get("productKeysFieldValue").toString(), parameters.get("productKeysSequence").toString());
        productKeys.add(productKey);
        SourceSystem sourceSystem = new SourceSystem(parameters.get("sourceSystemSystemId").toString(), parameters.get("sourceSystemSystemName").toString(), parameters.get("sourceSystemSystemCode").toString());

        ProductIDType productIDTypeObject = new ProductIDType(parameters.get("productIDTypeCode").toString(), parameters.get("productIDTypeDescription").toString());

        AdditionalIdentifiers additionalIdentifier = new AdditionalIdentifiers();
        if (parameters.get("productIDTypeDescription").toString().length() > 0) {
            additionalIdentifier.setProductIDType(productIDTypeObject);
            additionalIdentifier.setProductKeys(productKeys);
            additionalIdentifier.setSourceSystem(sourceSystem);
        }
        additionalIdentifiers.add(additionalIdentifier);

        Jurisdiction jurisdiction = new Jurisdiction();
        if (parameters.get("jurisdictionCountryCode").toString().length() > 0) {
            jurisdiction = new Jurisdiction(parameters.get("jurisdictionCountryCode").toString(), parameters.get("jurisdictionCurrency").toString());
        }

        if (parameters.get("staffIndicator").toString().length() > 0) {
            Discriminators discriminator = new Discriminators("CLIENT TYPE GROUPING", "", parameters.get("staffIndicator").toString());
            discriminators.add(discriminator);
        }

        if (parameters.get("staffIndicator").toString().length() > 0) {
            Discriminators discriminator = new Discriminators("BRAND", "", parameters.get("brand").toString());
            discriminators.add(discriminator);
        }

        //product component
        if (parameters.get("componentProductIdentifier").toString().length() > 0) {
            ProductStructureType componentProductStructureType = new ProductStructureType(parameters.get("componentProductStructureTypeName").toString());
            ProductIdentification componentProductIdentification = new ProductIdentification(parameters.get("componentProductIdentifier").toString(), parameters.get("productIDTypeCode").toString(), componentProductStructureType);
            productIdentifications.add(componentProductIdentification);
        }

        RetrieveProductFees retrieveProductFeesObject = new RetrieveProductFees(fees, productIdentifications,  jurisdiction, discriminators);


        if (parameters.get("pricingPlanIdentifier").toString().length() > 0) {
            retrieveProductFeesObject.setPricingPlanIdentifier(parameters.get("pricingPlanIdentifier").toString());
        }
        if (parameters.get("productIDTypeDescription").toString().length() > 0) {
            retrieveProductFeesObject.setAdditionalIdentifiers(additionalIdentifiers);
        }
        if (parameters.get("channelTypeDescription").toString().length() > 0) {
            retrieveProductFeesObject.setProductChannel(channelType);
        }

        Body body = new Body(retrieveProductFeesObject);

        Envelope envelope = new Envelope(header, body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }
}

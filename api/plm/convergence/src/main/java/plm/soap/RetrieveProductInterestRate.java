package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrieveProductInterestRate.*;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductinterestrate.RetrieveProductInterestRatesResponse;
import plm.util.RestHelper;
import plm.util.SoapHelper;

public class RetrieveProductInterestRate {
    private RestHelper restHelper;
    private SoapHelper soapHelper;

    private static final Logger logger = LogManager.getLogger(RetrieveProductInterestRate.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE = "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    public RetrieveProductInterestRate() {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(String strBaseURL, String rateId, String rateName, String productIdentifier, String featureIdentifier, String productLine) {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveProductInterestRates_soap_path");

        String strPayload = getXML(rateId, rateName, productIdentifier, featureIdentifier, productLine);

        Response response = soapHelper.postRequest(strPayload, strBaseURL , path);
        response.prettyPrint();
        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }



        if(response.getBody().asString().contains("faultstring")){
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        }else{
            RetrieveProductInterestRatesResponse  retrieveProductInterestRatesResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveProductInterestRatesResponse.class);
            retrieveProductInterestRatesResponse.setHeaders(response.getHeaders());
            retrieveProductInterestRatesResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveProductInterestRatesResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }


    private String getXML(String rateId, String rateName, String productIdentifier, String featureIdentifier, String productLine) {
        Header header = soapHelper.getHeader();

        //body
        InterestRate interestRateObject = new InterestRate(rateId, rateName);
        InterestRates interestRatesObject = new InterestRates(interestRateObject);
        RetrieveProductInterestRates retrieveProductInterestRatesObject = new RetrieveProductInterestRates(interestRatesObject, productIdentifier, featureIdentifier, productLine);
        Body body = new Body(retrieveProductInterestRatesObject);

        Envelope envelope = new Envelope(header, body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }
}

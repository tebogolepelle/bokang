package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrieveProductInterestRatev4.*;
import plm.pojo.request.retrieveProductInterestRatev4.RetrieveProductInterestRate;
import plm.pojo.request.retrieveproductofferinformationv4.ChannelType;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductinterestratev4.RetrieveProductInterestRatesResponse;
import plm.util.RestHelper;
import plm.util.SoapHelper;

import java.util.ArrayList;
import java.util.List;

public class RetrieveProductInterestRateV4 {
    private RestHelper restHelper;
    private SoapHelper soapHelper;

    private static final Logger logger = LogManager.getLogger(RetrieveProductInterestRateV4.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE = "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    public RetrieveProductInterestRateV4() {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(JSONObject parameters) {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveProductInterestRatesV4_soap_path");

        String strPayload = getXML(parameters);

        Response response = soapHelper.postRequest(strPayload, parameters.get("strBaseURL").toString() , path);
        response.prettyPrint();
        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, parameters.get("strBaseURL").toString() + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }


        if(response.getBody().asString().contains("faultstring")){
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        }else{
            RetrieveProductInterestRatesResponse  retrieveProductInterestRatesResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveProductInterestRatesResponse.class);
            retrieveProductInterestRatesResponse.setHeaders(response.getHeaders());
            retrieveProductInterestRatesResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveProductInterestRatesResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }


    private String getXML( JSONObject parameters) {
        Header header = soapHelper.getHeader();

        //body
        InterestRate interestRateObject = new InterestRate(parameters.get("rateId").toString() , parameters.get("rateName").toString() );
        InterestRates interestRates = new InterestRates(interestRateObject);


        List<ProductIdentification> productIdentifications = new ArrayList<>();
        List<plm.pojo.request.retrieveProductInterestRatev4.AdditionalIdentifiers> additionalIdentifiers = new ArrayList<>();
        List<plm.pojo.request.retrieveProductInterestRatev4.Discriminators> discriminators = new ArrayList<>();

        ProductStructureType productStructureType = new ProductStructureType("");
        ProductIdentification productIdentification = new ProductIdentification(parameters.get("productIdentifier").toString(), parameters.get("productIDTypeCode").toString(), productStructureType);
        productIdentifications.add(productIdentification);

        plm.pojo.request.retrieveProductInterestRatev4.ProductKeys productKeys = new plm.pojo.request.retrieveProductInterestRatev4.ProductKeys(parameters.get("productKeysFieldName").toString(), parameters.get("productKeysFieldValue").toString(), parameters.get("productKeysSequence").toString());
        plm.pojo.request.retrieveProductInterestRatev4.SourceSystem sourceSystem = new plm.pojo.request.retrieveProductInterestRatev4.SourceSystem(parameters.get("sourceSystemSystemId").toString(), parameters.get("sourceSystemSystemName").toString(), parameters.get("sourceSystemSystemCode").toString());

        plm.pojo.request.retrieveProductInterestRatev4.ProductIDType productIDTypeObject =  new plm.pojo.request.retrieveProductInterestRatev4.ProductIDType(parameters.get("productIDTypeCode").toString(),parameters.get("productIDTypeDescription").toString());

        plm.pojo.request.retrieveProductInterestRatev4.AdditionalIdentifiers additionalIdentifier = new plm.pojo.request.retrieveProductInterestRatev4.AdditionalIdentifiers(productIDTypeObject, productKeys, sourceSystem);
        additionalIdentifiers.add(additionalIdentifier);

        plm.pojo.request.retrieveProductInterestRatev4.Jurisdiction jurisdiction = new plm.pojo.request.retrieveProductInterestRatev4.Jurisdiction(parameters.get("jurisdictionCountryCode").toString(),parameters.get("jurisdictionCurrency").toString());
        plm.pojo.request.retrieveProductInterestRatev4.Discriminators discriminator = new plm.pojo.request.retrieveProductInterestRatev4.Discriminators(parameters.get("discriminatorID").toString(),parameters.get("discriminatorTypeCode").toString(), parameters.get("discriminatorValue").toString());
        discriminators.add(discriminator);
        ChannelType channelType = new ChannelType("", parameters.get("channelTypeDescription").toString());


        //product component
        if(parameters.get("componentProductIdentifier").toString().length()>0){
            ProductStructureType componentProductStructureType = new ProductStructureType(parameters.get("componentProductStructureTypeName").toString());
            ProductIdentification componentProductIdentification = new ProductIdentification(parameters.get("componentProductIdentifier").toString(), parameters.get("productIDTypeCode").toString(), componentProductStructureType);
            productIdentifications.add(componentProductIdentification);
        }

        RetrieveProductInterestRate retrieveProductInterestRateObject = new RetrieveProductInterestRate(interestRates, productIdentifications,  additionalIdentifiers, channelType, parameters.get("pricingPlanIdentifier").toString() , parameters.get("productLine").toString() , jurisdiction,  discriminators);
        Body body = new Body(retrieveProductInterestRateObject);

        Envelope envelope = new Envelope(header, body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }
}

package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrieveproductofferinformation.*;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductofferinformation.RetrieveProductOfferInformationResponse;
import plm.util.RestHelper;
import plm.util.SoapHelper;

import java.util.ArrayList;
import java.util.Iterator;

public class RetrieveProductOfferInformation {
    private RestHelper restHelper;
    private SoapHelper soapHelper;

    private static final Logger logger = LogManager.getLogger(RetrieveProductOfferInformation.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE = "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    public RetrieveProductOfferInformation() {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(String strBaseURL, String productIdentifier, String productLine, String strChannelTypeDescription, JSONObject jsonCharacteristicFilters) {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveProductOfferInformation_soap_path");

        String strPayload = getXML(productIdentifier, productLine, strChannelTypeDescription, jsonCharacteristicFilters);

        Response response = soapHelper.postRequest(strPayload, strBaseURL , path);
        response.prettyPrint();
        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }


        if(response.getBody().asString().contains("faultstring")){
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        }else{
            RetrieveProductOfferInformationResponse retrieveProductOfferInformationResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveProductOfferInformationResponse.class);
            retrieveProductOfferInformationResponse.setHeaders(response.getHeaders());
            retrieveProductOfferInformationResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveProductOfferInformationResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }


    private String getXML(String productIdentifier, String productLine, String strChannelTypeDescription, JSONObject jsonCharacteristicFilters) {
        Header header = soapHelper.getHeader();

        //body
        ArrayList<CharacteristicFilters> characteristicFilters = addCharacteristicFilters(jsonCharacteristicFilters);
        ChannelType channelType = new ChannelType("", strChannelTypeDescription);

        plm.pojo.request.retrieveproductofferinformation.RetrieveProductOfferInformation retrieveProductOfferInformation = new plm.pojo.request.retrieveproductofferinformation.RetrieveProductOfferInformation(productIdentifier, channelType, productLine, characteristicFilters);
        Body body = new Body(retrieveProductOfferInformation);

        Envelope envelope = new Envelope(header, body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }

    private ArrayList<CharacteristicFilters> addCharacteristicFilters(JSONObject jsonCharacteristicFilters) {
        ArrayList<CharacteristicFilters> characteristicFilters = new ArrayList<>();

        Iterator<String> keys = jsonCharacteristicFilters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (jsonCharacteristicFilters.get(key) instanceof String) {
                CharacteristicFiltersType characteristicFiltersType = new CharacteristicFiltersType("", key );
                CharacteristicFilters characteristicFilters1 = new CharacteristicFilters(characteristicFiltersType, jsonCharacteristicFilters.getString(key));
                characteristicFilters.add(characteristicFilters1);
            }
        }

        return characteristicFilters;
    }


}

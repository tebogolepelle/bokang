package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrieveproductofferinformationv4.*;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductofferinformationv4.RetrieveProductOfferInformationResponse;
import plm.util.RestHelper;
import plm.util.SoapHelper;

import java.util.ArrayList;
import java.util.List;

public class RetrieveProductOfferInformationv4 {
    private RestHelper restHelper;
    private SoapHelper soapHelper;

    private static final Logger logger = LogManager.getLogger(RetrieveProductOfferInformationv4.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE = "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    public RetrieveProductOfferInformationv4() {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(JSONObject parameters) {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveProductOfferInformationV4_soap_path");

        String strPayload = getXML(parameters);

        Response response = soapHelper.postRequest(strPayload, parameters.get("strBaseURL").toString() , path);
        response.prettyPrint();
        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, parameters.get("strBaseURL").toString() + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }


        if(response.getBody().asString().contains("faultstring")){
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        }else{
            RetrieveProductOfferInformationResponse retrieveProductOfferInformationResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveProductOfferInformationResponse.class);
            retrieveProductOfferInformationResponse.setHeaders(response.getHeaders());
            retrieveProductOfferInformationResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveProductOfferInformationResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }


    private String getXML(JSONObject parameters) {
        Header header = soapHelper.getHeader();

        //body
        ChannelType channelType = new ChannelType("", parameters.get("channelTypeDescription").toString());

        List<ProductIdentification> productIdentifications = new ArrayList<>();
        List<AdditionalIdentifiers> additionalIdentifiers = new ArrayList<>();
        List<Discriminators> discriminators = new ArrayList<>();

        ProductStructureType productStructureType = new ProductStructureType("");
        ProductIdentification productIdentification = new ProductIdentification(parameters.get("productIdentifier").toString(), parameters.get("productIDTypeCode").toString(), productStructureType);
        productIdentifications.add(productIdentification);

        ProductKeys productKeys = new ProductKeys(parameters.get("productKeysFieldName").toString(), parameters.get("productKeysFieldValue").toString(), parameters.get("productKeysSequence").toString());
        SourceSystem sourceSystem = new SourceSystem(parameters.get("sourceSystemSystemId").toString(), parameters.get("sourceSystemSystemName").toString(), parameters.get("sourceSystemSystemCode").toString());

        Productidtype productidtypeObject =  new Productidtype(parameters.get("productIDTypeCode").toString(),parameters.get("productIDTypeDescription").toString());

        AdditionalIdentifiers additionalIdentifier = new AdditionalIdentifiers(productidtypeObject, productKeys, sourceSystem);
        additionalIdentifiers.add(additionalIdentifier);

        Jurisdiction jurisdiction = new Jurisdiction(parameters.get("jurisdictionCountryCode").toString(),parameters.get("jurisdictionCurrency").toString());
        Discriminators discriminator = new Discriminators(parameters.get("discriminatorID").toString(),parameters.get("discriminatorTypeCode").toString(), parameters.get("discriminatorValue").toString());
        discriminators.add(discriminator);

        //product component
        if(parameters.get("componentProductIdentifier").toString().length()>0){
            ProductStructureType componentProductStructureType = new ProductStructureType(parameters.get("componentProductStructureTypeName").toString());
            ProductIdentification componentProductIdentification = new ProductIdentification(parameters.get("componentProductIdentifier").toString(), parameters.get("productIDTypeCode").toString(), componentProductStructureType);
            productIdentifications.add(componentProductIdentification);
        }

        plm.pojo.request.retrieveproductofferinformationv4.RetrieveProductOfferInformation retrieveProductOfferInformation = new plm.pojo.request.retrieveproductofferinformationv4.RetrieveProductOfferInformation(productIdentifications, additionalIdentifiers, channelType, parameters.get("productLine").toString(),  jurisdiction, discriminators);
        Body body = new Body(retrieveProductOfferInformation);

        Envelope envelope = new Envelope(header, body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }


}

package plm.soap;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.Header;
import plm.pojo.request.retrieveproductsv4.*;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductsv4.RetrieveProductsResponse;
import plm.util.RestHelper;
import plm.util.SoapHelper;

public class RetrieveProductv4 {
    private RestHelper restHelper;
    private SoapHelper soapHelper;

    private static final Logger logger = LogManager.getLogger(RetrieveProductv4.class);
    private static final String RESPONSE = "response";
    private static final String RESPONSE_XML = "response_xml";
    private static final String PAYLOAD = "payload";
    private static final String STARTING_METHOD_MESSAGE = "Sending request started for class {}.....";
    private static final String REST_ENDPOINT_MESSAGE = "Soap end point used {}";
    private static final String PAYLOAD_MESSAGE = "Payload used {}";
    private static final String RESPONSE_BODY_MESSAGE = "Response body for response {}";
    private static final String ENDING_METHOD_MESSAGE = "Sending request finished for class {}.....";

    public RetrieveProductv4() {
        restHelper = new RestHelper();
        soapHelper = new SoapHelper();
    }

    public JSONObject sendSoapRequest(String strBaseURL, String productIDTypeCode, String productIDTypeDescription, String productKeysFieldName, String productKeysFieldValue, String productKeysSequence, String systemId, String systemName, String systemCode) {

        logger.info(STARTING_METHOD_MESSAGE, getClass().getName());
        JSONObject jsonResponse = new JSONObject();

        String path = restHelper.getApiConfig().getOtherElements().get("RetrieveProducts_v4_soap_path");

        String strPayload = getXML(productIDTypeCode,  productIDTypeDescription,  productKeysFieldName,  productKeysFieldValue,  productKeysSequence,  systemId,  systemName,  systemCode);

        Response response = soapHelper.postRequest(strPayload, strBaseURL ,path);
        response.prettyPrint();
        if (logger.isInfoEnabled()) {
            logger.info(REST_ENDPOINT_MESSAGE, strBaseURL + path);
            logger.info(PAYLOAD_MESSAGE, strPayload);
            logger.info(RESPONSE_BODY_MESSAGE, response.getBody().prettyPrint());
            logger.info(ENDING_METHOD_MESSAGE, getClass().getName());
        }


        if(response.getBody().asString().contains("faultstring")){
            FaultResponse faultResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), FaultResponse.class);
            jsonResponse.put(RESPONSE, faultResponse);
        }else{
            RetrieveProductsResponse retrieveProductsResponse = SoapHelper.unmarshall(response.getBody().asInputStream(), RetrieveProductsResponse.class);
            retrieveProductsResponse.setHeaders(response.getHeaders());
            retrieveProductsResponse.setStatusCode(response.getStatusCode());
            jsonResponse.put(RESPONSE, retrieveProductsResponse);
        }

        jsonResponse.put(PAYLOAD, strPayload);
        jsonResponse.put(RESPONSE_XML, response.getBody().prettyPrint());
        jsonResponse.put("status_code", response.getStatusCode());

        return jsonResponse;
    }


    private String getXML(String productIDTypeCode, String productIDTypeDescription, String productKeysFieldName, String productKeysFieldValue, String productKeysSequence, String systemId, String systemName, String systemCode) {
        Header header = soapHelper.getHeader();

        ProductIDType productIDType = new ProductIDType(productIDTypeCode, productIDTypeDescription);
        ProductKeys productKeys =  new ProductKeys(productKeysFieldName,productKeysFieldValue,productKeysSequence );
        SourceSystem sourceSystem = new SourceSystem(systemId, systemName,systemCode);
        ProductAlternativeIdentifier productAlternativeIdentifier = new ProductAlternativeIdentifier(productIDType, productKeys,sourceSystem);

        RetrieveProducts retrieveProducts = new RetrieveProducts(productAlternativeIdentifier);
        Body body = new Body(retrieveProducts);

        Envelope envelope = new Envelope(header, body);

        //marshall the POJO to xml and return string
        return soapHelper.marshallEnvelopeObject(envelope);
    }



}

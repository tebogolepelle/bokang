package plm.util;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;
import io.restassured.internal.RequestSpecificationImpl;
import io.restassured.response.Response;
import io.restassured.specification.ProxySpecification;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class RestHelper {

    private static final String AUTHORIZATION = "Authorization";
    private static final String ERROR_PREFIX = "Error {}";
    private String proxyUsername;
    private String proxyPassword;

    private ApiConfig apiConfig;
    private RestUtil restUtil;
    private static final Logger logger = LogManager.getLogger(RestHelper.class);
    String env = null;
    private static final String PROXY_USERNAME = "proxy_username";
    private static final String PROXY_PWORD = "proxy_password";


    public RestHelper() {
        logger.info("Initialling class {}.....", getClass().getName());

        restUtil = new RestUtil();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
            proxyUsername = this.apiConfig.getOtherElements().get("proxy_username");
            proxyPassword = this.apiConfig.getOtherElements().get("proxy_password");
            env = this.apiConfig.getEnvironment().toString();

        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
    }

    public RestUtil getRestUtil() {
        logger.info("getting RestUtil....");
        return restUtil;
    }

    public ApiConfig getApiConfig() {
        logger.info("getting apiConfig.....");
        return apiConfig;
    }

    private void clearProxyProperties() {
        logger.info("clearing Proxy properties started {}.....", getClass().getName());

        System.clearProperty("http.proxyHost");
        System.clearProperty("http.proxyPort");
        System.clearProperty("http.proxyUser");
        System.clearProperty("http.proxyPassword");
        System.clearProperty("https.proxyHost");
        System.clearProperty("https.proxyPort");
        System.clearProperty("https.proxyUser");
        System.clearProperty("https.proxyPassword");
        System.clearProperty("java.net.useSystemProxies");
    }


    public String readBearerFromFile(String consumer) {
        logger.info("reading  Bearer token From File  {}.....", getClass().getName());

        String bearerToken = readFileFromResources(apiConfig.getOtherElements().get(consumer + "_bearer_token_name"));

        logger.info("Bearer token {}.....", bearerToken);

        logger.info("reading CR3 Bearer token From File complete {}.....", getClass().getName());

        return bearerToken;
    }

    public static String replaceParameter(String strJson, String strParameterName, String strParameterValue) {
        logger.info("replacing parameter started {}.....", RestHelper.class.getName());
        logger.info("json: {}.....", strJson);
        logger.info("Parameter name to replace: {}.....", strParameterName);
        logger.info("New Value: {}.....", strParameterValue);

        String newJson = strJson.replace("{" + strParameterName + "}", strParameterValue);
        logger.info("Updated Json: {}.....", newJson);
        logger.info("replacing parameter complete {}.....", RestHelper.class.getName());
        return newJson;
    }

    public String readFileFromResources(String strFileName) {
        StringBuilder fileContent = new StringBuilder();

        try (InputStream is = RestHelper.class.getResourceAsStream("/" + strFileName)) {
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            String l;
            while ((l = r.readLine()) != null) {
                fileContent.append(l);
            }
        } catch (IOException e) {
            logger.info("Error reading file {}", e.getMessage());
        }

        return fileContent.toString();

    }

    public String createFeeParameter(String paramType, String paramValue, String operator) {
        return String.format("{" +
                "                    \"parmtype\": \"%s\"," +
                "                    \"parmvalue\": \"%s\"," +
                "                    \"operator\": \"%s\"" +
                "                }", paramType, paramValue, operator);
    }

    public String createTierTag(String tierType, String tierValue) {
        return String.format("," +
                "            \"tierType\": \"%s\"," +
                "            \"tierValue\": %s", tierType, tierValue);
    }


    public Response makeInternalCall(String strEndPoint, String bearerToken, String jsonBody, String path) {
        RestAssured.reset();
        clearProxyProperties();
        Response response = null;

        HashMap<String, String> postHeader = new HashMap<>();
        postHeader.put(AUTHORIZATION, bearerToken);
        try {
            response = getRestUtil().initSpec(strEndPoint)
                    .addContentType(ContentType.JSON)
                    .addPayloadfromString(jsonBody)
                    .post(path, postHeader);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }
        return response;
    }

    public Response makeInternalCall(String strEndPoint, String bearerToken, String path) {
        RestAssured.reset();
        clearProxyProperties();
        Response response = null;
        try {
            response = getRestUtil().initSpec(strEndPoint)
                    .appendHeader(AUTHORIZATION, bearerToken)
                    .addContentType(ContentType.JSON)
                    .get(path);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return response;
    }

    public Response makeInternalCallWithResource(String strEndPoint, String bearerToken, String path, String resource) {
        RestAssured.reset();
        clearProxyProperties();
        Response response = null;
        try {
            response = getRestUtil().initSpec(strEndPoint)

                    .addContentType(ContentType.JSON)
                    .appendHeader(AUTHORIZATION, bearerToken)
                    .get(path + resource);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return response;
    }

    public Response makeInternalCallWithResourceAndParams(String strEndPoint, String bearerToken, String path, String resource, JSONObject parameters) {
        RestAssured.reset();
        clearProxyProperties();
        Response response = null;
        restUtil = getRestUtil().initSpec(strEndPoint);

        Iterator<String> keys = parameters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
                restUtil.addParam(key, parameters.getString(key));
            }
        }

        try {
            response =
                    restUtil.addContentType(ContentType.JSON)
                            .appendHeader(AUTHORIZATION, bearerToken)
                            .get(path + resource);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }
        return response;
    }

    public Response makeExternalCall(String strEndPoint, String bearerToken, String jsonBody, String path) {
        RestAssured.reset();
        clearProxyProperties();

        HashMap<String, String> postHeader = new HashMap<>();
        postHeader.put(AUTHORIZATION, bearerToken);
        Response response = null;

        restUtil = getRestUtil().initSpec(strEndPoint).addContentType(ContentType.JSON);

        if(!env.contentEquals("DEV")){
            restUtil.addProxyAuthentication(proxyUsername, proxyPassword);
        }

        try {
            response = restUtil.addPayloadfromString(jsonBody)
                    .post(path, postHeader);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return response;
    }

    public Response makeExternalCallWithResource(String strEndPoint, String bearerToken, String path, String resource) {
        RestAssured.reset();
        clearProxyProperties();
        restUtil = getRestUtil().initSpec(strEndPoint);

        restUtil.initSpec(strEndPoint);


        Map<String, String> gfg = ((RequestSpecificationImpl) restUtil.getRequestSpecification()).getQueryParams();
        for (Map.Entry<String, String> entry : gfg.entrySet()) {
            restUtil.addQueryParameter(entry.getKey(), entry.getValue());
        }

        gfg = ((RequestSpecificationImpl) restUtil.getRequestSpecification()).getRequestParams();
        for (Map.Entry<String, String> entry : gfg.entrySet()) {
            restUtil.addQueryParameter(entry.getKey(), entry.getValue());
        }

        Response response = null;

        try {

            restUtil = getRestUtil().initSpec(strEndPoint).addContentType(ContentType.JSON);

            if(!env.contentEquals("DEV")){
                restUtil.addProxyAuthentication(proxyUsername, proxyPassword);
            }

            response = restUtil.appendHeader(AUTHORIZATION, bearerToken)
                    .get(path + resource);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return response;
    }

    public Response makeExternalCallWithResourceAndParams(String strEndPoint, String bearerToken, String path, String resource, JSONObject parameters) {
        RestAssured.reset();
        clearProxyProperties();
        restUtil = getRestUtil().initSpec(strEndPoint);

        restUtil.initSpec(strEndPoint);


        Iterator<String> keys = parameters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
                restUtil.addQueryParameter(key, parameters.getString(key));
            }
        }

        Response response = null;

        try {

            restUtil.addContentType(ContentType.JSON);

            if(!env.contentEquals("DEV")){
                restUtil.addProxyAuthentication(proxyUsername, proxyPassword);
            }

            response =
                    restUtil.appendHeader(AUTHORIZATION, bearerToken)
                            .get(path + resource);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return response;
    }

    public Response makeExternalCall(String strEndPoint, String bearerToken, String path) {
        RestAssured.reset();
        clearProxyProperties();

        restUtil.initSpec(strEndPoint);


        Response response = null;

        try {

            restUtil = getRestUtil().initSpec(strEndPoint).addContentType(ContentType.JSON);

            if(!env.contentEquals("DEV")){
                restUtil.addProxyAuthentication(proxyUsername, proxyPassword);
            }

            restUtil.appendHeader(AUTHORIZATION, bearerToken)
                    .get(path);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return response;
    }

    public RequestSpecification configureRequestForPostWithSpec(String baseUrl, String bearerToken) {
        logger.info("configuring request for post by base URL and bearer token  {}.....", getClass().getName());
        logger.info("Base URL:  {}", baseUrl);
        logger.info("bearerToken:  {}", bearerToken);

        String certPath = this.apiConfig.getOtherElements().get("cert_name");
        String certPassword = this.apiConfig.getOtherElements().get("cert_password");
        String proxyUsername = this.apiConfig.getOtherElements().get(PROXY_USERNAME);
        String proxyPassword = this.apiConfig.getOtherElements().get(PROXY_PWORD);
        String proxyHost = this.apiConfig.getOtherElements().get("proxyHost");
        String proxyPort = this.apiConfig.getOtherElements().get("proxyPort");

        RequestSpecification restAssuredSpec = new RequestSpecBuilder().build();

        if (baseUrl.contains("localhost")) {
            restAssuredSpec.baseUri(baseUrl);
            return restAssuredSpec;
        }

        restAssuredSpec.config(RestAssured.config().sslConfig(new SSLConfig().trustStore(certPath, certPassword)));

        System.setProperty("http.proxyHost", proxyHost);
        System.setProperty("http.proxyPort", proxyPort);
        System.setProperty("http.proxyUser", proxyUsername);
        System.setProperty("http.proxyPassword", proxyPassword);
        System.setProperty("https.proxyHost", proxyHost);
        System.setProperty("https.proxyPort", proxyPort);
        System.setProperty("https.proxyUser", proxyUsername);
        System.setProperty("https.proxyPassword", proxyPassword);

        ProxySpecification specification = new ProxySpecification(proxyHost, Integer.parseInt(proxyPort), "http").withAuth(proxyUsername, proxyPassword);

        restAssuredSpec.baseUri(baseUrl).proxy(specification).header(AUTHORIZATION, bearerToken);

        logger.info("Proxy details - proxyHost :  {}", proxyHost);
        logger.info("Proxy details - proxyPort :  {}", proxyPort);
        logger.info("Proxy details - proxyUsername :  {}", proxyUsername);
        logger.info("Proxy details - proxyPassword :  {}", proxyPassword);

        logger.info("configuring request for post by base URL and bearer token complete {}.....", getClass().getName());

        return restAssuredSpec;
    }

    public String getEndPoint(String strEntryPoint) {
        if (strEntryPoint.contentEquals("internal")) {
            return getApiConfig().getEnvironmentBaseUrl();
        } else {
            return getApiConfig().getOtherElements().get("externalBaseUrl");
        }
    }

}

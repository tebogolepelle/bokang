package plm.util;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import plm.pojo.request.header.*;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.*;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

public class SoapHelper {
    private static final Logger logger = LogManager.getLogger(SoapHelper.class);
    private static final String READING_FILE_ERROR = "Error reading file {}";
    private ApiConfig apiConfig;
    private RestUtil restUtil;
    private static final String ERROR_PREFIX = "Error {}";
    String env = null;


    public SoapHelper() {
        logger.info("Initialling class {}.....", getClass().getName());
        try {
            restUtil = new RestUtil();
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
            env = apiConfig.getEnvironment().toString();
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
    }

    private String getBody(String strOperationName) {
        logger.info("getting xml body started {}.....", SoapHelper.class.getName());

        String strFileName = strOperationName + ".xml";
        StringBuilder body = new StringBuilder();

        try (InputStream is = SoapHelper.class.getResourceAsStream("/soap_body/" + strFileName)) {
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            String l;
            while ((l = r.readLine()) != null) {
                body.append(l);
            }
        } catch (IOException e) {
            logger.info(READING_FILE_ERROR, e.getMessage());
        }

        logger.info("Body: {}", body);

        logger.info("getting xml body complete {}.....", SoapHelper.class.getName());

        return body.toString();
    }

    private String getHeader(String strServicePath) {
        logger.info("getting xml header started {}.....", SoapHelper.class.getName());
        String xmlnsVersion = strServicePath.substring(strServicePath.lastIndexOf('/') + 1);
        StringBuilder strHeader = new StringBuilder();
        try (InputStream is = SoapHelper.class.getResourceAsStream("/soap_body/header.xml")) {
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            String l;
            while ((l = r.readLine()) != null) {
                strHeader.append(l);
            }
        } catch (IOException e) {
            logger.info(READING_FILE_ERROR, e.getMessage());
        }

        strHeader = new StringBuilder(strHeader.toString().replace("{xmlns_version}", xmlnsVersion));
        logger.info("header: {}", strHeader);
        logger.info("getting xml header complete {}.....", SoapHelper.class.getName());

        return strHeader.toString();
    }

    private FileInputStream getCertificate() {
        logger.info("getting Certificate  started {}.....", SoapHelper.class.getName());
        FileInputStream fileInputStream = null;

        File tempFile;
        try {
            tempFile = File.createTempFile("prefix", "suffix");
        } catch (IOException e) {
            logger.error(e.getMessage());
            return null;
        }
        tempFile.deleteOnExit();

        try (InputStream is = SoapHelper.class.getResourceAsStream("/" + apiConfig.getOtherElements().get("cert_name"))) {
            FileUtils.copyInputStreamToFile(is, tempFile);
            fileInputStream = new FileInputStream(tempFile);
        } catch (IOException e) {
            logger.info(READING_FILE_ERROR, e.getMessage());
        }

        logger.info("getting Certificate  complete {}.....", SoapHelper.class.getName());
        return fileInputStream;
    }


    private void clearProxyProperties() {
        logger.info("clearing Proxy properties started {}.....", getClass().getName());

        System.clearProperty("http.proxyHost");
        System.clearProperty("http.proxyPort");
        System.clearProperty("http.proxyUser");
        System.clearProperty("http.proxyPassword");
        System.clearProperty("https.proxyHost");
        System.clearProperty("https.proxyPort");
        System.clearProperty("https.proxyUser");
        System.clearProperty("https.proxyPassword");
        System.clearProperty("java.net.useSystemProxies");

    }

    public Response postRequest(String strPayload, String endPoint, String path) {
        logger.info("posting Request started {}.....", SoapHelper.class.getName());

        RestAssured.reset();
        clearProxyProperties();
        String keyStorePassword = apiConfig.getOtherElements().get("cert_password");
        String keyStoreName = apiConfig.getOtherElements().get("cert_name");

        KeyStore keyStore;

        try {
            keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(this.getCertificate(), keyStorePassword.toCharArray());
        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        HashMap<String, String> postHeader = new HashMap<>();

        Response response = null;
        try {
            restUtil.initSpec(endPoint);
            if(!env.contentEquals("DEV")){
                restUtil.addSslCertContext(Objects.requireNonNull(getClass().getClassLoader().getResource(keyStoreName)).getPath()  ,keyStorePassword );
            }
            response = restUtil.addPayloadfromString(strPayload)
                    .post(path.toLowerCase(), postHeader);
        } catch (ApiException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }


        if (logger.isInfoEnabled()) {
            logger.info("Soap end point used {}", endPoint);
            logger.info("payload: {}", strPayload);
            assert response != null;
            logger.info("Response body for response {}", response.getBody().prettyPrint());
            logger.info("posting request finished for class {}.....", SoapHelper.class.getName());
        }
        return response;

    }



    public String createPayload(String strOperationName, String strServicePath) {
        logger.info("creating Payload started {}.....", SoapHelper.class.getName());

        logger.info("operation name: {}", strOperationName);
        logger.info("Service path: {}", strServicePath);

        String strHeader = getHeader(strServicePath);
        String strBody = getBody(strOperationName);
        strHeader = strHeader.replace("{service_path}", strServicePath);
        String payload = strHeader + strBody + "</soapenv:Envelope>";
        logger.info("payload: {}", payload);

        logger.info("creating Payload complete {}.....", SoapHelper.class.getName());

        return payload;
    }

    public String replaceParameter(String strXML, String strParameterName, String strParameterValue) {
        logger.info("replacing parameter started {}.....", SoapHelper.class.getName());
        logger.info("XML: {}.....", strXML);
        logger.info("Parameter name to replace: {}.....", strParameterName);
        logger.info("New Value: {}.....", strParameterValue);

        String newXML = strXML.replace("{" + strParameterName + "}", strParameterValue);

        logger.debug("Updated XML: {}.....", newXML);
        logger.info("replacing parameter complete {}.....", SoapHelper.class.getName());

        return newXML;
    }

    public String replaceAllParameters(String strXML, JSONObject parameters) {
        String vendorName = "<v2:feeParameters>\n" +
                "                <v2:type>Vendor</v2:type>\n" +
                "                <v2:value>{vendorName}</v2:value>\n" +
                "            </v2:feeParameters>";

        String lawTrust = "<v2:feeParameters>\n" +
                "                <v2:type>Law Trust</v2:type>\n" +
                "                <v2:value>{lawTrust}</v2:value>\n" +
                "            </v2:feeParameters>";

        String strPayload = strXML;
        Iterator<String> keys = parameters.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (parameters.get(key) instanceof String) {
                if (key.contentEquals("tierType") && parameters.getString(key).length() > 0) {
                    strPayload = this.replaceParameter(strPayload, key, "<v2:tierType>" + parameters.getString(key) + "</v2:tierType>");
                } else if (key.contentEquals("tierValue") && parameters.getString(key).length() > 0) {
                    strPayload = this.replaceParameter(strPayload, key, "<v2:tierValue>" + parameters.getString(key) + "</v2:tierValue>");
                } else if (key.contentEquals("vendorName") && parameters.getString(key).length() > 0) {
                    strPayload = this.replaceParameter(strPayload, key, replaceParameter(vendorName, "vendorName", parameters.getString(key)));
                } else if (key.contentEquals("lawTrust") && parameters.getString(key).length() > 0) {
                    strPayload = this.replaceParameter(strPayload, key, replaceParameter(lawTrust, "lawTrust", parameters.getString(key)));
                } else {
                    strPayload = this.replaceParameter(strPayload, key, parameters.getString(key));
                }
            }
        }

        strPayload = this.replaceParameter(strPayload, "userName", apiConfig.getOtherElements().get("UsernameTokenUser"));

        return strPayload;
    }


    public Header getHeader() {
        InstrumentationInfo instrumentationInfo = new InstrumentationInfo();
        RequestOriginator requestOriginator = new RequestOriginator();
        ContextInfo contextInfo = new ContextInfo();
        EnterpriseContext enterpriseContext = new EnterpriseContext(contextInfo, requestOriginator, instrumentationInfo);

        //Header Security
        UsernameToken usernameToken = new UsernameToken();
        SecurityObject securityObject = new SecurityObject(usernameToken);
        return new Header(enterpriseContext, securityObject);
    }

    public static <T> T unmarshall(InputStream xml, Class<T> clazz) {
        T obj = null;
        SOAPMessage message;
        try {
            message = MessageFactory.newInstance().createMessage(null, xml);
            JAXBContext jaxbContext;
            jaxbContext = JAXBContext.newInstance(clazz);

            Unmarshaller jaxbUnMarshaller = jaxbContext.createUnmarshaller();
            obj = clazz.cast(jaxbUnMarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument()));

        } catch (SOAPException | JAXBException | IOException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return obj;
    }

    public String marshallEnvelopeObject(Object ob) {
        StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ob.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);

            jaxbMarshaller.marshal(ob, sw);
            jaxbMarshaller.marshal(ob, System.out);
            return sw.toString();
        } catch (JAXBException e) {
            logger.info(ERROR_PREFIX, e.getMessage());
        }

        return sw.toString();

    }


}

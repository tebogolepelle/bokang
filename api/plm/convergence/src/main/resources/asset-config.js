var configFunction = function(envName) {
    if (!envName) {
        envName = 'DEV';
    }

    //Use this config object to declare properties such as paths to various APIs that you can use in your asset.
    //You can declare other appropriate properties. See below example.
    var config = {
        //soap
        RetrieveFees_soap_path: '/services/ent/arrangementmanagement/ArrangementPricing/v1',
        RetrieveFees_cashOnline_soap_path: '/services/ent/arrangementmanagement/ArrangementPricing/v2',
        RetrieveFees_v3_soap_path: '/services/ent/arrangementmanagement/arrangementpricing/v3',
        RetrieveFees_v4_soap_path: '/services/ent/arrangementmanagement/arrangementpricing/v4',
        GetProduct_v4_soap_path: '/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4',
        RetrieveProducts_v4_soap_path: '/services/ent/productandservicedevelopment/ProductCatalogueInformation/v4',

        RetrieveInterestRates_soap_path: '/services/ent/arrangementmanagement/ArrangementPricing/v1',
        RetrieveInterestRates_v4_soap_path: '/services/ent/arrangementmanagement/ArrangementPricing/v4',

        RetrieveProductInterestRates_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v1',
        RetrieveProductOfferInformation_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v1',
        RetrieveProductOfferInformationV2_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v2',
        RetrieveProductOfferInformationV3_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v3',
        RetrieveProductOfferInformationV4_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v4',

        RetrieveProducts_soap_path: '/services/ent/productandservicedevelopment/ProductCatalogueInformation/v3',
        RetrieveProductInterestRatesV2_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v2',
        RetrieveProductInterestRatesV3_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v3',
        RetrieveProductInterestRatesV4_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v4',
        RetrieveProductFeesV4_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v4',

        RetrieveProductPricingPlansV4_soap_path: '/services/ent/productandservicedevelopment/ProductOfferInformation/v4',

        FDRatesEnqRq_soap_path: '/services/biz/arrangementmanagement/InvestmentMaintenance/v15',

        //Rest
        products_rest_path: '/enterpriseproductcatalogueinfo/v1/products/',
        RetrieveInterestRates_rest_path: '/arrangementpricing/v1/interestrates',
        RetrieveProductInterestRates_rest_path: '/productofferinformation/v1/products/{product_identifier}/interestrates/{interest_rate_name}',
        RetrieveProductInterestRatesV2_rest_path: '/productofferinformation/v2/products/interestrates',
        AccountsReferenceData_rest_path: '/accountsreferencedata/v1/accounts',
        RetrieveFees_rest_path : '/arrangementpricing/v3/fees',

        //proxy authentication
        proxy_username: 'CC319881',
        proxy_password: 'Kibana4life@02',
        proxyHost: '172.17.2.9',
        proxyPort: '80',


        //certification details for soap calls
        cert_name: 'CC319881_ETE_QA.pfx',
        //cert_name: 'CC319881_PLM.pfx',
        cert_password: 'Nhlakanhlaka@02',

        //mongo db connection properties
        mongo_password: 'Kibana4life@02',
        mongo_username: 'cc319881',
        mongo_port: '47500',
        mongo_db_name: 'epc_Cache',
        mongo_cert_path: 'cacert.pem',

        //soap header
        ProcessContextId:'6f0e17c4-399a-4499-8321-b72019021101' ,
        ExecutionContextId: '6f0e17c4-399a-4499-8321-b72019021101',
        MachineIPAddress:'10.74.38.74',
        UserPrincipleName: 'cc319881',
        MachineDNSName: 'V105P10PRA0651',
        ChannelId: '555',
        ParentInstrumentationId: '6f0e17c4-399a-4499-8321-b72019021101',
        ChildInstrumentationId: '6f0e17c4-399a-4499-8321-b72019021101',
        UsernameToken: 'cc319881',

        //---------------------------------------json paths-----------------------------------------------//

        //error friendly message
        friendly_message_rest_path: 'friendly_message',

        //result set
        resultCode_rest_path: 'resultSet.resultCode',
        resultDescription_rest_path: 'resultSet.resultDescription',

        //---------------------------------------xml paths-----------------------------------------------//

        resultDescription_xml_path: 'Envelope.Body.{operationName}Response.resultSet.resultDescription',
        ResultCode_xml_path: 'Envelope.Body.{operationName}Response.resultSet.resultCode'
    };

    //config.baseUrl is the property that holds the base URL for all the rsoap_end_pointequests.
    if (envName == 'DEV') {
        config.baseUrl = 'http://localhost';
        config.soap_end_point = 'http://localhost';
        config.externalBaseUrl = 'http://localhost';
        config.internalBaseUrl = 'http://localhost';

        config.bearer_token_name = 'ete_bearer_token.txt';
        config.cr3internal_bearer_token_name = 'cr3internal_ete_bearer_token.txt';
         config.cr3external_bearer_token_name = 'cr3external_ete_bearer_token.txt';
        config.cr3_fullrateinternal_bearer_token_name = 'cr3_fullrateinternal_ete_bearer_token.txt';

         config.cashOnlineinternal_bearer_token_name = 'cashOnlineInternal_ete_bearer_token.txt';
         config.cashOnlineexternal_bearer_token_name = 'cashOnlineExternal_ete_bearer_token.txt';
         config.browseexternal_bearer_token_name = 'browseexternal_ete_bearer_token.txt';

        config.mypocketinternal_bearer_token_name = 'mypocketinternal_ete_bearer_token.txt';
        config.mypocketexternal_bearer_token_name = 'mypocketexternal_ete_bearer_token.txt';

        config.srBaseUrl = 'http://localhost';
        config.sr43_bearer_token_name = 'sr4.3_qa_bearer_token.txt';

        config.mongo_server = 'xeteepcmdb01.africa.nedcor.net';
        config.UsernameTokenUser = 'cc319881';
        config.cr3_bearer_token_end_point = 'https://ete-staff.nednet.co.za/content/nedbank-eco.jwt.json';
        config.administration_service_endpoint = 'https://zeepcwn1.africa.nedcor.net:9444/nedj-service-configuration/api/config/namespace/za.co.nedj.app.plm.product.model';
        config.cr3_bearer_token_end_point = 'https://ete-staff.nednet.co.za/content/nedbank-eco.jwt.json';

    } else if (envName == 'ETE') {
        config.baseUrl = 'https://api-ete.it.nednet.co.za/nedbank/ete';
        config.externalBaseUrl = 'https://api-e.nedsecure.co.za/nedbankext/e2e-public';

        config.srBaseUrl = 'https://api-ete.it.nednet.co.za/nedbank/ete';

        config.soap_end_point = 'https://ssg-e.it.nednet.co.za:454';
        config.soap_end_point_FDRatesEnqRq = 'https://ssg-e.it.nednet.co.za:443';

        config.bearer_token_name = 'ete_bearer_token.txt';
        config.cr3internal_bearer_token_name = 'cr3internal_ete_bearer_token.txt';
        config.cr3external_bearer_token_name = 'cr3external_ete_bearer_token.txt';
        config.cr3_fullrateinternal_bearer_token_name = 'cr3_fullrateinternal_ete_bearer_token.txt';

        config.cashOnlineinternal_bearer_token_name = 'cashOnlineInternal_ete_bearer_token.txt';
        config.cashOnlineexternal_bearer_token_name = 'cashOnlineExternal_ete_bearer_token.txt';

        config.browseexternal_bearer_token_name = 'browseexternal_ete_bearer_token.txt';

        config.mypocketinternal_bearer_token_name = 'mypocketinternal_ete_bearer_token.txt';
        config.mypocketexternal_bearer_token_name = 'mypocketexternal_ete_bearer_token.txt';
        config.sr43_bearer_token_name = 'sr4.3_ete_bearer_token.txt';

        config.mongo_server = 'xeteepcmdb01.africa.nedcor.net';
        config.UsernameTokenUser = 'cc319881';
        config.cr3_bearer_token_end_point = 'https://ete-staff.nednet.co.za/content/nedbank-eco.jwt.json';
        config.administration_service_endpoint = 'https://zeepcwn1.africa.nedcor.net:9444/nedj-service-configuration/api/config/namespace/za.co.nedj.app.plm.product.model';
        config.cr3_bearer_token_end_point = 'https://ete-staff.nednet.co.za/content/nedbank-eco.jwt.json';
        config.browse_bearer_token_name = 'browse_qa_bearer_token.txt';

    } else if (envName == 'QA') {
        config.baseUrl = 'https://api-qa.it.nednet.co.za/nedbank/qa';
        config.externalBaseUrl = 'https://api-q.nedsecure.co.za/nedbank';

        config.srBaseUrl = 'https://qaenterpriseplmcache.nednet.co.za:9444/plm-service-fulfillment-1.0/api';

        config.soap_end_point = 'https://ssg-q.it.nednet.co.za:454';
        config.bearer_token_name = 'qa_bearer_token.txt';

        config.mongo_server = 'xqaepcmdb03.africa.nedcor.net';
        config.UsernameTokenUser = 'cc319881';
        config.cr3internal_bearer_token_name = 'cr3internal_qa_bearer_token.txt';
        config.cr3external_bearer_token_name = 'cr3external_qa_bearer_token.txt';
        config.cr3_fullrateinternal_bearer_token_name = 'cr3_fullrateinternal_qa_bearer_token.txt';

        config.browseexternal_bearer_token_name = 'browseexternal_qa_bearer_token.txt';
        config.cashOnlineinternal_bearer_token_name = 'cashOnlineInternal_qa_bearer_token.txt';
        config.cashOnlineexternal_bearer_token_name = 'cashOnlineExternal_qa_bearer_token.txt';
        config.browse_bearer_token_name = 'browse_qa_bearer_token.txt';

        config.mypocketinternal_bearer_token_name = 'mypocketinternal_qa_bearer_token.txt';
        config.mypocketexternal_bearer_token_name = 'mypocketexternal_qa_bearer_token.txt';
        config.sr43_bearer_token_name = 'sr4.3_qa_bearer_token.txt';
        fullRateTable_bearer_token_name ='fullRateTable_ete_bearer_token.txt';

        config.cr3_bearer_token_end_point = 'https://qa-staff.nednet.co.za/content/nedbank-eco.jwt.json';
        config.administration_service_endpoint = 'https://zqepcwn1.africa.nedcor.net:9444/nedj-service-configuration/api/config/namespace/za.co.nedj.app.plm.product.model';
    }

    config.environment = envName;
    return config;
}


var reportConfig = function(envName, scenario) {
    if (!envName) {
        envName = 'QA';
    }

    if (!scenario) {
        scenario = 'Smoke Test';
    }
    var config = {
        platform: 'API',
        product: 'EWOC',
        project: 'LMD',
        tool: 'api-framework',
        version: '1.0.1',
        environment: envName,
        scenarioName: scenario
    };

    return config;
}
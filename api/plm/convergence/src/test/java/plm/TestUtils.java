package plm;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import plm.util.RestHelper;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class TestUtils {

    private static WireMockServer wireMockServer;
    private static final Logger logger = LogManager.getLogger(TestUtils.class);

    public static void setupStub(String serviceSubString) {
        RestHelper restHelper  = new RestHelper();
        String env = restHelper.getApiConfig().getEnvironment().toString();
        if(env.contentEquals("DEV")){
            logger.info("Setting up the stub for service...");
            try {
                wireMockServer = new WireMockServer();
                wireMockServer.start();
                logger.info("Mock server started");

            } catch (Exception exception) {
                logger.error(exception.getMessage());
            }
            wireMockServer.stubFor(any(urlPathMatching("^.*\\b(" + serviceSubString.toLowerCase() + ")\\b.*$"))
                    .willReturn(aResponse().withHeader("Content-Type", "application/json")
                            .withStatus(200)
                            .withBodyFile("/responses/mock_response.json")));
        }
    }


    public static void setupStub(String serviceSubString,String mockResponseFile) {
        RestHelper restHelper  = new RestHelper();
        String env = restHelper.getApiConfig().getEnvironment().toString();
        if(env.contentEquals("DEV")){
            logger.info("Setting up the stub for service...");
            try {
                wireMockServer = new WireMockServer();
                wireMockServer.start();
                logger.info("Mock server started");

            } catch (Exception exception) {
                logger.error(exception.getMessage());
            }
            String contentType = null;
            if(mockResponseFile.contains("json")){
                contentType = "application/json";
            }else{
                contentType = "application/xml";
            }
            wireMockServer.stubFor(any(urlPathMatching("^.*\\b(" + serviceSubString.toLowerCase() + ")\\b.*$"))
                    .willReturn(aResponse().withHeader("Content-Type", contentType)
                            .withStatus(200)
                            .withBodyFile("/responses/" + mockResponseFile)));
        }
    }

    public static void setupStubForAccountRefData(String serviceSubString) {
        RestHelper restHelper  = new RestHelper();
        String env = restHelper.getApiConfig().getEnvironment().toString();
        if(env.contentEquals("DEV")){
            logger.info("Setting up the stub for service...");
            try {
                wireMockServer = new WireMockServer();
                wireMockServer.start();
                logger.info("Mock server started");

            } catch (Exception exception) {
                logger.error(exception.getMessage());
            }
            wireMockServer.stubFor(any(urlPathMatching("^.*\\b(" + serviceSubString.toLowerCase() + ")\\b.*$"))
                    .willReturn(aResponse().withHeader("Content-Type", "text/plain")
                            .withStatus(200)
                            .withBodyFile("/json/mock_accountsreferencedata_response.json")));
        }
    }

    public static void stopMockServer() {
        if(wireMockServer != null){
            wireMockServer.stop();
            logger.info("Mock server stopped successfully");
        }else{
            logger.info("Mock server is not running");
        }
    }

}

package plm.rest;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.util.RestHelper;

import java.util.Iterator;

public class RestAccountsReferenceDataTest {

    private AccountsReferenceData accountsReferenceData;
    private RestHelper restHelper;
    private static final Logger logger = LogManager.getLogger(RestAccountsReferenceDataTest.class);

    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";

    @Before
    public void beforeMethod() {
        accountsReferenceData = new AccountsReferenceData();
        restHelper = new RestHelper();
        TestUtils.setupStubForAccountRefData("accountsreferencedata");
    }


    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        String baseURL = restHelper.getApiConfig().getOtherElements().get("srBaseUrl");
        JSONObject json = new JSONObject();
        json.put("adminsystem", "DS");
        json.put("accountcategory", "Investments");
        json.put("brand", "1");
        json.put("productidentifier", "1126");
        json.put("referencedatacategory", "Account Product Parameters");

        JSONObject responseJson = accountsReferenceData.sendRestRequest(baseURL, json);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertEquals(200, response.getStatusCode());
            }
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
        restHelper = null;
        accountsReferenceData = null;
    }

}

package plm.rest;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;

import java.util.Iterator;

public class RestGetProductsTest {
    private GetProducts getProducts;
    private static final Logger logger = LogManager.getLogger(RestGetProductsTest.class);

    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";

    @Before
    public void beforeMethod() {
        getProducts = new GetProducts();
        TestUtils.setupStub("enterpriseproductcatalogueinfo");
    }

    @Test
    public void sendExternalRequestWithParams() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
        String strEntryPoint = "external";

        String productAttribute = "effectiveStatusName";
        String productAttributeValue = "Available Product";


        JSONObject json = new JSONObject();
        json.put("productattribute", productAttribute);
        json.put("productattributevalue", productAttributeValue);

        JSONObject responseJson = getProducts.sendRestRequest(strEntryPoint, json);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

    }

    @Test
    public void sendExternalRequestWithResourceName() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
        String strEntryPoint = "external";
        String productId = "1332";
        JSONObject responseJson = getProducts.sendRestRequest(strEntryPoint, productId);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

    }

    @Test
    public void sendExternalRequestWithParamsAndResourceName() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
        String strEntryPoint = "external";

        String startDate = "2020-02-05";
        String endDate = "2020-02-05";
        String productId = "1013";

        JSONObject json = new JSONObject();
        json.put("startdate", startDate);
        json.put("enddate", endDate);
        JSONObject responseJson = getProducts.sendRestRequest(strEntryPoint, productId, json);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE,getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
        getProducts = null;
    }

}

package plm.rest;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;

import java.util.Iterator;

public class RestRetrieveFeesTest {
    private RetrieveInterestRates retrieveInterestRates;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";

    @Before
    public void setUp() {
        retrieveInterestRates = new RetrieveInterestRates();
        TestUtils.setupStub("Arrangementpricing");
    }

    @Test
    public void sendExternalRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        String strEntryPoint = "external";
        String interestFrequency = "M";
        String interestOption = "1";
        String requestedTerm = "12";
        String requestedAmount = "100000";
        String productLine = "investment";
        String channelType = "Staff Assisted";
        String productIdentifier = "1360";
        String rateId = "1";


        JSONObject jsonParameter = new JSONObject();
        jsonParameter.put("InterestFrequency", interestFrequency);
        jsonParameter.put("InterestOption", interestOption);
        jsonParameter.put("requestedTerm", requestedTerm);
        jsonParameter.put("requestedAmount", requestedAmount);
        jsonParameter.put("productLine", productLine);
        jsonParameter.put("channelType", channelType);
        jsonParameter.put("productIdentifier", productIdentifier);
        jsonParameter.put("rateId", rateId);

        JSONObject responseJson = retrieveInterestRates.sendRestRequest(strEntryPoint, jsonParameter);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

    }


    @Test
    public void sendInternalRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
        String strEntryPoint = "internal";

        String interestFrequency = "M";
        String interestOption = "1";
        String requestedTerm = "12";
        String requestedAmount = "100000";
        String productLine = "investment";
        String channelType = "Staff Assisted";
        String productIdentifier = "1360";
        String rateId = "1";


        JSONObject jsonParameter = new JSONObject();
        jsonParameter.put("InterestFrequency", interestFrequency);
        jsonParameter.put("InterestOption", interestOption);
        jsonParameter.put("requestedTerm", requestedTerm);
        jsonParameter.put("requestedAmount", requestedAmount);
        jsonParameter.put("productLine", productLine);
        jsonParameter.put("channelType", channelType);
        jsonParameter.put("productIdentifier", productIdentifier);
        jsonParameter.put("rateId", rateId);

        JSONObject responseJson = retrieveInterestRates.sendRestRequest(strEntryPoint, jsonParameter);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

    }

    @After
    public void tearDown() {
       TestUtils.stopMockServer();
        retrieveInterestRates = null;
    }

}

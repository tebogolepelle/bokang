package plm.rest;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;

import java.util.Iterator;

public class RestRetrieveFeesV3Test {
    private RetrieveFeesV3 retrieveFeesV3;

    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesV3Test.class);
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";

    @Before
    public void setUp() {
        retrieveFeesV3 = new RetrieveFeesV3();
        TestUtils.setupStub("Arrangementpricing");
    }

    @Test
    public void sendInternalRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
        String entryPoint ="internal";
        String channelType = "Staff Assisted";
        String feeName = "Subscription Fee (For Col Website)";
        String productLine = "CashSolutions";
        String productIdentifier = "1483";
        String featureIdentifier = "";
        String tierType = "";
        String tierValue = "";
        String segment = "CIB";
        String lawTrust = "WITH";

        JSONObject jsonParameter = new JSONObject();

        jsonParameter.put("Segment", segment);
        jsonParameter.put("Law Trust", lawTrust);

        JSONObject responseJson = retrieveFeesV3.sendRestRequest(entryPoint, feeName, productIdentifier, featureIdentifier, productLine, channelType, tierType, tierValue, jsonParameter);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }

    @Test
    public void sendExternalRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
        String entryPoint ="external";
        String channelType = "Staff Assisted";
        String feeName = "Subscription Fee (For Col Website)";
        String productLine = "CashSolutions";
        String productIdentifier = "1483";
        String featureIdentifier = "";
        String tierType = "";
        String tierValue = "";
        String segment = "CIB";
        String lawTrust = "WITH";

        JSONObject jsonParameter = new JSONObject();
        jsonParameter.put("Segment", segment);
        jsonParameter.put("Law Trust", lawTrust);

        JSONObject responseJson = retrieveFeesV3.sendRestRequest(entryPoint, feeName, productIdentifier, featureIdentifier, productLine, channelType, tierType, tierValue, jsonParameter);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
        retrieveFeesV3 = null;
    }

}

package plm.rest;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;

import java.util.Iterator;

public class RestRetrieveProductInterestRateTest {
    private RetrieveProductInterestRate retrieveProductInterestRate;

    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";

    @Before
    public void setUp(){
        retrieveProductInterestRate = new RetrieveProductInterestRate();
        TestUtils.setupStub("productofferinformation", "mock_response_retrieveProductInterestRate.json");
    }

    @Test
    public void sendInternalRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        String strEntryPoint = "internal";
        String interestRateName = "Nominal Rate";
        String featureIdentifier = "";
        String productLine = "Accounts";
        String productIdentifier = "1402";

        JSONObject jsonParameter = new JSONObject();
        jsonParameter.put("featureidentifier", featureIdentifier);
        jsonParameter.put("productline", productLine);

       JSONObject responseJson = retrieveProductInterestRate.sendRestRequest(strEntryPoint,productIdentifier,interestRateName, jsonParameter);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("result.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
    }

    @Test
    public void sendExternalRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        String strEntryPoint = "internal";
        String interestRateName = "Nominal Rate";
        String featureIdentifier = "";
        String productLine = "Accounts";
        String productIdentifier = "1402";

        JSONObject jsonParameter = new JSONObject();
        jsonParameter.put("featureidentifier", featureIdentifier);
        jsonParameter.put("productline", productLine);

        JSONObject responseJson = retrieveProductInterestRate.sendRestRequest(strEntryPoint,productIdentifier,interestRateName, jsonParameter);
        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("result.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
        retrieveProductInterestRate = null;
    }
}

package plm.rest;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;

import java.util.Iterator;

public class RestRetrieveProductInterestRate_FullRateTest {
    private RetrieveProductInterestRate_FullRateTable retrieveProductInterestRate_fullRateTable;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";

    @Before
    public void setUp(){
        retrieveProductInterestRate_fullRateTable = new RetrieveProductInterestRate_FullRateTable();
        TestUtils.setupStub("productofferinformation");
    }


    @Test
    public void sendFullRateInternalRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        String strEntryPoint = "internal";

        JSONObject jsonParameter = new JSONObject();

        jsonParameter.put("productidentifier","1133");
        jsonParameter.put("productLine","investment");

        JSONObject responseJson = retrieveProductInterestRate_fullRateTable.sendFullRateRestRequest(strEntryPoint, jsonParameter);
        System.out.println("responseJson = " + responseJson);

        Iterator<String> keys = responseJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (responseJson.get(key) instanceof Response) {
                Response response = ((Response) responseJson.get(key));
                Assert.assertTrue("Failed to find R00", response.body().path("resultSet.resultCode").toString().contains("R00"));
            }
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

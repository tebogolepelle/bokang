package plm.soap;

import io.restassured.internal.path.xml.NodeChildrenImpl;
import io.restassured.path.xml.XmlPath;
import io.restassured.path.xml.element.Node;
import io.restassured.path.xml.element.NodeChildren;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import plm.TestUtils;
import plm.pojo.response.FDRatesEnqRs.FDRatesEnqRsResponse;
import plm.rest.RestRetrieveFeesTest;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

import java.io.FileWriter;
import java.io.IOException;

public class SoapFDRatesEnqRqTest {
    private FDRatesEnqRq fDRatesEnqRq;
    private FDRatesEnqRsResponse fDRatesEnqRsResponse;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";

    @Before
    public void setUp() {
        fDRatesEnqRq = new FDRatesEnqRq();
        fDRatesEnqRsResponse = new FDRatesEnqRsResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("productofferinformation", "mock_response_retrieveProductOfferInformation.xml");


    }


    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());


        String baseUrl = apiConfig.getOtherElements().get("soap_end_point_FDRatesEnqRq");
        JSONObject json = new JSONObject();
        String ContinuationValue = "0";
        while (!ContinuationValue.contentEquals("null")) {
            json.put("ContinuationValue", ContinuationValue);
            JSONObject responseJson = fDRatesEnqRq.sendSoapRequest(baseUrl, json);
            Response res = (Response) responseJson.get("response");
            ContinuationValue = printAllValues(res);
        }

        // printAllValues(res);

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }


    private String printAllValues(Response res)  {
        String strCXMLPath = "Envelope.Body.FDRatesEnqRs.FDRateDetails";
        XmlPath xmlResponse = new XmlPath(res.getBody().asString());

        String ContinuationValue = String.valueOf(getValueFromPath(xmlResponse, "Envelope.Body.FDRatesEnqRs.ChunkResponseHeader.ContinuationValue"));


        FileWriter myWriter = null;
        try {
            myWriter = new FileWriter(ContinuationValue+".txt");
            myWriter.write(res.getBody().prettyPrint());
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Successfully wrote to the file.");
        return ContinuationValue;

        /*int i = 0;
        boolean isValueExist = true;
        while (isValueExist) {
            try {
                String strProductType = String.valueOf(getValueFromPath(xmlResponse, "Envelope.Body.FDRatesEnqRs.FDRateDetails[" + i + "].ProductType"));
                String InterestFrequency = String.valueOf(getValueFromPath(xmlResponse, "Envelope.Body.FDRatesEnqRs.FDRateDetails[" + i + "].InterestFrequency"));
                String InvestmentPeriod = String.valueOf(getValueFromPath(xmlResponse, "Envelope.Body.FDRatesEnqRs.FDRateDetails[" + i + "].InvestmentPeriod"));
                String PartRate = String.valueOf(getValueFromPath(xmlResponse, "Envelope.Body.FDRatesEnqRs.FDRateDetails[" + i + "].PartRate"));
                String LowerBand = String.valueOf(getValueFromPath(xmlResponse, "Envelope.Body.FDRatesEnqRs.FDRateDetails[" + i + "].LowerBand"));
                String UpperBand = String.valueOf(getValueFromPath(xmlResponse, "Envelope.Body.FDRatesEnqRs.FDRateDetails[" + i + "].UpperBand"));
                if(strProductType.contentEquals("null")){
                    isValueExist = false;
                }
                System.out.println(String.format("data: %s,%s,%s,%s,%s,%s", strProductType, InterestFrequency, InvestmentPeriod, PartRate, LowerBand, UpperBand));

                i++;
            } catch (Exception ex) {
                isValueExist = false;
            }
        }
*/

    }

    public static String getValueFromPath(XmlPath xmlResponse, String xmlPath) {

        try {
            NodeChildren contextNodes = xmlResponse.getNodeChildren(xmlPath);
            for (Node contextNode : contextNodes.list()) {
                System.out.println(contextNode.name() + " - " + contextNode.value());
            }

            if (xmlResponse.get(xmlPath) instanceof NodeChildrenImpl) {
                if (((NodeChildrenImpl) xmlResponse.get(xmlPath)).size() == 0) {
                    return null;
                }
            }
        } catch (ClassCastException ex) {
            return xmlResponse.getString(xmlPath);
        }

        return xmlResponse.getString(xmlPath);
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

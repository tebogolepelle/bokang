package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.getproductv4.GetProductResponse;
import plm.rest.RestRetrieveFeesTest;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapGetProductV4Test {
    private GetProductv4 getProductV4;
    private GetProductResponse getProductResponseV4;

    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";

    @Before
    public void setUp() {
        getProductV4 = new GetProductv4();
        getProductResponseV4 = new GetProductResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("ProductCatalogueInformation", "mock_response_getproductv4.xml");
    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        String productIdentifier = "1126";

        String baseUrl = apiConfig.getOtherElements().get("soap_end_point");

        JSONObject responseJson = getProductV4.sendSoapRequest(baseUrl, productIdentifier);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        } else {
            getProductResponseV4 = (GetProductResponse) responseJson.get("response");
            Assert.assertEquals(200, getProductResponseV4.getStatusCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

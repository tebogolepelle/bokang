package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.rest.RestRetrieveFeesTest;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveFeesTest {
    private RetrieveFees retrieveFees;

    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String SOAP_END_POINT = "soap_end_point";
    private static final String RESPONSE = "response";
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";

    @Before
    public void setUp() {
        retrieveFees = new RetrieveFees();

        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("arrangementmanagement", "mock_response_retrieveFees.xml");
    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        String feeID = "1";
        String feeName = "Overdraft Facility Fee";
        String feeParameterType = "Client Segment";
        String feeParameterValue = "BB";
        String feeParameterType2 = "Client NCA Status";
        String feeParameterValue2 = "Inside Natural";
        String feeParameterType3 = "Existing Overdraft Indicator";
        String feeParameterValue3 = "N";
        String requestedTerm = "12";
        String requestedLimitAmount = "300001";
        String productLine = "loans";
        String productIdentifier = "1408";
        String featureIdentifier = "FE96";
        String chanelTypeDescription = "";
        String baseUrl = apiConfig.getOtherElements().get(SOAP_END_POINT);

        JSONObject json = new JSONObject();

        json.put(feeParameterType, feeParameterValue);
        json.put(feeParameterType2, feeParameterValue2);
        json.put(feeParameterType3, feeParameterValue3);


        JSONObject responseJson = retrieveFees.sendSoapRequest(baseUrl, feeID,feeName,requestedTerm,requestedLimitAmount,productLine,productIdentifier,featureIdentifier, chanelTypeDescription, json);
        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            plm.pojo.response.retrievefees.RetrieveFeesResponse retrieveFeesResponse = (plm.pojo.response.retrievefees.RetrieveFeesResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("R00", retrieveFeesResponse.getResultSet().getResultCode());
        }


        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }



    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

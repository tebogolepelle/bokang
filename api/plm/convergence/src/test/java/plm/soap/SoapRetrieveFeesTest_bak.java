package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveinterestrate.RetrieveInterestRatesResponse;
import plm.rest.RestRetrieveFeesTest;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveFeesTest_bak {
    private RetrieveInterestRates retrieveInterestRate;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";


    @Before
    public void setUp()  {
        retrieveInterestRate = new RetrieveInterestRates();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }        TestUtils.setupStub("arrangementmanagement", "mock_response_retrieveInterestRate.xml");
    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        String interestFrequency = "M";
        String interestOption = "1";
        String requestedTerm = "12";
        String requestedAmount = "100000";
        String productLine = "investment";
        String channelType = "Staff Assisted";
        String productIdentifier = "1360";
        String rateId = "1";

        String baseUrl = apiConfig.getOtherElements().get("soap_end_point");

        JSONObject json = new JSONObject();

        json.put( "InterestFrequency", interestFrequency);
        json.put( "InterestOption", interestOption);

        JSONObject responseJson = retrieveInterestRate.sendSoapRequest(baseUrl,rateId, "", requestedAmount, requestedTerm, productLine, productIdentifier, "",  channelType, json);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            RetrieveInterestRatesResponse retrieveInterestRate = (RetrieveInterestRatesResponse) responseJson.get("response");
            Assert.assertEquals("R00", retrieveInterestRate.getResultSet().getResultCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

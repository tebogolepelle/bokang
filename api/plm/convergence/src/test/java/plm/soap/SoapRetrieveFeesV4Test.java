package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.rest.RestRetrieveFeesTest;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveFeesV4Test {
    private RetrieveFeesv4 retrieveFeesv4;
    private plm.pojo.response.retrievefeesv4.RetrieveFeesResponse retrieveFeesResponsev4;
    private FaultResponse faultResponse;

    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String SOAP_END_POINT = "soap_end_point";
    private static final String RESPONSE = "response";
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";

    @Before
    public void setUp() {
        retrieveFeesv4 = new RetrieveFeesv4();

        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("arrangementmanagement", "mock_response_retrieveFeesv4.xml");
    }


    @Test
    public void sendCashOnlineCITRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        String channelType = "Staff Assisted";
        String feeID = "1";
        String feeName = "CIT Service Fee";
        String vendorName = "G4S";
        String daysServiceValue = "1";
        String productLine = "CashSolutions";
        String productIdentifier = "1013";
        String componentProductIdentifier = "1483";
        String productIDType = "";
        String componentProductStructureTypeName = "Product Component";

        String featureIdentifier = "FE12";

        String baseUrl = apiConfig.getOtherElements().get(SOAP_END_POINT);

        JSONObject json = new JSONObject();
        json.put("Vendor", vendorName);
        json.put("Days Service", daysServiceValue);

        JSONObject responseJson = retrieveFeesv4.sendSoapRequest(baseUrl, channelType, feeID, feeName, productLine, productIdentifier, componentProductIdentifier, productIDType, componentProductStructureTypeName, featureIdentifier, "", "", json);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            retrieveFeesResponsev4 = (plm.pojo.response.retrievefeesv4.RetrieveFeesResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("R00", retrieveFeesResponsev4.getResultSet().getResultCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }

    @Test
    public void sendCashOnlineProductFeesRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        String channelType = "Staff Assisted";
        String feeName = "Cash Deposit Fee";
        String feeID = "1";
        String segment = "RRB";
        String vendorName = "G4S";
        String productLine = "CashSolutions";
        String productIdentifier = "1013";
        String componentProductIdentifier = "1483";
        String productIDType = "";
        String componentProductStructureTypeName = "Product Component";
        String featureIdentifier = "";

        String baseUrl = apiConfig.getOtherElements().get(SOAP_END_POINT);

        JSONObject json = new JSONObject();

        json.put("Vendor", vendorName);
        json.put("Segment", segment);

        JSONObject responseJson = retrieveFeesv4.sendSoapRequest(baseUrl, channelType, feeID, feeName, productLine, productIdentifier, componentProductIdentifier, productIDType, componentProductStructureTypeName, featureIdentifier, "", "", json);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            retrieveFeesResponsev4 = (plm.pojo.response.retrievefeesv4.RetrieveFeesResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("R00", retrieveFeesResponsev4.getResultSet().getResultCode());
        }


        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

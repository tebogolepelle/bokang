package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.rest.RestRetrieveFeesTest;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveFees_JO_ExcessFeeV3Test {
    private RetrieveFeesv3 retrieveFeesv3;
    //private FaultResponse faultResponse;


    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String SOAP_END_POINT = "soap_end_point";
    private static final String RESPONSE = "response";
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";

    @Before
    public void setUp() {
        retrieveFeesv3 = new RetrieveFeesv3();

        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("arrangementmanagement", "mock_response_retrieveFeesv3_JO_ExcessFee.xml");
    }

    @Test
    public void sendOverdraftExcessFeeRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        String feeName="Overdraft Excess Fee";
        String feeParameterType="Client Segment";
        String feeParameterValue="BB";
        String feeParameterType2="Client NCA Status";
        String feeParameterValue2="Outside NCA";

        String productLine="loans";
        String chanelTypeDescription = "Staff Assisted";
        String productIdentifier="1412";
        String featureIdentifier="FE96";

        String baseUrl = apiConfig.getOtherElements().get(SOAP_END_POINT);

        JSONObject json = new JSONObject();

        json.put(feeParameterType, feeParameterValue);
        json.put(feeParameterType2, feeParameterValue2);

        JSONObject responseJson = retrieveFeesv3.sendOverdraftExcessFeeSoapRequest(baseUrl,feeName,productLine,productIdentifier,featureIdentifier,chanelTypeDescription, json);
        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            plm.pojo.response.retrievefeesv3.RetrieveFeesResponse retrieveFeesResponse = (plm.pojo.response.retrievefeesv3.RetrieveFeesResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("R00", retrieveFeesResponse.getResultSet().getResultCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductfeesv4.RetrieveProductFeesResponse;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveProductFeesV4Test {
    private RetrieveProductFeesV4 retrieveProductFees;
    private RetrieveProductFeesResponse retrieveProductFeesResponse;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(SoapRetrieveProductFeesV4Test.class);
    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";

    @Before
    public void setUp(){
        retrieveProductFees = new RetrieveProductFeesV4();
        retrieveProductFeesResponse = new RetrieveProductFeesResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }        TestUtils.setupStub("ProductOfferInformation/v4","mock_response_retrieveProductFeesV4.xml");
    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
        String baseUrl = apiConfig.getOtherElements().get("soap_end_point");

        JSONObject json = new JSONObject();

        json.put("feeID", "");
        json.put("feeName", "Monthly Service Fee");
        json.put("pricingPlanIdentifier", "");

        json.put("productIdentifier", "1357");
        json.put("componentProductIdentifier", "");
        json.put("productIDTypeCode", "");
        json.put("productIDTypeDescription", "");

        json.put("componentProductStructureTypeName", "");
        json.put("productLine", "");
        json.put("featureIdentifier", "N");
        json.put("channelTypeDescription", "");
        json.put("productKeysFieldName", "");
        json.put("productKeysFieldValue", "");
        json.put("productKeysSequence", "");
        json.put("sourceSystemSystemId", "");
        json.put("sourceSystemSystemName", "");
        json.put("sourceSystemSystemCode", "");
        json.put("jurisdictionCountryCode", "");
        json.put("jurisdictionCurrency", "");
        json.put("discriminatorID", "");
        json.put("discriminatorTypeCode", "");
        json.put("discriminatorValue", "");
        json.put("staffIndicator", "");
        json.put("brand", "");

        json.put("strBaseURL", baseUrl);

        JSONObject responseJson = retrieveProductFees.sendSoapRequest(json);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            retrieveProductFeesResponse = (RetrieveProductFeesResponse)responseJson.get("response");
            Assert.assertEquals("R00", retrieveProductFeesResponse.getResultSet().getResultCode());
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

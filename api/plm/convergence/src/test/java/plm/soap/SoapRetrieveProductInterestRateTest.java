package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductinterestrate.RetrieveProductInterestRatesResponse;
import plm.rest.RestRetrieveFeesTest;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveProductInterestRateTest {
    private RetrieveProductInterestRate retrieveProductInterestRate;
    private RetrieveProductInterestRatesResponse retrieveProductInterestRatesResponse;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RestRetrieveFeesTest.class);
    private static final String STARTING_METHOD_MESSAGE =  "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE =  "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";


    @Before
    public void setUp(){
        retrieveProductInterestRate = new RetrieveProductInterestRate();
        retrieveProductInterestRatesResponse = new RetrieveProductInterestRatesResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }        TestUtils.setupStub("productofferinformation","mock_response_retrieveProductInterestRate.xml");
    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());

        String rateId = "12";
        String rateName = "Overdraft Penalty Interest";
        String productIdentifier = "1408";
        String featureIdentifier = "FE96";
        String productLine = "loans";
        String baseUrl = apiConfig.getOtherElements().get("soap_end_point");

        JSONObject responseJson = retrieveProductInterestRate.sendSoapRequest(baseUrl, rateId, rateName,  productIdentifier,  featureIdentifier,  productLine);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            retrieveProductInterestRatesResponse = (RetrieveProductInterestRatesResponse)responseJson.get("response");
            Assert.assertEquals("R00", retrieveProductInterestRatesResponse.getResultSet().getResultCode());
        }
        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {}.getClass().getEnclosingMethod().getName());
    }


    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

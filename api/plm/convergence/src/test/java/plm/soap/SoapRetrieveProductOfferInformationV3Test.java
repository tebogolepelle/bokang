package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductofferinformationv3.RetrieveProductOfferInformationResponse;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveProductOfferInformationV3Test {
    private RetrieveProductOfferInformationv3 retrieveProductOfferInformation;
    private RetrieveProductOfferInformationResponse retrieveProductOfferInformationResponse;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(SoapRetrieveProductOfferInformationV3Test.class);
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";

    @Before
    public void setUp() {
        retrieveProductOfferInformation = new RetrieveProductOfferInformationv3();
        retrieveProductOfferInformationResponse = new RetrieveProductOfferInformationResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("productofferinformation", "mock_response_retrieveProductOfferInformationV3.xml");

    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        String productIdentifier = "1026";
        String componentProductIdentifier = "";
        String productIDType = "";
        String componentProductStructureTypeName = "";
        String productLine = "CreditCards";
        String riskTierIndicator = "T1";
        String staffIndicator = "N";
        String greenbacksIndicator = "Y";
        String channelType = "?";

        String baseUrl = apiConfig.getOtherElements().get("soap_end_point");
        JSONObject json = new JSONObject();

        json.put("Risk Tier", riskTierIndicator);
        json.put("Staff Indicator", staffIndicator);
        json.put("Greenbacks Ind", greenbacksIndicator);

        JSONObject responseJson = retrieveProductOfferInformation.sendSoapRequest(baseUrl,  productIdentifier, componentProductIdentifier, productIDType, componentProductStructureTypeName, productLine, channelType, json);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            retrieveProductOfferInformationResponse = (RetrieveProductOfferInformationResponse) responseJson.get("response");
            Assert.assertEquals(200, retrieveProductOfferInformationResponse.getStatusCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

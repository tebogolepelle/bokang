package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductofferinformationv4.RetrieveProductOfferInformationResponse;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveProductOfferInformationV4 {

    private RetrieveProductOfferInformationv4 retrieveProductOfferInformation;
    private plm.pojo.response.retrieveproductofferinformationv4.RetrieveProductOfferInformationResponse retrieveProductOfferInformationResponse;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RetrieveProductOfferInformationv4.class);
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";

    @Before
    public void setUp() {
        retrieveProductOfferInformation = new RetrieveProductOfferInformationv4();
        retrieveProductOfferInformationResponse = new plm.pojo.response.retrieveproductofferinformationv4.RetrieveProductOfferInformationResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("productofferinformation", "mock_response_retrieveProductOfferInformationv4.xml");

    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        JSONObject json = new JSONObject();

        JSONObject jsonFilters = new JSONObject();
        jsonFilters.put("Risk Tier", "T1");
        jsonFilters.put("Staff Indicator", "N");
        jsonFilters.put("Greenbacks Ind", "Y");

        json.put("productIdentifier", "1026");
        json.put("componentProductIdentifier", "");
        json.put("productIDTypeCode", "");
        json.put("productIDTypeDescription", "");

        json.put("componentProductStructureTypeName", "");
        json.put("productLine", "CreditCards");
        json.put("jsonFilters", jsonFilters);
        json.put("channelTypeDescription", "?");
        json.put("productKeysFieldName", "");
        json.put("productKeysFieldValue", "");
        json.put("productKeysSequence", "");
        json.put("sourceSystemSystemId", "");
        json.put("sourceSystemSystemName", "");
        json.put("sourceSystemSystemCode", "");
        json.put("jurisdictionCountryCode", "");
        json.put("jurisdictionCurrency", "");
        json.put("discriminatorID", "Client Segment");
        json.put("discriminatorTypeCode", "");
        json.put("discriminatorValue", "BC");

        String baseUrl = apiConfig.getOtherElements().get("soap_end_point");
        json.put("strBaseURL", baseUrl);

        JSONObject responseJson = retrieveProductOfferInformation.sendSoapRequest(json);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            retrieveProductOfferInformationResponse = (RetrieveProductOfferInformationResponse) responseJson.get("response");
            Assert.assertEquals(200, retrieveProductOfferInformationResponse.getStatusCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

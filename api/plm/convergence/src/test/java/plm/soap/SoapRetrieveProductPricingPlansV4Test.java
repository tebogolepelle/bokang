package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.request.retrieveproductpricingplansv4.RetrieveProductPricingPlans;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductpricingplansv4.RetrieveProductPricingPlansResponse;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveProductPricingPlansV4Test {

    private RetrieveProductPricingPlansv4 retrieveproductpricingplansv4;
    private RetrieveProductPricingPlansResponse retrieveproductpricingplansResponsev4;
    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(RetrieveProductOfferInformationv4.class);
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";

    @Before
    public void setUp() {
        retrieveproductpricingplansv4 = new RetrieveProductPricingPlansv4();
        retrieveproductpricingplansResponsev4 = new RetrieveProductPricingPlansResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("productofferinformation", "mock_response_retrieveProductPricingPlansv4.xml");

    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        JSONObject json = new JSONObject();


        json.put("productIdentifier", "1389");
        json.put("componentProductIdentifier", "");
        json.put("productIDTypeCode", "");
        json.put("productIDTypeDescription", "SystemIdentifer");

        json.put("componentProductStructureTypeName", "");
        json.put("productLine", "");
        json.put("channelTypeDescription", "");
        json.put("productKeysFieldName", "PGMSOLCD");
        json.put("productKeysFieldValue", "7004");
        json.put("productKeysSequence", "1");
        json.put("sourceSystemSystemId", "");
        json.put("sourceSystemSystemName", "");
        json.put("sourceSystemSystemCode", "");
        json.put("jurisdictionCountryCode", "");
        json.put("jurisdictionCurrency", "");
        json.put("staffIndicator", "STAFF");
        json.put("brand", "NEDBANK");

        String baseUrl = apiConfig.getOtherElements().get("soap_end_point");
        json.put("strBaseURL", baseUrl);

        JSONObject responseJson = retrieveproductpricingplansv4.sendSoapRequest(json);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        }else{
            retrieveproductpricingplansResponsev4 = (RetrieveProductPricingPlansResponse) responseJson.get("response");
            Assert.assertEquals(200, retrieveproductpricingplansResponsev4.getStatusCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

package plm.soap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import plm.TestUtils;
import plm.pojo.response.fault.FaultResponse;
import plm.pojo.response.retrieveproductsv4.RetrieveProductsResponse;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class SoapRetrieveProductsV4Test {
    private RetrieveProductv4 retrieveProductv4;
    private RetrieveProductsResponse retrieveProductsResponse;

    private ApiConfig apiConfig;
    private static final Logger logger = LogManager.getLogger(SoapRetrieveProductsV4Test.class);
    private static final String STARTING_METHOD_MESSAGE = "Testing method started for  {} - {}.....";
    private static final String ENDING_METHOD_MESSAGE = "Testing method ended for {} - {}.....";
    private static final String RESPONSE = "response";

    @Before
    public void setUp() {
        retrieveProductv4 = new RetrieveProductv4();
        retrieveProductsResponse = new RetrieveProductsResponse();
        try {
            apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        } catch (ApiException e) {
            logger.error(e.getMessage());
        }
        TestUtils.setupStub("ProductCatalogueInformation", "mock_response_retrieveproductv4.xml");
    }

    @Test
    public void sendRequest() {
        logger.info(STARTING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());

        String productIDTypeCode = "1";
        String productIDTypeDescription = "System Of Record";
        String productKeysFieldName = "DSM020B_INVESTMENT_TYPE";
        String productKeysFieldValue = "3";
        String productKeysSequence = "1";
        String systemId = "1021";
        String systemName = "INVESTMENTS-DS";
        String systemCode = "1";


        String strBaseURL = apiConfig.getOtherElements().get("soap_end_point");

        JSONObject responseJson = retrieveProductv4.sendSoapRequest(strBaseURL, productIDTypeCode, productIDTypeDescription, productKeysFieldName, productKeysFieldValue, productKeysSequence, systemId, systemName, systemCode);

        if (responseJson.get(RESPONSE) instanceof FaultResponse) {
            FaultResponse faultResponse = (FaultResponse) responseJson.get(RESPONSE);
            Assert.assertEquals("soapenv:Sender", faultResponse.getFaultCode());
        } else {
            retrieveProductsResponse = (RetrieveProductsResponse) responseJson.get("response");
            Assert.assertEquals(200, retrieveProductsResponse.getStatusCode());
        }

        logger.info(ENDING_METHOD_MESSAGE, getClass().getName(), new Object() {
        }.getClass().getEnclosingMethod().getName());
    }

    @After
    public void tearDown() {
        TestUtils.stopMockServer();
    }
}

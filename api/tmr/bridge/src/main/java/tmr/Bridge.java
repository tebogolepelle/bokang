package tmr;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.nedbank.eqa.api.rest.JsonUtil;
import za.co.nedbank.eqa.api.rest.RestUtil;
import za.co.nedbank.execution.config.api.ApiConfig;
import za.co.nedbank.execution.config.api.ApiException;
import za.co.nedbank.execution.config.api.ConfigProxy;
import za.co.nedbank.execution.config.constants.ConfigType;

public class Bridge {

    protected RestUtil restUtil;
    protected ApiConfig apiConfig;
    protected JsonUtil jsonUtil;
    private String pfxFile;
    private String pfxPsword = "Y6iHdAD4802Vk568";
    private Logger logger = LogManager.getLogger(Bridge.class);

    public Bridge() throws ApiException {
        restUtil = new RestUtil();
        apiConfig = ConfigProxy.getConfig(ConfigType.JAVASCRIPT);
        jsonUtil = new JsonUtil();
        pfxFile = getClass().getClassLoader().getResource("AP234182.pfx").getPath();
        logger.info("Certificate Path:  {}.....", pfxFile);
    }

    public void configureRequest(String pfxPath,String password) {
        restUtil.initSpec(apiConfig.getEnvironmentBaseUrl()).addCertAuthentication(pfxPath, password);
    }

    public void readAndAppendToken(String pfxpath, String  pfxpswd, String auth){
        this.configureRequest(pfxpath, pfxpswd);
        this.jsonUtil = new JsonUtil();
        restUtil.relaxHttpsValidation("SSL").appendHeader("Authorization", auth);

    }

    public Response depositFundsOnline(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("depositFundsOnline"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response depositFundsReversal(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("depositFundsReversal"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response depositFundsUndo(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("depositFundsUndo"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response depositFundsOffline(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("depositFundsOffline"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response depositFundsForced(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("depositFundsForced"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response employeeHoldings(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("employeeHoldings"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response cashHoldings(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("cashHoldings"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response startOfDay(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("startOfDay"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response endOfDay(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("endOfDay"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response withdrawFundsOnline(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("withdrawFundsOnline"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response withdrawFundsReversal(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("withdrawFundsReversal"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response withdrawFundsUndo(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("withdrawFundsUndo"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response withdrawFundsOffline(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("withdrawFundsOffline"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

    public Response withdrawFundsForced(String token, String payload) throws ApiException {
        this.readAndAppendToken(pfxFile, pfxPsword, token);
        StringBuilder endpoint = new StringBuilder(this.apiConfig.getOtherElements().get("withdrawFundsForced"));
        Response response = this.restUtil.addContentType(ContentType.JSON).postFromString(String.valueOf(endpoint), payload);
        response.then().log().all();
        return response;

    }

}
var configFunction = function (envName) {
  if (!envName) {
    envName = 'QA';
  }

//Use this config object to declare properties such as paths to various APIs that you can use in your asset.
//You can declare other appropriate properties. See below example.
  var config = {
      Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiQmVhcmVyIiwic3ViIjoiQ0MzMTYzMjUiLCJhdXRoX3RrdF9pZF90eXAiOiJQU0kiLCJhdXRoX3RrdF9sb2MiOiI4IiwiaXBfYWRkciI6IjEwLjc0LjY1LjMiLCJpc3MiOiJpZHAubmVkYmFuay5jby56YSIsImF1dGhfdGt0X3NzbiI6IjEzOTM3IiwiYXV0aF90a3Rfc2lnbiI6IjZERjBEMTIwOTNDMEM1Q0MzQjIzOEM4NjMwMEM4NTE5MkFBNjJDOTkiLCJhdXRoX3RrdF9pZGVudGl0eSI6IjU1ODE4Njc1OSIsImZ1bGxuYW1lIjoiQ2hyaXN0aWFuIE5zZWthIiwiaWF0IjoxNTgzODM2NDAyLCJleHAiOjE1ODM4Nzk2MDIsImp0aSI6ImVmZjQ0MTNjMzYwMzQ0N2FhOWQxYzE4NjJhOTAzOWNiIiwic2NvcGVzIjpbIkVXT0NMZW5kaW5nRGViaXRPcmRlck1hbmFnZXIiLCJFV09DTGVuZGluZ0VucXVpcmVyIiwiRVdPQ0d1YXJkVmVyaWZpZXIiLCJFV09DQ2FzaE9wc0FkbWluaXN0cmF0b3IiLCJFV09DU1NCQ2FzaEFkbWluaXN0cmF0b3IiLCJFV09DVmlld0VOb3RlcyIsIlN0YWZmIl0sImxhc3RuYW1lIjoiTnNla2EiLCJhdXRoX3RrdF9leHAiOiIyMDIwLTAzLTExVDAwOjMzOjIxLjg3OCIsImN0eSI6IndlYiIsInVjaWQiOiJBc3Npc3QiLCJhdWQiOiI1YzU5ODNlOS1lNjY1LTQ1ZjYtODU2ZC1hMjE3YTc2NGQ0ZDUiLCJzZXNzaW9uaWQiOiI5MDlhNGFhZS02MmJhLTExZWEtOThjYy0wMDUwNTZhZmFjZTEiLCJuYmYiOjE1ODM4MzYzNDIsImFzc2V0X251bSI6InYxMDVwMTBwcmExNTYxLmFmcmljYS5uZWRjb3IubmV0IiwibWFpbCI6IkNocmlzdGlhbk5AbmVkYmFuay5jby56YSIsImdyYW50X3R5cGUiOiJjbGllbnRfY3JlZGVudGlhbHMiLCJjaWQiOiI0NDIifQ.gHdbp_VlTh1dNKInwd_90ZCesgwsPaHA3jyUKvETAEi9Vnf4c08BhPwRntfcrtJE8YnNvjglzQj_DgWMgpzdyDp-f_z-o_d80k0gugnLAp4kt_hen4W54NtqzVHTQZWgRBl3ieIWvONfnT_qUB3_M-HIBvjilpnJEGYMZZU3ScPWn8EeiproZ5FbEDouqO-iG6Vq0CBX6N1lH1WwlBTfumMsNcH2jDVIJU_yavOq4PUmZMNfL61LRdKboeZvFeEkS_EEwZh8TJ0FduDlEU33t_ZUu_P2JDg165oZKB-TOjuzHQcOvQLSKTvG_wgvI0gGzF5k6QILrly4_ngk3YBc0g",
      depositFundsOnline : "services/channeloperations/tellerchannelmanagement/v9/depositfundsonline",
      depositFundsReversal : "services/channeloperations/tellerchannelmanagement/v9/depositfundsreversal",
      depositFundsUndo: "services/channeloperations/tellerchannelmanagement/v9/depositfundsundo",
      depositFundsOffline: "services/channeloperations/tellerchannelmanagement/v9/depositfundsoffline",
      depositFundsForced: "services/channeloperations/tellerchannelmanagement/v9/depositfundsforced",
      employeeHoldings: "services/channeloperations/tellerchannelmanagement/v9/employeeholdings",
      cashHoldings: "services/channeloperations/tellerchannelmanagement/v9/holdings",
      startOfDay: "services/channeloperations/tellerchannelmanagement/v9/startofday",
      endOfDay: "services/channeloperations/tellerchannelmanagement/v9/endofday",
      withdrawFundsOnline : "services/channeloperations/tellerchannelmanagement/v9/withdrawfundsonline",
      withdrawFundsReversal : "services/channeloperations/tellerchannelmanagement/v9/withdrawfundsreversal",
      withdrawFundsUndo : "services/channeloperations/tellerchannelmanagement/v9/withdrawfundsundo",
      withdrawFundsOffline : "services/channeloperations/tellerchannelmanagement/v9/withdrawfundsoffline",
      withdrawFundsForced : "services/channeloperations/tellerchannelmanagement/v9/withdrawfundsforced",
  };

//config.baseUrl is the property that holds the base URL for all the requests.
  if (envName == 'DEV') {
    config.baseUrl = 'https://api-dev.it.nednet.co.za/nedbank/qa/';
  }
  else if(envName == 'ETE'){
    config.baseUrl = 'https://api-ete.it.nednet.co.za/nedbank/qa/';
  }
  else if(envName == 'QA'){
    config.baseUrl = 'https://api-qa.it.nednet.co.za/nedbank/qa/';
  }

  config.environment = envName;
  return config;
}

var reportConfig = function(envName,scenario) {
  if (!envName) {
    envName = 'QA';
  }

  if(!scenario) {
    scenario = 'Smoke Test';
  }
   var config = {
            platform:'API',
            product : 'TELLER',
            project : 'TRM',
            tool : 'api-framework',
            version : '1.3.0',
            environment:envName,
            scenarioName:scenario
   };

   return config;
}
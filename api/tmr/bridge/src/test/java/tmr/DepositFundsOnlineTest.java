package tmr;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import za.co.nedbank.execution.config.api.ApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class DepositFundsOnlineTest {

    private Bridge bridgeObject;
    private Logger logger = LogManager.getLogger(DepositFundsOnlineTest.class);
    File file = new File("src\\main\\resources\\Data\\token.txt");
    BufferedReader reader = new BufferedReader(new FileReader(file));
    private String token = reader.readLine();
    String depositSlipNumber = createRandomIntegerString(2);
    String sequenceNumber = createRandomIntegerString(3);

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    Date date = new Date();
    String timestamp = dateFormat.format(date);
    JsonParser parser = new JsonParser();
    Object obj = parser.parse(new FileReader("src\\main\\resources\\requests_body\\Deposit_Funds.json"));
    JsonObject jsonBody = (JsonObject)obj;
    String payload = jsonBody.toString();

    public DepositFundsOnlineTest() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        try {
            bridgeObject = new Bridge();
        } catch (ApiException e) {
            logger.info(e,e.getCause());
        }
    }

    @Test
    public void depositFundsOnline() throws ApiException {
        payload = payload.replace("{depositSlipNumber}", depositSlipNumber);
        payload = payload.replace("{sequenceNumber}", sequenceNumber);
        payload = payload.replace("{transactionGuid}", generateGuid());
        payload = payload.replace("{transactionTime}", timestamp);
        System.out.println("The token is:" +token);
        System.out.println("The payload is:" +payload);
        Response response = bridgeObject.depositFundsOnline(token, payload);
        Assert.assertEquals("A",response.getBody().jsonPath().getString("withdrawFundsOnlineRs.transactionStatus"));
    }

    public String generateGuid() {
        // Creating a random UUID (Universally unique identifier).
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();

        System.out.println("Random UUID String = " + randomUUIDString);
        return randomUUIDString;

    }

    public String createRandomIntegerString(int numberOfRandomIntegers){
        String randomIntegerString="";

        for (int i=0;i<numberOfRandomIntegers;i++){
            int addingInt=(int)(Math.random()*10);
            randomIntegerString += Integer.toString(addingInt);
        }
        return randomIntegerString;
    }
}
